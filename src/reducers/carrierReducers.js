import * as Types2 from '../actions/types';

let mainSidebar = null;

mainSidebar = localStorage.getItem('sidebar');

if(mainSidebar){
  mainSidebar = JSON.parse(mainSidebar)
}else {
  mainSidebar = {
    iconOrder: 'active',
    iconDrivers: '',
    iconSettings: ''
  };
}

const defaultConfig = {
  timeslice: 30,
  dashboard: ["Job Number", "Create Date", "Customer Name", "Pickup Date Time From", "Pickup Date Time To", "Delivery Date Time From", "Delivery Date Time To", "Status"],
  driver: ["Driver Name", "Vehicle Type", "Vehicle Rego", "Driver License No.", "Phone Number", "Delivery On Time", "Status"],
  timeline: ["Driver Name", "Address", "Quantity", "Schedule Time", "ETA/ ON"],
}
//http://13.54.44.169:8000
//http://52.62.137.8:8000
// mainUrl: 'https://carriers.kargologic.com/',
// localUrl: 'https://carriers.kargologic.com/',

const initialState = {
  mainUrl: 'http://52.62.137.8:8000/',
  localUrl: 'http://52.62.137.8:3000/',
  appdata: 'app_id=esBAm0zTiqXbeDGNwS1p&app_code=lOzcJ7mrJKwW5msoPX121A',
  app_id: 'esBAm0zTiqXbeDGNwS1p',
  app_code: 'lOzcJ7mrJKwW5msoPX121A',
  bucketUrl: '',
  allJobs:[],
  jobsCount:[],
  orderDetails:[],
  orderComments: [],
  userDetails:[],
  allDrivers:[],
  driversCount:[],
  driverDetails:[],
  userpassword:[],
  newOrder:[],
  submitOrder:[],
  submitOrderAll: [],
  draftOrder:[],
  customerList:[],
  mainSidebar: mainSidebar,
  editOrder: false,
  userConfiguration: defaultConfig,
  configMsg: {},
  senderReceiver: [],
  assetsList: [],
  assetsListDriver: [],
  assetNo: '',
  assetList: [],
  editAsset: {},
  senderList: [],
  editSender: {},
  receivertList: [],
  editReceiver: {},
  custList: [],
  editCustomer: {},
}

export default function(state= initialState, action) {
  switch (action.type) {
    case Types2.MAIN_URL:
      return{
          ...state,
      };
    case Types2.DO_LOGIN:
      return{
          ...state,
          userDetails: action.payload
      };
    case Types2.FETCH_ALL_JOBS:
      return{
          ...state,
          allJobs: action.payload
      }
    case Types2.JOBS_COUNT:
      return{
          ...state,
          jobsCount: action.payload
      }
    case Types2.SEARCH_JOBS:
      return{
          ...state,
          allJobs: action.payload
      }
    case Types2.FETCH_ALL_DRIVERS:
      return{
          ...state,
          allDrivers: action.payload
      }
    case Types2.DRIVERS_COUNT:
      return{
          ...state,
          driversCount: action.payload
      }
    case Types2.SEARCH_DRIVERS:
      return{
          ...state,
          allDrivers: action.payload
      }
    case Types2.DRIVERS_DETAILS:
      return{
          ...state,
          driverDetails: action.payload
      }
    case Types2.UPDATE_PASSWORD:
      return{
          ...state,
          userpassword: action.payload
      }
    case Types2.CLEAR_ORDER:
      return{
          ...state,
          newOrder: [],
          submitOrder: [],
          submitOrderAll: [],
          allJobs: [],
          draftOrder: [],
          orderDetails: [],
          orderComments: [],
      }
    case Types2.CLEAR_DRAFT:
      return{
          ...state,
          draftOrder:[]
      }
    case Types2.CLEAR_CONFIG_MSG:
      return{
          ...state,
          configMsg:[]
      }
    case Types2.CREATE_NEW_ORDER:
      return{
          ...state,
          newOrder: [...state.newOrder, action.payload]
      }
    case Types2.CREATE_NEW_ORDER_CSV:
      return{
          ...state,
          newOrder: action.payload
      }
    case Types2.MODIFY_NEW_ORDER_CSV:
      return{
          ...state,
          newOrder: action.payload
      }
    case Types2.SUBMIT_NEW_ORDER:
      return{
          ...state,
          submitOrder: action.payload,
      }
    case Types2.SUBMIT_NEW_ORDER_ALL:
      return{
          ...state,
          submitOrderAll: action.payload,
      }
    case Types2.DRAFT_ORDER:
      return{
          ...state,
          draftOrder: action.payload,
      }
    case Types2.DELETE_ORDER:
      return{
          ...state,
          newOrder: action.payload
      }
    case Types2.FETCH_JOB_DETAILS:
      return{
          ...state,
          orderDetails: action.payload
      }
    case Types2.FETCH_JOB_COMMENTS:
      return{
          ...state,
          orderComments: action.payload
      }
    case Types2.CUSTOMERS_LIST:
      return{
          ...state,
          customerList: action.payload
      }
    case Types2.MAIN_SIDEBAR:
        return{
            ...state,
            mainSidebar: action.payload
        }
    case Types2.EDIT_ORDER:
        return{
            ...state,
            editOrder: action.payload
        }
    case Types2.SUBMIT_EDIT_ORDER:
      return{
          ...state,
          submitOrder: action.payload,
      }
    case Types2.SET_USER_CONFIGURATION:
      return{
          ...state,
          configMsg: action.payload,
      }
    case Types2.GET_USER_CONFIGURATION:
      return{
          ...state,
          userConfiguration: action.payload,
      }
    case Types2.SENDER_RECEIVER:
      return{
        ...state,
        senderReceiver: action.payload
      }
    case Types2.PUSH_SENDER_RECEIVER:
      return{
        ...state,
        senderReceiver: action.payload
      }
    case Types2.GET_ASSETS:
      return{
        ...state,
        assetsList: action.payload
      }
      case Types2.GET_ASSETS_DRIVER:
        return{
          ...state,
          assetsListDriver: action.payload
        }
    case Types2.SET_ASSET_NO:
      return{
        ...state,
        assetNo: action.payload
      }
    case Types2.EDIT_ASSET:
      return{
        ...state,
        editAsset: action.payload
      }
    case Types2.EDIT_ASSET_GET:
      return{
        ...state,
        editAsset: action.payload
      }
    case Types2.GET_SENDER:
      return{
        ...state,
        senderList: action.payload
      }
    case Types2.EDIT_SENDER:
      return{
        ...state,
        editSender: action.payload
      }
    case Types2.GET_RECEIVER:
      return{
        ...state,
        receivertList: action.payload
      }
    case Types2.EDIT_RECEIVER:
      return{
        ...state,
        editReceiver: action.payload
      }
    case Types2.CLEAR_SUBMIT_MSG:
      return{
          ...state,
          submitOrder: [],
          submitOrderAll: [],
      }
    case Types2.GET_CUSTOMER:
      return{
        ...state,
        custList: action.payload
      }
    case Types2.EDIT_CUSTOMER:
      return{
        ...state,
        editCustomer: action.payload
      }
    case Types2.CLEAR_EDIT:
      return{
        ...state,
        editAsset: [],
        editSender: [],
        editReceiver: [],
        editCustomer: [],
      }
    default:
      return state;
  }
}
