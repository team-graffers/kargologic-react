import {combineReducers} from 'redux';
import carrierReducers from './carrierReducers';
import customerReducers from './customerReducers';

export default combineReducers({
  carrier: carrierReducers,
  customer: customerReducers,
});
