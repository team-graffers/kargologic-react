import * as CustomerTypes2 from '../actions/customerTypes';

let mainSidebar = null;

mainSidebar = localStorage.getItem('sidebar');

if(mainSidebar){
  mainSidebar = JSON.parse(mainSidebar)
}else {
  mainSidebar = {
    iconOrder: 'active',
    iconDrivers: '',
    iconSettings: ''
  };
}
const defaultConfig = {
  timeslice: 30,
  dashboard: ["Job Number", "Create Date", "Customer Name", "Pickup Date Time From", "Pickup Date Time To", "Delivery Date Time From", "Delivery Date Time To", "Status"],
  driver: ["Driver Name", "Vehicle Type", "Vehicle Rego", "Driver License No.", "Phone Number", "Delivery On Time", "Status"],
  timeline: ["Driver Name", "Address", "Quantity", "Schedule Time", "ETA/ ON"],
}
//http://52.62.137.8:8000
//http://13.54.44.169:8000
// mainUrl: 'https://carriers.kargologic.com/',
// localUrl: 'https://carriers.kargologic.com/',
const initialState = {
  mainUrl: 'https://carriers.kargologic.com/',
  localUrl: 'https://carriers.kargologic.com/',
  mainSidebar: mainSidebar,
  appData: 'app_id=JzenTjrL762jlMB5Rvm9&app_code=wQ9RzJk55gYw1z2OT-OzBQ',
  app_id: 'JzenTjrL762jlMB5Rvm9',
  app_code: 'wQ9RzJk55gYw1z2OT-OzBQ',
  userConfiguration: defaultConfig,
  configMsg: {},
  allJobs: [],
  jobsCount: [],
  allCarriers: [],
  carriersCount: [],
  newOrder:[],
  submitOrderAll: [],
  submitOrder:[],
  orderDetails: [],
  carrierList: [],
  carrierDetails: [],
  draftOrder:[],
  editOrder: false,
  senderReceiver: [],
  senderList: [],
  editSender: {},
  receivertList: [],
  editReceiver: {},
}

export default function(state= initialState, action) {
  switch (action.type) {
    case CustomerTypes2.CUSTOMER_MAIN_SIDEBAR:
      return{
        ...state,
        mainSidebar: action.payload
      };
      case CustomerTypes2.CUSTOMER_JOBS:
        return{
            ...state,
            allJobs: action.payload
        }
      case CustomerTypes2.CUSTOMER_JOBS_COUNT:
        return{
            ...state,
            jobsCount: action.payload
        }
      case CustomerTypes2.CUSTOMER_SEARCH_JOBS:
        return{
            ...state,
            allJobs: action.payload
        }
      case CustomerTypes2.CUSTOMER_CARRIER:
        return{
            ...state,
            allCarriers: action.payload
        }
      case CustomerTypes2.CUSTOMER_CARRIER_COUNT:
        return{
            ...state,
            carriersCount: action.payload
        }
      case CustomerTypes2.CUSTOMER_CARRIER_SEARCH:
        return{
            ...state,
            allCarriers: action.payload
        }
      case CustomerTypes2.CUSTOMER_CLEAR_ORDER:
        return{
            ...state,
            newOrder: [],
            submitOrderAll: [],
            submitOrder: [],
            allJobs: [],
            orderDetails: [],
            draftOrder: [],
        }
      case CustomerTypes2.CUSTOMER_NEW_ORDER:
        return{
            ...state,
            newOrder: [...state.newOrder, action.payload]
        }
      case CustomerTypes2.CUSTOMER_NEW_ORDER_CSV:
        return{
            ...state,
            newOrder: action.payload
        }
      case CustomerTypes2.CUSTOMER_MODIFY_NEW_ORDER_CSV:
        return{
            ...state,
            newOrder: action.payload
        }
      case CustomerTypes2.CUSTOMER_SUBMIT_NEW_ORDER:
        return{
            ...state,
            submitOrder: action.payload,
        }
      case CustomerTypes2.CUSTOMER_SUBMIT_NEW_ORDER_ALL:
        return{
            ...state,
            submitOrderAll: action.payload,
        }
      case CustomerTypes2.CARRIERS_LIST:
        return{
            ...state,
            carrierList: action.payload
        }
      case CustomerTypes2.CUSTOMER_JOB_DETAILS:
        return{
            ...state,
            orderDetails: action.payload
        }
      case CustomerTypes2.CUSTOMER_CARRIER_DETAILS:
        return{
            ...state,
            carrierDetails: action.payload
        }
      case CustomerTypes2.CUSTOMER_DELETE_ORDER:
        return{
            ...state,
            newOrder: action.payload
        }
      case CustomerTypes2.CUSTOMER_DRAFT_ORDER:
        return{
            ...state,
            draftOrder: action.payload,
        }
      case CustomerTypes2.CUSTOMER_CLEAR_DRAFT:
        return{
            ...state,
            draftOrder:[]
        }
      case CustomerTypes2.CUSTOMER_EDIT_ORDER:
          return{
              ...state,
              editOrder: action.payload
          }
      case CustomerTypes2.CUSTOMER_SUBMIT_EDIT_ORDER:
        return{
            ...state,
            submitOrder: action.payload,
        }
      case CustomerTypes2.CUSTOMER_SENDER_RECEIVER:
        return{
          ...state,
          senderReceiver: action.payload
        }
      case CustomerTypes2.CUSTOMER_PUSH_SENDER_RECEIVER:
        return{
          ...state,
          senderReceiver: action.payload
        }
      case CustomerTypes2.CUSTOMER_SET_USER_CONFIGURATION:
        return{
            ...state,
            configMsg: action.payload,
        }
      case CustomerTypes2.CUSTOMER_GET_USER_CONFIGURATION:
        return{
            ...state,
            userConfiguration: action.payload,
        }
      case CustomerTypes2.CUSTOMER_CLEAR_CONFIG_MSG:
        return{
            ...state,
            configMsg: [],
            editSender: {},
            editReceiver: {},
        }
        case CustomerTypes2.CUSTOMER_GET_SENDER:
          return{
            ...state,
            senderList: action.payload
          }
        case CustomerTypes2.CUSTOMER_EDIT_SENDER:
          return{
            ...state,
            editSender: action.payload
          }
        case CustomerTypes2.CUSTOMER_GET_RECEIVER:
          return{
            ...state,
            receivertList: action.payload
          }
        case CustomerTypes2.CUSTOMER_EDIT_RECEIVER:
          return{
            ...state,
            editReceiver: action.payload
          }
        case CustomerTypes2.CLEAR_EDIT:
          return{
            ...state,
            editSender: [],
            editReceiver: [],
          }
        case CustomerTypes2.CLEAR_SUBMIT_MSG:
          return{
              ...state,
              submitOrder: [],
              submitOrderAll: [],
          }
  default:
        return state;
    }
  }
