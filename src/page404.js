import React, {Component} from 'react';
import SideImg from './component/include/side-img';
class Page404 extends Component{
  render(){
    return(
      <article>
        <section className="sectionColumns">
          <SideImg/>
        <div className="rightColumn">
                  <div className="content-rightColumn">
                      <h5 className="mb-4">404 Page not found</h5>
                  </div>
              </div>
          </section>
      </article>
    )
  }
}

export default Page404;
