import React, { Component } from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Page404 from './page404';
import './App.css';
import CarrierLogin from './component/login';
import CarrierSignup from './component/signup';
import Dashboard from './component/dashboard';
import CreateOrder from './component/createOrder';
import Drivers from './component/drivers';
import DriverDetails from './component/driverDetails';
import Settings from './component/settings';
import Password from './component/updatePassword';
import Logout from './component/logout';
import CustomerSignup from './component/customerSignup';
import Orderdetails from './component/orderDetails';
import DriverProfile from './component/driverProfile';
import ForgetPassword from './component/forgetPassword';
import RecoverPassword from './component/recoverPassword';
import TrackOrder from './component/trackorder';
import Configure from './component/configure';
import AssetsManager from './component/assetsManager';
import Senders from './component/sender';
import Receivers from './component/receiver';
import Customers from './component/customers';

import CustomerSignupNew from './customerComponent/customerSignup';
import CustomerLogin from './customerComponent/customerLogin';
import CustomerLogout from './customerComponent/customerLogout';
import CustomerForgetPassword from './customerComponent/customerForgetPassword';
import CustomerRecoverPassword from './customerComponent/customerRecoverPassword';
import CustomerDashboard from './customerComponent/customerDashboard';
import CustomerCarriers from './customerComponent/carriers';
import CarrierDetails from './customerComponent/carrierDetails';
import CustomerSettings from './customerComponent/customerSettings';
import CustomerPassword from './customerComponent/customerPassword';
import CustomerCreateOrder from './customerComponent/customerCreateOrder';
import CustomerOrderDetails from './customerComponent/customerOrderDetail';
import CustomerCarrierSignup from './customerComponent/carrierSignup';

import CustomerConfigure from './customerComponent/customerConfigure';
import CustomerSenders from './customerComponent/customerSender';
import CustomerReceivers from './customerComponent/customerReceiver';

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route path="/" component={() => <CarrierLogin/>} exact/>
            <Route path="/carrier/login" component={() => <CarrierLogin/>} exact/>
            <Route path="/carrier/signup" component={() => <CarrierSignup/>} exact/>
            <Route path="/carrier/forgot-password" component={() => <ForgetPassword/>} exact/>
            <Route path="/carrier/recover-password" component={() => <RecoverPassword/>} exact/>
            <Route path="/dashboard/:status" component={(props) => <Dashboard {...props} />} />
            <Route path="/createorder/" component={() => <CreateOrder/>} exact/>
            <Route path="/orderdetails/:id" component={() => <Orderdetails/>} exact/>
            <Route path="/trackorder/:id" component={() => <TrackOrder/>} exact/>
            <Route path="/drivers/:status" component={() => <Drivers/>} />
            <Route path="/driverdetails/:id" component={() => <DriverDetails/>} exact/>
            <Route path="/driverprofile/:name/:id" component={() => <DriverProfile/>} exact/>
            <Route path="/settings" component={() => <Settings/>} exact/>
            <Route path="/password" component={() => <Password/>} exact/>
            <Route path="/configure" component={() => <Configure/>} exact/>
            <Route path="/assetsmanager" component={() => <AssetsManager/>} exact/>
            <Route path="/receiver" component={() => <Receivers/>} exact/>
            <Route path="/sender" component={() => <Senders/>} exact/>
            <Route path="/customer" component={() => <Customers/>} exact/>
            <Route path="/logout" component={() => <Logout/>} exact/>
            <Route path="/customer/signup" component={() => <CustomerSignup/>} exact/>

            <Route path="/customer/signup2" component={() => <CustomerSignupNew/>} exact/>
            <Route path="/customer/login" component={() => <CustomerLogin/>} exact/>
            <Route path="/customer/forgot-password" component={() => <CustomerForgetPassword/>} exact/>
            <Route path="/customer/recover-password" component={() => <CustomerRecoverPassword/>} exact/>
            <Route path="/customerdashboard/:status" component={() => <CustomerDashboard/>} exact/>
            <Route path="/customerorderdetail/:id" component={() => <CustomerOrderDetails/>} exact/>
            <Route path="/customercreateorder" component={() => <CustomerCreateOrder/>} exact/>
            <Route path="/customercarrier/:status" component={() => <CustomerCarriers/>} exact/>
            <Route path="/carrierdetails/:id" component={() => <CarrierDetails/>} exact/>
            <Route path="/customersettings" component={() => <CustomerSettings/>} exact/>
            <Route path="/customerconfigure" component={() => <CustomerConfigure/>} exact/>
            <Route path="/customersender" component={() => <CustomerSenders/>} exact/>
            <Route path="/customerreceiver" component={() => <CustomerReceivers/>} exact/>
            <Route path="/customerpassword" component={() => <CustomerPassword/>} exact/>
            <Route path="/customerlogout" component={() => <CustomerLogout/>} exact/>
            <Route path="/carriersignup" component={()=> <CustomerCarrierSignup/>} exact/>
            <Route component={()=> <Page404/>} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
