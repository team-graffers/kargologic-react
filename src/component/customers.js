import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import SettingSidebar from './include/settingSidebar';
import Path from './include/path';
import Datatable from './subComponent/datatable';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getCustList, editCustomerSet, clearEdit} from '../actions/carrierActions';
import {formateDate} from './include/date';
import InviteCustomer2 from './subComponent/inviteCustomer2';
import {DownloadExcel} from './ajax/downloadExcel';
import PostFetch from './ajax/postFetch';
import {successAlert, errorAlert} from './include/alert';
import UploadExcelASR from './subComponent/uploadExcelASR';

class Customers extends Component{
  constructor(props){
    super(props);
    this.state = {
      file: '',
      custList: [],
      check: false,
      temp: [],
      excelModel: false,
      customerModel: false,
      columns: [
        {
          name: 'Primary Contact',
          selector: 'name',
          sortable: true,
        },
        {
          name: 'Primary Mobile No.',
          selector: 'mobile_num',
          sortable: true,
        },
        {
          name: 'Primary Email',
          selector: 'email',
          sortable: true,
        },
        {
          name: 'Secondary Contact',
          selector: 'secondary_contact',
          sortable: true,
        },
        {
          name: 'Secondary Email',
          selector: 'sec_email_id',
          sortable: true,
        },
        {
          name: 'Secondary Mobile No',
          selector: 'sec_phone',
          sortable: true,
        },
        {
          name: 'Business Name',
          selector: 'business_name',
          sortable: true,
        },
        {
          name: 'Business Address',
          selector: 'business_address',
          sortable: true,
        },
        {
          name: 'Contract Start Date',
          selector: 'contract_start_date',
          sortable: true,
          cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{formateDate(row.contract_start_date)}</span>
        },
        {
          name: 'Contract End Date',
          selector: 'contract_end_date',
          sortable: true,
          cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{formateDate(row.contract_end_date)}</span>
        },
        {
          name: 'Customer Code',
          selector: 'Customer_code',
          sortable: true,
        },
        {
          name: 'Status',
          selector: 'r_status.status_name',
          sortable: true,
          cell: row=> <span style={{position: 'relative', zIndex: '999'}}>{this.filterStatus(row.r_status)}</span>
        },
        {
          name: 'Contract Documents',
          selector: 'custt_att',
          cell: row => <div style={{"zIndex":"999", "position":"relative", "width": "120px"}}>
            <ul style={{margin: 0, padding: 0, overflow: 'hidden'}}>
            {row.custt_att && row.custt_att.length>0?
              row.custt_att.map((e, i)=>(
                <li key={i} className="id-selector marquee"><a href={e[0]} style={{'position': 'absolute'}} download>{i+1}. Document</a></li>
              ))
            :null}
            </ul>
          </div>
        },
        {
          name: 'Edit',
          selector: 'id',
          cell: row => <div style={{"zIndex":"999", "position":"absolute"}}><a href="#a" onClick={(e)=>{
            e.preventDefault();
            this.props.editCustomerSet(row);
            this.openOneModel();
          }}>Edit</a></div>,
        }
      ]
    }
  }
  openOneModel=()=>{
    this.setState({
      customerModel: true
    })
  }
  closeOneModel=()=>{
    this.setState({
      customerModel: false
    })
  }
  openExcelModel = ()=>{
    this.setState({
      excelModel: true
    })
  }
  closeExcelModel = ()=>{
    this.refreshData();
    this.setState({
      excelModel: false
    })
  }
  filterStatus = (data) =>{
    if(data && data.status_name){
      data = data.status_name.split(' ')
      return data[0]
    }else {
      return 'pending'
    }

  }
  componentWillMount(){
    this.props.getCustList('carr/custlistconfig/');
  }
  refreshData =()=>{
    this.setState({
      custList: [],
      check: false
    })
    this.props.getCustList('carr/custlistconfig/');
  }
  handleCheckbox = (data)=>{
    console.log(data);
    this.setState({
      temp:data
    });
  }
  changeData = ()=>{
    if(this.state.temp && this.state.temp.length>0){
      let jid = [];
      this.state.temp.map((e, i)=>{
        jid.push(e.id);
        return('')
      });
      return jid;
    }else{
      return [];
    }
  }
  changeDataDelete = ()=>{
    if(this.state.temp && this.state.temp.length>0){
      let jid = [];
      this.state.temp.map((e, i)=>{
        if(e.CustomercompanyID && e.CustomercompanyID.id){
          jid.push(e.CustomercompanyID.id);
        }
        return('')
      });
      return jid;
    }else{
      return [];
    }
  }
  handleDelete = async ()=>{
    const payload ={
      cust_list: this.changeDataDelete()
    };
    let data = await PostFetch('carr/cust_delete/', payload, null, 'PUT');
    if(data && data.message){
      successAlert(data.message)
      this.refreshData();
    }else {
      errorAlert('try again')
    }
  }
  handleDownload = ()=>{
    let payload = this.changeData();
    let carrier = JSON.parse(localStorage.getItem("userDetails"));
    payload = 'carr/download_cust_list/?custlist='+JSON.stringify(payload)+'&carr_com_id='+carrier.carr_com_id;
    DownloadExcel(payload)
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.custList && nextProps.custList.data && nextProps.custList.data.length>0){
      const data = nextProps.custList.data.filter((e, i)=>{
        return e.CustomercompanyID.name !== 'NA';
      })
      this.setState({
        custList: data,
        check: true
      })
    }
  }
  openCustomerModel = () =>{
    this.setState({
      customerModel: true
    })
  }
  closeCustomerModel = () =>{
    this.props.clearEdit();
    this.refreshData();
    this.setState({
      customerModel: false
    })
  }
  render(){
    return(
      <article>
        {this.state.excelModel?
          <UploadExcelASR
            name= "Upload Customer"
            fetchUrl= "carr/cust_upload/"
            sampleUrl='carr/cust_sample/'
            refreshData={this.refreshData}
            closeModel={this.closeExcelModel}
          />
        :null}
        {this.state.customerModel?
          <InviteCustomer2 closemodel={this.closeCustomerModel} />
        :null}
          <div className="dashboardMain">
            <Header/>
              <div className="dashboardContent">
                <MainSidebar/>
                  <div className="rightColumn">
                    <SettingSidebar/>
                    <div className="rightColumnContent">
                    <Path path={[
                                  {
                                    "path": "Manage your account",
                                    "url": "/settings"
                                  },
                                  {
                                    "path": "Customers",
                                    "url": "/customer"
                                  }
                                ]} />
                          <div className="tableHeader">
                              <div className="row align-items-center">
                                  <div className="col-12 col-sm-8 col-md-8">
                                    <ul className="listFilter">
                                      <li><a href="#a" onClick={(e)=>{
                                        e.preventDefault();
                                        this.handleDelete();
                                      }}>Delete</a></li>
                                      <li><a href="#a" onClick={(e)=>{
                                        e.preventDefault();
                                        this.handleDownload();
                                      }}>Download</a></li>
                                      <li><a href="#uploadexcelasr" onClick={(e)=>{
                                        e.preventDefault();
                                        this.openExcelModel();
                                      }}>Upload</a></li>

                                      <li><a href="#a" onClick={(e)=>{
                                        e.preventDefault();
                                        this.openCustomerModel();
                                      }}>Add New Customer</a></li>
                                    </ul>
                                  </div>

                              </div>
                          </div>
                          <div style={{width: 'auto', overflow: 'auto'}}>
                            {this.state.custList.length>0 && this.state.check===true?
                              <Datatable returnFunc={this.handleCheckbox} data={this.state.custList} columns={this.state.columns}  />:
                            <p>no records to display</p>}
                          </div>
                  </div>
              </div>
          </div>
        </div>
      </article>
    )
  }
}

Customers.propTypes = {
  getCustList: PropTypes.func.isRequired,
  editCustomerSet: PropTypes.func.isRequired,
  clearEdit: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  custList: state.carrier.custList,
  editCustomer: state.carrier.editCustomer,
});

export default connect(mapStateToProps, {getCustList, editCustomerSet, clearEdit})(Customers);
/*<li><a href="#uploadexcelasr" onClick={(e)=>{
  e.preventDefault();
  this.openExcelModel();
}}>Upload</a></li>*/
