import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import DriverSidebar from './include/driverDetailsSidebar';
import Path from './include/path';
import GetFetch from './ajax/getFetch';
import APIFetch from './ajax/apiFetch';
import axios from 'axios';
import store from '../store';
//import makeid from './subComponent/makeid';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getDriverDetails} from '../actions/carrierActions';
import {successAlert} from './include/alert';
import {Redirect } from 'react-router';

class DriverProfile extends Component{
  constructor(props){
    super(props);
    this.state = {
      driver_id: '',
      name:'',
      truck_reg_num:'',
      truck_category:'',
      truck_id: '',
      license_no:'',
      phone:'',
      email:'',
      address: '',
      driver_dp: '',
      newDp: "",
      phoneError: '',
      flg:0,
      appdata: '',
      data: [],
      drivers: [],
      redirect: false,
      path: [
              {
                path: "All Drivers",
                url: "/drivers/all"
              },
              {
                path: "Drivers",
                url: "/drivers/"
              },
              {
                path: "Profile",
                url: "/driverprofile/"
              }
            ],
    }
    this.submitForm = this.submitForm.bind(this);
  }
  getDriverList = async ()=>{
    let json = await GetFetch('truck_categ/truck_categ_list/');
    if(json && json.data){
      if(json.data.length>0){
        let drivers = [];
        json.data.map((e, i)=>{
          let arr = [];
          if(e.parent_id===null){
            arr.push(e);
            json.data.map((f, j)=>{
              if(e.categ_name===f.parent_id){
                arr.push(f);
              }
              return(
              ''
              )
            });
            drivers.push({
              'parent': arr
            });
          }
          return(
          ''
          )
        });
        this.setState({
          drivers: drivers
        });
      }
    }
  }
  componentWillMount(){
    const state = store.getState();
    this.setState({
      appdata: state.carrier.appdata
    })
    let url = window.location.href;
    try{
      url = url.split('/driverprofile/');
      url = url[1].split('/');
      let path = this.state.path;
      path[1].url = '/driverdetails/'+url[1];
      path[2].url = '/driverprofile/'+url[0]+'/'+url[1];
      let name = '';
      let n = url[0].split('%20')
      if(n[1]) {
        name = n[0]+" "+n[1];
        path[1].path = name;
      }else{
        name = url[0];
        path[1].path = name;
      }
      this.setState({
        driver_id: url[1],
        name: name,
        path: path
      })
      this.props.getDriverDetails('/driv/driv-order-detail/?dri_id='+url[1]);
    }catch(e){

    }
    this.getDriverList();
  }

  submitForm = (e) =>{
    e.preventDefault();
    const payload ={
      driver_id: parseInt(this.state.driver_id),
      name: this.state.name,
      truck_reg_num: this.state.truck_reg_num,
      truck_category: this.state.truck_id,
      license_num: this.state.license_no,
      phone: this.state.phone,
      email: this.state.email,
      address: this.state.address,
    }
    const data = new FormData();
    data.append('file', this.state.newDp);
    data.append('payload', JSON.stringify(payload));
    const state = store.getState();
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Token '+localStorage.getItem("token")
    }
    axios.post(state.carrier.mainUrl+'driv/carr_driver_profile_update/', data, {headers: headers})
    .then(res => {
       if(res.data && res.data.message){
         this.setState({
           redirect: true,
         })
         if(res.data.image_url){
           this.setState({
             driver_dp: res.data.image_url,
           })
         }
         successAlert(res.data.message)
       }
     });
  }

  changeDP = (e)=>{
    let file = e.target.files[0];
    const types = ['image/png', 'image/jpeg', 'image/gif'];
    if (types.every(type => file.type !== type)) {
      e.target.value = null;
      return null;
    }
    if(file){
      var reader = new FileReader();
      reader.onloadend = ()=>{
        this.setState({
          driver_dp: reader.result
        })
      }
      reader.readAsDataURL(file);
      this.setState({
        newDp: file
      })
    }
  }
  getSuggestions = async (text)=>{
    const data = await APIFetch('http://autocomplete.geocoder.api.here.com/6.2/suggest.json?'+this.state.appdata+'&query='+text+'&country=aus');
    let options = [];
    if(data.suggestions){
       data.suggestions.map((s,i)=>(
        options.push({
          value:s.label,
          label:s.label,
        })
      ));
    }
    this.setState({
      data:options
    });
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.driverDetails && nextProps.driverDetails.driver_detail && nextProps.driverDetails.driver_detail.length){
      let driver = nextProps.driverDetails.driver_detail[0];
      this.setState({
        truck_reg_num: driver.TruckType.truck_reg_num,
        truck_category: driver.TruckType.truck_category.categ_name,
        truck_id: driver.TruckType.truck_category.id,
        license_no: driver.license_num,
        phone: driver.mobile_num,
        email: driver.email,
        address: driver.address,
        driver_dp: driver.image
      })
    }

  }
  render(){
    if(this.state.redirect){
      return(
        <Redirect to={`/driverdetails/${this.state.driver_id}`} />
      )
    }
    return(
      <article>
        <div className="dashboardMain">
        <Header/>
            <div className="dashboardContent">
                <MainSidebar/>
                <div className="rightColumn">
                    <DriverSidebar/>
                    <div className="rightColumnContent">
                    <Path path={this.state.path} />
                      <div className="boxContentInner">
                          <div className="contentAccountSetting">
                              <form method="post" onSubmit={this.submitForm}>
                                  <div className="boxProfile">
                                      <div className="boxProfilePhoto">
                                          <div className="custom-file">
                                              <i className="fas fa-camera"></i>
                                              <input type="file" onChange={this.changeDP} className="custom-file-input" aria-describedby="inputGroupFileAddon01"/>
                                          </div>
                                         <img src={this.state.driver_dp? this.state.driver_dp: '/assets/img/usergray.jpg'} alt="" title=""/>
                                      </div>
                                      <div className="row">
                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Driver Name</label>
                                                  <input type="text" onChange={(e)=>{
                                                     this.setState({name:e.target.value})
                                                   }} value={this.state.name} placeholder="Enter Name" required className="form-control"/>
                                              </div>
                                          </div>
                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Vehicle Registration No.</label>
                                                  <input onChange={(e)=>{
                                                    this.setState({truck_reg_num:e.target.value})
                                                  }} value={this.state.truck_reg_num} placeholder="Enter Vehicle Registration Number" required type="text" className="form-control"/>
                                              </div>
                                          </div>
                                      </div>
                                      <div className="row">
                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Driver License No.</label>
                                                  <input type="text" onChange={(e)=>{
                                                    this.setState({license_no:e.target.value})
                                                  }} value={this.state.license_no} placeholder="Enter License Number" required className="form-control"/>
                                              </div>
                                          </div>
                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Address</label>
                                                  <input list="address" type="text" onChange={(e)=>{
                                                    this.setState({address:e.target.value})
                                                    if(e.target.value.length>0){
                                                      this.getSuggestions(e.target.value);
                                                    }
                                                  }} value={this.state.address} placeholder="Enter Address" required className="form-control"/>
                                              </div>
                                          </div>
                                          <datalist id="address">
                                            <option value="">select</option>
                                            {this.state.data.length>0?
                                            this.state.data.map((e, i)=>{
                                              return(
                                                <option key={i} value={e.value}>{e.label}</option>
                                              )
                                            }):null}
                                          </datalist>
                                      </div>
                                      <div className="row">
                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Phone</label>
                                                  <input type="text" onChange={(e)=>{
                                                    this.setState({phone:e.target.value})
                                                  }} value={this.state.phone} placeholder="Enter Phone Number" required disabled className="form-control"/>
                                              </div>
                                          </div>
                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Email</label>
                                                  <input type="text" onChange={(e)=>{
                                                    this.setState({email:e.target.value})
                                                  }} value={this.state.email} placeholder="Enter Email Id" required disabled className="form-control"/>
                                              </div>
                                          </div>
                                      </div>
                                      <div className="row">
                                          <div className="col-md-6">
                                              <input type="submit" className="btn" value="Save Changes"/>
                                          </div>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>



              </div>
          </div>
      </div>
</div>
      </article>
    )
  }
}

DriverProfile.propTypes = {
  getDriverDetails: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  driverDetails: state.carrier.driverDetails,
});

export default connect(mapStateToProps, {getDriverDetails})(DriverProfile);


/*<div className="col-md-6">
    <div className="form-group">
        <label>Vehicle Type</label>
        <select required onChange={(e)=>{
          if(e.target.value!==""){
            let x = e.target.value.split('@');
            this.setState({
              truck_id: x[0],
              truck_category:x[1]
            });
          }
        }} value={`${this.state.truck_id}@${this.state.truck_category}`} id="inputState" className="form-control">
            <option value="">Select Vehicle Type</option>
            {this.state.drivers.length>0?
              this.state.drivers.map((e, i)=>(
                e.parent.map((f, j)=>{
                  let id1 = makeid();
                  let id2 = makeid();
                  if(f.is_last){
                    return(
                      <option key={id2} value={`${f.id}@${f.categ_name}`}>{f.categ_name}</option>
                    )
                  }else {
                    return(
                      <option key={id1} disabled>{f.categ_name}</option>
                    )
                  }
                })
              )):null}
        </select>
    </div>
</div>*/
