import React, {Component} from 'react';
import {GetFetch2, PostFetch2} from './ajax/ajaxFetch';
import {getDecrypt} from './include/encryptDecrypt';
import {formateDate2, formateDate3, dateDiff, generateETA} from './include/date';
import OrderMap from './subComponent/orderMap';
import moment from 'moment';
import {GetLocation, GetRoute} from './include/hereMapApi';

class TrackOrder extends Component {
  constructor() {
    super();
    this.state = {
      order: '',
      driverDetails: [],
      orders: '',
      enRoute: true,
      sortArr: [],
    }
  }
  getData = async ()=>{
    let url = window.location.href;
    url = url.split('trackorder/')
    let id = getDecrypt(url[1]);
    let orders = await GetFetch2('orders/ord_tracking/?order_id='+id);
    if(orders && orders.data){
      let driverDetails = [];
      orders = orders.data[0];
      this.generateTimeline(orders);

      if(orders && orders.assign_pick.length>0 && orders.assign_drop.length>0){
          orders.assign_pick.map((f, i)=>{
            let flg=1;
            for(let j=0;j<driverDetails.length;j++){
              if(driverDetails[j].name===f.OrderDriverID.DriverID.name){
                flg=0;
                break;
              }
            }
            if(flg===1){
              driverDetails.push({
                driver_id: f.OrderDriverID.DriverID.id,
                name: f.OrderDriverID.DriverID.name,
                truck_type: f.OrderDriverID.DriverID.TruckType.truck_category.categ_name,
                pickup: [],
                delivery: []
              });
            }
            return(
              ''
            )
          });
          orders.assign_pick.map((f, i)=>{
            driverDetails.map((e, i)=>{
              if(e.driver_id===f.OrderDriverID.DriverID.id && i<driverDetails.length){
                driverDetails[i].pickup.push({
                  pick_id: f.id,
                  DriverID: f.OrderDriverID.DriverID.id,
                  actual_pickup_date_time: f.actual_pickup_date_time,
                  address: f.address,
                  assigned_quantity: f.assigned_quantity,
                  created_DT: f.created_DT,
                  latitude: f.latitude,
                  longitude: f.longitude,
                  pickup_date_time: f.pickup_date_time,
                  pickup_date_time_to: f.pickup_date_time_to,
                  updated_DT: f.updated_DT,
                });
              }
              return(
                ''
              )
            });
            return(
              ''
            )
          });
          orders.assign_drop.map((f, i)=>{
            driverDetails.map((e, i)=>{
              if(e.driver_id===f.OrderDriverID.DriverID.id  && i<driverDetails.length){
                driverDetails[i].delivery.push({
                  drop_id: f.id,
                  DriverID: f.OrderDriverID.DriverID.id,
                  actual_delivery_date_time: f.actual_delivery_date_time,
                  address: f.address,
                  assigned_quantity: f.assigned_quantity,
                  created_DT: f.created_DT,
                  latitude: f.latitude,
                  longitude: f.longitude,
                  delivery_date_time: f.delivery_date_time,
                  delivery_date_time_to: f.delivery_date_time_to,
                  updated_DT: f.updated_DT,
                });
              }
              return(
                ''
              )
            });
            return(
              ''
            )
          });
        }

      this.setState({
        driverDetails: driverDetails,
        orders: orders
      });
    }
  }
  componentWillMount(){
    this.getData();
  }
  changeBool=(val)=>{
    if(val){
      return(
        'Yes'
      )
    }else{
      return(
        'No'
      )
    }
  }
  changeColor = (actual_date, to_date) =>{
    if(actual_date){
      actual_date = moment(actual_date, 'YYYY-MM-DDTHH:mmZ').utc();
      to_date = moment(to_date, 'YYYY-MM-DDTHH:mmZ').utc();
      if(actual_date.diff(to_date, 'minutes')<=0){
        return 'active';
      }else {
        return 'pending';
      }
    }
  }
  generateTimeline = async (orders)=>{
    let enRoute = true;
    let sortArr = [];
    let pod = [];
    if(orders && orders.progressbar.length>0){
      let enRouteArr = [];
      orders.progressbar.map((e, i)=>{
        if(e.order_enroute){
          enRouteArr.push(e)
        }
        return('')
      })
      if(enRouteArr && enRouteArr.length>0 && enRouteArr[0].order_enroute){
        enRoute = enRouteArr[enRouteArr.length-1].order_enroute;
      }
    }

    let margeArr = [];
    let trackingIdArr = [];
    if(orders && orders.assign_pick.length>0){
      orders.assign_pick.map((e, i)=>{
        trackingIdArr.push(e.OrderDriverID.DriverID.id);
        margeArr.push({
          id: e.id,
          driver_id: e.OrderDriverID.DriverID.id,
          driver: e.OrderDriverID.DriverID.name,
          is_pick: true,
          date_time: e.pickup_date_time,
          date_time_to: e.pickup_date_time_to,
          actual_date_time: e.actual_pickup_date_time,
          address: e.address,
          assigned_quantity: e.assigned_quantity,
          addressPos: {},
          driverPos: {},
          eta: '',
        })
        return('')
      })
    }

    if(orders && orders.assign_drop.length>0){
      orders.assign_drop.map((e, i)=>{
        trackingIdArr.push(e.OrderDriverID.DriverID.id);
        margeArr.push({
          id: e.id,
          driver_id: e.OrderDriverID.DriverID.id,
          driver: e.OrderDriverID.DriverID.name,
          is_pick: false,
          date_time: e.delivery_date_time,
          date_time_to: e.delivery_date_time_to,
          actual_date_time: e.actual_delivery_date_time,
          address: e.address,
          assigned_quantity: e.assigned_quantity,
          addressPos: {},
          driverPos: {},
          eta: '',
        });
        if(e.pod_image){
          pod.push({
            driver_id: e.OrderDriverID.DriverID.id,
            driver: e.OrderDriverID.DriverID.name,
            address: e.address,
            pod_image: e.pod_image
          })
        }
        return('')
      })
    }
      trackingIdArr = [...new Set(trackingIdArr)];
      if (trackingIdArr && trackingIdArr.length>0){
        let json = await PostFetch2('driv/get_driver_cor/', {driver_ids: trackingIdArr});
        if(json && json.data && json.data.length>0){
          margeArr = margeArr.sort(function(a,b){
            return moment(a.date_time_to, 'YYYY-MM-DDTHH:mm:ssZ').utc() - moment(b.date_time_to, 'YYYY-MM-DDTHH:mm:ssZ').utc();
          });
          for(let i=0;i<margeArr.length; i++){
            let pos = await GetLocation(margeArr[i].address);
            if(pos && pos.lat && pos.lng){
              margeArr[i].addressPos.lat = pos.lat;
              margeArr[i].addressPos.lng = pos.lng;
            }
            for(let j=0;j<json.data.length; j++){
              if(margeArr[i].driver_id === json.data[j].id){
                margeArr[i].driverPos.lat = json.data[j].lat;
                margeArr[i].driverPos.lng = json.data[j].lng;
              }
            }
            if(margeArr[i].addressPos && margeArr[i].addressPos.lat && margeArr[i].addressPos.lng && margeArr[i].driverPos && margeArr[i].driverPos.lat && margeArr[i].driverPos.lng){
              if(margeArr[i].actual_date_time){
                continue;
              }else if(i>0 && margeArr[i-1].actual_date_time && !margeArr[i].actual_date_time) {
                let json = await GetRoute('waypoint0=geo!'+margeArr[i].driverPos.lat+','+margeArr[i].driverPos.lng+'&waypoint1=geo!'+margeArr[i].addressPos.lat+','+margeArr[i].addressPos.lng+'');

                if(json && json.trafficTime){
                  margeArr[i].eta = json.trafficTime;
                }else if(json && json.trafficTime === 0){
                  margeArr[i].eta = 0;
                }else{
                  margeArr[i].eta = 'No Route Found 1';
                }

              }else if(i>0 && !margeArr[i-1].actual_date_time && !margeArr[i].actual_date_time) {
                let json = await GetRoute('waypoint0=geo!'+margeArr[i-1].addressPos.lat+','+margeArr[i-1].addressPos.lng+'&waypoint1=geo!'+margeArr[i].addressPos.lat+','+margeArr[i].addressPos.lng+'');

                if(json && json.trafficTime){
                  let p = json.trafficTime;
                  if(parseInt(margeArr[i-1].eta)){
                      margeArr[i].eta = p + margeArr[i-1].eta
                  }else{
                    margeArr[i].eta = p;
                  }
                }else{
                  margeArr[i].eta = 'No Route Found 3';
                }
                //  margeArr[i].eta = await GetRoute('waypoint0=geo!'+margeArr[i].driverPos.lat+','+margeArr[i].driverPos.lng+'&waypoint1=geo!'+margeArr[i].addressPos.lat+','+margeArr[i].addressPos.lng+'');
              }else if(i === 0){
                let json = await GetRoute('waypoint0=geo!'+margeArr[i].driverPos.lat+','+margeArr[i].driverPos.lng+'&waypoint1=geo!'+margeArr[i].addressPos.lat+','+margeArr[i].addressPos.lng+'');
                if(json && json.trafficTime){
                  margeArr[i].eta = json.trafficTime;
                }else{
                  margeArr[i].eta = 'No Route Found';
                }
              }
            }
          }

          this.setState({
            enRoute: enRoute,
            sortArr: margeArr,
            pod: pod,
            showMap: true
          })
        }
      }else {
        sortArr = margeArr.sort(function(a,b){
          return moment(a.date_time_to, 'YYYY-MM-DDTHH:mm:ssZ').utc() - moment(b.date_time_to, 'YYYY-MM-DDTHH:mm:ssZ').utc();
        });
        this.setState({
          enRoute: enRoute,
          sortArr: sortArr,
          pod: pod,
          showMap: true
        })
      }
  }
  render(){
    if(this.state.orders){
      let driverDetails = this.state.driverDetails;
      let enRoute = this.state.enRoute;
      let sortArr = this.state.sortArr;
      let orders = this.state.orders;
      const status = orders.order_details[0].StatusID.status_name !== 'delivered' || false;
      return(
        <React.Fragment>
          <article>
              <div className="dashboardMain">
                  <div className="dashboardContent">
                      <div className="">
                          <div className="">
                              <div className="rowOrderDetail">
                                  <div className="contentOrderDetail">
                                      <div className="boxOrderdetail">
                                          <div className="row align-items-center">
                                              <div className="col-12 col-sm-5 col-md-5">
                                                  <h3>ORDER Details</h3>
                                                  <ul className="listorderDetail">
                                                      <li>Job Number: <span>{orders.order_details[0].job_id}</span></li>
                                                      <li>Order Placed: <span>{(orders.order_details[0].order_create_time)}</span></li>
                                                      <li>Create By: <span>{orders.order_details[0].Created_by_CustomerID? orders.order_details[0].Created_by_CustomerID.name: orders.order_details[0].Created_by_CarrierID.name}</span></li>
                                                  </ul>
                                              </div>
                                              <div className="col-12 col-sm-7 col-md-7">
                                                  <ul className="listCustomerDetail">
                                                      <li>
                                                          <h6>Customer Name</h6>
                                                          <h5>{orders.order_details[0].CustomerCompanyID.name}</h5>
                                                      </li>
                                                  </ul>

                                              </div>
                                          </div>
                                      </div>
                                      <hr/>
                                      <div className="boxOrderstatus">
                                          <h4>ORDER STATUS</h4>
                                          {!this.state.changes && orders.assign_pick.length===0?
                                          <p className="font-medium">No Driver Assigned. </p>:null}
                                          <div style={{"width":"1024px", "overflow": "auto"}}>
                                          <ul className="listOrderstatus">
                                            {sortArr.length>0 && enRoute!==true?
                                              <li className="active">
                                                  <i className="fas fa-check-circle"></i> En-route to Pickup
                                                  <br/>
                                                  <span style={{"color":"black"}}>{formateDate3(enRoute)}</span>
                                              </li>:
                                              <li>
                                                  <i className="fas fa-check-circle"></i> En-route to Pickup
                                              </li>}
                                              {sortArr.length>0?
                                                sortArr.map((e, i)=>{
                                                  let name = null;
                                                  if(e.is_pick){
                                                    name = "Pick Up "
                                                  }else{
                                                    name = "Delivery "
                                                  }
                                                  return(
                                                    <li key={i} className={this.changeColor(e.actual_date_time, e.date_time_to)}>
                                                        <i className="fas fa-check-circle"></i> {name}
                                                        <br/>
                                                        <address style={{"color":"black", "width": "170px", "overflow": "auto"}}>
                                                          Address:
                                                          <span style={{"color": "#7a7c75"}}> {e.address}</span>
                                                        </address>
                                                        <span style={{"color":"black"}}>
                                                          <span style={{"color": "black"}}>Quantity: </span>
                                                          <span style={{"color": "#7a7c75"}}>{e.assigned_quantity}</span>
                                                        </span><br/>
                                                        <span style={{"color":"black"}}>
                                                          <span>Schedule Time: </span>
                                                          <span style={{"color": "#7a7c75"}}> {formateDate2(e.date_time_to)}</span>
                                                        </span><br/>
                                                        {!e.actual_date_time && e.eta!==null?
                                                          <span style={{"color":"black"}}>
                                                            <span>ETA: </span>
                                                            <span style={{"color": "#7a7c75"}}> {generateETA(e.eta)}</span>
                                                          </span>:
                                                          <span style={{"color":"black"}}>
                                                            <span>ON: </span>
                                                            <span style={{"color": "#7a7c75"}}> {formateDate3(e.actual_date_time)}</span>
                                                          </span>}<br/>
                                                        <span style={{"color":"black"}}>
                                                          {dateDiff(e.date_time_to, e.actual_date_time, e.date_time)}
                                                        </span>
                                                    </li>
                                                  )
                                                }):
                                                <React.Fragment>
                                                <li>
                                                    <i className="fas fa-check-circle"></i> Pick Up
                                                    <br/>
                                                    <span style={{"color":"black"}}></span>
                                                </li>
                                                <li>
                                                    <i className="fas fa-check-circle"></i> Delivery
                                                    <br/>
                                                    <span style={{"color":"black"}}></span>
                                                </li>
                                                </React.Fragment>
                                              }
                                          </ul>
                                          </div>
                                      </div>
                                      <hr/>
                                      {status && driverDetails.length>0?
                                        <div className="boxMap">
                                          <h4 className="pt-3">Map</h4>
                                          <OrderMap drivers={this.state.orders.tracking_driv} driverDetails={driverDetails}/>
                                        </div>
                                      :null}
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

          </article>
        </React.Fragment>
      )
    }else {
      return('')
    }
  }
}
export default TrackOrder;
