import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import DriverSidebar from './include/driverDetailsSidebar';
import Path from './include/path';
import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import InviteDriver from './subComponent/inviteDriver';
import {connect} from 'react-redux';
import {getAllDrivers, getDriverDetails, getDriversCount, getCustomerList} from '../actions/carrierActions';
import Datatable from './subComponent/datatable'
import PostFetch from './ajax/postFetch';
import GetFetch from './ajax/getFetch';
import {formateDate} from './include/date';
import {successAlert, errorAlert, infoAlert} from './include/alert';
import {DownloadExcel} from './ajax/downloadExcel';

class DriverDetails extends Component{
  constructor(props){
    super(props);
    let url = window.location.href;
    url = url.split('driverdetails/');
    this.state = {
      driver_id: url[1],
      customers:'All Customers',
      check: false,
      jobs: [],
      allCustomers: [],
      total: 0,
      pending: 0,
      inprocess: 0,
      delivered: 0,
      cancelled: 0,
      job_id: [],
      invite: false,
      search:'',
      path: [
              {
                "path": "All Drivers",
                "url": "/drivers/all"
              },
              {
                "path": "",
                "url": "/driverdetails/"+url[1]
              }
            ],
      columns:[
                {
                  name: 'Job Number',
                  selector: 'job_id',
                  sortable: true,
                  width:'150px',
                  cell: row => <NavLink style={{"zIndex":"999", "position":"relative"}} to={row.status==="draft"?`/createorder/?id=${row.id}`:`/orderdetails/${row.id}`}>{row.job_id}</NavLink>,
                },
                {
                  name: 'Created Date',
                  selector: 'order_create_time',
                  sortable: true,
                },
                {
                  name: 'Customer Name',
                  selector: 'name',
                  sortable: true,
                  width:'200px',
                },
                {
                  name: 'Pick Up Time From',
                  selector: 'pickup_date_time',
                  sortable: true,
                },
                {
                  name: 'Pick Up Time To',
                  selector: 'pickup_date_time_to',
                  sortable: true,
                },
                {
                  name: 'Delivery Time From',
                  selector: 'delivery_date_time',
                  sortable: true,
                },
                {
                  name: 'Delivery Time To',
                  selector: 'delivery_date_time_to',
                  sortable: true,
                },
                {
                  name: 'Status',
                  selector: 'status',
                  sortable: true,
                  width:'150px',
                  cell: row => <span className={row.bgColor}>{row.status}</span>,
                }
              ]
    }
    this.changePath = this.changePath.bind(this);
    this.searchJobs = this.searchJobs.bind(this);
  }
  componentWillMount(){
    this.props.getCustomerList();
    this.props.getDriversCount('carr/show_count_driver/');
    this.props.getAllDrivers('driv/driver-list-status/?status=all');
  this.changeDriver();
  }

  changeDriver =(id=null)=>{
    if(id){
      this.setState({
        check: false,
        jobs: [],
        driver_id:id,
        allCustomers: [],
        customers:'All Customers',
      });
    }else{
      id = this.state.driver_id;
      this.setState({
        check: false,
        jobs: [],
        customers:'All Customers',
      });
    }
    this.props.getCustomerList();
    this.props.getDriverDetails('/driv/driv-order-detail/?dri_id='+id);
  }
  changePath = (e, url, fetchUrl = null) =>{
    //this.props.getAllDrivers(fetchUrl);
    let path = this.state.path;
    path[1].path = e;
    path[1].url = url;
    this.setState({path:path});
  }
  shortName = (name) =>{
  if(name){
    let main = '';
    let first = '';
    //let last = '';
    if(name.indexOf(' ')>0){
      main = name.split(' ');
      first = main[0].split('');
    }else{
      first = name.split('');
    }
    if(main[1]){
    //  last = main[1].split('');
    //  last = last[0].toUpperCase()
    }
    return (
      <span className="bg-info text-center" style={{"margin":"0",
      "borderRadius": "50%",
      "fontWeight": "600",
      "padding":"18px",
      "paddingTop":"10px",
      "paddingBottom":"10px",
        "color":"#fff",
        "fontSize":"22px"}}>{first[0].toUpperCase()}</span>
      )
    }else{
      return(
        ''
      )
    }
  }

  handleCheckbox = (data)=>{
    this.setState({
      job_id:data
    });
  }

  handleDelete = async ()=>{
    if(this.state.job_id.length>0){
      this.setState({
        jobs: [],
        check: false,
      });
      let jid = [];
      this.state.job_id.map((e, i)=>{
        jid.push(e.id);
        return(
          ''
        )
      });
      let obj ={
        orderlist: jid
      };
      let data = await PostFetch('carr/order_delete/', obj, null, 'PUT');
      if(data && data.message){
        successAlert(data.message);
        this.setState({
          check: false,
        });
        this.props.getAllDrivers('driv/driver-list-status/?status=all');
      }else{
        errorAlert('try again!')
      }
      this.props.getDriverDetails('/driv/driv-order-detail/?dri_id='+this.state.driver_id);
    }else{
      errorAlert('Please select orders!')
    }
  }
  changeData = ()=>{
    if(this.state.job_id.length>0){
      let jid = [];
      this.state.job_id.map((e, i)=>{
        jid.push(e.id);
        return(
          ''
        )
      });
      let obj ={
        orderlist: jid
      };
      return obj;
    }else{
      let obj ={
        orderlist: []
      };
      return obj;
    }
  }
  handleDownload = ()=>{
    let payload = this.changeData();
    if(payload.orderlist && payload.orderlist.length>0){
      let carrier = JSON.parse(localStorage.getItem("userDetails"));
      if(carrier && carrier.carr_com_id){
        payload = 'orders/carr_order_csv_download/?orderlist='+JSON.stringify(payload.orderlist)+'&carr_com_id='+carrier.carr_com_id;
        DownloadExcel(payload)
      }
    }else{
      errorAlert('Please select orders!')
    }
  }
  componentWillReceiveProps(nextProps) {
    let jobs = [];
    let total = 0;
    let pending = 0;
    let inprocess = 0;
    let delivered = 0;
    let cancelled = 0;
    if(nextProps.driverDetails && nextProps.driverDetails.order_list){
      let path = this.state.path;
      path[1].path = nextProps.driverDetails.driver_detail[0].name;
      path[1].url = "/driverdetails/"+this.state.driver_id;
      this.setState({
        path: path
      })
      nextProps.driverDetails.order_list.map(d =>{
        total++;
        let status = d.OrderID.StatusID.status_name;
        let bgColor = "";
        if(status==="unassigned"){
          bgColor = "status statusUnassigned";
        }else if(status==="pending"){
          pending++;
          bgColor = "status statusPending";
        }else if(status==="in process"){
          inprocess++;
          status = "in-progress";
          bgColor = "status statusProgress";
        }else if(status==="delivered"){
          delivered++;
          bgColor = "status statusDelivered";
        }else if(status==="cancelled"){
          cancelled++;
          bgColor = "status statusCancelled";
        }
         if(!d.OrderID.is_active && d.OrderID.Created_by_CarrierID && parseInt(d.OrderID.Created_by_CarrierID)){
          status = "draft";
          bgColor = "status statusUnassigned";
        }
        jobs.push({
          id: d.OrderID.id,
          job_id: d.OrderID.job_id,
          order_create_time: d.OrderID.order_create_time,
          name: d.OrderID.CustomerCompanyID.name,
          pickup_date_time: formateDate(d.pickup_date_time[0]),
          delivery_date_time: formateDate(d.delivery_date_time[0]),
          pickup_date_time_to: formateDate(d.pickup_date_time_to[0]),
          delivery_date_time_to: formateDate(d.delivery_date_time_to[0]),
          dot:'',
          status: status,
          bgColor: bgColor
        })
        return(
          ''
        )
      });
      this.setState({
        check: true,
        jobs: jobs,
        allCustomers: nextProps.customerList,
        total: total,
        pending: pending,
        inprocess: inprocess,
        delivered: delivered,
        cancelled: cancelled,
      });
    }else{
      this.setState({
        check: true,
        jobs: [],
        allCustomers: nextProps.customerList,
        total: total,
        pending: pending,
        inprocess: inprocess,
        delivered: delivered,
        cancelled: cancelled,
      });
    }
  }
  searchJobs= async (text)=>{
    this.setState({
      check: false,
      jobs: [],
    })
    let json = await GetFetch('carr/search_in_driver/?search='+text+'&driver_id='+this.state.driver_id);
    if(json && json.length>0){
      let jobs = [];
      json.map(d=>{
        let status = d.StatusID.status_name;
        let bgColor = "";
        if(status==="unassigned"){
          bgColor = "status statusUnassigned";
        }else if(status==="pending"){
          bgColor = "status statusPending";
        }else if(status==="in process"){
          bgColor = "status statusProgress";
        }else if(status==="delivered"){
          bgColor = "status statusDelivered";
        }else if(status==="cancelled"){
          bgColor = "status statusCancelled";
        }else if(status==="draft"){
          bgColor = "status statusUnassigned";
        }
        jobs.push({
          id: d.id,
          job_id: d.job_id,
          order_create_time: formateDate(d.order_create_time),
          name: d.CustomerCompanyID.name,
          pickup_date_time: formateDate(d.pickup_date_time[0]),
          delivery_date_time: formateDate(d.delivery_date_time[0]),
          pickup_date_time_to: formateDate(d.pickup_date_time_to[0]),
          delivery_date_time_to: formateDate(d.delivery_date_time_to[0]),
          dot:'',
          status: d.StatusID.status_name,
          bgColor: bgColor
        })
        return(
          ''
        )
      });
      this.setState({
        check: true,
        jobs: jobs,
      });
    }
  }
  render(){

  return(
    <article>
        <div className="dashboardMain">
            <Header/>
            <div className="dashboardContent">
                <MainSidebar/>
                <div className="rightColumn">
                  <DriverSidebar driversCount={this.props.driversCount}/>
                    <div className="rightColumnContent">
                        <Path path={this.state.path} />
                        <div className="rowOrderDetail">
                            <div className="columnJobno">
                                <h6 className="nameDriver">Driver Name <i className="fas fa-sort-down"></i></h6>
                                <ul className="listJobNo listDrivername">
                                {this.props.allDrivers && this.props.allDrivers.data && this.props.allDrivers.data.driver_list && this.props.allDrivers.data.driver_list.length>0?
                                   this.props.allDrivers.data.driver_list.map(d=>(
                                  <li key={d.DriverID.id}>
                                    <a href="#driver" className={parseInt(d.DriverID.id)===parseInt(this.state.driver_id)? 'active': ''} onClick={(e)=>{
                                      e.preventDefault();
                                      window.history.pushState('', 'id', `/driverdetails/${d.DriverID.id}`);
                                      this.setState({
                                        check: false
                                      });
                                      this.changeDriver(d.DriverID.id);
                                    }} >
                                    {d.DriverID.image!==null?
                                      <img className="imgUser" src={d.DriverID.image} alt="" title=""/>:
                                      <div className="text-center">
                                      {this.shortName(d.DriverID.name)}
                                      </div>
                                    }
                                  </a></li>
                                )):null}
                                </ul>
                            </div>
                            <div className="contentOrderDetail p-0 boxDriverDetail">
                                    <div className="contentdriverDetail">
                                        <h3>Driver Details</h3>
                                        {this.props.driverDetails && this.props.driverDetails.driver_detail?
                                          <DispDetails
                                            id={this.state.driver_id}
                                            driverDetails={this.props.driverDetails.driver_detail}
                                          />
                                          :null}
                                    </div>
                                    <div className="contentOrderDetails">
                                        <div className="row align-items-center">
                                            <div className="col-md-6">
                                                <h3 className="mb-0">Order Details</h3>
                                            </div>
                                            <div className="col-md-6 ">
                                                <div className="rowTotalOrder">
                                                    <div className="boxtotalOrders">
                                                                 {this.state.total}
                                                        <span>Total Orders</span>
                                                    </div>
                                                    <div>
                                                        <ul className="listOrderStatus">
                                                            <li>Pending <span>{this.state.pending}</span></li>
                                                            <li>In-Progress <span>{this.state.inprocess}</span></li>
                                                            <li>Delivered <span>{this.state.delivered}</span></li>
                                                            <li>Cancelled <span>{this.state.cancelled}</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div className="tableHeader">
                                            <div className="row align-items-center">
                                                <div className="col-12 col-sm-8 col-md-8">
                                                    <ul className="listFilter">

                                                    <li><a href="#delete" onClick={(e)=>{
                                                      e.preventDefault();
                                                      this.handleDelete()
                                                    }} className="iconDelete">Delete</a></li>

                                                    <li><a href="#download" onClick={(e)=>{
                                                      e.preventDefault();
                                                      if(this.state.job_id.length>0){
                                                        this.handleDownload()
                                                      }else{
                                                        infoAlert('Please select order')
                                                      }
                                                    }} className="iconDownload">Download</a></li>
                                                      <li>
                                                      <select onChange={(e)=>{
                                                        this.setState({customers:e.target.value});
                                                        if(e.target.value!=="All Customers"){
                                                          this.searchJobs(e.target.value);
                                                        }else{
                                                          this.changeDriver();
                                                        }
                                                      }} value={this.state.customers} className="form-control selectCustomers">
                                                          <option>All Customers</option>
                                                          {this.state.allCustomers.length>0?
                                                          this.state.allCustomers.map((e, i)=>(
                                                            <option key={i}>{e.CompanyID.name}</option>
                                                          )): null}
                                                      </select>
                                                      </li>
                                                    </ul>
                                                </div>
                                                <div className="col-12 col-sm-4 col-md-4">
                                                    <div className="boxSearch">
                                                        <input type="text" className="form-control" onChange={(e)=>{
                                                          this.setState({search:e.target.value});
                                                          if(e.target.value){
                                                            this.searchJobs(e.target.value);
                                                          }else{
                                                            this.changeDriver();
                                                          }
                                                        }} placeholder="Search" />
                                                        <button className="btnSearch"><i className="fas fa-search"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {this.state.jobs.length>0 && this.state.check===true?
                                          <Datatable returnFunc={this.handleCheckbox} data={this.state.jobs} columns={this.state.columns}  />:
                                          <p>no records to display</p>}
                                          </div>
                                          {this.state.invite?<InviteDriver close={(e)=>{
                                            this.setState({
                                              invite:false
                                            });
                                          }} />: null}
                    </div>
                  </div>
                </div>
              </div>
              </div>
    </article>
  )
}
}

const DispDetails = (props)=>{
  if(props.driverDetails.length && props.id){
    let driver = props.driverDetails[0];
      return(
        <div className="row align-items-center">
            <div className="col-12 col-sm-5 col-md-5">
                <div className="rowUser">
                    <div>
                      <img className="imgUser" src={driver.image? driver.image : '/assets/img/usergray.jpg'} alt="" title=""/>
                    </div>
                    <div>
                        <h3 className="mb-0">
                        {driver.name}
                        <NavLink to={`/driverprofile/${driver.name}/${props.id}`}> <i style={{"fontSize":"14px", "color": "darkgrey"}} className="fas fa-pencil-alt"></i></NavLink>
                        </h3>
                        <p className="font-medium mb-0">Location <span className="textGray">{driver.address}</span></p>
                    </div>
                </div>
            </div>
            <div className="col-12 col-sm-7 col-md-7 boxInfo">
                <h5 className="headingBorderSmall">Info</h5>
                <div className="row">
                    <div className="col-md-6">
                        <div className="row">
                          <div className="col-md-6">License Number</div>
                          <div className="col-md-6 textGray">{driver.license_num}</div>
                        </div>
                        <div className="row">
                          <div className="col-md-6">Phone Number</div>
                          <div className="col-md-6 textGray">{driver.mobile_num}</div>
                        </div>
                    </div>
                    <div className="col-md-6">

                        <div className="row">

                        </div>
                    </div>
                </div>
            </div>
        </div>
      )
  }else{
    return (
      'not loaded'
    )
  }
}

DriverDetails.propTypes = {
  getDriverDetails: PropTypes.func.isRequired,
  getAllDrivers: PropTypes.func.isRequired,
  getDriversCount: PropTypes.func.isRequired,
  getCustomerList: PropTypes.func.isRequired,
//  driversCount: PropTypes.array.isRequired,
//  allDrivers: PropTypes.array.isRequired,
//  driverDetails: PropTypes.object.isRequired,
}

const mapStateToProps = state =>({
  driverDetails: state.carrier.driverDetails,
  allDrivers: state.carrier.allDrivers,
  driversCount: state.carrier.driversCount,
  customerList: state.carrier.customerList,
});

export default connect(mapStateToProps, {getAllDrivers, getDriverDetails, getDriversCount, getCustomerList})(DriverDetails);

//delete
/**/
