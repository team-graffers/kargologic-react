import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import SettingSidebar from './include/settingSidebar';
import Path from './include/path';
import {successAlert, errorAlert} from './include/alert';
import axios from 'axios';
import store from '../store';
import Datatable from './subComponent/datatable';
import AddAssets from './subComponent/addAssets';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getAssets, editAssetSet, clearEdit} from '../actions/carrierActions';
import {formateDate5} from './include/date';

import UploadExcelASR from './subComponent/uploadExcelASR';
import {DownloadExcel} from './ajax/downloadExcel';
import PostFetch from './ajax/postFetch';

class AssetsManager extends Component{
  constructor(props){
    super(props);
    this.state = {
      file: '',
      assets: [],
      check: false,
      addAssetModel: false,
      excelModel: false,
      columns: [
        {
          name: 'Asset Id',
          selector: 'asset_id',
          sortable: true,
        },
        {
          name: 'Asset Type',
          selector: 'asset_type',
          sortable: true,
        },
        {
          name: 'Make',
          selector: 'make',
          sortable: true,
        },
        {
          name: 'Model',
          selector: 'model',
          sortable: true,
        },
        {
          name: 'Configuration',
          selector: 'configuration',
          sortable: true,
        },
        {
          name: 'VIN/Chassis',
          selector: 'vin_chassis',
          sortable: true,
        },
        {
          name: 'Engine No',
          selector: 'engine_no',
          sortable: true,
        },
        {
          name: 'Build Date',
          selector: 'build_date',
          sortable: true,
          cell: row => <span style={{"zIndex":"999", "position":"relative"}}>{formateDate5(row.build_date)!=='Invalid date undefined'? formateDate5(row.build_date): ''}</span>,
        },
        {
          name: 'E-Tag Id',
          selector: 'e_tag_id',
          sortable: true,
        },
        {
          name: 'GPS Id',
          selector: 'gps_id',
          sortable: true,
        },
        {
          name: 'Dangerous Good Compliance',
          selector: 'dangerous_good_compliance',
          sortable: true,
        },
        {
          name: 'Dangerous Good License No',
          selector: 'dangerous_good_license_no',
          sortable: true,
        },
        {
          name: 'Registeration No',
          selector: 'registeration_no',
          sortable: true,
        },
        {
          name: 'Registeration Due Date',
          selector: 'registeration_due_date',
          sortable: true,
          cell: row => <span style={{"zIndex":"999", "position":"relative"}}>{formateDate5(row.registeration_due_date)!=='Invalid date undefined'? formateDate5(row.registeration_due_date): ''}</span>,
        },
        {
          name: 'State',
          selector: 'state',
          sortable: true,
        },
        {
          name: 'Asset Status',
          selector: 'asset_status',
          sortable: true,
        },
        {
          name: 'ODOMeter',
          selector: 'odometer',
          sortable: true,
        },
        {
          name: 'Maintenance Intervals',
          selector: 'maintenance_intervals',
          sortable: true,
        },
        {
          name: 'Maintenance Type',
          selector: 'maintenance_type',
          sortable: true,
        },
        {
          name: 'Asset Capacity',
          selector: 'asset_capacity',
          sortable: true,
        },
        {
          name: 'Gross Weight',
          selector: 'gross_weight',
          sortable: true,
        },
        {
          name: 'Tare Weight',
          selector: 'tare_weight',
          sortable: true,
        },
        {
          name: 'Purchase Cost',
          selector: 'purchase_cost',
          sortable: true,
        },
        {
          name: 'Ownership Type',
          selector: 'ownership_type',
          sortable: true,
        },
        {
          name: 'Financed Term',
          selector: 'financed_term',
          sortable: true,
        },
        {
          name: 'Hourly Cost',
          selector: 'hourly_cost',
          sortable: true,
        },

        {
          name: 'Attached',
          selector: 'is_attach',
          sortable: true,
          cell: row => <span style={{"zIndex":"999", "position":"absolute"}}>{row.is_attach? 'Yes': 'No'}</span>,
        },
        {
          name: 'Edit',
          selector: 'id',
          cell: row => <div style={{"zIndex":"999", "position":"absolute"}}><a href="#a" onClick={(e)=>{
            e.preventDefault();
            this.props.editAssetSet(row);
            this.openOneModel();
          }}>Edit</a></div>,
        }
      ]
    }

  }
  openOneModel=()=>{
    this.setState({
      addAssetModel: true
    })
  }
  closeOneModel=()=>{
    this.props.clearEdit();
    this.setState({
      addAssetModel: false
    })
  }
  openExcelModel = ()=>{
    this.setState({
      excelModel: true
    })
  }
  closeExcelModel = ()=>{
    this.refreshData();
    this.setState({
      excelModel: false
    })
  }
  submitForm = (e) =>{
    if(this.state.file){
      const data = new FormData();
      data.append('data_file', this.state.file);
      const state = store.getState();
      const headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token '+localStorage.getItem("token")
      }
      axios.post(state.carrier.mainUrl+'asset/uploadasset/', data, {headers: headers})
      .then(res => {
         if(res.data && res.data.message && res.data.message==="success"){
           successAlert('File Uploaded');
         }else {
           errorAlert('try again')
         }
       });
    }
  }

  componentWillMount(){
    this.props.getAssets('asset/assetmanagerlist/');
  }
  refreshData = ()=>{
    this.setState({
      assets: [],
      check: false
    })
    this.props.getAssets('asset/assetmanagerlist/');
  }
  handleCheckbox = (data)=>{
    this.setState({
      temp:data
    });
  }
  changeData = ()=>{
    if(this.state.temp && this.state.temp.length>0){
      let jid = [];
      this.state.temp.map((e, i)=>{
        jid.push(e.id);
        return('')
      });
      return jid;
    }else{
      return [];
    }
  }
  handleDelete = async ()=>{
    const ids = this.changeData();
    const payload = {
      asset_list: ids
    }
    let data = await PostFetch('asset/delete_asset/', payload);
    if(data && data.message){
      successAlert(data.message)
      this.refreshData();
    }else {
      errorAlert('try again')
    }
  }
  handleDownload = ()=>{
    let payload = this.changeData();
    let carrier = JSON.parse(localStorage.getItem("userDetails"));
    payload = 'asset/download_asset/?asset_list='+JSON.stringify(payload)+'&carr_com_id='+carrier.carr_com_id;
    DownloadExcel(payload)
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.assetsList && nextProps.assetsList.data && nextProps.assetsList.data.length>0){
      this.setState({
        assets: nextProps.assetsList.data,
        check: true
      })
    }
  }

  render(){
    return(
      <article>
        {this.state.excelModel?
          <UploadExcelASR
            name= 'Upload Assets'
            fetchUrl= 'asset/uploadasset/'
            sampleUrl='/asset/sampleasset/'
            refreshData={this.refreshData}
            closeModel={this.closeExcelModel}
          />
        :null}
          <div className="dashboardMain">
            <Header/>
              <div className="dashboardContent">
                <MainSidebar/>
                  <div className="rightColumn">
                    <SettingSidebar/>
                    <div className="rightColumnContent">
                    <Path path={[
                                  {
                                    "path": "Manage your account",
                                    "url": "/settings"
                                  },
                                  {
                                    "path": "Assets",
                                    "url": "/assetsmanager"
                                  }
                                ]} />
                          <div className="tableHeader">
                              <div className="row align-items-center">
                                  <div className="col-12 col-sm-8 col-md-8">
                                    <ul className="listFilter">
                                      <li><a href="#a" onClick={(e)=>{
                                        e.preventDefault();
                                        this.handleDelete();
                                      }}>Delete</a></li>
                                      <li><a href="#a" onClick={(e)=>{
                                        e.preventDefault();
                                        this.handleDownload();
                                      }}>Download</a></li>
                                      <li><a href="#uploadexcelasr" onClick={(e)=>{
                                        e.preventDefault();
                                        this.openExcelModel();
                                      }}>Upload</a></li>
                                      <li><a href="#new" onClick={(e)=>{
                                        e.preventDefault();
                                        this.openOneModel();
                                      }}>Add New Asset</a></li>
                                    </ul>
                                  </div>

                              </div>
                          </div>
                          <div style={{width: 'auto', overflow: 'auto'}}>
                            {this.state.assets.length>0 && this.state.check===true?
                              <Datatable returnFunc={this.handleCheckbox} data={this.state.assets} columns={this.state.columns}  />:
                            <p>no records to display</p>}
                          </div>
                  </div>
              </div>
          </div>
        </div>
          {this.state.addAssetModel?
            <AddAssets
              closeOneModel={this.closeOneModel}
              refreshData={this.refreshData}
            />
          :null}
      </article>
    )
  }
}

AssetsManager.propTypes = {
  editAssetSet: PropTypes.func.isRequired,
  getAssets: PropTypes.func.isRequired,
  clearEdit: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  editAsset: state.carrier.editAsset,
  assetsList: state.carrier.assetsList
});

export default connect(mapStateToProps, {getAssets, editAssetSet, clearEdit})(AssetsManager);
