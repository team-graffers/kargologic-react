import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import DriverSidebar from './include/driverSidebar';
import Path from './include/path';
import addUser from '../assets/img/iconaddUser.svg';
import InviteDriver from './subComponent/inviteDriver';
import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getAllDrivers, searchDrivers, getDriversCount, updateEditOrder, clearOrder} from '../actions/carrierActions';
import Datatable from './subComponent/datatable'
import DownloadCSV from './ajax/downloadCSV';
import PostFetch from './ajax/postFetch';
import {successAlert, errorAlert, infoAlert} from './include/alert';
import DriversMap from './subComponent/driversMap';
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

class Drivers extends Component{
  constructor(props){
    super(props);
    let url = window.location.href;
    url = url.split('/drivers/');
    this.state = {
      driver_id:[],
      search:'',
      status: url[1],
      check:false,
      drivers:[],
      tracking: [],
      driversList: [],
      showMap: false,
      driverInviteModel: false,
      path: [
              {
                "path": "Drivers",
                "url": "/drivers/all"
              },
              {
                "path": "All",
                "url": "/drivers/all"
              }
            ],
      columns:[
        {
          name: 'Driver Name',
          selector: 'name',
          sortable: true,
          cell: row => <span><NavLink style={{"zIndex":"999", "position":"relative"}} to={`/driverdetails/${row.id}`}>{row.name}</NavLink><br/>{row.address}</span>,
        },
        {
          name: 'Vehicle Rego',
          selector: 'reg_num',
          sortable: true,
        },
        {
          name: 'Driver License No.',
          selector: 'license_num',
          sortable: true,
        },
        {
          name: 'Phone Number',
          selector: 'mobile_num',
          sortable: true,
        },
        {
          name: 'Delivery On Time',
          selector: 'dot',
          cell: row => <div ><CircularProgressbar value={parseFloat(row.dot).toFixed(1)} text={`${parseFloat(row.dot).toFixed(1)}%`} /> </div>,
        },
        {
          name: 'Status',
          selector: 'status',
          sortable: true,
          cell: row => <span className={row.bgColor}>{row.status}</span>,
        }
      ]
    }
    this.handleCheckbox = this.handleCheckbox.bind(this);
  }
  setDriversList = async ()=>{
    if(this.state.driver_id.length>0){
      let driverid = [];
//      let track = [];
      this.state.driver_id.map((e, i)=>{
        driverid.push(e.id)
        return('')
      });
      if(driverid && driverid.length>0){
          this.setState({
            tracking: driverid,
            showMap: true
          })
      }
    }else {
      errorAlert('Please select a record')
    }
  }
  componentWillMount(){
    this.props.updateEditOrder(false);
    this.props.clearOrder();
    if(this.state.status==="all"){
      this.changePath('All Drivers', '/drivers/all', 'all');
    }else if(this.state.status==="added"){
      this.changePath('Added Drivers', '/drivers/added', 'driver added');
    }else if(this.state.status==="pending"){
      this.changePath('Pending Drivers', '/drivers/pending', 'driver pending');
    }
    this.props.getDriversCount('carr/show_count_driver/');

  //  this.trackDrivers([]);
  }
  changePath = (e, url, fetchUrl = null) =>{
    this.props.getAllDrivers('driv/driver-list-status/?status='+fetchUrl);
    let path = this.state.path;
    path[1].path = e;
    path[1].url = url;
    this.setState({
      path: path,
      check: false,
    });
  }

  handleCheckbox = (data) =>{
    this.setState({
      driver_id:data
    });
  }
  changeData = ()=>{
    if(this.state.driver_id.length>0){
      let jid = [];
      this.state.driver_id.map((e, i)=>{
        jid.push(e.id);
        return(
          ''
        )
      });
      let obj ={
        driverlist: jid
      };
      return obj;
    }else{
      let obj ={
        driverlist: []
      };
      return obj;
    }
  }

  handleDelete = async ()=>{
    let payload = this.changeData();
    if(payload.driverlist.length>0){
      let data = await PostFetch('carr/driver_delete/', payload, null, 'PUT');
      if(data && data.message){
        successAlert(data.message);
        this.setState({
          check: false,
        });
        this.props.getAllDrivers('driv/driver-list-status/?status=all');
      }else{
        errorAlert('try again!')
      }
    }else{
      errorAlert('Please select a record')
    }
  }

  handleDownload = async ()=>{
    let payload = this.changeData();
    await DownloadCSV('driv/drivlist_download_carr/', payload, 'driver list.csv');
  }
  componentWillReceiveProps(nextProps){
    let drivers = [];
    if(nextProps.allDrivers && nextProps.allDrivers.data && nextProps.allDrivers.data.driver_list && nextProps.allDrivers.data.driver_list.length>0){
      nextProps.allDrivers.data.driver_list.map(d =>{
        let dot = 0;
        nextProps.allDrivers.data.dot_data.map(v =>{
          if(parseInt(v.driver_id) === parseInt(d.DriverID.id)){
            dot = v.dot
          }
          return('')
        });
        let status = d.status.status_name;
        let bgColor = "";
        if(status==="driver added"){
          bgColor = "status statusDelivered";
        }else if(status==="driver pending"){
          bgColor = "status statusPending";
        }else if(status==="driver invited"){
          bgColor = "status progressUnassigned";
        }
        drivers.push({
          id: d.DriverID.id,
          name: d.DriverID.name,
          address: d.DriverID.address,
          type: d.DriverID.TruckType.truck_category.categ_name,
          reg_num: d.DriverID.TruckType.truck_reg_num,
          license_num: d.DriverID.license_num,
          mobile_num: d.DriverID.mobile_num,
          bgColor: bgColor,
          status: d.status.status_name,
          dot: dot,
          trackingId: d.DriverID.trackingId
        })
        return(
          ''
        )
      });
      this.setState({
        drivers: drivers,
        driversList: nextProps.allDrivers.data.driver_list,
        check: true,
      })
    }
  }
  openDriverModel = () =>{
    this.setState({
      driverInviteModel: true
    })
  }
  closeDriverModel = () =>{
    this.setState({
      driverInviteModel: false
    })
  }
  render(){
    return(
      <article>
        <div className="dashboardMain">
          <Header/>
          <div className="dashboardContent">
            <MainSidebar/>
              <div className="rightColumn">
                <DriverSidebar driversCount={this.props.driversCount} changePath={this.changePath}/>
                <div className="rightColumnContent">

                    <Path path={this.state.path} />
                    <div className="tableHeader">
                        <div className="row align-items-center">
                            <div className="col-12 col-sm-8 col-md-8">
                                <ul className="listFilter">
                                <li><a href="#delete" className="iconDelete" onClick={(e)=>{
                                  e.preventDefault();
                                  this.handleDelete()
                                }}><i className="far fa-trash-alt"></i> Delete</a></li>
                                <li><a href="#download" className="iconDownload" onClick={(e)=>{
                                  e.preventDefault();
                                  if(this.state.driver_id.length>0){
                                    this.handleDownload()
                                  }else{
                                    infoAlert('Please select a record')
                                  }
                                }}><i className="fas fa-download"></i> Download</a></li>
                                <li><a href="#delete" className="iconDelete" onClick={(e)=>{
                                    e.preventDefault();
                                    this.setDriversList();
                                  }}><i className="fas fa-map-marked-alt"></i> Track On Map</a></li>
                                <li>
                                  <a onClick={(e)=>{
                                    e.preventDefault();
                                    this.openDriverModel();
                                  }} href="#invitedriver" className="btn btn-md">Invite Driver
                                   <img src={addUser} alt="" title=""/>
                                  </a>
                                </li>
                                </ul>
                            </div>
                            <div className="col-12 col-sm-4 col-md-4">
                                <div className="boxSearch">
                                <input type="search" onChange={(e)=>{
                                  this.setState({
                                    search: e.target.value,
                                    check: false
                                  });
                                  if(e.target.value.length>0){
                                    this.props.searchDrivers('driv/search/?search='+e.target.value);
                                  }else {
                                    this.props.getAllDrivers('driv/driver-list-status/?status=all');
                                  }
                                }} value={this.state.search} className="form-control" placeholder="search"/>
                                    <button className="btnSearch"><i className="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.drivers.length>0 && this.state.check===true?
                      <Datatable returnFunc={this.handleCheckbox} data={this.state.drivers} columns={this.state.columns}  />:
                      <p>no records to display</p>}
              </div>
            </div>
          </div>
        </div>
        {this.state.driverInviteModel?
          <InviteDriver closeOneModel={this.closeDriverModel} />
        :null}
          {this.state.showMap && this.state.driversList.length>0?
          <DriversMap
            driversList = {this.state.driversList}
            driver_id = {this.state.tracking}
            close={()=>{
              this.setState({
                showMap: false
              })
            }}
          />: null}
      </article>
    )
  }
}

Drivers.propTypes = {
  getAllDrivers: PropTypes.func.isRequired,
  searchDrivers: PropTypes.func.isRequired,
  getDriversCount: PropTypes.func.isRequired,
//  driversCount: PropTypes.array.isRequired,
//  allDrivers: PropTypes.array.isRequired,
  updateEditOrder: PropTypes.func.isRequired,
  clearOrder: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  allDrivers: state.carrier.allDrivers,
  driversCount: state.carrier.driversCount
});

export default connect(mapStateToProps, {getAllDrivers, searchDrivers, getDriversCount, updateEditOrder, clearOrder})(Drivers);

//delete
/**/
