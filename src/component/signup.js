import React, {Component} from 'react';
import SideImg from './include/side-img';
import {NavLink} from 'react-router-dom';
import PostFetch from './ajax/postFetch';
import {Redirect } from 'react-router';
import PasswordMask from 'react-password-mask';
import store from '../store';
import {successAlert} from './include/alert';
import {validatePassword, verifyEmail, verifyNumber} from './include/validation';

class CarrierSignup extends Component{
  constructor(props){
    super(props);
    this.state = {
      email:"",
      user_name:"",
      phone:"",
      phoneError: "",
      password:"",
      domainID: 2,
      roleID:4,
      company_name:"",
      redirect: false,
      error_email: '',
      disSubmit: true,
    }
    this.formSubmit = this.formSubmit.bind(this);
    localStorage.removeItem("token");
    localStorage.removeItem("sidebar");
    localStorage.removeItem("userDetails");
    localStorage.removeItem("userConfiguration");
  }
  formSubmit = async (e) =>{
    e.preventDefault();
    this.setState({
      disSubmit: false,
    })
    if(this.state.phoneError==='' && this.state.error_email===''){
      const payload = {
        ...this.state,
        ExtraInfo:{
          email:this.state.email,
          user_name:this.state.user_name,
          phone:this.state.phone,
          domainID: 2,
          roleID:4,
          somevalue:"main create"
        }
      }
      const headers = {
        'content-type': 'application/json',
      }
      let json = await PostFetch('carr/carr_user/create', payload, headers);
      if(json!==null){
        successAlert(json.message);
        this.setState({
          email:"",
          user_name:"",
          phone:"",
          password:"",
          company_name:'',
          disSubmit: true,
          redirect: true
        });
      }else {
        this.setState({
          disSubmit: true,
        })
      }
    }
  }

  render(){
    if(this.state.redirect){
      return(
        <Redirect to="/carrier/login" />
      )
    }
    const state = store.getState();
    return(
      <article>
          <section className="sectionColumns">
              <SideImg msg={''}/>
              <div className="rightColumn">
                  <div className="content-rightColumn">
                      <h5 className="mb-4">Sign up for your account</h5>
                      <div className="boxComman">
                          <form autoComplete="off" action="" onSubmit={this.formSubmit} method="POST">
                              <div className="form-group">
                                  <label>Name</label>
                                  <input type="text" value={this.state.user_name} onChange={(e)=>{
                                    this.setState({user_name:e.target.value})
                                  }} placeholder="enter name" required className="form-control"/>
                              </div>
                              <div className="form-group">
                                  <label>Email address</label>
                                  <input type="email" value={this.state.email} onChange={(e)=>{
                                    this.setState({email:e.target.value})
                                  }} onBlur={ async (e)=>{
                                    let data = await verifyEmail(e.target.value);
                                    if(data){
                                      if(data.error!==''){
                                        this.setState({
                                          error_email: data.error
                                        })
                                      }else if(data.data==='Customer' || data.data==='Driver'){
                                        this.setState({
                                          error_email: 'Already registered with '+data.data
                                        })
                                      }else if(data.data==='' && data.error===''){
                                        this.setState({
                                          error_email: ''
                                        })
                                      }else if(data.data==='Carrier' && data.error===''){
                                        this.setState({
                                          error_email: ''
                                        })
                                      }
                                    }
                                  }} required className="form-control" placeholder="Enter Email"/>
                                  <span className="text-danger">{this.state.error_email}</span>
                              </div>
                              <div className="form-group">
                                  <label>Company Name</label>
                                  <input type="text" value={this.state.company} onChange={(e)=>{
                                    this.setState({company_name:e.target.value})
                                  }} required className="form-control" placeholder="Enter Company Name"/>
                              </div>
                              <div className="form-group">
                                  <label>Phone Number</label>
                                  <input type="number" value={this.state.phone} onChange={(e)=>{
                                    this.setState({phone:e.target.value})
                                  }} onBlur={ async (e)=>{
                                    let json = await verifyNumber(e.target.value);
                                    this.setState({
                                      phoneError: json.error,
                                    });
                                  }} required className="form-control" placeholder="Enter Phone Number"/>
                                  <span className="text-danger">{this.state.phoneError}</span>
                              </div>
                              <div className="form-group">
                                  <label>Password</label>
                                  <PasswordMask
                                    inputClassName={"form-control"}
                                    buttonClassName={"btnEye"}
                                    className={"inputPassword"}
                                    placeholder="Enter password"
                                    value={this.state.password}
                                    onChange={(e)=>{
                                      let data = validatePassword(e.target.value);
                                      this.setState({
                                        password:e.target.value,
                                        strength: data.strength,
                                        color: data.color
                                      });
                                    }}
                                    required
                                    showButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                    hideButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                     useVendorStyles={false}
                                  />
                                  <p style={{marginTop: '5px'}}><strong className={this.state.color}>{this.state.strength}</strong></p>
                              </div>
                              <div className="row align-items-center">
                                  <div className="col-md-5">
                                  {!this.state.disSubmit?
                                    <a href="#submit" className="btn">Sign Up</a>
                                  :<input type="submit" title="Sign Up" value="Sign Up" />}
                                  </div>
                                  <div className="col-md-7 text-right">
                                      <p className="mb-0 font-weight-medium f12">By signing up, you agree to our <a href="https://kargologic.com/terms-of-use" target="_blank" rel="noopener noreferrer">T&C</a> and <a href="https://kargologic.com/privacy-policy" target="_blank" rel="noopener noreferrer" title="Privacy Policy">Privacy Policy.</a></p>
                                  </div>
                              </div>
                          </form>
                      </div>
                      <div className="boxComman p-4 text-center font-weight-medium">
                          <p className="mb-0">Already have an account? <NavLink to="/carrier/login">Login</NavLink></p>
                      </div>
                  </div>
              </div>
          </section>
      </article>
    )
  }
}

export default  CarrierSignup;
