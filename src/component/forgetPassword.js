import React, {Component} from 'react';
import SideImg from './include/side-img';
import PostFetch from './ajax/postFetch';
import {successAlert, errorAlert} from './include/alert';
import {verifyEmail} from './include/validation';

class ForgetPassword extends Component{
  constructor(props){
    super(props);
    this.state = {
      email: '',
      error_email: '',
      submitDis: true,
    }
  }
  formSubmit = async (e)=>{
    e.preventDefault();
    if(this.state.submitDis && this.state.email && this.state.error_email === ""){
      this.setState({
        submitDis: false
      })
      let payload = {
        email: this.state.email
      }
      const headers = {
        'content-type': 'application/json',
      }
      let json = await PostFetch('carr/carr_pass_forget/', payload, headers);
      if(json && json.message==="success"){
        this.setState({
          email: ''
        })
        successAlert('Password reset link sent, please check your email.')
      }else if (json && json.message==="fail") {
          errorAlert('Email does not exist')
      }else {
        errorAlert('try again!')
      }
      this.setState({
        submitDis: true
      })
    }
  }
  render(){
    return(
      <article>
        <section className="sectionColumns">
          <SideImg msg={''}/>
        <div className="rightColumn">
                  <div className="content-rightColumn">
                      <h5 className="mb-4">Password Recovery</h5>
                      <div className="boxComman">
                          <form action="" onSubmit={this.formSubmit} method="POST">
                              <div className="form-group">
                                  <label>Email Address</label>
                                  <input type="email" value={this.state.email} onChange={(e)=>{
                                    this.setState({email:e.target.value})
                                  }} onBlur={ async (e)=>{
                                    let json = await verifyEmail(e.target.value);
                                    if(json && json.error!==""){
                                      this.setState({
                                        error_email: json.error
                                      })
                                    } else if(json && json.data!=="Carrier"){
                                      this.setState({
                                        error_email: 'this email does not exist'
                                      })
                                    } else if(json && json.data==="Carrier"){
                                      this.setState({
                                        error_email: ''
                                      })
                                    }else{
                                      this.setState({
                                        error_email: 'valid email is required'
                                      })
                                    }
                                  }} placeholder="Enter email address" required className="form-control"/>
                                  <span className="text-danger">{this.state.error_email}</span>
                              </div>

                              <div className="row align-items-center">
                                  <div className="col-md-6">
                                      <input type="submit" title="Login" value="Reset Password"/>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </section>
      </article>
    )
  }
}
export default ForgetPassword;
