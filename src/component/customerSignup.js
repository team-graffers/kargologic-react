import React, {Component} from 'react';
import SideImg from './include/side-img';
import PostFetch from './ajax/postFetch';
import {GetFetch2} from './ajax/ajaxFetch';
import {Redirect } from 'react-router';
import store from '../store';
import {successAlert, errorAlert} from './include/alert';
import APIFetch from './ajax/apiFetch';
import {validatePassword} from './include/validation';

class CustomerSignup extends Component{
  constructor(props){
    super(props);
    this.state = {
      user_name: '',
      phone: '',
      email:'',
      pin: '',
      country: '',
      carr_com_id: '',
      password: '',
      business_name: '',
      secondary_contact: '',
      sec_email_id: '',
      sec_phone: '',
      business_address: '',
      contract_start_date: '',
      contract_end_date: '',
      Customer_code: '',
      attach: [],
      strength: '',
      color: '',
      matchColor: '',
      matchText: '',

      carr_com_name: '',

      error_user_name: '',
      error_phone: '',
      error_email:'',
      error_pin: '',
      error_country: '',
      error_password: '',
      error_business_name: '',
      error_business_address: '',
      redirect: false,
      disSubmit: true,
      appdata: '',

    }
    this.formSubmit = this.formSubmit.bind(this);
    localStorage.removeItem("token");
    localStorage.removeItem("sidebar");
    localStorage.removeItem("userDetails");
    localStorage.removeItem("userConfiguration");
  }
  parseQueryString = function() {
    let str = window.location.search;
    let objURL = {};
    str.replace(
        new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
        function( $0, $1, $2, $3 ){
            objURL[ $1 ] = $3;
        }
    );
    return objURL;
  }
  componentWillMount(){
    const state = store.getState();
    this.setState({
      appdata: state.carrier.appdata
    })
    let params = this.parseQueryString();
    if(params && params.email && params.carr_com_id && params.carr_com_name && params.cust_extra_id && params.cust_com_id){
      console.log(params);
      this.getCustomerDetails(params.cust_extra_id, params.cust_com_id);
      //
      let name = params.carr_com_name;
      if(name === 'None'){
        name = ''
      }else{
        name = name.toUpperCase();
        if(name.search('%20')){
          let nameArr = name.split('%20');
          let nameSt = '';
          if(nameArr && nameArr.length>0){
            nameArr.map((e, i)=>{
              if(i===0){
                  nameSt = e
              }else {
                  nameSt += ' '+e
              }
              return('')
            })
            name = nameSt
          }
        }
      }
      let id = params.carr_com_id;
      if(id==="None"){
        id = '';
      }
      this.setState({
        carr_com_id: id,
        email: params.email,
        carr_com_name: name
      });
    }else{
      this.setState({
        redirect: true
      });
    }
  }
  getSuggestions = async (text)=>{
    const data = await APIFetch('https://autocomplete.geocoder.api.here.com/6.2/suggest.json?'+this.state.appdata+'&query='+text+'&country=aus');
    let options = [];
    if(data.suggestions){
      data.suggestions.map((s,i)=>{
       options.push({
         value:s.label,
         label:s.label,
       })
       return('')
     });
    }
    this.setState({
      data:options
    });
  }

  changeCountry = async (pin) =>{
      if(this.state.business_address){
        let url = 'https://geocoder.api.here.com/6.2/geocode.json?searchtext='+this.state.business_address+'&'+this.state.appdata;
        const data = await APIFetch(url);
        if(data && data.Response && data.Response.View[0] && data.Response.View[0].Result[0] && data.Response.View[0].Result[0].Location){
          let pos = data.Response.View[0].Result[0].Location;
          this.setState({
            pin:pos.Address.PostalCode,
            country:pos.Address.Country,
          });
        }
      }
  }
  getCustomerDetails = async(cust_extra_id, cust_com_id)=>{
    const data = await GetFetch2(`cust/sign_prepo/?cust_extra_id=${cust_extra_id}&cust_com_id=${cust_com_id}`);
    if(data && data.cust_obj && data.cust_obj[0] && data.com_obj && data.com_obj[0]){
      console.log(data);
      const customerData = data.cust_obj[0];
      const companyData = data.com_obj[0];
      this.setState({
        user_name: customerData.name,
        phone: customerData.mobile_num,
        email: companyData.email,
        pin: companyData.pin,
        country: companyData.country,
        business_name: customerData.business_name,
        secondary_contact: customerData.secondary_contact,
        sec_email_id: customerData.sec_email_id,
        sec_phone: customerData.sec_phone,
        business_address: customerData.business_address,
        contract_start_date: customerData.contract_start_date,
        contract_end_date: customerData.contract_end_date,
        Customer_code: customerData.Customer_code,
        attach: [],
      })
    }
  }
  formSubmit = async (e) =>{
    e.preventDefault();
    this.setState({
      disSubmit: false,
    })
    const payload ={
      user_name: this.state.user_name,
      phone: this.state.phone,
      email:this.state.email,
      pin: this.state.pin,
      country: this.state.country,
      carr_com_id: this.state.carr_com_id,
      password: this.state.password,
      extra_details: {
        business_name: this.state.business_name,
        secondary_contact: this.state.secondary_contact,
        sec_email_id: this.state.sec_email_id,
    		sec_phone: this.state.sec_phone,
        business_address: this.state.business_address,
        contract_start_date: this.state.contract_start_date,
    		contract_end_date: this.state.contract_end_date,
    		Customer_code: this.state.Customer_code,
      },
      attach: this.state.attach
    }
//    console.log(payload);
    const headers = {
      'content-type': 'application/json',
    }
    let json = await PostFetch('cust/customer_create', payload, headers);
    if(json && json.message && json.message !== "Exception"){
      successAlert(json.message);
      this.setState({
          user_name: '',
          phone: '',
          email:'',
          pin: '',
          country: '',
          carr_com_id: '',
          password: '',
          business_name: '',
          secondary_contact: '',
          sec_email_id: '',
          sec_phone: '',
          business_address: '',
          contract_start_date: '',
          contract_end_date: '',
          Customer_code: '',

          carr_com_name: '',

          error_user_name: '',
          error_phone: '',
          error_email:'',
          error_pin: '',
          error_country: '',
          error_password: '',
          error_business_name: '',
          error_business_address: '',
          redirect: true,
          disSubmit: true,
        });
    }else if(json && json.message){
      this.setState({
        disSubmit: true,
      })
      errorAlert('try again!')
    }else {
      this.setState({
        disSubmit: true,
      })
      errorAlert('try again!')
    }

  }
  getFiles = (files)=>{
    this.setState({
      attach: [...this.state.attach, files],
    })
  }
  getBase64 = (files, getFiles)=>{
    Array.from(files).map((file, i)=>{
     var reader = new FileReader();
     reader.readAsDataURL(file);
     reader.onload = function () {
       let f = {
         file_name: file.name,
         file_data: reader.result
       }
       getFiles(f)
     };
     reader.onerror = function (error) {
       console.log('Error: ', error);
     };
     return('')
   })
  }
  render(){
    if(this.state.redirect){
      return(
        <Redirect to="/customer/login"/>
      )
    }
  //  const state = store.getState();
    return(
      <article>
          <section className="sectionColumns">
              <SideImg  msg={this.state.carr_com_name}/>
              <div className="rightColumn">
                  <div className="content-rightColumn">
                      <h5 className="mb-4">Sign up for your account</h5>
                      <div className="boxComman">
                          <form action="" onSubmit={this.formSubmit} method="POST">
                          <div className="form-group">
                              <label>Password <span className="text-danger">*</span></label>
                              <input type="password" onChange={(e)=>{
                                let data = validatePassword(e.target.value);
                                this.setState({
                                  password:e.target.value,
                                  strength: data.strength,
                                  color: data.color
                                });
                              }} required className="form-control" value={this.state.password} placeholder="Enter password" />
                              <p style={{marginTop: '5px'}}><strong className={this.state.color}>{this.state.strength}</strong></p>
                          </div>
                          <div className="form-group">
                              <label>Confirm Password <span className="text-danger">*</span></label>
                              <input type="password" onChange={(e)=>{
                                if(this.state.password === e.target.value){
                                  this.setState({
                                    matchText: 'Matched',
                                    matchColor: 'text-success',
                                    disSubmit: true
                                  })
                                }else {
                                  this.setState({
                                    matchText: 'Not Matched',
                                    matchColor: 'text-danger',
                                    disSubmit: false
                                  })
                                }
                                this.setState({
                                  conPassword:e.target.value,
                                });
                              }} required className="form-control" value={this.state.conPassword} placeholder="Re-enter password" />
                              <p style={{marginTop: '5px'}}><strong className={this.state.matchColor}>{this.state.matchText}</strong></p>
                          </div>
                              <div className="row align-items-center">
                                  <div className="col-md-5">
                                  {!this.state.disSubmit?
                                    <a href="#submit" className="btn">Sign Up</a>
                                  :<input type="submit" title="Sign Up" value="Sign Up" />}
                                  </div>
                                  <div className="col-md-7 text-right">
                                      <p className="mb-0 font-weight-medium f12">By signing up, you agree to our <a href="https://kargologic.com/terms-of-use" target="_blank" rel="noopener noreferrer">T&C</a> and <a href="https://kargologic.com/privacy-policy" target="_blank" rel="noopener noreferrer" title="Privacy Policy">Privacy Policy.</a></p>
                                  </div>
                              </div>
                          </form>
                      </div>

                  </div>
              </div>
          </section>
      </article>
    )
  }
}
export default CustomerSignup;
