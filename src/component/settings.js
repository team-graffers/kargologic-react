import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import SettingSidebar from './include/settingSidebar';
import Path from './include/path';
import {successAlert, errorAlert} from './include/alert';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {updateEditOrder, clearOrder} from '../actions/carrierActions';
import APIFetch from './ajax/apiFetch';
import axios from 'axios';
import store from '../store';
import {verifyEmail, verifyNumber} from './include/validation';

class Settings extends Component{
  constructor(props){
    super(props);
    this.state = {
      company_name:'',
      name:'',
      address_1:'',
      address_2:'',
      post_code:'',
      country:'',
      phone:'',
      res_phone: '',
      email:'',
      res_email:'',
      lat:'',
      lang:'',
      data:[],
      dp: '',
      check_phone: '',
      check_phone_color: '',
      check_email: '',
      check_email_color: '',
      appdata: '',
    }
    this.submitForm = this.submitForm.bind(this);
    this.changeCountry = this.changeCountry.bind(this);
    this.validateEmail = this.validateEmail.bind(this);
  }
  componentWillMount(){
    this.props.updateEditOrder(false);
    this.props.clearOrder();
    const state = store.getState();
    this.setState({
      appdata: state.carrier.appdata
    })
    let userDetails = localStorage.getItem('userDetails');
    if(userDetails){
      userDetails = JSON.parse(userDetails);
      const state = store.getState();
      this.setState({
        company_name:userDetails.carrier_company_name,
        name:userDetails.carrier_name,
        email:userDetails.email,
        res_email:userDetails.email,
        address_1: userDetails.address1,
        address_2: userDetails.address2,
        phone:userDetails.carr_phone,
        res_phone:userDetails.carr_phone,
        post_code:userDetails.pin,
        country:userDetails.country,
        dp: state.carrier.bucketUrl+userDetails.carr_image,
        newDp: '',
      })
    }
  }
  submitForm = (e) =>{
    e.preventDefault();
    let newEmail = this.state.res_email;
    if(this.state.check_email === 'available'){
      newEmail = this.state.email
    }else {
      this.setState({
        email: newEmail,
        check_email: '',
        check_email_color: '',
      })
    }
    let newPhone = this.state.res_phone;
    if(this.state.check_phone === 'available'){
      newPhone = this.state.phone
    }else {
      this.setState({
        phone: newPhone,
        check_phone: '',
        check_phone_color: '',
      })
    }
    const payload = {
    	CompanyID:{
    		name:this.state.company_name,
    		location:this.state.address_1,
    		location2:this.state.address_1,
    		mobile_num:newPhone,
        pin:this.state.post_code,
        country:this.state.country,
    	},
    	Created_by_CarrierID:{
    		name:this.state.name,
    		mobile_num:this.state.phone,
    		email:newEmail,
    	}
    }
    const data = new FormData();
    data.append('file', this.state.newDp);
    data.append('payload', JSON.stringify(payload));
    const state = store.getState();
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Token '+localStorage.getItem("token")
    }
    axios.post(state.carrier.mainUrl+'carr/setting/', data, {headers: headers})
    .then(res => {
       if(res.data && res.data.message){
         if(res.data.image_url){
           this.setState({
             dp: res.data.image_url,
             check_phone: '',
             check_phone_color: '',
             check_email: '',
             check_email_color: '',
           })
         }
         successAlert(res.data.message);
         let userDetails = localStorage.getItem("userDetails");
         const details = {
           token: userDetails.token,
           email: this.state.email,
           carr_id: userDetails.carr_id,
           carrier_name: this.state.name,
           carr_phone: this.state.phone,
           carr_image: res.data.image_url,
           carr_com_id: userDetails.carr_com_id,
           carrier_company_name: this.state.company_name,
           address1: this.state.address_1,
           address2: this.state.address_2,
           pin: this.state.post_code,
           country: this.state.country,
           message: userDetails.message}
         localStorage.setItem("userDetails", JSON.stringify(details))
       }else {
         errorAlert('try again')
       }
     });
  }

  getSuggestions = async (text)=>{
    const data = await APIFetch('https://autocomplete.geocoder.api.here.com/6.2/suggest.json?'+this.state.appdata+'&query='+text+'&country=aus');
    let options = [];
    if(data.suggestions){
       data.suggestions.map((s,i)=>(
        options.push({
          value:s.label,
          label:s.label,
        })
      ));
    }
    this.setState({
      data:options
    });
  }

  changeCountry = async (pin) =>{
      if(this.state.address_1){
        let url = 'https://geocoder.api.here.com/6.2/geocode.json?searchtext='+this.state.address_1+'&'+this.state.appdata;
        const data = await APIFetch(url);
        if(data && data.Response && data.Response.View[0] && data.Response.View[0].Result[0] && data.Response.View[0].Result[0].Location){
          let pos = data.Response.View[0].Result[0].Location;
          this.setState({
            post_code:pos.Address.PostalCode,
            country:pos.Address.Country,
            lat:pos.DisplayPosition.Latitude,
            lang:pos.DisplayPosition.Longitude
          });
        }
      }
  }
  validatePhone = async (phone) =>{
    if(phone !== this.state.res_phone){
      let data = await verifyNumber(phone);
      if(data && data.error === ""){
        this.setState({
          check_phone: 'available',
          check_phone_color: 'text-success',
        });
      }else{
        this.setState({
          check_phone: data && data.error? data.error: 'already exist',
          check_phone_color: 'text-danger',
        });
      }
    }else {
      this.setState({
        check_phone: '',
        check_phone_color: ''
      });
    }
  }
  validateEmail = async (email) =>{
    if(email !== this.state.res_email){
      let data = await verifyEmail(email);
      if(data && !data.data && !data.error){
        this.setState({
          check_email: 'available',
          check_email_color: 'text-success',
        });
      }else{
        this.setState({
          check_email: data && data.error? data.error: 'already exist',
          check_email_color: 'text-danger',
        });
      }
    }else{
      this.setState({
        check_email: '',
        check_email_color: ''
      });
    }
  }
  changeDP = (e)=>{
    let file = e.target.files[0];
    const types = ['image/png', 'image/jpeg', 'image/gif'];
    if (file && types.every(type => file.type !== type)) {
      e.target.value = null;
      return null;
    }
    if(file){
      var reader = new FileReader();
      reader.onloadend = ()=>{
        this.setState({
          dp: reader.result
        })
      }
      reader.readAsDataURL(file);
      this.setState({
        newDp: file
      })
    }
  }
  render(){
    return(
      <article>
        <div className="dashboardMain">
        <Header/>
            <div className="dashboardContent">
                <MainSidebar/>
                <div className="rightColumn">
                    <SettingSidebar/>
                    <div className="rightColumnContent">
                    <Path path={[
                                  {
                                    "path": "Manage your account",
                                    "url": "/settings"
                                  },
                                  {
                                    "path": "Account Settings",
                                    "url": "/settings"
                                  }
                                ]} />
                      <div className="boxContentInner">
                          <div className="contentAccountSetting">
                              <form method="post" onSubmit={this.submitForm}>
                                  <div className="boxProfile">
                                      <div className="boxProfilePhoto">
                                          <div className="custom-file">
                                              <i className="fas fa-camera"></i>
                                              <input type="file" onChange={this.changeDP} className="custom-file-input" aria-describedby="inputGroupFileAddon01"/>
                                          </div>
                                         <img src={this.state.dp} alt="" title=""/>
                                      </div>
                                      <div className="row">
                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Company Name</label>
                                                  <input type="text" onChange={(e)=>{
                                                     this.setState({company_name:e.target.value})
                                                   }} value={this.state.company_name} required className="form-control"/>
                                              </div>
                                          </div>
                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Name</label>
                                                  <input onChange={(e)=>{
                                                    this.setState({name:e.target.value})
                                                  }} value={this.state.name} required type="text" className="form-control"/>
                                              </div>
                                          </div>
                                      </div>
                                      <div className="row">
                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Address 1</label>
                                                  <input type="text" onChange={(e)=>{
                                                    this.getSuggestions(e.target.value);
                                                    this.setState({
                                                      address_1:e.target.value,
                                                      address_2:e.target.value
                                                    });
                                                  }} list="datalist" value={this.state.address_1} onBlur={this.changeCountry} required placeholder="Enter your Address" className="form-control" />
                                                  <datalist id="datalist">
                                                    <option value="">select</option>
                                                    {this.state.data.length>0?
                                                    this.state.data.map((e, i)=>{
                                                      return(
                                                        <option key={i} value={e.value}>{e.label}</option>
                                                      )
                                                    }):null}
                                                  </datalist>
                                              </div>
                                          </div>
                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Suburb and postcode</label>
                                                  <div className="inputpostcode">
                                                      <img alt="" className="iconFlag" src="https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/assets/img/iconFlag.png" title=""/>
                                                      <input type="number" onChange={(e)=>{
                                                        this.setState({post_code:e.target.value});
                                                      }} value={this.state.post_code} required disabled placeholder="Enter Suburb and Postcode" className="form-control"/>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div className="row">
                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Country</label>
                                                  <input type="text"  value={this.state.country} required disabled placeholder="Enter Country Name" className="form-control"/>
                                              </div>
                                          </div>
                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Phone</label>
                                                  <input type="text" onChange={(e)=>{
                                                    this.setState({phone:e.target.value})
                                                    this.validatePhone(e.target.value)
                                                  }} value={this.state.phone} required className="form-control"/>
                                                  <span className={this.state.check_phone_color}>{this.state.check_phone}</span>
                                              </div>
                                          </div>
                                      </div>
                                      <div className="row">

                                          <div className="col-md-6">
                                              <div className="form-group">
                                                  <label>Email</label>
                                                  <input type="text" onChange={(e)=>{
                                                    this.setState({email:e.target.value})
                                                    this.validateEmail(e.target.value)
                                                  }} value={this.state.email} required className="form-control"/>
                                                  <span className={this.state.check_email_color}>{this.state.check_email}</span>
                                              </div>
                                          </div>
                                      </div>
                                      <div className="row">
                                          <div className="col-md-6">
                                              <input type="submit" className="btn" value="Save Changes"/>
                                          </div>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>
              </div>
          </div>
      </div>
      </div>
      </article>
    )
  }
}

Settings.propTypes = {
  updateEditOrder: PropTypes.func.isRequired,
  clearOrder: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  allDrivers: state.carrier.allDrivers,
  driversCount: state.carrier.driversCount
});

export default connect(mapStateToProps, {updateEditOrder, clearOrder})(Settings);
