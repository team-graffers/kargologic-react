import store from '../../store';

export default async function PostFetch(name, payload, header=null , method=null){
  let jsonD = [];
  let m = 'POST';
  if(method!==null){
    m = method;
  }
  let headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'Token '+localStorage.getItem("token")
  };
  if(header!==null){
    headers = header
  }
  const state = store.getState();
  await fetch(state.carrier.mainUrl+name, {
    method: m,
    headers: headers,
    body: JSON.stringify(payload)
  })
  .then((response) => response.json())
  .then((responseJSON) => {
     jsonD = responseJSON;
     if(jsonD && jsonD.detail && jsonD.detail === "Invalid token."){
       localStorage.removeItem("token");
       localStorage.removeItem("sidebar");
       localStorage.removeItem("userDetails");
       window.location.href = '/';
     }
  }).catch(err =>{
      jsonD = null;
  });
  return jsonD;
}


/*fetch('API_ENDPOINT', OBJECT)
  .then(function(res) {
    return res.json();
   })
  .then(function(resJson) {
    return resJson;
   })

I structured my object as such:

var obj = {
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Origin': '',
    'Host': 'api.producthunt.com'
  },
  body: JSON.stringify({
    'client_id': '(API KEY)',
    'client_secret': '(API SECRET)',
    'grant_type': 'client_credentials'
  })*/
