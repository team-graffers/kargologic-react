import store from '../../store';

export default async function DownloadCSV(csvUrl, payload, filename){
  let jsonD = '';
  let headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'Token '+localStorage.getItem("token")
  };
  const state = store.getState();
  await fetch(state.carrier.mainUrl+csvUrl, {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(payload)
  })
  .then((response) => response.text())
  .then((responseJSON) => {
     jsonD = responseJSON;
  }).catch(err =>{
      jsonD = null;
  });
  if(jsonD!==null){
    let blob = new Blob([jsonD], { type: 'application/vnd.ms-excel' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    }else{
      let link = document.createElement("a");
      if (link.download !== undefined) { // feature detection
        let url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", filename);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }
}
/*headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': token
  },*/
