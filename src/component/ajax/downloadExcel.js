import store from '../../store';

export const DownloadExcel = (payload) =>{
  const state = store.getState();
  let link = document.createElement("a");
  if (link.download !== undefined) { // feature detection
    link.setAttribute("href", `${state.carrier.mainUrl}${payload}`);
    link.setAttribute("download", 'orders.xlsx');
    link.style.visibility = 'hidden';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }
}
