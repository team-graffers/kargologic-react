
export default async function APIFetch(url){
  let jsonD = [];
  await fetch(url)
  .then((response) => response.json())
  .then((responseJSON) => {
     jsonD = responseJSON;
  }).catch(err =>{
      jsonD = null;
  });
  return jsonD;
}
