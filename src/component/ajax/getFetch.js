import store from '../../store';

export default async function GetFetch(name){
  let jsonD = [];
  let headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'Token '+localStorage.getItem("token")
  };
  const state = store.getState();
  await fetch(state.carrier.mainUrl+name, {
    method: 'GET',
    headers: headers
  })
  .then((response) => response.json())
  .then((responseJSON) => {
      jsonD = responseJSON;
     if(jsonD && jsonD.detail && jsonD.detail === "Invalid token."){
       localStorage.removeItem("token");
       localStorage.removeItem("sidebar");
       localStorage.removeItem("userDetails");
       window.location.href = '/';
     }
  }).catch(err =>{
      jsonD = null;
  });
  return jsonD;
}
/*headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': token
  },*/
