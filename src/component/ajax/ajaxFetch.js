import store from '../../store';

export const GetFetch2 = async(name)=>{
  let jsonD = [];
  const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  };

  const state = store.getState();
  await fetch(state.carrier.mainUrl+name, {
    method: 'GET',
    headers: headers
  })
  .then((response) => response.json())
  .then((responseJSON) => {
      jsonD = responseJSON;
  }).catch(err =>{
      jsonD = null;
  });
  return jsonD;
}

export const PostFetch2 = async(name, payload)=>{
  let jsonD = [];
  let headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  };
  const state = store.getState();
  await fetch(state.carrier.mainUrl+name, {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(payload)
  })
  .then((response) => response.json())
  .then((responseJSON) => {
     jsonD = responseJSON;
  }).catch(err =>{
      jsonD = null;
  });
  return jsonD;
}
