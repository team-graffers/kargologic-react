// eslint-disable-next-line
import React from 'react';

const Logout = () =>{
  localStorage.removeItem("token");
  localStorage.removeItem("sidebar");
  localStorage.removeItem("userDetails");
  localStorage.removeItem("userConfiguration");
  window.location.href= "/carrier/login";
  return(
    ''
  )
}

export default Logout;
