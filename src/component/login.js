import React, {Component} from 'react';
import SideImg from './include/side-img';
import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {doLogin} from '../actions/carrierActions';
import {Redirect } from 'react-router';
// import PasswordMask from 'react-password-mask';
// import store from '../store';
import {errorAlert, infoAlert} from './include/alert';
import {validatePassword, verfyPassword} from './include/validation';

class CarrierLogin extends Component{
  constructor(props){
    super(props);
    this.state = {
      email:'',
      password:'',
      redirect: false
    }
    localStorage.removeItem("sidebar");
    this.formSubmit = this.formSubmit.bind(this);
  }

  formSubmit = (e) =>{
    e.preventDefault();
    const payload = {
      email:this.state.email,
      password:this.state.password,
    }
    const headers = {
      'content-type': 'application/json',
    }
    this.props.doLogin(payload, headers);
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.userDetails.token){
      this.setState({
        email:'',
        password:'',
        redirect: true,
        strength: '',
        color: ''
      });

    }else{
      errorAlert(nextProps.userDetails.message)
    }
  }
  componentWillMount(){
    let x = localStorage.getItem("token");
    let y = localStorage.getItem("userDetails");
    let z = localStorage.getItem("userType");
    if(x && y && z && z === "carrier"){
      localStorage.removeItem("customerDetails");
      this.setState({
        redirect: true,
      })
    }else if (x && y && z && z !== "carrier") {
      localStorage.removeItem("userDetails");
    }
    window.history.pushState('', 'login', `/carrier/login`);
  }

  render(){
    if(this.state.redirect){
      return(
        <Redirect to="/dashboard/all" />
      )
    }
  //  const state = store.getState();
    return(
      <article>
        <section className="sectionColumns">
          <SideImg msg={''}/>
        <div className="rightColumn">
                  <div className="content-rightColumn">
                      <h5 className="mb-4">Login to your account</h5>
                      <div className="boxComman">
                          <form action="" onSubmit={this.formSubmit} method="POST">
                              <div className="form-group">
                                  <label>Email address</label>
                                  <input type="email" value={this.state.email} onChange={(e)=>{
                                    this.setState({email:e.target.value})
                                  }} placeholder="Enter email address" required className="form-control"/>
                              </div>
                              <div className="form-group">
                                  <label>Password</label>
                                  <input type="password" className="form-control" placeholder="Enter password" value={this.state.password} onChange={(e)=>{
                                    let data = validatePassword(e.target.value);
                                    this.setState({
                                      password:e.target.value,
                                      strength: data.strength,
                                      color: data.color
                                    });
                                  }}
                                  onBlur={(e)=>{
                                    if(!verfyPassword(e.target.value)){
                                      this.setState({password:''})
                                      infoAlert('Password must contain:- minimum 8 character, at least one capital letter, at least one small letter, at least one number and one special symbol. ')
                                    }
                                  }}
                                  required/>
                              </div>
                              <div className="row align-items-center">
                                  <div className="col-md-6">
                                      <input type="submit" title="Login" value="Login"/>
                                  </div>
                                  <div className="col-md-6 text-right">
                                      <p className="mb-0"><NavLink to="/carrier/forgot-password" title="Forgot Password?" className="font-weight-medium"> Forgot Password?</NavLink> </p>
                                  </div>
                              </div>
                          </form>
                      </div>
                      <div className="boxComman p-4 text-center font-weight-medium">
                          <p className="mb-0">Don't have an account? <NavLink to="/carrier/signup">Sign up</NavLink></p>
                      </div>
                  </div>
              </div>
          </section>
      </article>
    )
  }
}
CarrierLogin.propTypes = {
  doLogin: PropTypes.func.isRequired,
  //userDetails: PropTypes.array.isRequired,
}

const mapStateToProps = state =>({
  userDetails: state.carrier.userDetails,
});

export default connect(mapStateToProps, {doLogin})(CarrierLogin);
