import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import SubSidebar from './include/subSidebar';
import Path from './include/path';
import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getConfigure, clearOrder, getAllJobs, searchJobs, getJobsCount, getCustomerList, updateEditOrder} from '../actions/carrierActions';
import PostFetch from './ajax/postFetch';
import {formateDate} from './include/date';
import {successAlert, errorAlert, infoAlert} from './include/alert';
import {DownloadExcel} from './ajax/downloadExcel';
import GetFetch from './ajax/getFetch';
import DataTable from 'react-data-table-component';

class Dashboard extends Component{
  constructor(props){
    super(props);
    let url = window.location.href;
    url = url.split('/dashboard/');
    this.state = {
      job_id: [],
      jobs: [],
      status: url[1],
      check:true,
      search:'',
      customers:'All Customers',
      customerOptions: [],
      path: [
              {
                path: "Orders",
                url: "/dashboard/all"
              },
              {
                path: "All",
                url: "/dashboard/all"
              }
            ],
      columns:[
              {
                name: 'Job Number',
                selector: 'job_id',
                sortable: true,
                width:'150px',
                cell: row => <NavLink style={{"zIndex":"999", "position":"relative"}} to={row.status === "draft"?`/createorder/?id=${row.id}`:`/orderdetails/${row.id}`}>{row.job_id}</NavLink>,
              },
              {
                name: 'Created Date',
                selector: 'order_create_time',

                sortable: true,
              },
              {
                name: 'Customer Name',
                selector: 'name',
                sortable: true,

              },
              {
                name: 'Created By',
                selector: 'created_by',
                sortable: true,

              },
              {
                name: 'Pick Up Time From',
                selector: 'pickup_date_time',
                sortable: true,
              },
              {
                name: 'Pick Up Time To',
                selector: 'pickup_date_time_to',
                sortable: true,
              },
              {
                name: 'Delivery Time From',
                selector: 'delivery_date_time',
                sortable: true,
              },
              {
                name: 'Delivery Time To',
                selector: 'delivery_date_time_to',
                sortable: true,
              },
              {
                name: 'Customer Code',
                selector: 'customer_code',
                sortable: true,
              },
              {
                name: 'Price ($)',
                selector: 'price',
                sortable: true,
              },
              {
                name: 'Load Carried',
                selector: 'load_carried',
                sortable: true,
              },
              {
                name: 'Load Type',
                selector: 'load_type',
                sortable: true,
              },
              {
                name: 'Load Weight',
                selector: 'load_weight',
                sortable: true,
              },
              {
                name: 'Measure Unit',
                selector: 'measure_unit',
                sortable: true,
              },
              {
                name: 'Units',
                selector: 'units',
                sortable: true,
              },
              {
                name: 'Status',
                selector: 'status',
                sortable: true,
                width:'150px',
                cell: row => <span className={row.bgColor}>{row.status}</span>,
              }
            ],
      newColumns: [],
    }
    this.changePath = this.changePath.bind(this);
  }
  /*{
    name: 'Delivery on Time (DOT)',
    selector: 'dot',
    cell: row => <div className="progress progressDelivered process-child">
          <span className="progressText">{row.dot}%</span>
          <div className="progress-bar" role="progressbar" style={{"width": row.dot}} aria-valuenow={row.dot} aria-valuemin="0" aria-valuemax="100"></div>
    </div>,
  },*/
  getConfig= async()=>{
    try{
      const x = await PostFetch('companyconfig/getconfig/', '');
      localStorage.setItem("userConfiguration", JSON.stringify(x));
      let orders_table = '';
      x.data.map((e, i)=>{
        if(e.ConfigureID.label === 'orders_table'){
          orders_table = e.configure_val.split(', ')
        }
        return('')
      })
      if(orders_table && orders_table.length>0){
        let col1 = this.state.columns;
        let col2 = []
        col1.map((e, i)=>{
          for(let j=0;j<orders_table.length; j++){
            if(orders_table[j] === e.name){
              col2.push(e)
              break;
            }
          }
          return('')
        })
        this.setState({
          newColumns: col2
        })
      }
    }catch(e){
      console.log('catch', e);
      let col1 = this.state.columns;
      let col2 = []
      for(let j=0;j<8; j++){
        col2.push(col1[j])
      }
      this.setState({
        newColumns: col2
      })
    }
  }
  getAllJobs = async (fetchUrl)=>{
    let json = await GetFetch(fetchUrl);
    let jobs = [];
    if(json && json.order_list && json.order_list.length>0){
      json.order_list.map(d =>{
        let status = d.StatusID.status_name;
        let bgColor = "";
        let dot = 0;
        if(json && json.ord_per_list && json.ord_per_list.length>0){
          for(let r=0;r<json.ord_per_list.length;r++){
            if(json.ord_per_list[r].order_id===d.id){
              dot = json.ord_per_list[r].perc;
              break;
            }
          }
        }
        if(status==="unassigned"){
          bgColor = "status statusUnassigned";
        }else if(status==="pending"){
          bgColor = "status statusPending";
        }else if(status==="in process"){
          status = "in-progress";
          bgColor = "status statusProgress";
        }else if(status==="delivered"){
          bgColor = "status statusDelivered";
        }else if(status==="cancelled"){
          bgColor = "status statusCancelled";
        }
        if(!d.is_active){
          status = "draft";
          bgColor = "status statusUnassigned";
        }
        jobs.push({
          id: d.id,
          job_id: d.job_id,
          order_create_time: d.order_create_time,
          name: d.CustomerCompanyID && d.CustomerCompanyID.name? d.CustomerCompanyID.name: '',
          pickup_date_time: formateDate(d.pickup_date_time[0]),
          delivery_date_time: formateDate(d.delivery_date_time[0]),
          pickup_date_time_to: formateDate(d.pickup_date_time_to[0]),
          delivery_date_time_to: formateDate(d.delivery_date_time_to[0]),
          created_by: d.Created_by_CarrierID && d.Created_by_CarrierID.name? d.Created_by_CarrierID.name: d.Created_by_CustomerID && d.Created_by_CustomerID.name? d.Created_by_CustomerID.name: '',
          customer_code: d.customer_code,
          price: d.price,
          load_carried: d.load_carried,
          load_type: d.load_type,
          load_weight: d.load_weight,
          measure_unit: d.measure_unit,
          units: d.units,
          dot: dot,
          status: status,
          bgColor: bgColor,
        })
        return(
          ''
        )
      });
      this.setState({
        check: true,
        jobs: jobs,
      });
    }
  }
  componentWillMount(){
    this.getConfig();
    this.props.updateEditOrder(false);
    this.props.clearOrder();
    this.props.getCustomerList();
    if(this.state.status === "all"){
      this.changePath('All', 'all');
    }
    else if(this.state.status === "unassigned"){
      this.changePath('Unassigned', 'unassigned');
    }
    else if(this.state.status === "pending"){
      this.changePath('Pending', 'pending');
    }
    else if(this.state.status === "in-process"){
      this.changePath('In-progress', 'in process');
    }
    else if(this.state.status === "delivered"){
      this.changePath('Delivered', 'delivered');
    }
    else if(this.state.status === "cancelled"){
      this.changePath('Cancelled', 'cancelled');
    }
    else if(this.state.status === "draft"){
      this.changePath('Draft', 'draft');
    }else{
    //  this.getAllJobs('carr/status/?status=all');
    }
    this.props.getJobsCount('carr/order_count/');


  }
  changePath = (e, fetchUrl) =>{
    this.getAllJobs('carr/status/?status='+fetchUrl);
    let path = this.state.path;
    path[1].path = e;
    path[1].url = '/customerdashboard/'+fetchUrl
    this.setState({
      path:path,
      jobs: [],
      check: false,
      search: '',
    });
  }
  handleCheckbox = (data)=>{
    this.setState({
      job_id:data.selectedRows
    });
  }
  changeData = ()=>{
    if(this.state.job_id.length>0){
      let jid = [];
      this.state.job_id.map((e, i)=>{
        jid.push(e.id);
        return(
          ''
        )
      });
      let obj ={
        orderlist: jid
      };
      return obj;
    }else{
      let obj ={
        orderlist: []
      };
      return obj;
    }
  }
  handleDelete = async ()=>{
    let payload = this.changeData();
    if(payload.orderlist.length>0){
      let json = await PostFetch('carr/order_delete/', payload, null, 'PUT');
      if(json && json.message){
        successAlert(json.message);
        this.setState({
          jobs: [],
          check: false,
        });
        const url = window.location.pathname;
        const path = url.split('/dashboard/')
        this.getAllJobs('carr/status/?status='+path[1]);
        this.props.getJobsCount('carr/order_count/');
      }else{
        errorAlert('try again!')
      }
    }else{
      errorAlert('Please select orders!')
    }
  }
  handleDownload = ()=>{
    let payload = this.changeData();
    if(payload.orderlist && payload.orderlist.length>0){
      let carrier = JSON.parse(localStorage.getItem("userDetails"));
      if(carrier && carrier.carr_com_id){
        payload = 'orders/carr_order_csv_download/?orderlist='+JSON.stringify(payload.orderlist)+'&carr_com_id='+carrier.carr_com_id;
        DownloadExcel(payload)
      }
    }else{
      errorAlert('Please select orders!')
    }
  }
  componentWillReceiveProps(nextProps) {
    let jobs = [];
    // && this.props.allJobs.length !== nextProps.allJobs.length
    if(nextProps.allJobs && nextProps.allJobs.order_list && nextProps.allJobs.order_list.length>0){
      nextProps.allJobs.order_list.map(d =>{
        let status = d.StatusID.status_name;
        let bgColor = "";
        let dot = 0;
        if(nextProps.allJobs && nextProps.allJobs.ord_per_list && nextProps.allJobs.ord_per_list.length>0){
          for(let r=0;r<nextProps.allJobs.ord_per_list.length;r++){
            if(nextProps.allJobs.ord_per_list[r].order_id===d.id){
              dot = nextProps.allJobs.ord_per_list[r].perc;
              break;
            }
          }
        }
        if(status==="unassigned"){
          bgColor = "status statusUnassigned";
        }else if(status==="pending"){
          bgColor = "status statusPending";
        }else if(status==="in process"){
          status = "in-progress";
          bgColor = "status statusProgress";
        }else if(status==="delivered"){
          bgColor = "status statusDelivered";
        }else if(status==="cancelled"){
          bgColor = "status statusCancelled";
        }
        if(!d.is_active && d.Created_by_CarrierID && parseInt(d.Created_by_CarrierID)){
          status = "draft";
          bgColor = "status statusUnassigned";
        }
        jobs.push({
          id: d.id,
          job_id: d.job_id,
          order_create_time: d.order_create_time,
          name: d.CustomerCompanyID && d.CustomerCompanyID.name? d.CustomerCompanyID.name: '',
          pickup_date_time: formateDate(d.pickup_date_time[0]),
          delivery_date_time: formateDate(d.delivery_date_time[0]),
          pickup_date_time_to: formateDate(d.pickup_date_time_to[0]),
          delivery_date_time_to: formateDate(d.delivery_date_time_to[0]),
          created_by: d.Created_by_CarrierID && d.Created_by_CarrierID.name? d.Created_by_CarrierID.name: d.Created_by_CustomerID && d.Created_by_CustomerID.name? d.Created_by_CustomerID.name: '',
          customer_code: d.customer_code,
          price: d.price,
          load_carried: d.load_carried,
          load_type: d.load_type,
          load_weight: d.load_weight,
          measure_unit: d.measure_unit,
          units: d.units,
          dot: dot,
          status: status,
          bgColor: bgColor,
        })
        return(
          ''
        )
      });
    }
    this.setState({
      check: true,
      jobs: jobs,
      customerOptions: nextProps.customerList
    });
    return true;
  }
  render(){
    return(
      <article>
          <div className="dashboardMain">
            <Header/>
              <div className="dashboardContent">
                <MainSidebar/>
                  <div className="rightColumn">
                    <SubSidebar jobsCount={this.props.jobsCount} changePath={this.changePath} />
                    <div className="rightColumnContent">
                          <Path path={this.state.path} />
                          <div className="tableHeader">
                              <div className="row align-items-center">
                                  <div className="col-12 col-sm-8 col-md-8">
                                    <ul className="listFilter">
                                      <li><a href="#delete" onClick={(e)=>{
                                        e.preventDefault();
                                        this.handleDelete()
                                      }} className="iconDelete"><i className="far fa-trash-alt"></i> Delete</a></li>
                                      <li><a href="#download" onClick={(e)=>{
                                        e.preventDefault();
                                        if(this.state.job_id.length>0){
                                          this.handleDownload()
                                        }else{
                                          infoAlert('Please select order')
                                        }
                                      }} className="iconDownload"><i className="fas fa-download"></i> Download</a></li>
                                      <li>
                                      <select onChange={(e)=>{
                                          this.setState({
                                            customers:e.target.value,
                                            jobs: [],
                                            check: false,
                                          });
                                          if(e.target.value!=="All Customers"){
                                            this.props.searchJobs('carr/search/?search='+e.target.value);
                                          }else{
                                            this.getAllJobs('carr/status/?status=all');
                                          }
                                        }} value={this.state.customers} className="form-control selectCustomers">
                                        <option>All Customers</option>
                                          {this.state.customerOptions && this.state.customerOptions.length>0?
                                            this.state.customerOptions.map((e, i)=>(
                                            <option key={i}>{e.CompanyID.name}</option>
                                          )):null}
                                      </select>
                                      </li>
                                    </ul>
                                  </div>
                                  <div className="col-12 col-sm-4 col-md-4">
                                      <div className="boxSearch">
                                        <input type="search" onChange={(e)=>{
                                          this.setState({
                                            search: e.target.value,
                                          });
                                          if(e.target.value.length>0){
                                            this.setState({
                                              jobs: [],
                                              check: false,
                                            });
                                            this.props.searchJobs('carr/search/?search='+e.target.value);
                                          }else{
                                            this.setState({
                                              jobs: [],
                                              check: false,
                                            });
                                            this.getAllJobs('carr/status/?status=all');
                                          }
                                        }} value={this.state.search} className="form-control" placeholder="search"/>
                                        <button className="btnSearch"><i className="fas fa-search"></i></button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div style={{"marginTop":"32px"}}>
                          {this.state.newColumns && this.state.newColumns.length>0?
                          <DataTable
                            paginationRowsPerPageOptions={[10,25,50,100]}
                            pagination={true}
                            columns={this.state.newColumns}
                            data={this.state.jobs}
                            selectableRows
                            onTableUpdate={this.handleCheckbox}
                          />
                          :null}
                          </div>

                  </div>
              </div>
          </div>
        </div>
      </article>

    )
  }
}

Dashboard.propTypes = {
  clearOrder: PropTypes.func.isRequired,
  getAllJobs: PropTypes.func.isRequired,
  searchJobs: PropTypes.func.isRequired,
  getJobsCount: PropTypes.func.isRequired,
  getCustomerList: PropTypes.func.isRequired,
  updateEditOrder: PropTypes.func.isRequired,
  getConfigure: PropTypes.func.isRequired,
//  jobsCount: PropTypes.object.isRequired,
//  allJobs: PropTypes.array.isRequired,
}

const mapStateToProps = state =>({
  allJobs: state.carrier.allJobs,
  jobsCount: state.carrier.jobsCount,
  customerList: state.carrier.customerList,
  userConfiguration: state.carrier.userConfiguration
});

export default connect(mapStateToProps, {getConfigure, clearOrder, getAllJobs, searchJobs, getJobsCount, getCustomerList, updateEditOrder})(Dashboard);


//delete
/**/
