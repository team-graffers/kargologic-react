import React, {Component} from 'react';
import store from '../../store';
import createOrder from '../../assets/img/icon-createOrder.png';
import inviteDriver from '../../assets/img/icon-inviteDriver.png';
import inviteCustomer from "../../assets/img/icon-InviteCustomer.png";
import {NavLink} from 'react-router-dom';
import {Redirect } from 'react-router';
import GetFetch from '../ajax/getFetch';
import {formateDate2} from './date';
import InviteDriver from '../subComponent/inviteDriver';
import InviteCustomer from '../subComponent/inviteCustomer';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {setSidebar} from '../../actions/carrierActions';


class Header extends Component{
  constructor(props){
    super(props);
    let user = '';
    let email = '';
    let dp = '';
    let status = true;
    let carrier = JSON.parse(localStorage.getItem("userDetails"));
    let userType = localStorage.getItem("userType");
    const state = store.getState();
    if(carrier && userType && userType === "carrier"){
      user = carrier.carrier_name
      email = carrier.email;
      dp = state.carrier.bucketUrl+carrier.carr_image;
      status = false;
    }
    this.state = {
      user: user,
      email: email,
      status: status,
      dp: dp,
      notification: [],
      notiAlert: false,
      driverInviteModel: false
    }
  }
  openDriverModel = () =>{
    this.setState({
      driverInviteModel: true
    })
  }
  closeDriverModel = () =>{
    this.setState({
      driverInviteModel: false
    })
  }
  getNotifications = async ()=>{
    let noti = await GetFetch('carr/notify_carr/');
    try{
      if(noti){
        if(noti.data.length>0){
          let count = localStorage.getItem('notiCount');
          if(!count){
            count = 0;
          }
          if(count<noti.data.length){
            this.setState({
              notiAlert: true,
              notification: noti.data
            });
          }else{
            this.setState({
              notification: noti.data
            });
          }

        }
      }
    }catch(e){

    }
  }
  componentWillMount(){
    if(!this.props.dummy){
      this.getNotifications();
    }
  }
  componentDidMount(){
    setInterval(function() {
      this.getNotifications();
    }.bind(this),5000);
  }
  openOneModel=(modalId)=>{
    try{
      const modal = document.getElementById(modalId);
      modal.classList.add('show');
      //modal.setAttribute('aria-hidden', 'true');
      modal.setAttribute('style', 'display: block');
      const modalBackdrops = document.getElementsByClassName('modal-backdrop');
      document.body.appendChild(modalBackdrops[0]);
    }catch(e){

    }
  }
  closeOneModel=(modalId)=>{
    const modal = document.getElementById(modalId);
    modal.classList.remove('show');
    modal.setAttribute('aria-hidden', 'true');
    modal.setAttribute('style', 'display: none');
    try{
      const modalBackdrops = document.getElementsByClassName('modal-backdrop');
      document.body.removeChild(modalBackdrops[0]);
    }catch(e){

    }
  }
  render(){
    if(this.state.status){
      return(
        <Redirect to="/carrier/login" />
      )
    }
    return(
      <React.Fragment>
        <div className="dasboardHeader">
            <div className="topbar">
                <div className="container-fluid">
                    <div className="row align-items-center">
                        <div className="col-12 col-sm-3 col-md-6">
                            <h1 id="logo">
                              <NavLink className="navbar-brand" to="/dashboard/all">
                                <img src="https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/logo.svg" alt="Kargologic" title="Kargologic"/>
                              </NavLink>
                            </h1>
                        </div>
                        <div className="col-12 col-sm-9 col-md-6 text-right">
                            <ul className="listTopMenu">
                            <li className="dropdown dropdownAdd">
                                <a href="#a" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" className="btnAdd">.</a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <ul className="listAddOptions">
                                        <li>
                                            <NavLink to="/createorder">
                                                <img src={createOrder} alt="" title="" />
                                                <h6>Create Order</h6>
                                            </NavLink>
                                        </li>
                                        <li>
                                            <a href="#inviteDriver" onClick={(e)=>{
                                              e.preventDefault();
                                              this.openDriverModel();
                                            }}>
                                                <img src={inviteDriver} alt="" title="" />
                                                <h6>Invite Driver</h6>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#inviteCustomer" data-toggle="modal" data-target="#modelInviteCustomer">
                                                <img src={inviteCustomer} alt="" title="" />
                                                <h6>Invite Customer</h6>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                                <li className="dropdown dropdownNotification">
                                    <a href="#a" onClick={(e)=>{
                                      localStorage.setItem('notiCount', this.state.notification.length);
                                      this.setState({
                                        notiAlert: false
                                      })
                                    }} className="btnNotification" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {this.state.notiAlert?
                                    <span className="signNotification"></span>
                                    :null}
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right">
                                        <h4>Notification</h4>
                                        <ul className="listNotification">
                                          {this.state.notification.length>0?
                                            this.state.notification.map((e, i)=>(
                                              <li key={i}>
                                              {e.OrderID?
                                                <a href={`/orderdetails/${e.OrderID}`}>
                                                  <div className="contentNotification">
                                                    <div className="boxUserimg"><img src="assets/img/img-user.png" alt="" title="" /></div>
                                                    <div>
                                                      <p>{e.comments}</p>
                                                      <span className="textTime"></span>
                                                    </div>
                                                  </div>
                                                </a>
                                              :
                                                <a href="#a">
                                                  <div className="contentNotification">
                                                    <div className="boxUserimg"><img src="assets/img/img-user.png" alt="" title="" /></div>
                                                    <div>
                                                      <p>{e.comments}</p>
                                                      <p className="textTime" align="right">{formateDate2(e.updated_DT)}</p>
                                                    </div>
                                                  </div>
                                                </a>}
                                              </li>
                                            )):<li>
                                              <a href="#a">
                                                <div className="contentNotification">
                                                  <div>
                                                    <p>No Notification</p>
                                                  </div>
                                                </div>
                                              </a>
                                            </li>}

                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div className="dropdown dropdownSetting">
                                        <a className="btn_icon dropdown-toggle" href="#a" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img className="imgUser" src={this.state.dp} alt="" title="" />   Hi, {this.state.user}  </a>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <div className="userDetail">
                                                <img className="imgUser" src={this.state.dp} alt="" title="" />
                                                <h5>{this.state.user}</h5>
                                                <h6>{this.state.email}</h6>
                                            </div>
                                            <NavLink className="dropdown-item" onClick={(e)=>{
                                              this.props.setSidebar('iconSettings', 'iconDrivers', 'iconOrder');
                                            }} to="/settings">Account</NavLink>
                                            <NavLink className="dropdown-item" onClick={(e)=>{
                                              this.props.setSidebar('iconSettings', 'iconDrivers', 'iconOrder');
                                            }} to="/password">Update Password</NavLink>
                                            <NavLink className="dropdown-item" to="/logout">Logout</NavLink>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="modal fade popupAssignDriver model-inviteDriverFront modelInviteDriver" id="modelInviteCustomer" tabIndex="-1" style={{"zIndex":"99999"}} role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-body">
                <a href="#a" onClick={(e)=>{
                  e.preventDefault()
                  this.closeOneModel('modelInviteCustomer');
                }} className="btnClose" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </a>
                  <div className="row rowAssign align-items-center">
                    <div className="col-md-12">
                      <h4 className="pb-0 mb-0">Invite Customer</h4>
                    </div>
                  </div>
                  <InviteCustomer closeOneModel={this.closeOneModel} />
                </div>
              </div>
            </div>
          </div>

          {this.state.driverInviteModel?
            <InviteDriver closeOneModel={this.closeDriverModel} />
          :null}

      </React.Fragment>
  );
  }
}

Header.propTypes = {
  setSidebar: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  mainSidebar: state.carrier.mainSidebar,
});

export default connect(mapStateToProps, {setSidebar})(Header);
