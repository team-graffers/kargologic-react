import GetFetch from '../ajax/getFetch';

export const GetJobId = async (payload)=>{
  const json = await GetFetch('orders/jobgenerate/?company='+payload);
  if(json && json.message==="success" && json.data){
    return json.data
  }else {
    return false
  }
}
