import React  from 'react';

let all = 0;
let added = 0;
let pending = 0;

let allClass = "active";
let addedClass = "";
let pendingClass = "";

const allNull=()=>{
  allClass = "";
  addedClass = "";
  pendingClass = "";
}

const DriverSidebar = (props) =>{
  if(props.driversCount){
    all = props.driversCount.all_driver;
    added = props.driversCount.added_driver;
    pending = props.driversCount.pending_driver;
  }
  return(
    <div className="rightColumnMenu">
        <ul className="menuRightColumn">
          <li><a href="#a" className={allClass} onClick={(e)=>{
            e.preventDefault();
            allNull();
            allClass = "active";
            window.history.pushState('', 'drivers', '/drivers/all');
            props.changePath('All Drivers', '/drivers/all', 'all');
          }}>
            All Drivers <span>{all}</span>
          </a></li>
          <li><a href="#a" className={addedClass} onClick={(e)=>{
            e.preventDefault();
            allNull();
            addedClass = "active";
            window.history.pushState('', 'drivers', '/drivers/added');
            props.changePath('Added Drivers', '/drivers/added', 'driver added');
          }}>
            Added Drivers <span>{added}</span>
          </a></li>
          <li><a href="#a" className={pendingClass} onClick={(e)=>{
            e.preventDefault();
            allNull();
            pendingClass = "active";
            window.history.pushState('', 'drivers', '/drivers/pending');
            props.changePath('Pending Drivers', '/drivers/pending', 'driver pending');
          }}>
            Pending Drivers <span>{pending}</span>
          </a></li>
        </ul>
    </div>
  );
}

export default DriverSidebar;
