import React  from 'react';
import {NavLink} from 'react-router-dom';

let all = 0;
let added = 0;
let pending = 0;

const DriverSidebar = (props) =>{
  if(props.driversCount){
    all = props.driversCount.all_driver;
    added = props.driversCount.added_driver;
    pending = props.driversCount.pending_driver;
  }
  return(
    <div className="rightColumnMenu">
        <ul className="menuRightColumn">
        <li>
          <NavLink className="active" to="/drivers/all">
            All Drivers <span>{all}</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/drivers/added">
            Added Drivers <span>{added}</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/drivers/pending">
            Pending Drivers <span>{pending}</span>
          </NavLink>
        </li>
        </ul>
    </div>
  );
}

export default DriverSidebar;
