import React  from 'react';
import {NavLink} from 'react-router-dom';

let all = 0;
let unassigned = 0;
let pending = 0;
let inprocess = 0;
let delivered = 0;
let draft = 0;

let allClass = "active";
let unassignedClass = "";
let pendingClass = "";
let inprocessClass = "";
let deliveredClass = "";
let draftClass = "";

const allNull=()=>{
  allClass = "";
  unassignedClass = "";
  pendingClass = "";
  inprocessClass = "";
  deliveredClass = "";
  draftClass = "";
}

const SubSidebar = (props) =>{
  if(props.jobsCount){
    all = props.jobsCount.all;
    unassigned = props.jobsCount.unassigned;
    pending = props.jobsCount.pending;
    inprocess = props.jobsCount.process;
    delivered = props.jobsCount.delivered;
    draft = props.jobsCount.draft;
  }
  return(
    <div className="rightColumnMenu">
        <div className="boxBtnOrders">
          <NavLink to="/createorder" className="btn btn-md">Create Order</NavLink>
        </div>
        <ul className="menuRightColumn">
        <li>
          <a href="#a" className={allClass} onClick={(e)=>{
            e.preventDefault();
            allNull();
            allClass = "active";
            window.history.pushState('', 'dashboard', '/dashboard/all');
            props.changePath('All', 'all');
          }}>
            All Jobs <span>{all}</span>
          </a>
        </li>
        <li>
          <a href="#a" className={unassignedClass} onClick={(e)=>{
            e.preventDefault();
            allNull();
            unassignedClass = "active";
            window.history.pushState('', 'dashboard', '/dashboard/unassigned');
            props.changePath('Unassigned', 'unassigned');
          }}>
            Unassigned <span>{unassigned}</span>
          </a>
        </li>
        <li>
          <a href="#a" className={pendingClass} onClick={(e)=>{
            e.preventDefault();
            allNull();
            pendingClass = "active";
            window.history.pushState('', 'dashboard', '/dashboard/pending');
            props.changePath('Pending', 'pending');
          }}>
            Pending <span>{pending}</span>
          </a>
        </li>
        <li>
          <a href="#a" className={inprocessClass} onClick={(e)=>{
            e.preventDefault();
            allNull();
            inprocessClass = "active";
            window.history.pushState('', 'dashboard', '/dashboard/inprocess');
            props.changePath('In-progress', 'in process');
          }}>
            In-progress <span>{inprocess}</span>
          </a>
        </li>
        <li>
          <a href="#a" className={deliveredClass} onClick={(e)=>{
            e.preventDefault();
            allNull();
            deliveredClass = "active";
            window.history.pushState('', 'dashboard', '/dashboard/delivered');
            props.changePath('Delivered', 'delivered');
          }}>
            Delivered <span>{delivered}</span>
          </a>
        </li>

        <li>
            <a href="#a" className={draftClass} onClick={(e)=>{
              e.preventDefault();
              allNull();
              draftClass = "active";
              window.history.pushState('', 'dashboard', '/dashboard/draft');
              props.changePath('Cancelled', 'draft');
            }}>
              Draft <span>{draft}</span>
            </a>
          </li>

        </ul>
    </div>
  );
}

export default SubSidebar;

/*    */
