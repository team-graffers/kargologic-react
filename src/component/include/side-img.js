import React from 'react';

const SideImg = (props) =>{
  return(
    <div className="leftColumn">
        <div className="content-leftColumn">
            <h1 id="logo"><a href="#a" title="Kargologic"><img src="https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/logo.svg" alt="Kargologic" title="Kargologic"/></a></h1>
            <div className="columnImg">
                <img src="https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/leftHeaderImg.jpg" alt=""/>
            </div>
            <div className="container">
                <div className="bottomContent">
                  {props.msg!==''?
                    <h2><span>{props.msg}</span> invites you to join KargoLogic to view delivery status updates in realtime.</h2>
                  :null}
                      <p className="mb-0">Kargologic is a cloud based solution that connects Customers, Transport Carriers and Logistics
                          companies on one platform.
                      </p>

                </div>
            </div>
        </div>
    </div>
  );
}

export default SideImg;
