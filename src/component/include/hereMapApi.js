import PostFetch from '../ajax/postFetch';
import APIFetch from '../ajax/apiFetch';
import store from '../../store';

const state = store.getState();
const appdata = state.carrier.appdata;

export const HereMapLogin = async () =>{
  let data = await PostFetch('carr/heremap_admin_tok/', []);
  if(data && data.data){
    return data.data;
  }else {
    return null;
  }
}

export const BatchShadows = async (token, payload) =>{
  let jsonD = [];
  await fetch('https://tracking.api.here.com/shadows/v2/batch', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+token
    },
    body: JSON.stringify(payload)
  })
  .then((response) => response.json())
  .then((responseJSON) => {
     jsonD = responseJSON;
  }).catch(err =>{
      jsonD = null;
  });
  return jsonD;
}

export const GetLocation = async (address)=>{
  if(address){
    const data = await APIFetch('https://geocoder.api.here.com/6.2/geocode.json?searchtext='+address+'&'+appdata);
    if(data && data.Response.View.length>0 && data.Response.View[0].Result.length>0 && data.Response.View[0].Result[0].Location){
      let pos = data.Response.View[0].Result[0].Location.DisplayPosition;
      return {
        lat: pos.Latitude,
        lng: pos.Longitude
      }
    }else {
      return null
    }
  }else {
    return null
  }
}

export const GetRoute = async (payload)=>{
  let jsonD = [];
  await fetch('https://route.api.here.com/routing/7.2/calculateroute.json?'+appdata+'&'+payload+'&mode=balanced;truck', {
    method: 'GET',
  })
  .then((response) => response.json())
  .then((responseJSON) => {
     jsonD = responseJSON;
  }).catch(err =>{
      jsonD = null;
  });
  if(jsonD && jsonD.response && jsonD.response.route && jsonD.response.route[0] && jsonD.response.route[0].summary){
    return jsonD.response.route[0].summary;
  }
  return jsonD;
}
