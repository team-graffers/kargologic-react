import React  from 'react';
import {NavLink} from 'react-router-dom';

const SettingSidebar = () =>{
  return(
    <div className="rightColumnMenu">
        <ul className="menuRightColumn">
        <li><NavLink to="/settings">Account Settings</NavLink></li>
        <li><NavLink to="/password">Password</NavLink></li>
        <li><NavLink to="/configure">Configure</NavLink></li>
        <li><NavLink to="/assetsmanager">Assets Manager</NavLink></li>
        <li><NavLink to="/sender">Senders</NavLink></li>
        <li><NavLink to="/receiver">Receivers</NavLink></li>
        <li><NavLink to="/customer">Customers</NavLink></li>
        </ul>
    </div>

  );
}

export default SettingSidebar;
