import PostFetch from '../ajax/postFetch';
import {infoAlert} from './alert';

const headers = {
  'content-type': 'application/json',
}

export const validateNumber = (data, len)=>{
 if(data && data.length>len && !isNaN(parseInt(data))){
   return true;
 }else{
   return false;
 }
}

export const verifyNumber = async (phone)=>{
  if(/^\d{10}$/.test(phone)){
    const payload = {
      number: phone
    }
    let json = await PostFetch('driv/unique_user_num/', payload, headers);
    if(json && json.message==="does not exist"){
      return {
        error: '',
        flg: 0,
      }
    }else if(json && json.message==="exist"){
      return {
        error: 'phone number already exist',
        flg: 1
      }
    }
  }else{
    return {
      error: 'valid phone number is required',
      flg: 1
    }
  }
}

export const chackString = (data, len=null)=>{
 if(data){
   return true;
 }else{
   return false;
 }
}

export const verifyEmail = async (email)=>{
  //if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
  if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(.\w{2,3})+$/.test(email)){
    const payload = {
      email: email,
    }
    let json = await PostFetch('user_role/email_check', payload, headers)
    if(json && json.message){
      return {
        data: json.message,
        error: ''
      }
    }else if(json && json.message==="" && json.data==="") {
        return {
          data: '',
          error: ''
        }
    }else{
      return {
        data: '',
        error: 'something went wrong'
      }
    }
  }else{
    return {
      data: '',
      error: 'valid email is required'
    }
  }
}

export const alphaNum = (str) =>{
  if(/^([a-zA-Z0-9]+)$/.test(str)){
    return true
  }else{
    return false
  }
}

export const verfyPassword = (str)=>{
  return true
  // if(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/.test(str)){
  //   return true
  // }else{
  //   return false
  // }
}

export const validatePassword = (password)=>{
    if (password.length === 0) {
        return {
          strength: '',
          color: ''
        }
    }
    // Create an array and push all possible values that you want in password
    var matchedCase = [];
    matchedCase.push("[$@$!%*#?&]"); // Special Charector
    matchedCase.push("[A-Z]");      // Uppercase Alpabates
    matchedCase.push("[0-9]");      // Numbers
    matchedCase.push("[a-z]");     // Lowercase Alphabates

    // Check the conditions
    var ctr = 0;
    for (var i = 0; i < matchedCase.length; i++) {
        if (new RegExp(matchedCase[i]).test(password)) {
            ctr++;
        }
    }
    // Display it
    var color = "";
    var strength = "";
    switch (ctr) {
        case 0:
        case 1:
        case 2:
            strength = "Very Weak";
            color = "text-danger";
            break;
        case 3:
            strength = "Medium";
            color = "text-warning";
            break;
        case 4:
            strength = "Strong";
            color = "text-success";
            break;
        default:
            break;
    }
    return{
      strength: strength,
      color: color
    }
}

export const charValidation = (val)=>{
  try {
    if(val && val.length<101){
      return true
    }else {
      infoAlert('max limit exceeded.')
      return false
    }
  } catch (e) {
    return false
  }
}
