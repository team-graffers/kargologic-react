import toast from 'toasted-notes';
import 'toasted-notes/src/styles.css';
import React from 'react';

export const successAlert = (msg)=>{
  return(
    toast.notify(<div style={{"minWidth":"200px", "textAlign": "left"}}>
      <i className="far fa-check-circle" style={{"color": "#489147", "fontSize": "20px"}}></i>
      <span style={{"verticalAlign": "text-bottom", "fontWeight": "400"}}> {msg}</span>
    </div>,{
      duration: 3000
    })
  )
}
export const infoAlert = (msg)=>{
  return(
    toast.notify(<div style={{"minWidth":"200px", "textAlign": "left"}}>
      <i className="fas fa-exclamation-circle" style={{"color": "#d2db2b", "fontSize": "20px"}}></i>
      <span style={{"verticalAlign": "text-bottom", "fontWeight": "400"}}> {msg}</span>
    </div>,{
      duration: 3000
    })
  )
}
export const errorAlert = (msg)=>{
  return(
    toast.notify(<div style={{"minWidth":"200px", "textAlign": "left"}}>
      <i className="fas fa-exclamation-circle" style={{"color": "#cc261a", "fontSize": "20px"}}></i>
      <span style={{"verticalAlign": "text-bottom", "fontWeight": "400"}}> {msg}</span>
    </div>,{
      duration: 3000
    })
  )
}
