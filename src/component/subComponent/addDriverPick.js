import React, {Component} from 'react';
import Datetime from 'react-datetime';
import APIFetch from '../ajax/apiFetch';
import Map from './maps';
import moment from 'moment';
import makeid from './makeid';
//import {checkDate} from '../include/date';
import store from '../../store';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getSenderReceiver, pushSenderReceiver} from '../../actions/carrierActions';
import AddSender from './addSender';
import AddAssets from './addAssets';
import PostFetch from '../ajax/postFetch';
import {charValidation} from '../include/validation';

class AddDriverPick  extends Component{
  constructor(props){
    super(props);
    const state = store.getState();
    this.state = {
      pick_id: 0,
      address:"",
      longitude:"",
      latitude:"",
      assigned_quantity:"",
      pickup_date_time:"",
      pickup_date_time_to:'',
      checkQuantity: false,
      checkAddress: false,
      checkDateFrom: false,
      checkDateTo: false,
      data: [],
      appdata: state.carrier.appdata,
      showMap: false,
      datalistId: makeid(),
      dateChange: false,
      addSenderModel: false,
      addAssetModel: false,
      sender: '',
      senderReceiver: [],
      asset: [],
      assetsListDriver: [],
      assetId: [],
      timeslot: 30,
      asset_end: '',
      checkAssetDate: false,
      pick_sender_id: '',
    }
    this.changeCountry = this.changeCountry.bind(this);
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.senderReceiver && nextProps.senderReceiver.data && nextProps.senderReceiver.data.length>0){
      this.setState({
        senderReceiver: nextProps.senderReceiver.data
      })
      if(this.props.assign_pick && this.props.assign_pick.sender){
        for(let i = 0;i<nextProps.senderReceiver.data.length; i++){
          if(nextProps.senderReceiver.data[i].id === this.props.assign_pick.sender){
            this.setState({
              sender: i
            })
            setTimeout(function() {
              this.sendState();
            }.bind(this),1000);
            break;
          }
        }
      }else if (this.state.pick_sender_id) {
        for(let i = 0;i<nextProps.senderReceiver.data.length; i++){
          if(nextProps.senderReceiver.data[i].id === parseInt(this.state.pick_sender_id)){
            this.setState({
              sender: i
            })
            setTimeout(function() {
              this.sendState();
            }.bind(this),1000);
            break;
          }
        }
      }
    }
    if(nextProps.senderReceiver && nextProps.senderReceiver.length>0){
      this.setState({
        senderReceiver: nextProps.senderReceiver
      })
      if(this.props.assign_pick && this.props.assign_pick.sender){
        for(let i = 0;i<nextProps.senderReceiver.length; i++){
          if(nextProps.senderReceiver.data[i].id === this.props.assign_pick.sender){
            this.setState({
              sender: i
            })
            setTimeout(function() {
              this.sendState();
            }.bind(this),1000);
            break;
          }
        }
    }
  }


  }
  openAssetModel = ()=>{
    this.setState({
      addAssetModel: true
    })
  }
  closeAssetModel = ()=>{
    this.setState({
      addAssetModel: false
    })
  }
  openSenderModel = ()=>{
    this.setState({
      addSenderModel: true
    })
  }
  closeSenderModel = ()=>{
    this.setState({
      addSenderModel: false
    })
  }
  addMore = ()=>{
    this.setState({
      senderReceiver: [],
    })
      this.props.getSenderReceiver('orders/comsendreceive/');
  }
  refreshData = async (data=null)=>{
    this.setState({
      assetsListDriver: [],
    })
    let payload = "";
    if(data){
      payload = data
    }else {
      payload = {
        time_from: moment(this.state.pickup_date_time),
        time_to: moment(this.state.pickup_date_time_to)
      }
    }

    let json = await PostFetch('asset/listasset/', payload);
    if(json && json.data && json.data.length>0){
      this.setState({
        assetsListDriver: json.data
      })
     }
  }
  componentWillMount(){
    if(this.props.order && this.props.order.order_pick && this.props.order.order_pick[0] && this.props.order.order_pick[0].sender && this.props.order.order_pick[0].sender.id){
      this.setState({
        pick_sender_id: this.props.order.order_pick[0].sender.id
      })
    }
    let x = localStorage.getItem("userConfiguration");
      try{
        x = JSON.parse(x);
        let time_slot = 0;
        x.data.map((e, i)=>{
          if(e.ConfigureID.label === 'time_slot'){
            time_slot = parseInt(e.configure_val)
          }
          return('')
        })
        this.setState({
          timeslot: time_slot
        })
      }catch(e){
      }
    this.props.getSenderReceiver('orders/comsendreceive/');
    if(this.props.assign_pick && this.props.assign_pick.address){
      let pick = this.props.assign_pick;
      if(pick.pick_id){
        this.setState({
          pick_id: parseInt(pick.pick_id)
        })
      }
      this.changeCountry(pick.address);
      this.setState({
        DriverID: this.props.DriverID,
        address: pick.address,
        longitude: pick.longitude,
        latitude: pick.latitude,
        assigned_quantity: pick.assigned_quantity? pick.assigned_quantity: this.props.units? this.props.units: '',
        pickup_date_time: moment(pick.pickup_date_time).utc().format("DD/MM/YY hh:mm A"),
        pickup_date_time_to: moment(pick.pickup_date_time_to).utc().format("DD/MM/YY hh:mm A"),
        assetId: pick.asset
      })
      const payload = {
        time_from: moment(pick.pickup_date_time),
        time_to: moment(pick.pickup_date_time_to)
      }
      setTimeout(
        function() {
          this.refreshData(payload);
        }
        .bind(this),
        500
      );
    }
  }

  sendState=()=>{
    let from = null;
    let to = null;
    let sender = {};
    let carrier_id = this.props.Created_by_CarrierID;
    if(this.state.dateChange){
      from = this.state.pickup_date_time;
      to = this.state.pickup_date_time_to;

    }else if(this.props.assign_pick && this.props.assign_pick.address){
      let pick = this.props.assign_pick;
      from = pick.pickup_date_time;
      to = pick.pickup_date_time_to
    }

    if(this.state.sender && this.state.senderReceiver && this.state.senderReceiver.length>0){
      let senderReceiver = this.state.senderReceiver[this.state.sender];
      if(senderReceiver && senderReceiver.id === 0){
        sender = {
          kid: 0,
          name: senderReceiver.name,
          primary_contact: senderReceiver.primary_contact,
          pri_email_id: senderReceiver.pri_email_id,
          pri_phone: senderReceiver.pri_phone,
          secondary_contact: senderReceiver.secondary_contact,
          sec_email_id: senderReceiver.sec_email_id,
          sec_phone: senderReceiver.sec_phone,
          business_address: senderReceiver.business_address,
          contract_start_date: senderReceiver.contract_start_date,
          contract_end_date: senderReceiver.contract_end_date,
          contract_documents: senderReceiver.contract_documents,
          Customer_code: senderReceiver.Customer_code,
          is_sender : true
        }
      }else if(senderReceiver && senderReceiver.id !== 0){
        sender = {
          kid: senderReceiver.id,
          name: senderReceiver.name,
          is_sender : true
        }
      }
    }

    const payload = {
      sender:sender,
      asset: this.state.asset,
      pick_id: this.state.pick_id,
      address: this.state.address,
      DriverID: this.props.DriverID,
      name: this.props.name,
      truck_type:this.props.type,
      ass_truck_id: parseInt(this.props.ass_truck_id),
      ass_truck_name: this.props.ass_truck_name,
      longitude: this.state.longitude,
      latitude: this.state.latitude,
      assigned_quantity: this.state.assigned_quantity,
      pickup_date_time: from,
      pickup_date_time_to: to,
    //  asset_end: this.state.asset_end,
      Created_by_CarrierID: carrier_id
    }
    this.props.receivePick(this.props.form_key, payload);
  }
  getSuggestions = async (text)=>{
    const data = await APIFetch('https://autocomplete.geocoder.api.here.com/6.2/suggest.json?'+this.state.appdata+'&query='+text+'&country=aus');
    let options = [];
    if(data.suggestions){
       data.suggestions.map((s,i)=>(
        options.push({
          value:s.label,
          label:s.label,
        })
      ));
    }
    this.setState({
      data:options
    });
  }

  changeCountry = async (addr) =>{
      if(addr){
        let url = 'https://geocoder.api.here.com/6.2/geocode.json?searchtext='+addr+'&'+this.state.appdata;
        const data = await APIFetch(url);
        if(data && data.Response && data.Response.View[0] && data.Response.View[0].Result[0] && data.Response.View[0].Result[0].Location){
          let pos = data.Response.View[0].Result[0].Location;
          this.setState({
            latitude:pos.DisplayPosition.Latitude,
            longitude:pos.DisplayPosition.Longitude
          });
          this.sendState();
        }
      }
  }
  putAssetInclude = (id, asset_id, registeration_no, checked, att_detta)=>{
    if(checked){
      let assetId = this.state.assetId;
      let asset = this.state.asset;

      const arrId = assetId.filter((e, i)=>{
        return parseInt(e) === parseInt(id);
      })

      const arr = asset.filter((e, i)=>{
        return parseInt(e.aid) === parseInt(id);
      })

      if(arr && arr.length === 0 && arrId && arrId.length === 0){
        asset.push({
          aid: id,
          is_for: 'add',
          asset_id: asset_id,
          registeration_no: registeration_no
        })
        this.setState({
          asset: asset
        })
      }
    }else {
      let assetId = this.state.assetId;
      const arrId = assetId.filter((e, i)=>{
        return parseInt(e) !== parseInt(id);
      })
      let asset = this.state.asset;
      asset.push({
        aid: id,
        adid: att_detta[0].id,
        is_for: 'remove',
        asset_id: asset_id,
        registeration_no: registeration_no
      })
      this.setState({
        asset: asset,
        assetId: arrId
      })
    }

    setTimeout(function() {
      this.sendState();
    }.bind(this),200);
  }
  putAsset = (id, asset_id, registeration_no, checked)=>{
    if(checked){
      let asset = this.state.asset;
      const arr = asset.filter((e, i)=>{
        return parseInt(e.aid) === parseInt(id);
      })


      if(arr && arr.length === 0){
        asset.push({
          aid: id,
          is_for: 'add',
          asset_id: asset_id,
          registeration_no: registeration_no
        })
        this.setState({
          asset: asset
        })
      }else if (arr && arr.length>0) {
        if(arr[0].is_for === 'remove'){
          const temp = asset;
          asset.map((e, i)=>{
            if(parseInt(temp[i].aid) === parseInt(id)){
              temp[i].is_for = 'add';
            }
            return('')
          })
          this.setState({
            asset: temp
          })
        }
      }
    }else {
      let asset = this.state.asset;
      const temp = asset.filter((e, i)=>{
        return parseInt(e.aid) !== parseInt(id);
      })
      this.setState({
        asset: temp
      })
    }

    setTimeout(function() {
      this.sendState();
    }.bind(this),200);
  }
  getAssetCount = ()=>{
    const temp = this.state.asset.filter((e, i)=>{
      return e.is_for === 'add';
    })
    const ass = this.state.assetId && this.state.assetId.length>0 ? this.state.assetId.length: 0;
    return temp.length + ass;
  }
  render(){
    return(
      <React.Fragment>
      <tr>
      <td style={{width: '80px', overflow:"hidden"}}>
        {this.props.job_id}-{this.props.form_key+1}
      </td>
      <td>
        <input style={{"width":"60px"}} type="text" onChange={(e)=>{
          if(e.target.value && charValidation(e.target.value)){
            this.setState({
              assigned_quantity:e.target.value
            });
          }else {
            this.setState({
              assigned_quantity: ''
            })
          }
        }}  onBlur={(e)=>{
          if(this.state.assigned_quantity){
            this.setState({
              checkQuantity: false
            });
            this.sendState();
          }else{
            this.setState({
              checkQuantity: true
            });
          }
        }} value={this.state.assigned_quantity} className="form-control" />

      </td>
      <td>
        <input list={this.state.datalistId}  style={{"width":"250px"}} type="text" onChange={(e)=>{
          if(e.target.value && charValidation(e.target.value)){
            this.getSuggestions(e.target.value);
            this.setState({
              address:e.target.value,
            });
          }else {
            this.setState({
              address: ''
            })
          }
        }} onBlur={(e)=>{
          if(this.state.address){
            this.changeCountry(this.state.address);
            this.setState({
              checkAddress: false,
              data: [],
            });
            this.sendState();
          }else{
            this.setState({
              checkAddress: true
            });
          }
        }}  value={this.state.address} className="form-control" />
        {this.state.checkAddress?
          <React.Fragment>

            <span className="text-danger"> invalid address</span>
          </React.Fragment>:
          <React.Fragment>
          {this.state.latitude && this.state.longitude?
            <a href="#map" onClick={(e)=>{
              e.preventDefault();
              this.setState({
                showMap: true
              })
            }}>view on map</a>:
          null}
          </React.Fragment>
        }
          <datalist id={this.state.datalistId}>
            <option value="">select</option>
            {this.state.data.length>0?
            this.state.data.map((e, i)=>{
              return(
                <option key={i} value={e.value}>{e.label}</option>
              )
            }):null}
          </datalist>
      </td>

        <td>
        <div className="row no-gutters rowTimeSlot">
            <div className="col-md-6">
            <Datetime
              dateFormat="DD/MM/YY"
              utc={true}
              onChange={(e)=>{
                this.setState({
                  pickup_date_time: e,
                  dateChange: true,
                });
              }} onBlur={(e)=>{
                if(this.state.pickup_date_time){
                  this.setState({
                    checkDateFrom: false,
                    pickup_date_time_to: e.add(this.state.timeslot, 'minutes')
                  });
                  setTimeout(
                    function() {
                      const payload = {
                        time_from: moment(this.state.pickup_date_time),
                        time_to: moment(this.state.pickup_date_time_to)
                      }
                      this.refreshData(payload);
                      this.sendState();
                    }
                    .bind(this),
                    500
                  );
                }else{
                  this.setState({
                    checkDateFrom: true
                  });
                }
              }} value={this.state.pickup_date_time}
              inputProps={{required: true}}
            />
            {this.state.checkDateFrom?
              <React.Fragment>

                <span className="text-danger">invalid datetime</span>
              </React.Fragment>: null}
            </div>
            <div className="col-md-6">
            <Datetime
              dateFormat="DD/MM/YY"
              utc={true}
              onChange={(e)=>{
                this.setState({
                  pickup_date_time_to:e,
                });
              }} onBlur={(e)=>{
                if(this.state.pickup_date_time_to){
                  this.setState({
                    checkDateTo: false,
                  });
                  this.sendState();
                }else{
                  this.setState({
                    checkDateTo: true
                  });
                }
              }} value={this.state.pickup_date_time_to}
              inputProps={{required: true}}
            />
            {this.state.checkDateTo?
              <React.Fragment>

                <span className="text-danger">invalid datetime</span>
              </React.Fragment>: null}
            </div>
        </div>
        </td>
        {this.state.showMap?
        <Map
          address={this.state.address}
          lat={this.state.latitude}
          lng={this.state.longitude}
          close={()=>{
            this.setState({
              showMap: false
            })
          }}
        />: null}
        <td>
          <select style={{"width":"150px"}} onChange={(e)=>{
            if(e.target.value==="addMore"){
              this.openSenderModel();
            }else{
              this.setState({sender:e.target.value});
            }
          }} onBlur={()=>{
              this.sendState();
          }} value={this.state.sender} className="form-control">
            <option value="">select</option>
            {this.state.senderReceiver && this.state.senderReceiver.length>0?
              this.state.senderReceiver.map((e, i)=>{
                if(e.is_sender){
                  return(
                    <option key={i} value={i}>{e.name}</option>
                  )
                }else {
                  return(
                    ''
                  )
                }
              })
            :null}
            <option value="addMore">Add Sender</option>
          </select>
        </td>
        <td>
          <span style={{color: 'blue', fontSize: '16px'}}>{this.getAssetCount()} selected</span><br/>
          <div style={{width: '160px', height: '100px', overflow: 'auto'}}>
          {this.state.assetsListDriver && this.state.assetsListDriver.length>0?
            this.state.assetsListDriver.map((e, i)=>{
              if(this.state.assetId && this.state.assetId.length>0 && this.state.assetId.includes(e.id)){
                return(
                  <div key={i}><input type="checkbox" checked="checked" style={{fontSize: '16px'}} onChange={(el)=>{
                    console.log(e);
                    this.putAssetInclude(e.id, e.asset_id, e.registeration_no, el.target.checked, e.att_detta)
                  }} value={e.id} />{e.asset_id}-{e.registeration_no}</div>
                )
              }else if(this.state.asset && this.state.asset.length>0){
                let flg = false;
                this.state.asset.map((el, i)=>{
                  if(el.id === e.id){
                    flg = true;
                  }
                  return('')
                })
                if(flg){
                  return(
                    <div key={i}><input type="checkbox" checked="checked" style={{fontSize: '16px'}} onChange={(el)=>{
                      this.putAsset(e.id, e.asset_id, e.registeration_no, el.target.checked)
                    }} value={e.id} />{e.asset_id}-{e.registeration_no}</div>
                  )
                }
              }
              return(
                <div key={i}><input type="checkbox" style={{fontSize: '16px'}} onChange={(el)=>{
                  this.putAsset(e.id, e.asset_id, e.registeration_no, el.target.checked)
                }} value={e.id} />{e.asset_id}-{e.registeration_no}</div>
              )
            })
          :null}
          </div>

          <a href="#a" onClick={(e)=>{
            e.preventDefault();
            this.openAssetModel();
          }}>Add New Asset</a>
        </td>

        <td>
          <a href="#a" onClick={(e)=>{
            e.preventDefault();
            this.props.deletePickDrop(this.props.form_key, "assign_pick")
          }}><i className="text-danger far fa-trash-alt"></i></a>
          {this.state.addSenderModel?
            <AddSender
              closeOneModel={this.closeSenderModel}
              addMore={this.addMore}
              />
          :null}
          {this.state.addAssetModel?
            <AddAssets
              closeOneModel={this.closeAssetModel}
              refreshData={this.refreshData}
            />
          :null}
        </td>

      </tr>

      </React.Fragment>
    )
  }
}

AddDriverPick.propTypes = {
  getSenderReceiver: PropTypes.func.isRequired,
  pushSenderReceiver: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  senderReceiver: state.carrier.senderReceiver,
});

export default connect(mapStateToProps, {getSenderReceiver, pushSenderReceiver})(AddDriverPick);
