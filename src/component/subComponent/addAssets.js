import React, {Component} from 'react';
import PostFetch from '../ajax/postFetch';
import {successAlert, errorAlert} from '../include/alert';
import Datetime from 'react-datetime';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getAssets, setAssetNo, editAssetGet} from '../../actions/carrierActions';
import store from '../../store';
import moment from 'moment';
import {charValidation} from '../include/validation';

class AddAssets extends Component{
  constructor(props){
    super(props);
    this.state = {
      id: 0,
      asset_id: '',
      asset_type: '',
      make: '',
      asset: '',
      model: '',
      configuration: '',
      vin_chassis: '',
      engine_no: '',
      build_date: '',
      e_tag_id: '',
      gps_id: '',
      dangerous_good_compliance: '',
      dangerous_good_license_no: '',
      registeration_no: '',
      registeration_due_date: '',
      state: '',
      asset_status: '',
      odometer: '',
      maintenance_intervals: '',
      maintenance_type: '',
      asset_capacity: '',
      gross_weight: '',
      tare_weight: '',
      purchase_cost: '',
      ownership_type: '',
      financed_term: '',
      hourly_cost: '',
      submitDis: false,
      btnLabel: 'Add Asset',
      assetLeave: false
    }
    this.formSubmit = this.formSubmit.bind(this);
  }
  componentWillMount(){
    if(this.props.asset_type_field){
      this.setState({
        asset_type: this.props.asset_type_field
      })
    }
    const state = store.getState();
    if(state.carrier.editAsset && state.carrier.editAsset.id){
      let prevAsset = state.carrier.editAsset;
      this.setState({
        btnLabel: 'Update Asset',
        id: prevAsset.id,
        asset_id: prevAsset.asset_id,
        asset_type: prevAsset.asset_type,
        make: prevAsset.make,
        asset: prevAsset.asset,
        model: prevAsset.model,
        configuration: prevAsset.configuration,
        vin_chassis: prevAsset.vin_chassis,
        engine_no: prevAsset.engine_no,
        build_date: prevAsset.build_date? moment(prevAsset.build_date): '',
        e_tag_id: prevAsset.e_tag_id,
        gps_id: prevAsset.gps_id,
        dangerous_good_compliance: prevAsset.dangerous_good_compliance,
        dangerous_good_license_no: prevAsset.dangerous_good_license_no,
        registeration_no: prevAsset.registeration_no,
        registeration_due_date: prevAsset.registeration_due_date? moment(prevAsset.registeration_due_date): '',
        state: prevAsset.state,
        asset_status: prevAsset.asset_status,
        odometer: prevAsset.odometer,
        maintenance_intervals: prevAsset.maintenance_intervals,
        maintenance_type: prevAsset.maintenance_type,
        asset_capacity: prevAsset.asset_capacity,
        gross_weight: prevAsset.gross_weight,
        tare_weight: prevAsset.tare_weight,
        purchase_cost: prevAsset.purchase_cost,
        ownership_type: prevAsset.ownership_type,
        financed_term: prevAsset.financed_term,
        hourly_cost: prevAsset.hourly_cost,
        is_attach: prevAsset.is_attach
      })
    }
  }
  componentWillReceiveProps(nextProps){

  }
  formSubmit = async () =>{
    if(this.state.asset_id && this.state.asset_type && this.state.configuration){
      this.setState({
        submitDis: true
      })
      const payload = {
        id: this.state.id,
        asset_id: this.state.asset_id,
        asset_type: this.state.asset_type,
        make: this.state.make,
        asset: this.state.asset,
        model: this.state.model,
        configuration: this.state.configuration,
        vin_chassis: this.state.vin_chassis,
        engine_no: this.state.engine_no,
        build_date: this.state.build_date,
        e_tag_id: this.state.e_tag_id,
        gps_id: this.state.gps_id,
        dangerous_good_compliance: this.state.dangerous_good_compliance,
        dangerous_good_license_no: this.state.dangerous_good_license_no,
        registeration_no: this.state.registeration_no,
        registeration_due_date: this.state.registeration_due_date,
        state: this.state.state,
        asset_status: this.state.asset_status,
        odometer: this.state.odometer,
        maintenance_intervals: this.state.maintenance_intervals,
        maintenance_type: this.state.maintenance_type,
        asset_capacity: this.state.asset_capacity,
        gross_weight: this.state.gross_weight,
        tare_weight: this.state.tare_weight,
        purchase_cost: this.state.purchase_cost,
        ownership_type: this.state.ownership_type,
        financed_term: this.state.financed_term,
        hourly_cost: this.state.hourly_cost,
        is_attach: this.state.is_attach
      }
      let json = await PostFetch('asset/createasset/', payload);
      if(json && json.message){
        if(this.props.refreshData){
          this.props.refreshData();
        }
      //  this.props.getAssets('asset/listasset/');
        this.props.setAssetNo(json.data);
        successAlert(json.message);
        this.setState({
          id: 0,
          asset_id: '',
          asset_type: '',
          make: '',
          asset: '',
          model: '',
          configuration: '',
          vin_chassis: '',
          engine_no: '',
          build_date: '',
          e_tag_id: '',
          gps_id: '',
          dangerous_good_compliance: '',
          dangerous_good_license_no: '',
          registeration_no: '',
          registeration_due_date: '',
          state: '',
          asset_status: '',
          odometer: '',
          maintenance_intervals: '',
          maintenance_type: '',
          asset_capacity: '',
          gross_weight: '',
          tare_weight: '',
          purchase_cost: '',
          ownership_type: '',
          financed_term: '',
          hourly_cost: '',
          submitDis: false
        });
        this.props.closeOneModel();
      }else if(json && json.message){
        successAlert(json.message);
        this.setState({
          submitDis: false
        })
      }else{
        errorAlert('try again');
        this.setState({
          submitDis: false
        })
      }
    }
  }
  closeConfirmation = ()=>{
    if(this.state.assetLeave){
      var r = window.confirm("You have unsaved changes, are you sure you want to leave?");
      if (r === true) {
        this.props.closeOneModel();
      }
    }else {
      this.props.closeOneModel();
    }
  }
  render(){
    return(
      <div className="modal show popupAssignDriver model-inviteDriverFront modelInviteDriver" style={{display: "block"}} id="modelAddAssetsManager" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <a href="#a" onClick={(e)=>{
                e.preventDefault();
                this.closeConfirmation();
              }} className="btnClose" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </a>
                <div className="row rowAssign align-items-center">
                  <div className="col-md-12">
                    <h4 className="pb-0 mb-0">Asset</h4>
                  </div>
                </div>
      <div className="boxComman">
              <div className="form-group">
                  <label>Asset Id <span className="text-danger">*</span></label>
                  <input type="text" required onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({asset_id:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        asset_id: ''
                      })
                    }
                  }} value={this.state.asset_id} className="form-control" placeholder="Enter asset id"/>
              </div>
              <div className="form-group">
                  <label>Asset Type <span className="text-danger">*</span></label>
                  <input type="text" required onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({asset_type:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        asset_type: ''
                      })
                    }
                  }} value={this.state.asset_type} className="form-control" placeholder="Enter asset type"/>
              </div>
              <div className="form-group">
                  <label>Make</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({make:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        make: ''
                      })
                    }
                  }} value={this.state.make} className="form-control" placeholder="Enter make"/>
              </div>
              <div className="form-group">
                  <label>Configuration <span className="text-danger">*</span></label>
                  <input type="text" required onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({configuration:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        configuration: ''
                      })
                    }
                  }} value={this.state.configuration} className="form-control" placeholder="Enter configuration"/>
              </div>
              <div className="form-group">
                  <label>VIN/Chassis</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({vin_chassis:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        vin_chassis: ''
                      })
                    }
                  }} value={this.state.vin_chassis} className="form-control" placeholder="Enter VIN/Chassis"/>
              </div>
              <div className="form-group">
                  <label>Engine No</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({engine_no:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        engine_no: ''
                      })
                    }
                  }} value={this.state.engine_no} className="form-control" placeholder="Enter engine no"/>
              </div>
              <div className="form-group">
                  <label>Build date</label>
                  <Datetime
                    dateFormat="DD/MM/YY"
                    timeFormat={false}
                    onChange={(e)=>{
                      this.setState({
                        build_date: e,
                        assetLeave: true
                      });
                    }} value={this.state.build_date}
                    required
                  />
              </div>
              <div className="form-group">
                  <label>E-Tag Id</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({e_tag_id:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        e_tag_id: ''
                      })
                    }
                  }} value={this.state.e_tag_id} className="form-control" placeholder="Enter E-Tag id"/>
              </div>
              <div className="form-group">
                  <label>GPS Id</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({gps_id:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        gps_id: ''
                      })
                    }
                  }} value={this.state.gps_id} className="form-control" placeholder="Enter gps id"/>
              </div>
              <div className="form-group">
                  <label>Dangerous Good License No</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({dangerous_good_license_no:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        dangerous_good_license_no: ''
                      })
                    }
                  }} value={this.state.dangerous_good_license_no} className="form-control" placeholder="Enter dangerous good license no"/>
              </div>
              <div className="form-group">
                  <label>Dangerous Good Compliance</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({dangerous_good_compliance:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        dangerous_good_compliance: ''
                      })
                    }
                  }} value={this.state.dangerous_good_compliance} className="form-control" placeholder="Enter dangerous good compliance"/>
              </div>
              <div className="form-group">
                  <label>Registeration No <span className="text-danger">*</span></label>
                  <input type="text" required onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({registeration_no:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        registeration_no: ''
                      })
                    }
                  }} value={this.state.registeration_no} className="form-control" placeholder="Enter registeration no"/>
              </div>
              <div className="form-group">
                  <label>Registeration Due Date</label>
                  <Datetime
                    dateFormat="DD/MM/YY"
                    timeFormat={false}
                    onChange={(e)=>{
                      this.setState({
                        registeration_due_date: e,
                        assetLeave: true
                      });
                    }} value={this.state.registeration_due_date}
                    required
                  />
              </div>
              <div className="form-group">
                  <label>State</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({state:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        state: ''
                      })
                    }
                  }} value={this.state.state} className="form-control" placeholder="Enter state"/>
              </div>
              <div className="form-group">
                  <label>Asset Status</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({asset_status:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        asset_status: ''
                      })
                    }
                  }} value={this.state.asset_status} className="form-control" placeholder="Enter asset status"/>
              </div>
              <div className="form-group">
                  <label>Odo Meter</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({odometer:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        odometer: ''
                      })
                    }
                  }} value={this.state.odometer} className="form-control" placeholder="Enter Odo meter"/>
              </div>
              <div className="form-group">
                  <label>Maintenance Intervals</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({maintenance_intervals:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        maintenance_intervals: ''
                      })
                    }
                  }} value={this.state.maintenance_intervals} className="form-control" placeholder="Enter maintenance intervals"/>
              </div>
              <div className="form-group">
                  <label>Maintenance Type</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({maintenance_type:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        maintenance_type: ''
                      })
                    }
                  }} value={this.state.maintenance_type} className="form-control" placeholder="Enter maintenance type"/>
              </div>
              <div className="form-group">
                  <label>Asset Capacity</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({asset_capacity:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        asset_capacity: ''
                      })
                    }
                  }} value={this.state.asset_capacity} className="form-control" placeholder="Enter asset capacity"/>
              </div>
              <div className="form-group">
                  <label>Gross Weight</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({gross_weight:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        gross_weight: ''
                      })
                    }
                  }} value={this.state.gross_weight} className="form-control" placeholder="Enter gross weight"/>
              </div>
              <div className="form-group">
                  <label>Tare Weight</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({tare_weight:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        tare_weight: ''
                      })
                    }
                  }} value={this.state.tare_weight} className="form-control" placeholder="Enter tare weight"/>
              </div>
              <div className="form-group">
                  <label>Purchase Cost</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({purchase_cost:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        purchase_cost: ''
                      })
                    }
                  }} value={this.state.purchase_cost} className="form-control" placeholder="Enter purchase cost"/>
              </div>
              <div className="form-group">
                  <label>Ownership Type</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({ownership_type:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        ownership_type: ''
                      })
                    }
                  }} value={this.state.ownership_type} className="form-control" placeholder="Enter ownership type"/>
              </div>
              <div className="form-group">
                  <label>Financed Term</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({financed_term:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        financed_term: ''
                      })
                    }
                  }} value={this.state.financed_term} className="form-control" placeholder="Enter financed term"/>
              </div>
              <div className="form-group">
                  <label>Hourly Cost</label>
                  <input type="text" onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({hourly_cost:e.target.value, assetLeave: true})
                    }else {
                      this.setState({
                        hourly_cost: ''
                      })
                    }
                  }} value={this.state.hourly_cost} className="form-control" placeholder="Enter hourly cost"/>
              </div>



              <div className="form-group text-center">
                {this.state.submitDis?
                  <input type="button" title={this.state.btnLabel} disabled value={this.state.btnLabel}/>
                :<input type="button" onClick={this.formSubmit} title={this.state.btnLabel} value={this.state.btnLabel}/>
                }
                <input type="button" style={{marginLeft: '15px'}} value="Close" onClick={()=>{
                    this.closeConfirmation();
                }}/>
              </div>

      </div>
      </div>
    </div>
  </div>
</div>
  );
  }
}


AddAssets.propTypes = {
  getAssets: PropTypes.func.isRequired,
  setAssetNo: PropTypes.func.isRequired,
  editAssetGet: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  assetsList: state.carrier.assetsList,
  editAsset: state.carrier.editAsset,
});

export default connect(mapStateToProps, {getAssets, setAssetNo, editAssetGet})(AddAssets);
