import React, {Component} from 'react';
import PostFetch from '../ajax/postFetch';
import GetFetch from '../ajax/getFetch';
//import makeid from './makeid';
import {successAlert, errorAlert} from '../include/alert';
import {verifyEmail, verifyNumber} from '../include/validation';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getAllDrivers} from '../../actions/carrierActions';
import {charValidation} from '../include/validation';

class InviteDriver extends Component{
  constructor(props){
    super(props);
    this.state = {
      driver_name:'',
      vehicle_rego:'',
      vehicle_type:'',
      driver_lic_no:'',
      driver_phone_no:'',
      email:'',
      phoneError: '',
      flg:0,
      drivers: [],
      submitDis: false,
      error_email: ' ',
    }
    this.formSubmit = this.formSubmit.bind(this);
  }
  getDriverList = async ()=>{
    let json = await GetFetch('truck_categ/truck_categ_list/');
    if(json && json.data){
      if(json.data.length>0){
        let drivers = [];
        json.data.map((e, i)=>{
          let arr = [];
          if(e.parent_id===null){
            arr.push(e);
            json.data.map((f, j)=>{
              if(e.categ_name===f.parent_id){
                arr.push(f);
              }
              return(
              ''
              )
            });
            drivers.push({
              'parent': arr
            });
          }
          return(
          ''
          )
        });
        this.setState({
          drivers: drivers,
          truck_id: drivers[0]['parent'][0].id,
          vehicle_type: drivers[0]['parent'][0].categ_name
        });
      }
    }
  }
  componentWillMount(){
    this.getDriverList();
  }

  checkPhoneNumber= async (phone)=>{
    let json = await verifyNumber(phone);
    this.setState({
      phoneError: json.error,
      flg: json.flg
    });
  }

  formSubmit = async () =>{
    if(this.state.driver_name && this.state.email && this.state.driver_phone_no && this.state.vehicle_rego && this.state.driver_lic_no && this.state.flg===0 && this.state.phoneError==='' && this.state.error_email===''){
      this.setState({
        submitDis: true
      })
      const payload = {
      	user_name: this.state.driver_name,
        email: this.state.email,
      	phone: this.state.driver_phone_no,
      	password: '123',
      	status: "driver pending",
      	truck_id: this.state.truck_id,
      	truck_reg_num: this.state.vehicle_rego,
      	license_num: this.state.driver_lic_no,
      }
      let json = await PostFetch('/driv/driv_invite_user/', payload);
      if(json && json.message==="Check Inbox and Spam folder for verification e-mail"){
        successAlert("Your Invite has been sent to "+this.state.driver_name);
        this.setState({
          driver_name:'',
          vehicle_rego:'',
          vehicle_type:'',
          driver_lic_no:'',
          driver_phone_no:'',
          email:'',
          flg:0,
          submitDis: false
        });
        this.props.getAllDrivers('driv/driver-list-status/?status=all');
        this.props.closeOneModel();
      }else if(json && json.message){
        successAlert(json.message);
        this.setState({
          submitDis: false
        })
        this.props.closeOneModel();
      }else{
        errorAlert('try again');
        this.setState({
          submitDis: false
        })
      }
    }
  }
  render(){
    return(
      <div style={{display: 'block', "zIndex":"99999"}} className="modal show popupAssignDriver model-inviteDriverFront modelInviteDriver" id="modelInviteDriver" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-body">
              <a href="#a" onClick={(e)=>{
                e.preventDefault()
                this.props.closeOneModel();
              }} className="btnClose" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </a>
                <div className="row rowAssign align-items-center">
                  <div className="col-md-12">
                    <h4 className="pb-0 mb-0">Invite Driver</h4>
                  </div>
                </div>
                <div className="boxComman">

                        <div className="form-group">
                            <label>Driver Name <span className="text-danger">*</span></label>
                            <input type="text" required onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.setState({driver_name:e.target.value})
                              }else {
                                this.setState({
                                  driver_name: ''
                                })
                              }
                            }} value={this.state.driver_name} className="form-control" placeholder="Enter Driver Name"/>
                        </div>
                        <div className="form-group">
                            <label>Driver’s Email id <span className="text-danger">*</span></label>
                            <input type="text" required onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.setState({email:e.target.value})
                              }else {
                                this.setState({
                                  email: ''
                                })
                              }
                            }} onBlur={ async (e)=>{
                              let data = await verifyEmail(e.target.value);
                              if(data){
                                if(data.error!==""){
                                  this.setState({
                                    error_email: data.error
                                  })
                                }else if(data.data==="Driver" || data.data===""){
                                  this.setState({
                                    error_email: ''
                                  })
                                }else if(data.data!=="Driver" && data.data!==""){
                                  this.setState({
                                    error_email: 'Already registered as '+data.data
                                  })
                                }
                              }
                            }} value={this.state.email} className="form-control" placeholder="Enter Driver’s Email id"/>
                            <span className="text-danger">{this.state.error_email}</span>
                        </div>
                        <div className="form-group">
                            <label>Driver’s Phone Number <span className="text-danger">*</span></label>
                            <input type="text" required onChange={(e)=>{
                              this.setState({driver_phone_no:e.target.value})
                            }} onBlur={(e)=>{
                              this.checkPhoneNumber(e.target.value);
                            }} value={this.state.driver_phone_no} className="form-control" placeholder="Enter Driver’s Phone Number"/>
                            <span className="text-danger">{this.state.phoneError}</span>
                        </div>
                        <div className="form-group">
                            <label>Vehicle Rego <span className="text-danger">*</span></label>
                            <input type="text" required onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.setState({vehicle_rego:e.target.value})
                              }else {
                                this.setState({
                                  vehicle_rego: ''
                                })
                              }
                            }} value={this.state.vehicle_rego} className="form-control" placeholder="Enter Vehicle Rego"/>
                        </div>

                        <div className="form-group">
                            <label>Driver’s License Number <span className="text-danger">*</span></label>
                            <input type="text" onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.setState({driver_lic_no:e.target.value})
                              }else {
                                this.setState({
                                  driver_lic_no: ''
                                })
                              }
                            }} value={this.state.driver_lic_no} className="form-control" placeholder="Enter Driver’s License Number"/>
                        </div>
                        <div className="form-group text-center">
                          {this.state.submitDis?
                            <input type="button" title="Send Invite" disabled value="Send Invite"/>
                          :<input type="button" onClick={this.formSubmit} title="Send Invite" value="Send Invite"/>
                          }
                        </div>

                </div>
                </div>
              </div>
            </div>
          </div>
  );
  }
}

InviteDriver.propTypes = {
  getAllDrivers: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  allDrivers: state.carrier.allDrivers,
});

export default connect(mapStateToProps, {getAllDrivers})(InviteDriver);


/*<div className="form-group">
    <label htmlFor="inputState">Vehicle Type <span className="text-danger">*</span></label>
    <select required onChange={(e)=>{
      if(e.target.value!==""){
        let x = e.target.value.split('@');
        this.setState({
          truck_id: x[0],
          vehicle_type:x[1]
        });
      }
    }} value={`${this.state.truck_id}@${this.state.vehicle_type}`} id="inputState" className="form-control">
        <option value="">Select Vehicle Type <span className="text-danger">*</span></option>
        {this.state.drivers.length>0?
          this.state.drivers.map((e, i)=>(
            e.parent.map((f, j)=>{
              let id1 = makeid();
              let id2 = makeid();
              if(f.is_last){
                return(
                  <option key={id2} className="op_act" value={`${f.id}@${f.categ_name}`}>{f.categ_name}</option>
                )
              }else {
                return(
                  <option className="op-dis" key={id1} disabled>{f.categ_name}</option>
                )
              }
            })
          )):null}

    </select>
</div>*/
