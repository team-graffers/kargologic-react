import React, {Component} from 'react';
import PostFetch from '../ajax/postFetch';
import {successAlert, errorAlert} from '../include/alert';
import moment from 'moment';

class OrderComment extends Component{
  constructor(props) {
    super(props);
    this.state = {
      order_id: '',
      comment: '',
      error: '',
    }
  }
  componentWillMount(){
    this.setState({
      order_id: parseInt(this.props.order_id)
    })
  }
  doComment = async ()=>{
    if(this.state.comment){
      let date = moment().format();
      date = date.split('+')
      const payload = {
        order_id: this.state.order_id,
        comment: this.state.comment,
        date_time_of_create: date[0]+'.000Z',
      }
      let json = await PostFetch('carr/order_comment/?user_type='+this.props.userType, payload);
      if(json && json.message){
        successAlert(json.message);
        this.setState({
          comment: '',
          error: ''
        });
        this.props.getComments();
        this.closeOneModel('modelOrderComment');
      }else{
        errorAlert('try again!')
      }
    }
  }
  closeOneModel=(modalId)=>{
    const modal = document.getElementById(modalId);
    modal.classList.remove('show');
    modal.setAttribute('aria-hidden', 'true');
    modal.setAttribute('style', 'display: none');
    try{
      const modalBackdrops = document.getElementsByClassName('modal-backdrop');
      document.body.removeChild(modalBackdrops[0]);
    }catch(e){

    }
  }
  render(){
    return(
      <div className="modal fade modelSecondary" id="modelOrderComment" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                  <div className="modal-header">
                      <h4 className="modal-title" id="exampleModalCenterTitle">Add Comment</h4>
                      <button style={{position: 'relative'}} type="button" onClick={(e)=>{
                        this.closeOneModel('modelOrderComment');
                      }} className="btnClose" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div className="modal-body">
                  <div className="innerInviteCustomer">
                    <div className="form-group">
                      <label>Order Comment</label>
                      <textarea value={this.state.comment} onChange={(e)=>{
                        if(e.target.value && e.target.value.length<100){
                          this.setState({
                            comment: e.target.value
                          })
                        }else {
                          this.setState({
                            comment: ''
                          })
                        }
                      }}className="form-control"></textarea>
                      <span className="text-danger">{this.state.error}</span>
                    </div>
                    <p className="text-center mb-0"><a href="#comment" onClick={(e)=>{
                      e.preventDefault();
                      this.doComment()
                    }} className="btn">Save</a></p>
                  </div>
                  </div>
              </div>
          </div>
      </div>
    )
  }
}

export default OrderComment;
