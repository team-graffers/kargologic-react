import React, {Component} from 'react';
import upload from '../../assets/img/iconUpload.png';
import FileDrop from 'react-file-drop';
import UploadExcelAfter from './uploadExcelAfterASR';
import UploadExcelBefore from './uploadExcelBeforeASR';
import store from '../../store';

class UploadExcelASR extends Component{
  constructor(props) {
    super(props);
    this.state = {
      files: [],
      selectedFiles: [],
      upload: false,
      header: this.props.name,
      carr: JSON.parse(localStorage.getItem("userDetails")),
    }
  }
  handleDrop = (files, event) => {
    if(files.length>0){
      this.setState({
        selectedFiles: files,
      })
    }
  }
  deleteFile = (i)=>{
    let files = Array.from(this.state.selectedFiles);
    files.splice(i,1);
    this.setState({
      selectedFiles: files
    });
  }
  uploadFiles=()=>{
    let files = this.state.files;
    files.push(...this.state.selectedFiles);
    this.setState({
      files: files,
      selectedFiles: [],
      upload: true
    });
  }
  render(){
    const state = store.getState();
    return(
      <div style={{display: 'block'}} className="modal show popupAssignDriver modelInviteDriver" id="uploadexcelasr" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
          <div className="modal-dialog" role="document">
              <div className="modal-content">
                  <div className="modal-body">
                      <a href="#a"  className="btnClose" onClick={(e)=>{
                        e.preventDefault();
                        this.props.closeModel();
                      }}>
                          <span aria-hidden="true">&times;</span>
                      </a>
                      <div className="row rowAssign align-items-center">
                          <div className="col-md-12">
                              <h4 className="pb-0 mb-0">{this.state.header}</h4>
                          </div>
                      </div>
                      <div className="mainDragDrop">
                          <FileDrop className="boxDragDrop" onDrop={this.handleDrop}>
                              <div className="boxInput">
                              {this.state.selectedFiles.length===0?
                                <React.Fragment>
                                  <div className="boxIcon"><img src={upload} alt="" title="" /></div>
                                  <input type="file" name="filess[]" onChange={(e)=>{
                                    if(e.target.files.length>0){
                                      this.setState({
                                        selectedFiles: e.target.files,
                                        upload: false
                                      })
                                    }
                                  }} id="files" className="boxFile" data-multiple-caption="{count} files selected" multiple />
                                  <label htmlFor="files">Drag and drop to upload <span>or <strong>browse</strong> to choose a file.</span></label>
                                </React.Fragment>
                                :<h4 style={{"color":"#4c6476"}}>{this.state.selectedFiles.length} files selected</h4>}
                                <a href="#attachments" onClick={(e)=>{
                                  e.preventDefault();
                                  this.uploadFiles()
                                }} className="boxButton btn btn-sm">Upload File</a>
                              </div>
                              <div className="boxUploading">Uploading&hellip;</div>
                              <div className="boxSuccess">Done! <a href="#a" className="boxRestart" role="button">Upload more?</a></div>
                              <div className="boxError">Error! <span></span>. <a href="#a" className="boxRestart" role="button">Try again!</a></div>
                              <p className="mb-0"><a href={`${state.carrier.mainUrl}${this.props.sampleUrl}`} download>Click here</a> to download sample Excel Format</p>
                          </FileDrop>

                      </div>



                      <div className="uploadStatus">
                              <ul className="listuploadStatus">
                                {this.state.selectedFiles.length>0?
                                  Array.from(this.state.selectedFiles).map((e, i)=>(
                                    <UploadExcelBefore key={i} i={i} file={e} deleteFile={this.deleteFile} />
                                  ))
                                :null}
                                {this.state.files.length>0?
                                  Array.from(this.state.files).map((e, i)=>(
                                    <UploadExcelAfter key={i} refreshData={this.props.refreshData} fetchUrl={this.props.fetchUrl} i={i} file={e} deleteFile={this.deleteFile} />
                                  ))
                                :null}
                              </ul>
                      </div>
                      <div className="text-center">
                        <a href="#attachments" onClick={(e)=>{
                          this.props.closeModel();
                        }} className="boxButton btn btn-sm" style={{marginRight: '35px'}}>Save Attachments</a>
                        <a href="#attachments" onClick={(e)=>{
                          e.preventDefault();
                          this.props.closeModel();
                        }} className="boxButton btn btn-sm">Close</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    )
  }
}


export default UploadExcelASR;
