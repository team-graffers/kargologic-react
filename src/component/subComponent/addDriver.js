import React, {Component} from 'react';
import AddDriverPick from './addDriverPick';
import AddDriverDrop from './addDriverDrop';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getAssets, getSenderReceiver, getAllDrivers} from '../../actions/carrierActions';
import makeid from './makeid';
import {successAlert} from '../include/alert';
import GetFetch from '../ajax/getFetch';
import AddAssets from './addAssets';
import InviteDriver from '../subComponent/inviteDriver';

class AddDriver extends Component{
  constructor(props){
    super(props);
    this.state = {
      assign_pick: [],
      assign_drop: [],
      job_id: this.props.job_id,
      DriverID: '',
      type: '',
      name: '',
      driver:'',
      display: false,
      ass_truck_list: [],
      ass_truck_id: '',
      ass_truck_name: '',
      addAssetModel: false,
      driverInviteModel: false,
    }
    this.splitFunc = this.splitFunc.bind(this);
  }
  openDriverModel = () =>{
    this.setState({
      driverInviteModel: true
    })
  }
  closeDriverModel = () =>{
    this.setState({
      driverInviteModel: false
    })
  }
  getAssTruckList = async ()=>{
    let json = await GetFetch('asset/trucklist/');
    if(json && json.data && json.data.length>0){
      this.setState({
        ass_truck_list: json.data
      })
    }
  }
  openAssetModel = ()=>{
    this.setState({
      addAssetModel: true
    })
  }
  closeAssetModel = ()=>{
    this.setState({
      addAssetModel: false
    })
  }
  openOneModel=(modalId)=>{
    try{
      const modal = document.getElementById(modalId);
      modal.classList.add('show');
      //modal.setAttribute('aria-hidden', 'true');
      modal.setAttribute('style', 'display: block');

      const modalBackdrops = document.getElementsByClassName('modal-backdrop');
      document.body.appendChild(modalBackdrops[0]);
    }catch(e){

    }
  }
  closeOneModel=(modalId)=>{
    try{
      const modal = document.getElementById(modalId);
      modal.classList.remove('show');
      modal.setAttribute('aria-hidden', 'true');
      modal.setAttribute('style', 'display: none');

      const modalBackdrops = document.getElementsByClassName('modal-backdrop');
      document.body.removeChild(modalBackdrops[0]);
    }catch(e){

    }
  }
  receivePick =(key, data)=>{
    let pick = this.state.assign_pick;
    pick[key] = data;
    this.setState({
      assign_pick: pick
    });
    this.props.assignPick(this.props.form_key, this.state.assign_pick);
  }
  receiveDrop =(key, data)=>{
    let drop = this.state.assign_drop;
    drop[key] = data;
    this.setState({
      assign_drop: drop
    });
    this.props.assignDrop(this.props.form_key, this.state.assign_drop);
  }
  newPick=(pick='')=>{
    let data = this.state.assign_pick;
    if(pick && pick.length>0){
      pick.map((e,i)=>{
        let aid = [];
        if(e.asset && e.asset.length>0){
          e.asset.map((e, i)=>{
            if(e.id){
              aid.push(e.id)
            }else if (e.aid) {
              aid.push(e.aid)
            }
            return('')
          })
        }
        data.push({
          DriverID: this.props.drivers.driver_id,
          address: e.address,
          pick_id: e.pick_id,
          assigned_quantity: e.assigned_quantity,
          latitude: e.longitude,
          longitude: e.longitude,
          pickup_date_time: e.pickup_date_time,
          pickup_date_time_to: e.pickup_date_time_to,
          collapseID: '',
          sender: e.sender && e.sender.id? e.sender.id: '',
          asset: aid
        });
        return(
          ''
        )
      })
    }else{
      if(this.props.address && this.state.assign_pick.length===0){
        data.push({
          address: this.props.address.pickup_location,
          pickup_date_time: this.props.address.pickup_date_time,
          pickup_date_time_to: this.props.address.pickup_date_time_to,
          sender: this.props.order && this.props.order[0] && this.props.order[0].pickup && this.props.order[0].pickup[0].sender && this.props.order[0].pickup[0].sender.kid? this.props.order[0].pickup[0].sender.kid: ''
        });
      }else{
        data.push({});
      }
    }

    this.setState({
      assign_pick: data,
    });
  }
  newDrop=(drop='')=>{
    let data = this.state.assign_drop;
    if(drop && drop.length>0){
      drop.map((e, i)=>{
        let aid = [];
        if(e.asset && e.asset.length>0){
          e.asset.map((e, i)=>{
            if(e.id){
              aid.push(e.id)
            }else if (e.aid) {
              aid.push(e.aid)
            }
            return('')
          })
        }
        data.push({
          DriverID: this.props.drivers.driver_id,
          address: e.address,
          drop_id: e.drop_id,
          assigned_quantity: e.assigned_quantity,
          latitude: e.longitude,
          longitude: e.longitude,
          delivery_date_time: e.delivery_date_time,
          delivery_date_time_to: e.delivery_date_time_to,
          receiver: e.receiver && e.receiver.id? e.receiver.id: '',
          asset: aid
        });
        return(
          ''
        )
      })
    }else{
      if(this.props.address && this.state.assign_drop.length===0){
        data.push({
          address: this.props.address.delivery_location,
          delivery_date_time: this.props.address.delivery_date_time,
          delivery_date_time_to: this.props.address.delivery_date_time_to,
          receiver: this.props.order && this.props.order[0] && this.props.order[0].delivery && this.props.order[0].delivery[0].receiver && this.props.order[0].delivery[0].receiver.kid? this.props.order[0].delivery[0].receiver.kid: ''
        });
      }else{
        data.push({});
      }
    }
    this.setState({
      assign_drop: data,
    });
  }

  makeid =()=>{
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (let i = 0; i < 6; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  componentWillMount(){
    this.setState({
      collapseID: makeid(),
    })
    this.getAssTruckList();
  //  this.props.getSenderReceiver('orders/comsendreceive/');
    if(this.props.drivers){
      this.setState({
        DriverID: this.props.drivers.driver_id,
        name: this.props.drivers.name,
        driver: this.props.drivers.driver_id+"-"+this.props.drivers.truck_type+"-"+this.props.drivers.name,
        type: this.props.drivers.truck_type,
        ass_truck_id: this.props.drivers.ass_truck_id,
        ass_truck_name: this.props.drivers.ass_truck_name
      });
      this.newPick(this.props.drivers.pickup);
      this.newDrop(this.props.drivers.delivery);
    }
  //  this.props.getAllDrivers('driv/driver-list-status/?status=all');
  }
  splitFunc = async (driver)=>{
    let d = driver.split('-');
    this.setState({
      DriverID: d[0],
      type: d[1],
      name: d[2],
      driver: driver,
    });
  }
  deletePickDrop = (i, obj)=>{
    let x = this.state[obj];
    delete x[i];
    this.setState({
      [obj]: x
    })
  }
  render(){
    //{d.status.status_name==="pending"? '(Pending)': null}
    let driverList = null;
    if(this.props.allDrivers && this.props.allDrivers.data && this.props.allDrivers.data.driver_list && this.props.allDrivers.data.driver_list.length>0){
    driverList = this.props.allDrivers.data.driver_list.map((d, i)=>(
      <option key={i} value={`${d.DriverID.id}-${d.DriverID.TruckType.truck_category.categ_name}-${d.DriverID.name}`}>{d.DriverID.name}</option>
    ));
    }
    return(
      <React.Fragment>
      {this.state.driverInviteModel?
        <InviteDriver closeOneModel={this.closeDriverModel} />
      :null}

      <div className="accordion accordionAssignDriver" id="accordionExample">
        <div className="card">
                <div className="card-header" id="headingOne">
                  <div data-toggle="collapse" className=" tablecollapsedHeader" data-target={`#${this.state.collapseID}`} aria-expanded="true" aria-controls={this.state.collapseID}>
                      <table className="table tableDriver">
                          <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Driver Name</th>
                                  <th>Vehicle TYPE</th>
                                  <th>Actions</th>
                              </tr>
                          </thead>
                      </table>
                  </div>
                </div>
                <div id={this.state.collapseID} className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                  <div className="card-body">
                      <div className="maintableAssignDriver">
                          <table className="table tableDriver">
                              <tbody>
                                  <tr>
                                      <td>{this.props.job_id}</td>
                                      <td>
                                        {!this.state.display && !this.state.driver?
                                        <select onChange={(e)=>{
                                          if(e.target.value!=="addnewdriver"){
                                            this.splitFunc(e.target.value);
                                          }else{
                                            this.openDriverModel();
                                          }
                                        }} value={this.state.driver} className="form-control">
                                          <option value="">select</option>
                                          {driverList}
                                          <option value="addnewdriver">Add New Driver</option>
                                        </select>
                                        : <span>{this.state.name}</span>}
                                      </td>
                                      <td className="font-medium">
                                        {this.state.driver?
                                        <select onChange={(e)=>{
                                          if(e.target.value && e.target.value === "add@truck"){
                                            this.setState({
                                              addAssetModel: true
                                            })
                                          }else if(e.target.value){
                                            let x = e.target.value.split('@')
                                            this.setState({
                                              ass_truck_id: x[0],
                                              ass_truck_name: x[1],
                                              display: true
                                            })
                                            if(this.state.assign_pick.length===0){
                                              this.newPick('');
                                              this.newDrop('');
                                            }
                                          }
                                        }} value={`${this.state.ass_truck_id}@${this.state.ass_truck_name}`} className="form-control">
                                          <option value="">select</option>
                                          {this.state.ass_truck_list && this.state.ass_truck_list.length>0?
                                            this.state.ass_truck_list.map((e, i)=>(
                                              <option key={i} value={`${e.id}@${e.asset_id}-${e.registeration_no}`}>{e.asset_id}-{e.registeration_no}</option>
                                            ))
                                          :null}
                                          <option value="add@truck">Add Truck</option>
                                        </select>
                                        :null}
                                      </td>
                                      <td>
                                          <ul className="listAction">
                                              <li><a href="#a" className="textRed" onClick={(e)=>{
                                                e.preventDefault();
                                                this.props.deletePick(this.props.form_key);
                                                this.props.deleteDrop(this.props.form_key);
                                                this.setState({
                                                  assign_pick: [],
                                                  assign_drop: [],
                                                  job_id: this.props.job_id,
                                                  DriverID: '',
                                                  type: '',
                                                  name: '',
                                                  driver:'',
                                                });
                                          //      this.props.modifyOrder('assign_pick_driver', this.state.assign_pick);
                                            //    this.props.modifyOrder('assign_drop_driver', this.state.assign_drop);
                                                successAlert('deleted');
                                              }}><i className="far fa-trash-alt"></i></a></li>
                                          </ul>
                                      </td>
                                  </tr>
                              </tbody>
                          </table>
                          {this.state.DriverID && this.state.ass_truck_id?
                            <React.Fragment>
                              <div className="tableAssignDriver">
                                <div className="table-responsive">
                                  <table className="table">
                                    <thead>
                                      <tr>
                                        <th>#</th>
                                        <th>Order QTY.</th>
                                        <th>Pick Up Location</th>
                                        <th>Pick Up Time Slot</th>
                                        <th>Sender</th>
                                        <th>Asset</th>
                                        <th>Actions</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {this.state.assign_pick.map((e,i)=>(
                                        <AddDriverPick
                                          key={i}
                                          order={this.props.order}
                                          assign_pick={e}
                                          form_key={i}
                                          job_id={this.props.job_id}
                                          DriverID={this.state.DriverID}
                                          name={this.state.name}
                                          type={this.state.type}
                                          ass_truck_id={this.state.ass_truck_id}
                                          ass_truck_name={this.state.ass_truck_name}
                                          receivePick={this.receivePick}
                                          Created_by_CarrierID={this.props.Created_by_CarrierID}
                                          deletePickDrop={this.deletePickDrop}
                                          units={this.props.address && this.props.address.units? this.props.address.units: ''}
                                        />
                                      ))}
                                    </tbody>
                                  </table>
                                </div>
                                <p className="textAddMore mb-0">
                                  <a href="#a" className="font-medium" onClick={(e)=>{
                                    e.preventDefault();
                                    this.newPick('');
                                  }}>+ Add Another Pickup</a>
                                </p>
                            </div>

                            <div className="tableAssignDriver">
                              <div className="table-responsive">
                                <table className="table">
                                    <thead>
                                      <tr>
                                        <th>#</th>
                                        <th>Order QTY.</th>
                                        <th>Delivery Location</th>
                                        <th>Delivery Time Slot</th>
                                        <th>Receiver</th>
                                        <th>Asset</th>
                                        <th>Actions</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      {this.state.assign_drop.map((e,i)=>(
                                        <AddDriverDrop
                                          key={i}
                                          order={this.props.order}
                                          assign_drop={e}
                                          form_key={i}
                                          job_id={this.props.job_id}
                                          DriverID={this.state.DriverID}
                                          name={this.state.name}
                                          type={this.state.type}
                                          ass_truck_id={this.state.ass_truck_id}
                                          ass_truck_name={this.state.ass_truck_name}
                                          receiveDrop={this.receiveDrop}
                                          Created_by_CarrierID={this.props.Created_by_CarrierID}
                                          deletePickDrop={this.deletePickDrop}
                                          units={this.props.address && this.props.address.units? this.props.address.units: ''}
                                        />
                                      ))}
                                    </tbody>
                                </table>
                              </div>
                              <p className="textAddMore mb-0">
                                <a href="#a" className="font-medium" onClick={(e)=>{
                                  e.preventDefault();
                                  this.newDrop('');
                                }}>+ Add Another Delivery</a>
                              </p>
                            </div>
                            </React.Fragment>
                          :null}
                      </div>
                  </div>
                </div>
        </div>
      </div>
      {this.state.addAssetModel?
        <AddAssets
          openOneModel={this.openAssetModel}
          closeOneModel={this.closeAssetModel}
          refreshData={this.getAssTruckList}
          asset_type_field="truck"
        />
      :null}
      </React.Fragment>
    )
  }
}

AddDriver.propTypes = {
  getAllDrivers: PropTypes.func.isRequired,
  getSenderReceiver: PropTypes.func.isRequired,
  getAssets: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  allDrivers: state.carrier.allDrivers,
  senderReceiver: state.carrier.senderReceiver,
  assetsList: state.carrier.assetsList,
});

export default connect(mapStateToProps, {getSenderReceiver, getAssets, getAllDrivers})(AddDriver);

//<th>Asset Date</th>
