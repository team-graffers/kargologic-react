import React, {Component} from 'react';

class Alert extends Component{
  constructor(props) {
    super(props);
    this.state = {
      msg: ''
    }
  }
  componentWillMount(){
    this.setState({
      msg: this.props.msg
    });
    setTimeout(function(){
      this.setState({
        msg: ''
      });
    }.bind(this),4000);
  }

  render(){
    if(this.state.msg){
      return(
        <div className="alert alert-success" role="alert">
          <i className="fas fa-check-circle"></i> {this.state.msg}
        </div>
      )
    }else{
      return(
        ''
      )
    }
  }
}

export default Alert;
