import React, {Component} from 'react';
import AddDriver from './addDriver';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getAssets, getSenderReceiver, getAllDrivers} from '../../actions/carrierActions';

class AssignDriver extends Component{
  constructor(props){
    super(props);
    this.state = {
      assign_pick_driver:[],
      assign_drop_driver:[],
      addDriverContainer:[],
    }
    this.assignPick = this.assignPick.bind(this);
    this.assignDrop = this.assignDrop.bind(this);
    this.addForm = this.addForm.bind(this);
  }
  assignPick = (key, data)=>{
    let x = this.state.assign_pick_driver;
    x[key] = data;
    this.setState({
      assign_pick_driver: x
    });

  }
  assignDrop = (key, data)=>{
    let x = this.state.assign_drop_driver;
    x[key] = data;
    this.setState({
      assign_drop_driver: x
    });
  }
  deletePick =(i)=>{
    let array = this.state.assign_pick_driver;
    if (i > -1) {
      array.splice(i, 1);
      this.setState({
          assign_pick_driver: array
      });
    }
  }
  deleteDrop =(i)=>{
    this.deleteDriverContainer(i);
    let array = this.state.assign_drop_driver;
    if (i > -1) {
      array.splice(i, 1);
      this.setState({
          assign_drop_driver: array
      });
    }
  }
  deleteDriverContainer =(i)=>{
    let array = this.state.addDriverContainer;
    if (i > -1) {
      array.splice(i, 1);
      this.setState({
          addDriverContainer: array
      });
    }
  }
  addForm =(drivers=null)=>{
    let container = this.state.addDriverContainer;
    let data = '';
    if(this.props.address!==null && container.length===0){
      data = <AddDriver
              Created_by_CarrierID={this.props.Created_by_CarrierID}
              order={this.props.order}
              drivers={drivers}
              assignPick={this.assignPick}
              assignDrop={this.assignDrop}
              deletePick={this.deletePick}
              deleteDrop={this.deleteDrop}
              form_key={container.length}
              job_id={this.props.job_id}
              newSubForm={this.newSubForm}
              address={this.props.address}
              units={this.props.units}
            />
    }else{
      data = <AddDriver
              Created_by_CarrierID={this.props.Created_by_CarrierID}
              order={this.props.order}
              drivers={drivers}
              assignPick={this.assignPick}
              assignDrop={this.assignDrop}
              deletePick={this.deletePick}
              deleteDrop={this.deleteDrop}
              form_key={container.length}
              job_id={this.props.job_id}
              newSubForm={this.newSubForm}
            />
    }
    container.push({
      id:container.length,
      data: data
    });
    this.setState({
      addDriverContainer: container,
    });
  }
  componentWillMount(){
    this.props.getSenderReceiver('orders/comsendreceive/');
    this.props.getAllDrivers('driv/driver-list-status/?status=all');
  //  this.props.getAssets('asset/listasset/');
    if(this.props.drivers && this.props.drivers.length>0){
      this.props.drivers.map((e, i)=>{
      this.addForm(e);
        return(
          ''
        )
      })
    }else{
      this.addForm();
    }
  }

  render(){
    return(
      <div className="fixedAssignDriver assign-big">
          <div className="modal-dialog" role="document">
              <div className="modal-content">
                  <div className="modal-body">
                      <div className="row rowAssign align-items-center">
                        <div className="col-md-6">
                            <h4 className="pb-0 mb-0">AllOCATE DRIVER</h4>
                        </div>
                    <div className="col-md-6">
                            <p className="textGray mb-0 text-md-right">Total Qty: {this.props.address.units}</p>
                        </div>
                      </div>
                      {this.state.addDriverContainer.map((e, i)=>(
                        <div key={i}>{e.data}</div>
                      ))}
                      <a href="#a" onClick={(e)=>{
                        e.preventDefault();
                        this.addForm();
                      }}>Add another driver to this order</a>
                      <br/><br/>
                      <div className="text-center">
                        <a href="#a" onClick={(e)=>{
                          e.preventDefault();
                          this.props.modifyOrder('assign_pick_driver', this.state.assign_pick_driver);
                          this.props.modifyOrder('assign_drop_driver', this.state.assign_drop_driver);
                          this.props.close();
                        }} className="btn btn-primary">
                        {this.props.drivers && this.props.drivers.length>0?
                        "Save":
                        "Allocate Driver"}

                        </a>
                        <a href="#a" onClick={(e)=>{
                          e.preventDefault();
                          this.props.cancel();
                        }} className="btn btn-danger" style={{"marginLeft":"15px"}}>Cancel</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>

    )
  }
}

AssignDriver.propTypes = {
  getAllDrivers: PropTypes.func.isRequired,
  getSenderReceiver: PropTypes.func.isRequired,
  getAssets: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  allDrivers: state.carrier.allDrivers,
  senderReceiver: state.carrier.senderReceiver,
  assetsList: state.carrier.assetsList,
});

export default connect(mapStateToProps, {getSenderReceiver, getAssets, getAllDrivers})(AssignDriver);
//export default AddDriver;
