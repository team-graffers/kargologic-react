import React, {Component} from 'react';
import {successAlert} from '../include/alert';
import Datetime from 'react-datetime';
import APIFetch from '../ajax/apiFetch';
import PostFetch from '../ajax/postFetch';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getSenderList} from '../../actions/carrierActions';
import moment from 'moment';
import store from '../../store';
import {charValidation} from '../include/validation';

class AddSender extends Component{
  constructor(props) {
    super(props);
    this.state = {
      id: 0,
      name: '',
      primary_contact: '',
      pri_email_id: '',
      pri_phone: '',
      secondary_contact: '',
      sec_email_id: '',
      sec_phone: '',
      business_address: '',
      contract_start_date:'',
      contract_end_date: '',
      contract_documents: '',
      Customer_code: '',

      name_error: '',
      primary_contact_error: '',
      pri_phone_error: '',
      business_address_error: '',
      appdata: '',
      data: [],
      image_read: [],
      editSender:  false
    }
  }
  componentWillMount(){
    const state = store.getState();
    this.setState({
      appdata: state.carrier.appdata
    })
    if(state.carrier.editSender && state.carrier.editSender.id){
      let data = state.carrier.editSender;
      this.setState({
        editSender: true,
        id: data.id,
        name: data.name,
        primary_contact: data.primary_contact,
        pri_email_id: data.pri_email_id,
        pri_phone: data.pri_phone,
        secondary_contact: data.secondary_contact,
        sec_email_id: data.sec_email_id,
        sec_phone: data.sec_phone,
        business_address: data.business_address,
        contract_start_date: data && data.contract_start_date? moment(data.contract_start_date): '',
        contract_end_date: data && data.contract_end_date? moment(data.contract_end_date): ''
      })
    }
    if(state.customer.editSender && state.customer.editSender.id){
      let data = state.customer.editSender;
      this.setState({
        editSender: true,
        id: data.id,
        name: data.name,
        primary_contact: data.primary_contact,
        pri_email_id: data.pri_email_id,
        pri_phone: data.pri_phone,
        secondary_contact: data.secondary_contact,
        sec_email_id: data.sec_email_id,
        sec_phone: data.sec_phone,
        business_address: data.business_address,
        contract_start_date: data && data.contract_start_date? moment(data.contract_start_date): '',
        contract_end_date: data && data.contract_end_date? moment(data.contract_end_date): ''
      })
    }
  }
  componentWillReceiveProps(nextProps){

  }
  getSuggestions = async (text)=>{
    const data = await APIFetch('https://autocomplete.geocoder.api.here.com/6.2/suggest.json?'+this.state.appdata+'&query='+text+'&country=aus');
    let options = [];
    if(data.suggestions){
       data.suggestions.map((s,i)=>(
        options.push({
          value:s.label,
          label:s.label,
        })
      ));
    }
    this.setState({
      data:options
    });
  }
  getFiles = (files)=>{
    this.setState({
      image_read: [...this.state.image_read, files],
    })
  }
  getBase64 = (files, getFiles)=>{
    Array.from(files).map((file, i)=>{
     var reader = new FileReader();
     reader.readAsDataURL(file);
     reader.onload = function () {
       let f = {
         attachment_name: file.name,
         size: file.size,
         image_read: reader.result
       }
       getFiles(f)
     };
     reader.onerror = function (error) {
       console.log('Error: ', error);
     };
     return('')
   })
  }
  sendInvitation = async (e)=>{
    e.preventDefault();
    if(this.state.name && this.state.primary_contact && this.state.pri_phone && this.state.business_address){


      const payload = {
        id: this.state.id,
        name: this.state.name,
        primary_contact: this.state.primary_contact,
        pri_email_id: this.state.pri_email_id,
        pri_phone: this.state.pri_phone,
        secondary_contact: this.state.secondary_contact,
        sec_email_id: this.state.sec_email_id,
        sec_phone: this.state.sec_phone,
        business_address: this.state.business_address,
        contract_start_date: this.state.contract_start_date,
        contract_end_date: this.state.contract_end_date,
        sendrecemany: this.state.image_read,
        Customer_code: this.state.Customer_Name,
        is_sender: true
      }
      let json = await PostFetch('companysendrecei/createsendrec/', payload);
      if(json && json.message && json.message === "success"){
      //  this.props.getSenderList('companysendrecei/managersenderrecei/');
        if(this.props.addMore){
          this.props.addMore();
        }
        successAlert("Sender added successfully");
        this.setState({
          name: '',
          primary_contact: '',
          pri_email_id: '',
          pri_phone: '',
          secondary_contact: '',
          sec_email_id: '',
          sec_phone: '',
          business_address: '',
          contract_start_date:'',
          contract_end_date: '',
          contract_documents: '',
          Customer_code: '',

          name_error: '',
          primary_contact_error: '',
          pri_phone_error: '',
          business_address_error: '',
          senderLeave: false,
        })
        this.props.closeOneModel();
      }
    }else{
      this.setState({
        name_error: 'required',
        primary_contact_error: 'required',
        pri_phone_error: 'required',
        business_address_error: 'required',
      })
    }
  }
  closeConfirmation = ()=>{
    if(this.state.senderLeave){
      var r = window.confirm("You have unsaved changes, are you sure you want to leave?");
      if (r === true) {
        this.props.closeOneModel();
      }
    }else {
      this.props.closeOneModel();
    }
  }
  render(){
    return(
      <div className="modal show fixedAssignDriver" style={{zIndex: 1000000000000, display: "block"}} id="modelAddS" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
                <h4 className="modal-title" id="exampleModalCenterTitle">Sender</h4>
                <a href="#a" onClick={(e)=>{
                  e.preventDefault();
                  this.closeConfirmation();
                }} className="btnClose">
                    <span aria-hidden="true">&times;</span>
                </a>
            </div>
            <div className="modal-body">
              <div className="innerInviteCustomer">
                <div className="form-group">
                  <label>Customer Bussiness Name <span className="text-danger">*</span></label>
                  <input type="text" value={this.state.name} onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({
                        name: e.target.value,
                        senderLeave: true
                      })
                    }else {
                      this.setState({
                        name: ''
                      })
                    }
                  }} onBlur={ (e)=>{
                    if(e.target.value){
                      this.setState({
                        name_error: ''
                      })
                    }else{
                      this.setState({
                        name_error: 'required'
                      })
                    }
                  }} className="form-control"/>
                  <span className="text-danger">{this.state.name_error}</span>
                </div>
                <div className="form-group">
                  <label>Primary Contact <span className="text-danger">*</span></label>
                  <input type="text" value={this.state.primary_contact} onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({
                        primary_contact: e.target.value,
                        senderLeave: true
                      })
                    }else {
                      this.setState({
                        primary_contact: ''
                      })
                    }
                  }} onBlur={ (e)=>{
                    if(e.target.value){
                      this.setState({
                        primary_contact_error: ''
                      })
                    }else{
                      this.setState({
                        primary_contact_error: 'required'
                      })
                    }
                  }} className="form-control"/>
                  <span className="text-danger">{this.state.primary_contact_error}</span>
                </div>
                <div className="form-group">
                  <label>Primary Email Id</label>
                  <input type="email" value={this.state.pri_email_id} onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({
                        pri_email_id: e.target.value,
                        senderLeave: true
                      })
                    }else {
                      this.setState({
                        pri_email_id: ''
                      })
                    }
                  }}className="form-control"/>
                </div>
                <div className="form-group">
                  <label>Primary Phone No. <span className="text-danger">*</span></label>
                  <input type="number" value={this.state.pri_phone} onChange={(e)=>{
                    this.setState({
                      pri_phone: e.target.value,
                      senderLeave: true
                    })
                  }} onBlur={ (e)=>{
                    if(e.target.value){
                      this.setState({
                        pri_phone_error: ''
                      })
                    }else{
                      this.setState({
                        pri_phone_error: 'required'
                      })
                    }
                  }} className="form-control"/>
                  <span className="text-danger">{this.state.pri_phone_error}</span>
                </div>
                <div className="form-group">
                  <label>Secondary Contact</label>
                  <input type="text" value={this.state.secondary_contact} onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({
                        secondary_contact: e.target.value,
                        senderLeave: true
                      })
                    }else {
                      this.setState({
                        secondary_contact: ''
                      })
                    }
                  }} className="form-control"/>
                </div>
                <div className="form-group">
                  <label>Secondary Email Id</label>
                  <input type="email" value={this.state.sec_email_id} onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({
                        sec_email_id: e.target.value,
                        senderLeave: true
                      })
                    }else {
                      this.setState({
                        sec_email_id: ''
                      })
                    }
                  }}className="form-control"/>
                </div>
                <div className="form-group">
                  <label>Secondary Phone No.</label>
                  <input type="number" value={this.state.sec_phone} onChange={(e)=>{
                    this.setState({
                      sec_phone: e.target.value,
                      senderLeave: true
                    })
                  }}className="form-control"/>
                </div>
                <div className="form-group">
                  <label>Business Address <span className="text-danger">*</span></label>
                  <input type="text" list="senderAddress" value={this.state.business_address} onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.getSuggestions(e.target.value);
                      this.setState({
                        business_address: e.target.value,
                        senderLeave: true
                      })
                    }else {
                      this.setState({
                        business_address: ''
                      })
                    }
                  }}  onBlur={ (e)=>{
                    if(e.target.value){
                      this.setState({
                        business_address_error: ''
                      })
                    }else{
                      this.setState({
                        business_address_error: 'required'
                      })
                    }
                  }} className="form-control"/>
                  <datalist id="senderAddress">
                    <option value="">select</option>
                    {this.state.data.length>0?
                    this.state.data.map((e, i)=>{
                      return(
                        <option key={i} value={e.value}>{e.label}</option>
                      )
                    }):null}
                  </datalist>
                  <span className="text-danger">{this.state.business_address_error}</span>
                </div>
                <div className="form-group">
                  <label>Contract Start Date</label>
                  <Datetime
                    dateFormat="DD/MM/YY"
                    timeFormat={false}
                    onChange={(e)=>{
                      this.setState({
                        contract_start_date: e,
                        senderLeave: true
                      });
                    }} value={this.state.contract_start_date}
                  />
                </div>
                <div className="form-group">
                  <label>Contract End Date</label>
                  <Datetime
                    dateFormat="DD/MM/YY"
                    timeFormat={false}
                    onChange={(e)=>{
                      this.setState({
                        contract_end_date: e,
                        senderLeave: true
                      });
                    }} value={this.state.contract_end_date}
                  />
                </div>
                <div className="form-group">
                  <label>Contract Documents</label>
                  <input type="file" multiple onChange={(e)=>{
                    if(e.target.files && e.target.files.length>0){
                      this.setState({
                        contract_documents: e.target.files,
                        image_read: [],
                        senderLeave: true
                      })
                      this.getBase64(e.target.files, this.getFiles)
                    }
                  }} className="form-control" />
                </div>
                <div className="form-group">
                  <label>Customer Code</label>
                  <input type="text" value={this.state.Customer_code} onChange={(e)=>{
                    if(e.target.value && charValidation(e.target.value)){
                      this.setState({
                        Customer_code: e.target.value,
                        senderLeave: true
                      })
                    }else {
                      this.setState({
                        business_address: ''
                      })
                    }
                  }} className="form-control"/>
                </div>
                <p className="text-center mb-0">
                  <a href="#AddSenderReceiver" onClick={this.sendInvitation} className="btn">{this.state.editSender? 'Update Sender': 'Add Sender'}</a>
                  <input type="button" style={{marginLeft: '15px'}} value="Close" onClick={()=>{
                      this.closeConfirmation();
                  }}/>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

AddSender.propTypes = {
  getSenderList: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({

});

export default connect(mapStateToProps, {getSenderList})(AddSender);
