import React, {Component} from 'react';
import Datetime from 'react-datetime';
import APIFetch from '../ajax/apiFetch';
import PostFetch from '../ajax/postFetch';
import {errorAlert, successAlert} from '../include/alert';
import {verifyEmail} from '../include/validation';
import store from '../../store';
import {charValidation} from '../include/validation';

class InviteCustomer extends Component{
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      name: '',
      business_name: '',
  		mobile_num: '',
  		secondary_contact: '',
  		sec_email_id: '',
  		sec_phone: '',
  		business_address: '',
  		contract_start_date: '',
  		contract_end_date: '',
      contract_documents: '',
  		Customer_code: '',
      attach: [],
      data: [],
      error_email: '',
      error_name: '',
      error_business_name: '',
      error_mobile_num: '',
      error_business_address: '',
      appdata: '',
      submitDis: true,
    }
  }
  componentWillMount(){
    const state = store.getState();
    this.setState({
      appdata: state.carrier.appdata
    })
  }
  getFiles = (files)=>{
    this.setState({
      attach: [...this.state.attach, files],
    })
  }
  getBase64 = (files, getFiles)=>{
    Array.from(files).map((file, i)=>{
     var reader = new FileReader();
     reader.readAsDataURL(file);
     reader.onload = function () {
       let f = {
         file_name: file.name,
         file_data: reader.result
       }
       getFiles(f)
     };
     reader.onerror = function (error) {
       console.log('Error: ', error);
     };
     return('')
   })
  }
  sendInvitation = async ()=>{
    if(this.state.business_name && this.state.name && this.state.mobile_num && this.state.business_address && this.state.email && this.state.error_email.length === 0){
      const payload = {
        email: this.state.email,
        extra_details:{
          business_name: this.state.business_name,
          name: this.state.name,
      		mobile_num: this.state.mobile_num,
      		secondary_contact: this.state.secondary_contact,
      		sec_email_id: this.state.sec_email_id,
      		sec_phone: this.state.sec_phone,
      		business_address: this.state.business_address,
      		contract_start_date: this.state.contract_start_date,
      		contract_end_date: this.state.contract_end_date,
      		Customer_code: this.state.Customer_code,
        },
        attach: this.state.attach
      }
      let json = await PostFetch('cust/invite/', payload);
      if(json && json.message && json.message === "Customer invitation sent successfully"){
        successAlert("Customer details added successfully");
        this.setState({
          email: '',
          name: '',
      		mobile_num: '',
      		secondary_contact: '',
      		sec_email_id: '',
      		sec_phone: '',
      		business_address: '',
      		contract_start_date: '',
      		contract_end_date: '',
      		Customer_code: '',

          error_email: '',
          error_name: '',
          error_mobile_num: '',
          error_business_address: '',
          submitDis: true,
        });
        this.props.closeOneModel('modelInviteCustomer');
      }else{
        errorAlert('try again!')
      }
    }else {
      this.setState({
        error_email: 'required',
        error_name: 'required',
        error_business_name: 'required',
        error_mobile_num: 'required',
        error_business_address: 'required',
      })
    }
  }
  getSuggestions = async (text)=>{
    const data = await APIFetch('https://autocomplete.geocoder.api.here.com/6.2/suggest.json?'+this.state.appdata+'&query='+text+'&country=aus');
    let options = [];
    if(data.suggestions){
       data.suggestions.map((s,i)=>(
        options.push({
          value:s.label,
          label:s.label,
        })
      ));
    }
    this.setState({
      data:options
    });
  }
  render(){
    return(
      <div className="innerInviteCustomer">
      <div className="form-group">
        <label>Customer Bussiness Name <span className="text-danger">*</span></label>
        <input type="text" value={this.state.business_name} onChange={(e)=>{
          if(e.target.value && charValidation(e.target.value)){
            this.setState({
              business_name: e.target.value
            })
          }else {
            this.setState({
              business_name: ''
            })
          }
        }} onBlur={ (e)=>{
          if(e.target.value){
            this.setState({
              error_business_name: ''
            })
          }else{
            this.setState({
              name_error: 'required'
            })
          }
        }} className="form-control"/>
        <span className="text-danger">{this.state.error_business_name}</span>
      </div>
        <div className="form-group">
          <label>Primary Contact <span className="text-danger">*</span></label>
          <input type="text" value={this.state.name} onChange={(e)=>{
            if(e.target.value && charValidation(e.target.value)){
              this.setState({
                name: e.target.value
              })
            }else {
              this.setState({
                name: ''
              })
            }
          }} onBlur={(e)=>{
            if(!e.target.value){
              this.setState({
                error_name: 'required'
              })
            }else{
              this.setState({
                error_name: ''
              })
            }
          }} className="form-control"/>
          <span className="text-danger">{this.state.error_name}</span>
        </div>
        <div className="form-group">
          <label>Primary Email <span className="text-danger">*</span></label>
          <input type="email" value={this.state.email} onChange={(e)=>{
            if(e.target.value && charValidation(e.target.value)){
              this.setState({
                email: e.target.value
              })
            }else {
              this.setState({
                email: ''
              })
            }
          }} onBlur={ async (e)=>{
            let data = await verifyEmail(e.target.value);
            if(data){
              if(data.error!==""){
                this.setState({
                  error_email: data.error
                })
              }else if(data.data==="Customer" || data.data===""){
                this.setState({
                  error_email: ''
                })
              }else if(data.data!=="Customer" && data.data!==""){
                this.setState({
                  error_email: 'Already registered as '+data.data
                })
              }
            }
          }} className="form-control"/>
          <span className="text-danger">{this.state.error_email}</span>
        </div>
        <div className="form-group">
          <label>Primary Phone No. <span className="text-danger">*</span></label>
          <input type="number" value={this.state.mobile_num} onChange={(e)=>{
            this.setState({
              mobile_num: e.target.value
            })
          }} onBlur={(e)=>{
            if(!e.target.value){
              this.setState({
                error_mobile_num: 'required'
              })
            }else{
              this.setState({
                error_mobile_num: ''
              })
            }
          }} className="form-control"/>
          <span className="text-danger">{this.state.error_mobile_num}</span>
        </div>
        <div className="form-group">
          <label>Secondary Contact</label>
          <input type="text" value={this.state.secondary_contact} onChange={(e)=>{
            if(e.target.value && charValidation(e.target.value)){
              this.setState({
                secondary_contact: e.target.value
              })
            }else {
              this.setState({
                secondary_contact: ''
              })
            }
          }} className="form-control"/>
        </div>
        <div className="form-group">
          <label>Secondary Email</label>
          <input type="email" value={this.state.sec_email_id} onChange={(e)=>{
            if(e.target.value && charValidation(e.target.value)){
              this.setState({
                sec_email_id: e.target.value
              })
            }else {
              this.setState({
                sec_email_id: ''
              })
            }
          }} className="form-control"/>
        </div>
        <div className="form-group">
          <label>Secondary Phone No.</label>
          <input type="number" value={this.state.sec_phone} onChange={(e)=>{
            this.setState({
              sec_phone: e.target.value
            })
          }} className="form-control"/>
        </div>
        <div className="form-group">
          <label>Business Address <span className="text-danger">*</span></label>
          <input type="text" value={this.state.business_address} onChange={(e)=>{
            if(e.target.value && charValidation(e.target.value)){
              this.getSuggestions(e.target.value);
              this.setState({
                business_address: e.target.value
              })
            }else {
              this.setState({
                business_address: ''
              })
            }
          }} onBlur={(e)=>{
            if(!e.target.value){
              this.setState({
                error_business_address: 'required'
              })
            }else{
              this.setState({
                error_business_address: ''
              })
            }
          }} list="datalistA" className="form-control"/>
          <datalist id="datalistA">
            <option value="">select</option>
            {this.state.data.length>0?
            this.state.data.map((e, i)=>{
              return(
                <option key={i} value={e.value}>{e.label}</option>
              )
            }):null}
          </datalist>
          <span className="text-danger">{this.state.error_business_address}</span>
        </div>
        <div className="form-group">
          <label>Contract Start Date</label>
          <Datetime
            dateFormat="DD/MM/YY"
            timeFormat={false}
            onChange={(e)=>{
              this.setState({
                contract_start_date: e,
              });
            }} value={this.state.contract_start_date}
          />
        </div>
        <div className="form-group">
          <label>Contract End Date</label>
          <Datetime
            dateFormat="DD/MM/YY"
            timeFormat={false}
            onChange={(e)=>{
              this.setState({
                contract_end_date: e,
              });
            }} value={this.state.contract_end_date}
          />
        </div>
        <div className="form-group">
          <label>Contract Documents</label>
          <input type="file" multiple onChange={(e)=>{
            if(e.target.files && e.target.files.length>0){
              this.setState({
                contract_documents: e.target.files,
                attach: []
              })
              this.getBase64(e.target.files, this.getFiles)
            }
          }} className="form-control" />
        </div>
        <div className="form-group">
          <label>Customer Code</label>
          <input type="text" value={this.state.Customer_code} onChange={(e)=>{
            if(e.target.value && charValidation(e.target.value)){
              this.setState({
                Customer_code: e.target.value
              })
            }else {
              this.setState({
                Customer_code: ''
              })
            }
          }} className="form-control"/>
        </div>


        <p className="text-center mb-0">
        <a href="#customer" onClick={(e)=>{
          e.preventDefault();
          if(this.state.submitDis){
            this.sendInvitation()
          }
          this.setState({
            submitDis: false
          })
        }} className="btn">Add Customer</a>
        <button style={{marginLeft: '25px'}}  onClick={(e)=>{
          this.props.closeOneModel('modelInviteCustomer');
        }} className="btn">Close</button>
        </p>
      </div>
    )
  }
}

export default InviteCustomer;
