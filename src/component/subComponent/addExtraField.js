import React, {Component} from 'react';
import {charValidation} from '../include/validation';

class AddExtraField extends Component{
  constructor(props){
    super(props);
    this.state = {
      field_name:'',
      field_value:'',
      error_name:'',
      error_value:'',
    }
  }
  addField = (e)=>{
    e.preventDefault();
    if(this.state.field_name && this.state.field_value){
    this.props.addNewField(this.state);
    this.setState({
      field_name: '',
      field_value: '',
      error_name: '',
      error_value: '',
    });
    this.props.close();
    this.closeOneModal('myModal');
  }else{
    this.setState({
      error_name: 'required',
      error_value: 'required',
    })
  }
  }
  closeOneModal=(modalId)=>{
    const modal = document.getElementById(modalId);
    modal.classList.remove('show');
    modal.setAttribute('aria-hidden', 'true');
    modal.setAttribute('style', 'display: none');
    const modalBackdrops = document.getElementsByClassName('modal-backdrop');
    document.body.removeChild(modalBackdrops[0]);
  }
  render(){
    return(
      <div className="modal fade popupAssignDriver" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
          aria-hidden="true">
          <div className="modal-dialog" role="document">
              <div className="modal-content">
                  <div className="modal-body">
                      <a href="#a" className="btnClose" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </a>
                      <div className="row rowAssign align-items-center">
                        <div className="col-md-6">
                            <h4 className="pb-0 mb-0">Add New Extra Field</h4>
                        </div>
                      </div>
                      <div className="form-group">
                        <label className="col-form-label">Enter Field Label</label>
                        <input type="text" onChange={(e)=>{
                          if(e.target.value && charValidation(e.target.value)){
                            this.setState({
                              field_name: e.target.value
                            });
                          }else {
                            this.setState({
                              field_name: ''
                            })
                          }
                        }} onBlur={(e)=>{
                          if(!e.target.value){
                            this.setState({
                              error_name: 'required'
                            })
                          }else{
                            this.setState({
                              error_name: ''
                            })
                          }
                        }} value={this.state.field_name} className="form-control"/>
                        <span className="text-danger">{this.state.error_name}</span>
                      </div>
                      <div className="form-group">
                        <label className="col-form-label">Enter Field Value</label>
                        <input type="text" onChange={(e)=>{
                          if(e.target.value && charValidation(e.target.value)){
                            this.setState({
                              field_value: e.target.value
                            });
                          }else {
                            this.setState({
                              field_value: ''
                            })
                          }
                        }} onBlur={(e)=>{
                          if(!e.target.value){
                            this.setState({
                              error_value: 'required'
                            })
                          }else{
                            this.setState({
                              error_value: ''
                            })
                          }
                        }} value={this.state.field_value} className="form-control"/>
                        <span className="text-danger">{this.state.error_value}</span>
                      </div>
                      <div className="form-group">
                        <a className="btn btn-primary" onClick={this.addField} href="#a">Add</a>
                      </div>
                      </div>
              </div>
          </div>
      </div>
    )
  }
}
export default AddExtraField;
