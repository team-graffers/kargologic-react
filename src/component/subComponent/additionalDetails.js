import React from 'react';

const AdditionalDetails = (props)=>{
  const thead = props.thead.map((e,i)=>{
    return(
      <th key={i}>{e}</th>
    )
  });
  const tbody = props.tbody.map((e,i)=>{
    return(
      <td key={i}>{e}</td>
    )
  });
  return(
    <div className="table-responsive">
      <table className="table">
        <thead>
          <tr>
            {thead}
          </tr>
        </thead>
        <tbody>
          <tr>
            {tbody}
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default AdditionalDetails;
