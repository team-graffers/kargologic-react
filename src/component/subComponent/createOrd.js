import React, {Component} from 'react';
import AssignDriver from './assignDriver';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getSenderReceiver, pushSenderReceiver, modifyNewOrderCsv, getCustomerList} from '../../actions/carrierActions';
import APIFetch from '../ajax/apiFetch';
import split from "../../assets/img/icon-split.svg";
import AddExtraField from './addExtraField';
import Datetime from 'react-datetime';
import {formateDate} from '../include/date';//checkDate
import UploadAttachments from './uploadAttachmentsCreate';
import makeid from './makeid';
import Map from './maps';
import moment from 'moment';
import store from '../../store';
import AddSender from './addSender';
import AddReceiver from './addReceiver';
import {GetJobId} from '../include/jobId';
import {infoAlert} from '../include/alert';
import { Prompt } from 'react-router'
import {charValidation} from '../include/validation';

class CreateOrd extends Component{
  constructor(props){
    super(props);
    let carrier = JSON.parse(localStorage.getItem("userDetails"));
    this.state = {
      assignDriverModel: false,
      Carrier:carrier.carrier_company_name,
      job_id:'',
      order_id: '',
      customer_code:'',
      Customer_Name:'',
      CustomerCompanyID:'',
      price:'',
      pickup_date_time:'',
      pickup_date_time_to:'',
      pickup_location:'',
      delivery_date_time:'',
      delivery_date_time_to:'',
      delivery_location:'',
      load_carried:'',
      load_type:'',
      load_weight:'',
      length_load_dimen:'',
      width_load_dimen: '',
      height_load_dimen: '',
      measure_unit: '',
      units:'',
      additional_note:'',
      additional_note_count: 500,
      is_active: false,
      hazardous: false,
      extra_detail:[],
      model_class:'modal fade',
      model_style:'none',
      appdata: '',
      data: '',
      checkPickAddress:'',
      checkDropAddress:'',
      attachments: [],
      showMap: false,
      addSenderModel: false,
      addReceiverModel: false,
      map_lat: '',
      map_lng: '',
      map_address: '',
      Created_by_CarrierID: '',
      driverEdit: false,
      datalist: makeid(),
      datalist1: makeid(),
      sender: '',
      receiver: '',
      senderReceiver: [],
      promptStatus: false,
      sender_temp: '',
      receiver_temp: '',
      job_id_req: false,
      job_id_error:'',
      customer_code_error:'',
      Customer_Name_error:'',
      price_error:'',
      pickup_date_time_error:'',
      pickup_date_time_to_error:'',
      pickup_location_error:'',
      delivery_date_time_error:'',
      delivery_date_time_to_error:'',
      delivery_location_error:'',
      load_carried_error:'',
      load_type_error:'',
      load_weight_error:'',
      length_load_dimen_error:'',
      width_load_dimen_error: '',
      height_load_dimen_error: '',
      measure_unit_error: '',
      units_error:'',

      timeslot: 30,
    }
    this.getLocation = this.getLocation.bind(this);
    this.getAttachments = this.getAttachments.bind(this);
  }

  closeModel = (e) =>{
    this.setState({
      model_class: 'modal fade',
      model_style: 'none'
    });
  }
  openSenModel=()=>{
    this.setState({
      addSenderModel: true
    })
  }
  closeSenModel=()=>{
    this.setState({
      addSenderModel: false
    })
  }
  openRecModel=()=>{
    this.setState({
      addReceiverModel: true
    })
  }
  closeRecModel=()=>{
    this.setState({
      addReceiverModel: false
    })
  }
  submitIndData = ()=>{
    if(this.state.job_id && this.state.Customer_Name && this.state.CustomerCompanyID && this.state.pickup_date_time && this.state.pickup_date_time_to && this.state.pickup_location && this.state.delivery_date_time && this.state.delivery_date_time_to && this.state.delivery_location){
      this.props.submitInd(this.props.order_key);
    }else {
      infoAlert('fields are blank!')
    }
  }
  componentWillReceiveProps(nextProps){
    if(this.state.CustomerCompanyID && !parseInt(this.state.CustomerCompanyID)){
      const customer = nextProps.customerList.filter((e, i)=>{
        return e.CompanyID.name.toLowerCase() === this.state.CustomerCompanyID.toLowerCase().trim()
      })

      if(customer && customer.length>0){
        this.modifyOrder("order_detail", "Customer_Name", customer[0].CompanyID.name);
        this.modifyOrder("order_detail", "CustomerCompanyID", customer[0].CompanyID.id);
        this.setState({
          CustomerCompanyID: customer[0].CompanyID.id,
          Customer_Name: customer[0].CompanyID.name
        })
      }
    }
    //console.log();
    if(nextProps.senderReceiver && nextProps.senderReceiver.data && nextProps.senderReceiver.data.length>0){
      this.setState({
        senderReceiver: nextProps.senderReceiver.data
      })
      if(this.state.receiver_temp){
        for(let i = 0;i<nextProps.senderReceiver.data.length; i++){
          if(nextProps.senderReceiver.data[i].name === this.state.receiver_temp){
            this.setState({
              receiver: i
            })
            setTimeout(function() {
              this.setSenderReceiver('delivery', 'receiver', i, false)
            }.bind(this),1000);
            break;
          }
        }
      }
      if(this.state.sender_temp){
        for(let i = 0;i<nextProps.senderReceiver.data.length; i++){
          if(nextProps.senderReceiver.data[i].name === this.state.sender_temp){
            this.setState({
              sender: i
            })
            setTimeout(function() {
              this.setSenderReceiver('pickup', 'sender', i, true)
            }.bind(this),1000);
            break;
          }
        }
      }
      if(this.props.order && this.props.order.pickup && this.props.order.pickup[0] && this.props.order.pickup[0].sender && this.props.order.pickup[0].sender.name){
        for(let i = 0;i<nextProps.senderReceiver.data.length; i++){
          if(nextProps.senderReceiver.data[i].id === this.props.order.pickup[0].sender.id){
            this.setState({
              sender: i
            })
            setTimeout(function() {
              this.setSenderReceiver('pickup', 'sender', i, true)
            }.bind(this),1000);
            break;
          }
        }
      }
      if(this.props.order && this.props.order.delivery && this.props.order.delivery[0] && this.props.order.delivery[0].receiver && this.props.order.delivery[0].receiver.name){
        for(let i = 0;i<nextProps.senderReceiver.data.length; i++){
          if(nextProps.senderReceiver.data[i].id === this.props.order.delivery[0].receiver.id){
            this.setState({
              receiver: i
            })
            setTimeout(function() {
              this.setSenderReceiver('delivery', 'receiver', i, false)
            }.bind(this),1000);
            break;
          }
        }
      }
    }
  }
  getJob = async ()=>{
    const val = await GetJobId('carrier');
    if(val){
      let new_id = val+this.props.order_key;
      this.setState({
        job_id: new_id,
        job_id_req: true
      })
      this.modifyOrder("order_detail", "job_id", new_id);
    }else{
      this.setState({
        job_id: 1+this.props.order_key,
        job_id_req: false
      })
      this.modifyOrder("order_detail", "job_id", 1+this.props.order_key);
    }
  }
  componentWillMount(){
      try{
        let x = localStorage.getItem("userConfiguration");
        x = JSON.parse(x);
        let time_slot = 0;
        x.data.map((e, i)=>{
          if(e.ConfigureID.label === 'time_slot'){
            time_slot = parseInt(e.configure_val)
          }
          return('')
        })
        this.setState({
          timeslot: time_slot
        })
      }catch(e){
      }

    if(!this.props.editOrder){
      this.getJob();
    }
    const state = store.getState();
    this.setState({
      appdata: state.carrier.appdata
    })
  //  this.props.getCustomerList();
    if(this.props.order){
      let order_detail = this.props.order.order_detail;
      let pickup = this.props.order.pickup && this.props.order.pickup[0]? this.props.order.pickup[0]: '';
      let delivery = this.props.order.delivery && this.props.order.delivery[0]? this.props.order.delivery[0]: '';
      let extra_detail = this.props.order.extra_detail;
      if(order_detail && order_detail.hazardous && order_detail.hazardous){
        order_detail.hazardous = true;
      }else if(order_detail && order_detail.hazardous){
        order_detail.hazardous = false;
      }
      if(order_detail){
        this.props.getSenderReceiver('orders/comsendreceive/');
        this.setState({
          promptStatus: true,
          Created_by_CarrierID: order_detail && order_detail.Created_by_CarrierID? order_detail.Created_by_CarrierID: null,
          order_id:order_detail.order_id,
          job_id:order_detail.job_id,
          customer_code:order_detail.customer_code,
          Customer_Name:order_detail.Customer_Name,
          CustomerCompanyID:order_detail.CustomerCompanyID,
          price:order_detail.price,
          pickup_date_time:pickup.pickup_date_time,
          pickup_date_time_to:pickup.pickup_date_time_to,
          pickup_location:pickup.address,
          delivery_date_time:delivery.delivery_date_time,
          delivery_date_time_to:delivery.delivery_date_time_to,
          delivery_location:delivery.address,
          load_carried:order_detail.load_carried,
          load_type:order_detail.load_type,
          load_weight:order_detail.load_weight,
          length_load_dimen: parseInt(order_detail.length_load_dimen) || 0,
          width_load_dimen: parseInt(order_detail.width_load_dimen) || 0,
          height_load_dimen: parseInt(order_detail.height_load_dimen) || 0,
          measure_unit: order_detail.measure_unit,
          units:order_detail.units,
          additional_note:order_detail.additional_note,
          hazardous:order_detail.hazardous,
          sender_temp: order_detail.sender_name,
          receiver_temp: order_detail.receiver_name,
          extra_detail: extra_detail,
        });
        if(order_detail && pickup && pickup.address){
          setTimeout(function() {
            this.getLocation("pickup", "longitude", "latitude", pickup.address.toLowerCase(), "pickup_location" ,'checkPickAddress');
          }.bind(this),1000);
        }
        if(order_detail && delivery && delivery.address){
          setTimeout(function() {
            this.getLocation("delivery", "longitude", "latitude", delivery.address.toLowerCase(), "delivery_location", 'checkDropAddress');
          }.bind(this),1000);
        }
      }

    }
  }
  closeOneModel=(modalId)=>{
    try{
      const modal = document.getElementById(modalId);
      modal.classList.remove('show');
      modal.setAttribute('aria-hidden', 'true');
      modal.setAttribute('style', 'display: none');

      const modalBackdrops = document.getElementsByClassName('modal-backdrop');
      document.body.removeChild(modalBackdrops[0]);
    }catch(e){

    }
  }
  modifyOrder =(obj1, obj2, obj3=null, value=null)=>{
    try{
      if(obj2 !== 'job_id'){
        this.setState({
          promptStatus: true
        })
      }
      if(this.props.newOrder[0]){
      let orders = [...this.props.newOrder];
      if(obj1==="order_detail"){
        orders[this.props.order_key][obj1][obj2] = obj3;
      }else if(obj1==="extra_detail"){
        orders[this.props.order_key][obj1][parseInt(obj2)][obj3] = value;
      }else if(obj1==="assign_pick_driver" || obj1==="assign_drop_driver"){
        let arr = [];
        obj2.map((e, i)=>{
          e.map((f, j)=>{
            arr.push(f)
            return(
              ''
            )
          })
          return(
            ''
          )
        })
        orders[this.props.order_key][obj1] = arr;
      }else if(obj1==="assign_pick_driver_local" || obj1==="assign_drop_driver_local"){
        orders[this.props.order_key][obj2] = obj3;
      }else if (obj1==="attachment") {
        orders[this.props.order_key][obj1] = obj2;
      }else if(obj1 && obj2 && obj3){
        orders[this.props.order_key][obj1][0][obj2] = obj3;
      }

      this.props.modifyNewOrderCsv(orders);
    }
    }catch(e){
    }
  }
  removeEmptyDriver = ()=>{
    if(this.props.newOrder[0]){
      let orders = this.props.newOrder;
      let assign_pick = orders[this.props.order_key].assign_pick_driver;
      let assign_drop = orders[this.props.order_key].assign_drop_driver;

      orders[this.props.order_key].assign_pick_driver.map((f, j)=>{
        if(Object.entries(f).length === 0 && f.constructor === Object){
          assign_pick.splice(j, 1);
        }else if (!f.address || !f.pickup_date_time  || !f.pickup_date_time_to) {
          assign_pick.splice(j, 1);
        }
        return(
          ''
        )
      });
      orders[this.props.order_key].assign_drop_driver.map((f, j)=>{
        if(Object.entries(f).length === 0 && f.constructor === Object){
          assign_drop.splice(j, 1);
        }else if (!f.address || !f.delivery_date_time  || !f.delivery_date_time_to) {
          assign_drop.splice(j, 1);
        }
        return(
          ''
        )
      });
      orders[this.props.order_key].assign_pick_driver = assign_pick;
      orders[this.props.order_key].assign_drop_driver = assign_drop;
      this.props.modifyNewOrderCsv(orders);
    }
  }
  deleteDriver = (driver_id) =>{
    let orders = this.props.newOrder;
    let pick = [];
    let drop = [];
    orders[this.props.order_key].assign_pick_driver.map((f, j)=>{
      if(parseInt(f.DriverID)!==parseInt(driver_id)){
        pick.push(f);
      }
      return(
        ''
      )
    });
    orders[this.props.order_key].assign_drop_driver.map((f, j)=>{
      if(parseInt(f.DriverID)!==parseInt(driver_id)){
        drop.push(f);
      }
      return(
        ''
      )
    });
    orders[this.props.order_key].assign_pick_driver = pick;
    orders[this.props.order_key].assign_drop_driver = drop;
    this.props.modifyNewOrderCsv(orders);
    this.closeModel();
  }
  getAttachments = (files) =>{
    this.setState({
      attachments: files
    });
    this.modifyOrder('attachment', files)
  }
  deleteAttachments = (name)=>{
    let attachments  = this.state.attachments;
    let arr = [];
    attachments.map((e, i)=>{
      if(e.attachment_name!==name){
        arr.push(e)
      }
      return(
        ''
      )
    })
    this.setState({
      attachments: arr
    })
  }
  getSuggestions = async (text)=>{
    const data = await APIFetch('https://autocomplete.geocoder.api.here.com/6.2/suggest.json?'+this.state.appdata+'&query='+text+'&country=aus');
    let options = [];
    if(data.suggestions){
       data.suggestions.map((s,i)=>(
        options.push({
          value:s.label,
          label:s.label,
        })
      ));
    }
    this.setState({
      data:options
    });
  }
  getLocation = async (obj1, lat, lang, address, location=null, check=null)=>{
    let flg = 0;
    if(address){
    const data = await APIFetch('https://geocoder.api.here.com/6.2/geocode.json?searchtext='+address+'&'+this.state.appdata);
     if(data && data.Response.View.length>0){
       if(data.Response.View[0].Result.length>0){
         if(data.Response.View[0].Result[0].Location){
           flg = 0;
           let pos = data.Response.View[0].Result[0].Location.DisplayPosition;
           this.modifyOrder(obj1, lat, pos.Latitude);
           this.modifyOrder(obj1, lang, pos.Longitude);
         }else {
           flg = 1;
         }
       }else {
         flg = 1;
       }
      }else {
        flg = 1;
      }
    }else {
      flg = 1;
    }
    if(flg && location && check){
      this.setState({
        [location]: '',
        [check]: 'invalid address'
      })
    }else{
      this.setState({
        [check]: ''
      })
    }
    return null;
  }
  addNewField = (field) =>{
    if(field.field_name && field.field_value){
      let fields = this.state.extra_detail;
      fields.push({
        id: null,
        field_name:field.field_name,
        field_value:field.field_value
      });
      this.setState({
        extra_detail: fields
      });
      let orders = this.props.newOrder;
      orders[this.props.order_key].extra_detail = fields;
      this.props.modifyNewOrderCsv(orders);
    }
  }
  openOneModel=(modalId)=>{
    try{
      const modal = document.getElementById(modalId);
      modal.classList.add('show');
      //modal.setAttribute('aria-hidden', 'true');
      modal.setAttribute('style', 'display: block');

      const modalBackdrops = document.getElementsByClassName('modal-backdrop');
      document.body.appendChild(modalBackdrops[0]);
    }catch(e){

    }
  }
  makeid =()=>{
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (let i = 0; i < 6; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  setSenderReceiver = (type1, type2, data, value)=>{
    let payload = {};
    if(data && this.state.senderReceiver && this.state.senderReceiver.length>0){
      let senderReceiver = this.state.senderReceiver[data];
      if(senderReceiver && senderReceiver.id === 0){
        payload = {
          kid: 0,
          name: senderReceiver.name,
          primary_contact: senderReceiver.primary_contact,
          pri_email_id: senderReceiver.pri_email_id,
          pri_phone: senderReceiver.pri_phone,
          secondary_contact: senderReceiver.secondary_contact,
          sec_email_id: senderReceiver.sec_email_id,
          sec_phone: senderReceiver.sec_phone,
          business_address: senderReceiver.business_address,
          contract_start_date: senderReceiver.contract_start_date,
          contract_end_date: senderReceiver.contract_end_date,
          contract_documents: senderReceiver.contract_documents,
          Customer_code: senderReceiver.Customer_code,
          is_sender : value
        }
      }else if(senderReceiver && senderReceiver.id !== 0){
        payload = {
          kid: senderReceiver.id,
          name: senderReceiver.name,
          is_sender : value
        }

      }
      this.modifyOrder(type1, type2, payload);
    }
  }
  addMore = ()=>{
      this.props.getSenderReceiver('orders/comsendreceive/');
  }
  render(){
    let notReq = '';
    let driverDetails = [];
    let heading_id = [];
    let coll_id = [];
    if(this.props.newOrder[0]){
      let orders =  this.props.newOrder[this.props.order_key];
      if(this.state.driverEdit){
        if(orders && orders.assign_pick_driver.length>0 && orders.assign_drop_driver.length>0){
          orders.assign_pick_driver.map((f, i)=>{
            let flg=1;
            for(let j=0;j<driverDetails.length;j++){
              if(driverDetails[j].name===f.name){
                flg=0;
                break;
              }
            }

            if(flg===1){
              driverDetails.push({
                driver_id: f.DriverID,
                name: f.name,
                truck_type: f.truck_type,
                ass_truck_name: f.ass_truck_name,
                ass_truck_id: f.ass_truck_id,
                pickup: [],
                delivery: []
              });
            }
            return(
              ''
            )
          });

          orders.assign_pick_driver.map((f, i)=>{
            driverDetails.map((e, i)=>{
              if(e.driver_id===f.DriverID && i<driverDetails.length){
                let sender = '';
                if(f.sender && (f.sender.kid || f.sender.id)){
                  sender = {
                    id: f.sender.kid? f.sender.kid: f.sender.id,
                    name: f.sender.name
                  }
                }
                driverDetails[i].pickup.push({
                  sender: sender,
                  pick_id: f.id,
                  DriverID: f.DriverID,
                  actual_pickup_date_time: f.actual_pickup_date_time,
                  address: f.address,
                  assigned_quantity: f.assigned_quantity,
                  created_DT: f.created_DT,
                  latitude: f.latitude,
                  longitude: f.longitude,
                  pickup_date_time: f.pickup_date_time,
                  pickup_date_time_to: f.pickup_date_time_to,
                  updated_DT: f.updated_DT,
                  asset_end: f.asset_end,
                  asset: f.asset
                })
              }
              return(
                ''
              )
            });
            return(
              ''
            )
          });
          orders.assign_drop_driver.map((f, i)=>{
            driverDetails.map((e, i)=>{
              if(e.driver_id===f.DriverID && i<driverDetails.length){
                let receiver = '';
                if(f.receiver && (f.receiver.kid || f.receiver.id)){
                  receiver = {
                    id: f.receiver.kid? f.receiver.kid: f.receiver.id,
                    name: f.receiver.name
                  }
                }
                driverDetails[i].delivery.push({
                  receiver: receiver,
                  drop_id: f.id,
                  DriverID: f.DriverID,
                  actual_delivery_date_time: f.actual_delivery_date_time,
                  address: f.address,
                  assigned_quantity: f.assigned_quantity,
                  created_DT: f.created_DT,
                  latitude: f.latitude,
                  longitude: f.longitude,
                  delivery_date_time: f.delivery_date_time,
                  delivery_date_time_to: f.delivery_date_time_to,
                  updated_DT: f.updated_DT,
                  asset_start: f.asset_start,
                  asset: f.asset
                });
              }
              return(
                ''
              )
            });
            return(
              ''
            )
          });
        }
      }else{
        if(orders && orders.assign_pick_driver && orders.assign_pick_driver.length>0 && orders.assign_drop_driver && orders.assign_drop_driver.length>0){
          orders.assign_pick_driver.map((f, i)=>{
            let flg=1;
            for(let j=0;j<driverDetails.length;j++){
              if(driverDetails[j].name===f.OrderDriverID.DriverID.name){
                flg=0;
                break;
              }
            }
            if(flg===1){
              driverDetails.push({
                driver_id: f.OrderDriverID.DriverID.id,
                name: f.OrderDriverID.DriverID.name,
                truck_type: f.OrderDriverID.DriverID.TruckType.truck_category.categ_name,
                ass_truck_name: f.OrderDriverID.AssetID && f.OrderDriverID.AssetID.registeration_no? f.OrderDriverID.AssetID.asset_id+"-"+f.OrderDriverID.AssetID.registeration_no: '',
                ass_truck_id: f.OrderDriverID.AssetID && f.OrderDriverID.AssetID.id? f.OrderDriverID.AssetID.id: '',
                pickup: [],
                delivery: []
              });
            }
            return(
              ''
            )
          });
          orders.assign_pick_driver.map((f, i)=>{
            driverDetails.map((e, i)=>{
              if(e.driver_id===f.OrderDriverID.DriverID.id && i<driverDetails.length){
                let sender = '';
                if(f.sender && (f.sender.kid || f.sender.id)){
                  sender = {
                    id: f.sender.kid? f.sender.kid: f.sender.id,
                    name: f.sender.name
                  }
                }
                driverDetails[i].pickup.push({
                  sender: sender,
                  pick_id: f.id,
                  DriverID: f.OrderDriverID.DriverID.id,
                  actual_pickup_date_time: f.actual_pickup_date_time,
                  address: f.address,
                  assigned_quantity: f.assigned_quantity,
                  created_DT: f.created_DT,
                  latitude: f.latitude,
                  longitude: f.longitude,
                  pickup_date_time: f.pickup_date_time,
                  pickup_date_time_to: f.pickup_date_time_to,
                  updated_DT: f.updated_DT,
                  asset_end: f.asset_end,
                  asset: f.asset && f.asset.length>0? f.asset: []
                });
              }
              return(
                ''
              )
            });
            return(
              ''
            )
          });
          orders.assign_drop_driver.map((f, i)=>{
            driverDetails.map((e, i)=>{
              if(e.driver_id===f.OrderDriverID.DriverID.id  && i<driverDetails.length){
                let receiver = '';
                if(f.receiver && (f.receiver.kid || f.receiver.id)){
                  receiver = {
                    id: f.receiver.kid? f.receiver.kid: f.receiver.id,
                    name: f.receiver.name
                  }
                }
                driverDetails[i].delivery.push({
                  receiver: receiver,
                  drop_id: f.id,
                  DriverID: f.OrderDriverID.DriverID.id,
                  actual_delivery_date_time: f.actual_delivery_date_time,
                  address: f.address,
                  assigned_quantity: f.assigned_quantity,
                  created_DT: f.created_DT,
                  latitude: f.latitude,
                  longitude: f.longitude,
                  delivery_date_time: f.delivery_date_time,
                  delivery_date_time_to: f.delivery_date_time_to,
                  updated_DT: f.updated_DT,
                  asset_start: f.asset_start,
                  asset: f.asset && f.asset.length>0? f.asset: []
                });
              }
              return(
                ''
              )
            });
            return(
              ''
            )
          });
        }

      }

      for(let i = 0;i<driverDetails.length;i++){
        coll_id.push(makeid());
        heading_id.push(makeid());
        if(orders.pick_asset && orders.pick_asset.length>0){
          driverDetails[i].pickup.map((f, j)=>{
            orders.pick_asset.map((x, y)=>{
              if(f.pick_id === x.PickID){
                driverDetails[i].pickup[j].asset.push(x.AssetID)
              }
              return('')
            })
            return('')
          })
        }

        if(orders.drop_asset && orders.drop_asset.length>0){
          driverDetails[i].delivery.map((f, j)=>{
            orders.drop_asset.map((x, y)=>{
              if(f.drop_id === x.DropID){
                driverDetails[i].delivery[j].asset.push(x.AssetID)
              }
              return('')
            })
            return('')
          })
        }
      }
    }
    return(
      <React.Fragment>
      <Prompt
        when={this.state.promptStatus}
        message='You have unsaved changes, are you sure you want to leave?'
      />
      <div className="boxCommanBorder">
          <div className="row mb-3">
              <div className="col-md-4">
                  <ul className="listCustomerDetail text-left">
                      <li>
                          <div className="form-group">
                          <label>Job Number</label>
                          <input type="number" readOnly={this.state.job_id_req} required onChange={(e)=>{
                              this.setState({job_id:e.target.value});
                          }} onBlur={(e)=>{
                            if(e.target.value){
                              this.setState({
                                job_id_error: false
                              })
                              this.modifyOrder("order_detail", "job_id", e.target.value);
                            }else{
                              this.setState({
                                job_id_error: true
                              })
                            }
                          }} placeholder="" className="form-control" value={this.state.job_id} />
                          {this.state.job_id_error?
                            <span className="text-danger">required</span>
                          :null}
                          </div>
                      </li>
                  </ul>
              </div>
              <div className="col-md-4">
                  <ul className="listAttachment">
                    {this.state.attachments.length>0?
                      Array.from(this.state.attachments).map((e, i)=>(
                        <li key={i}>
                            <div style={{"marginBottom":"5px"}} className="alert alertAttachment alert-dismissible fade show" role="alert">
                                <button type="button" data-toggle="modal" data-target="#uploadattachments" className="close" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <i className="fas fa-paperclip"></i>
                                <h5>{e.attachment_name}</h5>
                                <h6>{e.size/1000}KB</h6>
                            </div>
                        </li>
                      )):null}
                  </ul>
              </div>
              <div className="col-md-4">
                  <ul className="listCustomerDetail">
                      <li><a href="#a" data-toggle="modal" data-target="#uploadattachments" className="btnIcon"><i className="fas fa-paperclip"></i></a></li>

                      {this.props.newOrder && this.props.newOrder[0] && this.props.newOrder[0].order_detail && this.props.newOrder[0].order_detail.create?
                        <li><a href="#a" onClick={(e)=>{
                          e.preventDefault();
                          this.props.saveAsDraft(this.props.order_key);
                        }} className="btnIcon"><i className="far fa-save"></i></a></li>
                      :null}
                        {this.props.editOrder? null:
                          <React.Fragment>
                          <li><a href="#a" onClick={(e)=>{
                            e.preventDefault();
                            this.props.deleteForm(this.props.order_key);
                          }} className="btnIcon textRed"><i className="far fa-trash-alt"></i></a></li>
                          <li>
                            <button type="button" onClick={(e)=>{
                              this.submitIndData();
                            }}>Submit</button>
                          </li>
                          </React.Fragment>
                        }
                  </ul>
              </div>
          </div>

              <h4 className="headingBorder"><span>Job Details</span></h4>
              <div className="row mb-2">
                <div className="col-md-4">
                    <div className="form-group">
                        <label>Customer Name <span className="text-danger">*</span></label>
                        <select required onFocus={(e)=>{
                          this.props.getCustomerList();
                        }} onChange={(e)=>{
                          let x = e.target.value;
                          if(x==="new$Customer"){
                              this.openOneModel('modelInviteCustomer');
                          }else{
                            x = x.split('$');
                            this.setState({
                              Customer_Name:x[0],
                              CustomerCompanyID: x[1],
                              customer_code: x[2]
                            });
                          }
                        }} onBlur={(e)=>{
                          if(e.target.value){
                            this.setState({
                              Customer_Name_error: false
                            })
                            let x = e.target.value;
                            if(x==="new$Customer"){
                                this.openOneModel('modelInviteCustomer');
                            }else{
                              x = x.split('$');
                              this.modifyOrder("order_detail", "Customer_Name", x[0]);
                              this.modifyOrder("order_detail", "CustomerCompanyID", x[1]);
                            }
                          }else{
                            this.setState({
                              Customer_Name_error: true
                            })
                          }
                        }} value={`${this.state.Customer_Name}$${this.state.CustomerCompanyID}`} className="form-control">
                          <option value="">select</option>
                          {this.props.customerList.length>0?
                            this.props.customerList.map((e, i)=>{
                              if(e.name!=="NOT REQUIRED" || e.name!=="NA"){
                                return(
                                  <option key={i} value={`${e.CompanyID.name}$${e.CompanyID.id}`}>{e.CompanyID.name}</option>
                                )
                              }else{
                                notReq = <option key={i} value={`${e.CompanyID.name}$${e.CompanyID.id}`}>{e.CompanyID.name}</option>
                                return('')
                              }
                            }):null}
                            {notReq}
                          <option value="new$Customer">Add Customer</option>
                        </select>
                        {this.state.Customer_Name_error?
                          <span className="text-danger">required</span>
                        :null}
                    </div>
                </div>
                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Customer Code</label>
                          <input type="text" onChange={(e)=>{
                            if(e.target.value && charValidation(e.target.value)){
                              this.setState({customer_code:e.target.value});
                            }else {
                              this.setState({
                                customer_code: ''
                              })
                            }
                          }} onBlur={(e)=>{
                              this.modifyOrder("order_detail", "customer_code", e.target.value);
                          }} value={this.state.customer_code} className="form-control"/>
                      </div>
                  </div>

                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Carrier Name</label>
                          <input type="text" required onChange={(e)=>{
                            if(e.target.value && charValidation(e.target.value)){
                              this.setState({Carrier:e.target.value});
                            }else {
                              this.setState({
                                Carrier: ''
                              })
                            }
                          }} onBlur={(e)=>{
                            this.modifyOrder("order_detail", "Carrier", e.target.value);
                          }} value={this.state.Carrier} placeholder="" readOnly className="form-control" />
                      </div>
                  </div>
              </div>
              <h4 className="headingBorder"><span>ORDER Details</span></h4>

              <div className="row">
                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Pick Up Time Slot <span className="text-danger">*</span></label>
                          <div className="row no-gutters rowTimeSlot">
                              <div className="col-md-6">
                              <Datetime
                                dateFormat="DD/MM/YY"
                                utc={true}
                                onChange={(e)=>{
                                  this.setState({pickup_date_time:e});
                              //    this.modifyOrder("order_detail", "pickup_date_time", e);
                                  this.modifyOrder("pickup", "pickup_date_time", e);
                                }} value={this.state.pickup_date_time}
                                inputProps={{required: true}}
                                onBlur={(e)=>{
                                  if(e){
                                    let d = moment(this.state.pickup_date_time).add(this.state.timeslot, 'minutes');
                                    this.setState({
                                      pickup_date_time_to:d,
                                      pickup_date_time_error: false,
                                      pickup_date_time_to_error: false
                                    })
                                    this.modifyOrder("pickup", "pickup_date_time_to", d);
                                  }else{
                                    this.setState({
                                      pickup_date_time_error: true
                                    })
                                  }
                                }}
                              />
                              {this.state.pickup_date_time_error?
                                <span className="text-danger">required</span>
                              :null}
                              </div>
                              <div className="col-md-6">
                              <Datetime
                                dateFormat="DD/MM/YY"
                                utc={true}
                                onChange={(e)=>{
                                  this.setState({pickup_date_time_to:e});
                              //    this.modifyOrder("order_detail", "pickup_date_time", e);
                                  this.modifyOrder("pickup", "pickup_date_time_to", e);
                                }} value={this.state.pickup_date_time_to}
                                inputProps={{required: true}}
                                onBlur={(e)=>{
                                  if(e){
                                    this.setState({
                                      pickup_date_time_to_error: false
                                    })
                                  }else{
                                    this.setState({
                                      pickup_date_time_to_error : true
                                    })
                                  }
                                }}
                              />
                              {this.state.pickup_date_time_to_error?
                                <span className="text-danger">required</span>
                              :null}
                              </div>
                          </div>

                      </div>
                  </div>

                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Pick Up Address <span className="text-danger">*</span></label>
                          <input list={this.state.datalist1} type="text" required onChange={(e)=>{
                            if(e.target.value && charValidation(e.target.value)){
                              this.getSuggestions(e.target.value);
                              this.setState({pickup_location:e.target.value});
                            }else {
                              this.setState({pickup_location:''});
                            }
                          }} onBlur={(e)=>{
                            if(e.target.value){

                            }else{
                              this.setState({

                              })
                            }
                            this.setState({data:[]});
                            this.getLocation("pickup", "longitude", "latitude", e.target.value, 'pickup_location', 'checkPickAddress');
                          //  this.modifyOrder("order_detail", "address", e.target.value);
                            this.modifyOrder("pickup", "address", e.target.value);
                          }} value={this.state.pickup_location} className="form-control" />
                          <datalist id={this.state.datalist1}>
                            <option value="">select</option>
                            {this.state.data.length>0?
                            this.state.data.map((e, i)=>{
                              return(
                                <option key={i} value={e.value}>{e.label}</option>
                              )
                            }):null}
                          </datalist>
                          {this.state.pickup_location?
                            <a href="#map" onClick={(e)=>{
                              e.preventDefault();
                              let order = this.props.newOrder[this.props.order_key].pickup[0];
                              if(order.latitude && order.longitude){
                                this.setState({
                                  map_address: this.state.pickup_location,
                                  map_lat: order.latitude,
                                  map_lng: order.longitude,
                                  showMap: true
                                })
                              }
                            }}>view on map</a>:
                          null}
                          <span className="text-danger"> {this.state.checkPickAddress}</span>
                      </div>
                  </div>
                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Sender</label>
                          <select onChange={(e)=>{
                            if(e.target.value==="addMore"){
                              this.openSenModel()
                            }else{
                              this.setState({sender:e.target.value});
                              this.setSenderReceiver('pickup', 'sender', e.target.value, true)
                            }
                          }} value={this.state.sender} className="form-control">
                            <option value="">select</option>
                            {this.state.senderReceiver && this.state.senderReceiver.length>0?
                              this.state.senderReceiver.map((e, i)=>{
                                if(e.is_sender){
                                  return(
                                    <option key={i} value={i}>{e.name}</option>
                                  )
                                }else {
                                  return(
                                    ''
                                  )
                                }
                              })
                            :null}
                            <option value="addMore">Add Sender</option>
                          </select>
                      </div>
                  </div>
              </div>
              <div className="row mb-2">
                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Delivery Time Slot <span className="text-danger">*</span></label>
                          <div className="row no-gutters rowTimeSlot">
                              <div className="col-md-6">
                              <Datetime
                                dateFormat="DD/MM/YY"
                                utc={true}
                                onChange={(e)=>{
                                  this.setState({delivery_date_time:e});
                                //  this.modifyOrder("order_detail", "delivery_date_time", e);
                                  this.modifyOrder("delivery", "delivery_date_time", e);
                                }} value={this.state.delivery_date_time}
                                inputProps={{required: true}}
                                onBlur={(e)=>{
                                  if(e){
                                    let d = moment(this.state.delivery_date_time).add(this.state.timeslot, 'minutes');
                                    this.setState({
                                      delivery_date_time_to:d,
                                      delivery_date_time_error: false,
                                      delivery_date_time_to_error: false
                                    })
                                    this.modifyOrder("delivery", "delivery_date_time_to", d);
                                  }else{
                                    this.setState({
                                      delivery_date_time_error: true
                                    })
                                  }
                                }}
                              />
                              {this.state.delivery_date_time_error?
                                <span className="text-danger">required</span>
                              :null}
                              </div>
                              <div className="col-md-6">
                              <Datetime
                                dateFormat="DD/MM/YY"
                                utc={true}
                                onChange={(e)=>{
                                  this.setState({delivery_date_time_to:e});
                                //  this.modifyOrder("order_detail", "delivery_date_time", e);
                                  this.modifyOrder("delivery", "delivery_date_time_to", e);
                                }} value={this.state.delivery_date_time_to}
                                inputProps={{required: true}}
                                onBlur={(e)=>{
                                  if(e){
                                    this.setState({
                                      delivery_date_time_to_error: false
                                    })
                                  }else{
                                    this.setState({
                                      delivery_date_time_to_error: true
                                    })
                                  }
                                }}
                              />
                              {this.state.delivery_date_time_to_error?
                                <span className="text-danger">required</span>
                              :null}
                              </div>
                          </div>

                      </div>
                  </div>

                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Delivery Address <span className="text-danger">*</span></label>
                          <input list={this.state.datalist} type="text" className="form-control" required onChange={(e)=>{
                            if(e.target.value && charValidation(e.target.value)){
                              this.getSuggestions(e.target.value);
                              this.setState({delivery_location:e.target.value});
                            }else {
                              this.setState({delivery_location:''});
                            }
                          }} onBlur={(e)=>{
                            if(e.target.value){

                            }else{
                              this.setState({

                              })
                            }
                            this.setState({data:[]});
                            this.getLocation("delivery", "longitude", "latitude", e.target.value, 'delivery_location', 'checkDropAddress');
                          //  this.modifyOrder("order_detail", "address", e.target.value);
                            this.modifyOrder("delivery", "address", e.target.value);
                          }} value={this.state.delivery_location}/>
                          <datalist id={this.state.datalist}>
                            <option value="">select</option>
                            {this.state.data.length>0?
                            this.state.data.map((e, i)=>{
                              return(
                                <option key={i} value={e.value}>{e.label}</option>
                              )
                            }):null}
                          </datalist>
                          {this.state.delivery_location?
                            <a href="#map" onClick={(e)=>{
                              e.preventDefault();
                              let order = this.props.newOrder[this.props.order_key].delivery[0];
                              if(order.latitude && order.longitude){
                                this.setState({
                                  map_address: this.state.delivery_location,
                                  map_lat: order.latitude,
                                  map_lng: order.longitude,
                                  showMap: true
                                })
                              }
                            }}>view on map</a>:
                          null}
                          <span className="text-danger"> {this.state.checkDropAddress}</span>
                      </div>
                  </div>
                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Receiver</label>
                          <select onChange={(e)=>{
                            if(e.target.value==="addMore"){
                              this.openRecModel();
                            }else{
                              this.setState({receiver:e.target.value});
                              this.setSenderReceiver('delivery', 'receiver', e.target.value, false)
                            }
                          }} value={this.state.receiver} className="form-control">
                            <option value="">select</option>
                            {this.state.senderReceiver && this.state.senderReceiver.length>0?
                              this.state.senderReceiver.map((e, i)=>{
                                if(!e.is_sender){
                                  return(
                                    <option key={i} value={i}>{e.name}</option>
                                  )
                                }else {
                                  return(
                                    ''
                                  )
                                }
                              })
                            :null}
                            <option value="addMore">Add Receiver</option>
                          </select>
                      </div>
                  </div>
              </div>
              <h4 className="headingBorder"><span>Load Details</span></h4>
              <div className="row">
                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Load Carried</label>
                          <input type="text" className="form-control" onChange={(e)=>{
                            if(e.target.value && charValidation(e.target.value)){
                              this.setState({load_carried:e.target.value});
                            }else {
                              this.setState({
                                load_carried: ''
                              })
                            }
                          }} onBlur={(e)=>{
                            if(e.target.value){
                              this.setState({
                                load_carried_error: false
                              })
                              this.modifyOrder("order_detail", "load_carried", e.target.value);
                            }else{
                              // this.setState({
                              //   load_carried_error: true
                              // })
                            }
                          }} value={this.state.load_carried}/>
                          {this.state.load_carried_error?
                            <span className="text-danger">required</span>
                          :null}
                      </div>
                  </div>
                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Load Type</label>
                          <input type="text" className="form-control" onChange={(e)=>{
                            if(e.target.value && charValidation(e.target.value)){
                              this.setState({load_type:e.target.value});
                            }else {
                              this.setState({
                                load_type: ''
                              })
                            }
                          }} onBlur={(e)=>{
                            if(e.target.value){
                              this.modifyOrder("order_detail", "load_type", e.target.value);
                              this.setState({
                                load_type_error:false
                              })
                            }else{
                              // this.setState({
                              //   load_type_error:true
                              // })
                            }

                          }} value={this.state.load_type}/>
                          {this.state.load_type_error?
                            <span className="text-danger">required</span>
                          :null}
                      </div>
                  </div>
                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Load Weight</label>
                          <input type="text" className="form-control" onChange={(e)=>{
                            if(e.target.value && charValidation(e.target.value)){
                              this.setState({load_weight:e.target.value});
                            }else {
                              this.setState({
                                load_weight: ''
                              })
                            }
                          }} onBlur={(e)=>{
                            if(e.target.value){
                              this.setState({
                                load_weight_error:false
                              })
                              this.modifyOrder("order_detail", "load_weight", e.target.value);
                            }else{
                              // this.setState({
                              //   load_weight_error:true
                              // })
                            }

                          }} value={this.state.load_weight}/>
                          {this.state.load_weight_error?
                            <span className="text-danger">required</span>
                          :null}
                      </div>
                  </div>
              </div>
              <div className="row mb-2">
                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Load Dimension</label>
                          <div className="row rowLoadDimension">
                              <div className="col-md-3">
                                  <input type="text" className="form-control" onChange={(e)=>{
                                    if(Number(e.target.value)){
                                      this.setState({
                                        length_load_dimen:e.target.value
                                      });
                                    }else{
                                      this.setState({
                                        length_load_dimen:''
                                      });
                                    }
                                  }} onBlur={(e)=>{
                                    if(e.target.value){
                                      this.setState({
                                        length_load_dimen_error: false
                                      })
                                      if(Number(e.target.value)){
                                        this.modifyOrder("order_detail", "length_load_dimen", e.target.value);
                                      }
                                    }else{
                                      // this.setState({
                                      //   length_load_dimen_error: true
                                      // })
                                    }
                                  }} value={this.state.length_load_dimen}/>
                                  {this.state.length_load_dimen_error?
                                    <span className="text-danger">required</span>
                                  :null}
                              </div>
                              <div className="col-md-3">
                                  <input type="text" className="form-control" onChange={(e)=>{
                                    if(Number(e.target.value)){
                                      this.setState({
                                        height_load_dimen:e.target.value
                                      });
                                    }else{
                                      this.setState({
                                        height_load_dimen:''
                                      });
                                    }
                                  }} onBlur={(e)=>{
                                    if(e.target.value){
                                      this.setState({
                                        height_load_dimen_error: false
                                      })
                                      if(Number(e.target.value)){
                                        this.modifyOrder("order_detail", "height_load_dimen", e.target.value);
                                      }
                                    }else{
                                      // this.setState({
                                      //   height_load_dimen_error: true
                                      // })
                                    }
                                  }} value={this.state.height_load_dimen}/>
                                  {this.state.height_load_dimen_error?
                                    <span className="text-danger">required</span>
                                  :null}
                              </div>
                              <div className="col-md-3">
                                  <input type="text" className="form-control" onChange={(e)=>{
                                    if(Number(e.target.value)){
                                      this.setState({
                                        width_load_dimen:e.target.value
                                      });
                                    }else{
                                      this.setState({
                                        width_load_dimen:''
                                      });
                                    }
                                  }} onBlur={(e)=>{
                                    if(e.target.value){
                                      this.setState({
                                        width_load_dimen_error: false
                                      })
                                      if(Number(e.target.value)){
                                        this.modifyOrder("order_detail", "width_load_dimen", e.target.value);
                                      }
                                    }else{
                                      // this.setState({
                                      //   width_load_dimen_error: true
                                      // })
                                    }

                                  }} value={this.state.width_load_dimen}/>
                                  {this.state.width_load_dimen_error?
                                    <span className="text-danger">required</span>
                                  :null}
                              </div>
                              <div className="col-md-3">
                              <select value={this.state.measure_unit} onChange={(e)=>{
                                this.setState({
                                  measure_unit:e.target.value
                                });
                              }} onBlur={(e)=>{
                                if(e.target.value){
                                  this.setState({
                                    measure_unit_error: false
                                  })
                                  this.modifyOrder("order_detail", "measure_unit", e.target.value);
                                }else{
                                  // this.setState({
                                  //   measure_unit_error: true
                                  // })
                                }
                              }} className="form-control" style={{"width":"80px"}} id="">
                                  <option value="">select</option>
                                  <option>cm</option>
                                  <option>inch</option>
                                  <option>feet</option>
                                  <option>meter</option>
                              </select>
                              {this.state.measure_unit_error?
                                <span className="text-danger">required</span>
                              :null}
                              </div>
                          </div>
                      </div>
                  </div>
                  <div className="col-md-4">
                      <div className="form-group">
                          <label>No. Of Units</label>
                          <input type="text" className="form-control" onChange={(e)=>{
                            if(e.target.value && charValidation(e.target.value)){
                              this.setState({units:e.target.value});
                            }else {
                              this.setState({
                                units: ''
                              })
                            }
                          }} onBlur={(e)=>{
                            if(e.target.value){
                              this.setState({
                                units_error: false
                              })
                              this.modifyOrder("order_detail", "units", e.target.value);
                            }else{
                              this.setState({
                                units_error: true
                              })
                            }
                          }} value={this.state.units}/>

                      </div>
                  </div>
                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Price</label>
                          <input type="text" className="form-control" onChange={(e)=>{
                            if(e.target.value && charValidation(e.target.value)){
                              this.setState({price:e.target.value});
                            }else {
                              this.setState({
                                price: ''
                              })
                            }
                          }} onBlur={(e)=>{
                            if(e.target.value){
                              this.setState({
                                price_error: false
                              })
                              this.modifyOrder("order_detail", "price", e.target.value);
                            }else{
                              this.setState({
                                price_error: true
                              })
                            }
                          }} value={this.state.price}/>

                      </div>
                  </div>
              </div>
              <h4 className="headingBorder"><span>Additional Information</span></h4>
              <div className="row">
                  <div className="col-md-12">
                      <div className="form-group pb-3">
                          <textarea className="form-control" onChange={(e)=>{
                            if(e.target.value && e.target.value.length<501){
                              this.setState({
                                additional_note:e.target.value,
                                additional_note_count: Number(500 - parseInt(e.target.value.length))
                              });
                            }else if(!e.target.value){
                              this.setState({
                                additional_note: '',
                                additional_note_count: 500
                              })
                            }
                          }} onBlur={(e)=>{
                            this.modifyOrder("order_detail", "additional_note", e.target.value);
                          }} value={this.state.additional_note} placeholder="Add Additional Notes"></textarea>
                          <span className="text-success">{this.state.additional_note_count} characters is remaining.</span>
                      </div>
                  </div>
                  <div className="col-md-12">

                          <input type="checkbox" checked={this.state.hazardous} onChange={(e)=>{
                            if(e.target.checked){
                              this.setState({hazardous: true});
                            }else{
                              this.setState({hazardous: false});
                            }
                          }} onBlur={(e)=>{
                            this.modifyOrder("order_detail", "hazardous", e.target.value);
                          }} value={this.state.hazardous} />
                          <label >&nbsp;Contains Hazardous & Dangerous Goods</label>

                  </div>
              </div>
              <h4 className="headingBorder"><span>DRIVER Details <span className="textSplit"><img src={split} alt="" title=""/> Split</span></span></h4>
              <div className="tableDriverDetails">
              <div className="accordion accordionAssignDriver" id="accordionExample">
                {!this.state.assignDriverModel && driverDetails.length>0?
                  driverDetails.map((e, i)=>{
                    return(
                  <div key={i} className="card">
                    <div className="card-header" id={heading_id[i]}>
                      <div data-toggle="collapse" className=" tablecollapsedHeader" data-target={`#${coll_id[i]}`} aria-expanded="true" aria-controls={coll_id[i]}>
                          <table className="table tableDriver">
                              <thead>
                                  <tr>
                                      <th>#</th>
                                      <th>Driver Name</th>
                                      <th>Vehicle TYPE</th>
                                      <th>Actions</th>
                                  </tr>
                              </thead>
                          </table>
                      </div>
                    </div>
                    <div id={coll_id[i]} className="collapse show" aria-labelledby={heading_id[i]} data-parent="#accordionExample">
                      <div className="card-body">
                          <div className="maintableAssignDriver">
                            <table className="table tableDriver">
                                <tbody>
                                    <tr>
                                      <td>{this.state.job_id}-{i+1}.</td>
                                      <td>{e.name}</td>
                                      <td className="font-medium">{e.ass_truck_name}</td>
                                      <td>

                                      <ul className="listAction">
                                          <li><a href="#a" onClick={(e)=>{
                                            e.preventDefault();
                                            this.setState({
                                              assignDriverModel: true
                                            })
                                          }}><i className="fas fa-pencil-alt"></i></a></li>
                                          <li><a href="#a" onClick={(el)=>{
                                            el.preventDefault();
                                            this.deleteDriver(e.driver_id);
                                          }} className="textRed"><i className="far fa-trash-alt"></i></a></li>
                                      </ul>

                                      </td>
                                      </tr>
                                </tbody>
                            </table>
                            <div className="tableAssignDriver">
                                <div className="table-responsive">
                                    <table className="table sub-table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Order QTY.</th>
                                                <th>PICK UP LOCATION</th>
                                                <th>PICK UP TIME FROM</th>
                                                <th>PICK UP TIME TO</th>
                                                <th>SENDER</th>
                                                <th>ASSETS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {driverDetails[i].pickup.map((x, i)=>{
                                        return(
                                          <tr key={i}>
                                            <td>{this.state.job_id}-{i+1}.</td>
                                            <td>{x.assigned_quantity}</td>
                                            <td style={{maxWidth: '150px'}}>{x.address}</td>
                                            <td>{formateDate(x.pickup_date_time)}</td>
                                            <td>{formateDate(x.pickup_date_time_to)}</td>
                                            <td>{x.sender && x.sender.name ? x.sender.name: null}</td>
                                            <td>
                                              <ul style={{listStyle: 'none'}}>
                                              {x.asset && x.asset.length>0?
                                                x.asset.map((e, i)=>(
                                                  <li key={i}>{i+1}. {e.asset_id} - {e.registeration_no}</li>
                                                ))
                                              :null}
                                              </ul>
                                            </td>
                                          </tr>
                                        )
                                      })}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div className="tableAssignDriver">
                                    <div className="table-responsive">
                                        <table className="table sub-table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Order QTY.</th>
                                                    <th>DELIVERY LOCATION</th>
                                                    <th>DELIVERY TIME FROM</th>
                                                    <th>DELIVERY TIME TO</th>
                                                    <th>RECEIVER</th>
                                                    <th>ASSETS</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            {driverDetails[i].delivery.map((x, i)=>{
                                            return(
                                              <tr key={i}>
                                                <td>{this.state.job_id}-{i+1}.</td>
                                                <td>{x.assigned_quantity}</td>
                                                <td style={{maxWidth: '150px'}}>{x.address}</td>
                                                <td>{formateDate(x.delivery_date_time)}</td>
                                                <td>{formateDate(x.delivery_date_time_to)}</td>
                                                <td>{x.receiver && x.receiver.name ? x.receiver.name: null}</td>
                                                <td>
                                                  <ul style={{listStyle: 'none'}}>
                                                  {x.asset && x.asset.length>0?
                                                    x.asset.map((e, i)=>(
                                                      <li key={i}>{i+1}. {e.asset_id} - {e.registeration_no}</li>
                                                    ))
                                                  :null}
                                                  </ul>
                                                </td>
                                              </tr>
                                            )
                                          })}
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                      </div>
                      </div>
                    </div>
                  </div>
                )})
                : <div className="rowAddMore">
                    <div className="row align-items-center">
                        <div className="col-sm-12 col-md-6">

                            <React.Fragment>
                            <a href="#assigndriver" onClick={(e)=>{
                              e.preventDefault();
                              this.setState({
                                assignDriverModel: true
                              })
                            }} className="font-medium"><strong>Allocate Driver</strong></a>

                            {this.state.job_id && this.state.pickup_date_time && this.state.pickup_date_time_to && this.state.pickup_location && this.state.delivery_date_time && this.state.delivery_date_time_to && this.state.delivery_location?
                              <React.Fragment>
                                {this.state.assignDriverModel ?
                                <AssignDriver
                                order={this.props.newOrder}
                                drivers={driverDetails}
                                modifyOrder={this.modifyOrder}
                                Created_by_CarrierID={this.state.Created_by_CarrierID? this.state.Created_by_CarrierID: null}
                                job_id={this.state.job_id}
                                close={(e)=>{
                                  this.removeEmptyDriver();
                                  this.setState({
                                    assignDriverModel:false,
                                    driverEdit: true
                                  });
                                }}
                                cancel={(e)=>{
                                  this.setState({
                                    assignDriverModel:false
                                  });
                                }}
                                address ={{
                                  units: this.state.units,
                                  pickup_date_time: this.state.pickup_date_time,
                                  pickup_date_time_to: this.state.pickup_date_time_to,
                                  pickup_location: this.state.pickup_location,
                                  delivery_date_time: this.state.delivery_date_time,
                                  delivery_date_time_to: this.state.delivery_date_time_to,
                                  delivery_location: this.state.delivery_location
                                }}
                               />:null}
                               </React.Fragment>
                               :<p className="text-danger"> enter the mandatory information before allocating the driver</p>}
                             </React.Fragment>

                        </div>
                    </div>
                </div>}
                </div>
                  <div className="rowAddMore">
                      <div className="row align-items-center">
                          <div className="col-sm-12 col-md-6">
                            {driverDetails.length>0?
                              <React.Fragment>
                                <a href="#a" onClick={(e)=>{
                                  e.preventDefault();
                                  this.setState({
                                    assignDriverModel: true
                                  })
                                }} className="font-medium"><strong>+ Add another driver to this order</strong></a>
                              </React.Fragment>
                            : null}
                          </div>

                      </div>
                  </div>
              </div>

              <br/>
              <h4 className="headingBorder"><span>Extra Fields</span></h4>

              <div className="row">
                  {this.state.extra_detail[0]? this.state.extra_detail.map((f,i)=>(
                    <div key={i} className="col-lg-4">
                      <div className="form-group">
                        <label className="col-form-label">{f.field_name}</label>
                        <input type="text" onChange={(e)=>{
                          if(e.target.value && charValidation(e.target.value)){
                            let fields = this.state.extra_detail;
                            fields[i].field_value = e.target.value;
                            this.setState({
                              extra_detail: fields
                            });
                            this.modifyOrder("extra_detail", i, f.field_name, e.target.value);
                          }else {
                            this.setState({
                              extra_detail: ''
                            })
                            this.modifyOrder("extra_detail", i, f.field_name, e.target.value);
                          }
                        }} className="form-control" value={f.field_value} />
                      </div>
                    </div>
                  )):
                  <div className="col-lg-4">
                    <p>No extra fields
                      <a data-toggle="modal" data-target="#myModal"
                      href="#myModal"> Add Field</a>
                     </p>
                  </div>}
              </div>
              {this.state.extra_detail[0]?<a data-toggle="modal" data-target="#myModal"
               href="#myModal"> Add Field</a>:null}
              <AddExtraField
                close={(e)=>{
                  this.setState({
                    assignDriverModel:false
                  });
                }}
                addNewField = {this.addNewField}
              />
              <UploadAttachments
                getAttachments={this.getAttachments}
                carrier = {this.props.order && this.props.order.order_detail?
                  this.props.order.order_detail.Created_by_CarrierID
                :null}
                valueType = "Created_by_CarrierID"
               />
              {this.state.showMap?
              <Map
                address={this.state.map_address}
                lat={this.state.map_lat}
                lng={this.state.map_lng}
                close={()=>{
                  this.setState({
                    showMap: false
                  })
                }}
              />: null}
      </div>

      {this.state.addSenderModel?
        <AddSender
         closeOneModel={this.closeSenModel}
         addMore={this.addMore}
         />
      :null}

      {this.state.addReceiverModel?
        <AddReceiver
         closeOneModel={this.closeRecModel}
         addMore={this.addMore}
         />
      :null}
    </React.Fragment>
    )
  }
}

CreateOrd.propTypes = {
  modifyNewOrderCsv: PropTypes.func.isRequired,
  getCustomerList: PropTypes.func.isRequired,
  getSenderReceiver: PropTypes.func.isRequired,
  pushSenderReceiver: PropTypes.func.isRequired,
  //newOrder: PropTypes.array.isRequired,
}

const mapStateToProps = state =>({
  newOrder: state.carrier.newOrder,
  customerList: state.carrier.customerList,
  editOrder: state.carrier.editOrder,
  senderReceiver: state.carrier.senderReceiver
});

export default connect(mapStateToProps, {getSenderReceiver, pushSenderReceiver, modifyNewOrderCsv, getCustomerList})(CreateOrd);
