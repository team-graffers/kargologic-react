import React, {Component} from 'react';

class UploadExcelBefore extends Component{
  render(){
    return(
      <li>
        <div className="boxUploadStatus">
          <div className="contentUpload">
              <i className="far fa-file-alt"></i>
              <a href="#a" onClick={(e)=>{
                e.preventDefault();
                this.props.deleteFile(this.props.i);
              }} className="linkCancel" title="Cancel">Cancel</a>
              <div className="progress">
                <span className="progressText">{this.props.file.name}</span>
                <div className="progress-bar" role="progressbar" style={{"width":"0%"}} aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
          </div>
          <ul className="listStatus">
            <li>{this.props.file.size/1000}Kb</li>
            <li className="textBlue">0% Uploaded</li>
          </ul>
        </div>
      </li>
    )
  }
}


export default UploadExcelBefore;
