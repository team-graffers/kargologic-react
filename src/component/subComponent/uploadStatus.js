import React, {Component} from 'react';
import axios from 'axios';
import store from '../../store';

class UploadStatus extends Component{
  constructor(props) {
    super(props);
    this.state = {
      files: [],
      selectedFiles: [],
      upload: false,
      loaded: '0'
    }
  }
  componentDidMount(){
    const data = new FormData();
    data.append('file', this.props.file);
    data.append('file_name', this.props.file.name);
    data.append('order_id', this.props.order_id);
    const state = store.getState();
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': 'Token '+localStorage.getItem("token")
    }
    //"carr/update_profileimage_carr/"
    axios.post(state.carrier.mainUrl+'order_attach/load_attachment/?user_type='+this.props.userType, data, {
      headers: headers,
      onUploadProgress: ProgressEvent => {
        this.setState({
          loaded: (ProgressEvent.loaded / ProgressEvent.total*100),
        })
      }
    })
    .then(res => {
       if(res.data && res.data.data){
         let payload = {
           attachment_name: res.data.data.attachment_name,
           physical_path: res.data.data.physical_path
         }
         this.props.getAttachments(payload);
       }
     });
  }
  render(){
    return(
      <li>
        <div className="boxUploadStatus">
          <div className="contentUpload">
              <i className="far fa-file-alt"></i>
              <div className="progress">
                <span className="progressText">{this.props.file.name}</span>
                <div className="progress-bar" role="progressbar" style={{"width":`${this.state.loaded}%`}} aria-valuenow={this.state.loaded} aria-valuemin="0" aria-valuemax="100"></div>
              </div>
          </div>
          <ul className="listStatus">
            <li>{this.props.file.size/1000}Kb</li>
            <li className="textBlue">{this.state.loaded}% Uploaded</li>
          </ul>
        </div>
      </li>
    )
  }
}


export default UploadStatus;
