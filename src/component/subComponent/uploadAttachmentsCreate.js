import React, {Component} from 'react';
import upload from '../../assets/img/iconUpload.png';
import FileDrop from 'react-file-drop';

class UploadAttachments extends Component{
  constructor(props) {
    super(props);
    this.state = {
      files: [],
      selectedFiles: [],
      upload: false,
    }
  }
  handleDrop = (files, event) => {
    if(files.length>0){
      this.getBase64(files, this.getFiles, this.props.carrier, this.props.valueType)
    }
  }
  getFiles = (files)=>{
    this.setState({
      selectedFiles: [...this.state.selectedFiles, files],
      upload: false
    })
  }
  getBase64 = (files, getFiles, carrier, valueType)=>{
    Array.from(files).map((file, i)=>{
     var reader = new FileReader();
     reader.readAsDataURL(file);
     reader.onload = function () {
       let f = {
         attachment_name: file.name,
         size: file.size,
         [valueType]: carrier,
         image_read: reader.result
       }
       getFiles(f)
     };
     reader.onerror = function (error) {
       console.log('Error: ', error);
     };
     return('')
   })
  }
  deleteFile = (i)=>{
    let files = this.state.files;
    files.splice(i,1);
    this.setState({
      files: files
    });
    this.props.getAttachments(files);
  }
  uploadFiles=()=>{
    let files = this.state.files;
    files.push(...this.state.selectedFiles);
    this.setState({
      files: files,
      selectedFiles: [],
      upload: true
    });
  }
  render(){
    return(
      <div className="modal fade popupAssignDriver modelInviteDriver" id="uploadattachments" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
          <div className="modal-dialog" role="document">
              <div className="modal-content">
                  <div className="modal-body">
                      <a href="#a"  className="btnClose" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </a>
                      <div className="row rowAssign align-items-center">
                          <div className="col-md-12">
                              <h4 className="pb-0 mb-0">Upload Attachments</h4>
                          </div>
                      </div>
                      <div className="mainDragDrop">
                          <FileDrop className="boxDragDrop" onDrop={this.handleDrop}>
                              <div className="boxInput">
                              {this.state.selectedFiles.length===0?
                                <React.Fragment>
                                  <div className="boxIcon"><img src={upload} alt="" title="" /></div>
                                  <input type="file" name="filess[]" onChange={(e)=>{
                                    if(e.target.files.length>0){
                                      this.getBase64(e.target.files, this.getFiles, this.props.carrier, this.props.valueType);
                                    }
                                  }} id="files" className="boxFile" data-multiple-caption="{count} files selected" multiple />
                                  <label htmlFor="files">Drag and drop to upload <span>or <strong>browse</strong> to choose a file.</span></label>
                                </React.Fragment>
                                :<h4 style={{"color":"#4c6476"}}>{this.state.selectedFiles.length} files selected</h4>}
                                  <a href="#attachments" onClick={(e)=>{
                                    e.preventDefault();
                                    this.uploadFiles()
                                  }} className="boxButton btn btn-sm">Upload File</a>
                              </div>
                              <div className="boxUploading">Uploading&hellip;</div>
                              <div className="boxSuccess">Done! <a href="#a" className="boxRestart" role="button">Upload more?</a></div>
                              <div className="boxError">Error! <span></span>. <a href="#a" className="boxRestart" role="button">Try again!</a></div>
                          </FileDrop>
                      </div>
                      <div className="uploadStatus">
                              <ul className="listuploadStatus">
                                {this.state.files.length>0?
                                  Array.from(this.state.files).map((e, i)=>(
                                    <li key={i}>
                                      <div className="boxUploadStatus">
                                        <div className="contentUpload">
                                            <i className="far fa-file-alt"></i>
                                            <a href="#a" onClick={(e)=>{
                                              e.preventDefault();
                                              this.deleteFile(i);
                                            }} className="linkCancel" title="Cancel">Cancel</a>
                                            <div className="progress">
                                              <span className="progressText">{e.attachment_name}</span>
                                              <div className="progress-bar" role="progressbar" style={{"width":"100%"}} aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <ul className="listStatus">
                                          <li>{e.size/1000}Kb</li>
                                          <li className="textBlue">100% Uploaded</li>
                                        </ul>
                                      </div>
                                    </li>
                                  ))
                                :null}
                              </ul>
                      </div>
                      <div className="text-center" style={{"marginTop":"15px"}}>
                      <a href="#a" data-dismiss="modal" aria-label="Close" onClick={()=>{
                        this.props.getAttachments(this.state.files);
                      }} className="boxButton btn btn-sm" style={{"marginRight": "25px"}}>Save Attachments</a>
                      <a href="#a" data-dismiss="modal" aria-label="Close" className="boxButton btn btn-sm">Cancel</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    )
  }
}


export default UploadAttachments;
