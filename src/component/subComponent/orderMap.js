import React, {Component} from 'react';
import {errorAlert} from '../include/alert';
import moment from 'moment';
import store from '../../store';
//import {HereMapLogin, BatchShadows} from '../include/hereMapApi';
import {PostFetch2} from '../ajax/ajaxFetch';

class OrderMap extends Component {
  constructor(props) {
      super(props);
      this.platform = null;
      this.map = null;
      this.marker= null;
      this.state = {
        address: this.props.address,
        useCIT: true,
        app_id: '',
        app_code: '',
        useHTTPS: true,
          center: {
            lat: '',
            lng: '',
          },
          driverRoutes: '',
          points: [],
          zoom: 14,
          map: null,
          marker: null,
          theme: 'normal.day',
      }
  }
  componentWillMount(){
    const state = store.getState();
    this.setState({
      app_id: state.carrier.app_id,
      app_code: state.carrier.app_code
    })
    let data = this.props.driverDetails[0];
    let driverRoutes = [];
    this.props.driverDetails.map((e, i)=>{
      let arr = [];
      if(e.pickup.length>0 && e.delivery.length>0){
        e.pickup.map((f, j)=>{
          arr.push({
            is_pick: true,
            DriverID: f.DriverID,
            actual_date_time: f.actual_pickup_date_time,
            address: f.address,
            assigned_quantity: f.assigned_quantity,
            created_DT: f.created_DT,
            date_time: f.pickup_date_time,
            date_time_to: f.pickup_date_time_to,
            id: f.pick_id,
            latitude: f.latitude,
            longitude: f.longitude,
          })
          return('')
        });
        e.delivery.map((f, j)=>{
          arr.push({
            is_pick: false,
            DriverID: f.DriverID,
            actual_date_time: f.actual_delivery_date_time,
            address: f.address,
            assigned_quantity: f.assigned_quantity,
            created_DT: f.created_DT,
            date_time: f.delivery_date_time,
            date_time_to: f.delivery_date_time_to,
            id: f.drop_id,
            latitude: f.latitude,
            longitude: f.longitude,
          })
          return('')
        })
        let sortArr = arr.sort(function(a,b){
          return moment(a.date_time_to, 'YYYY-MM-DDTHH:mm:ssZ').utc() - moment(b.date_time_to, 'YYYY-MM-DDTHH:mm:ssZ').utc();
        });
        driverRoutes.push(sortArr);
      }
      return('')
    });
  let center = this.state.center;
    center.lat = data.pickup[0].latitude;
    center.lng = data.pickup[0].longitude;
    this.setState({
      center: center,
      driverRoutes: driverRoutes,
    })

  }
  getPlatform() {
      return new window.H.service.Platform(this.state);
  }

  getMap(container, layers, settings) {
      return new window.H.Map(container, layers, settings);
  }

  getEvents(map) {
      return new window.H.mapevents.MapEvents(map);
  }

  getBehavior(events) {
      return new window.H.mapevents.Behavior(events);
  }

  getUI(map, layers) {
      return new window.H.ui.UI.createDefault(map, layers);
  }
  setDriversTrack = ()=>{
    let points = [];
    if(this.props.driverTrack && this.props.driverTrack[0] && this.props.driverTrack[0].body && this.props.driverTrack[0].body.reported && this.props.driverTrack[0].body.reported.position){

      this.props.driverTrack.map((e, i)=>{
        let name = '';
        this.props.drivers.map((g, k)=>{
          if(g.DriverID.trackingId===e.trackingId){
            name = g.DriverID.name;
          }
          return('')
        })
        points.push({
          name: name,
          lat: e.body.reported.position.lat,
          lng: e.body.reported.position.lng
        })
        return('')
      })
    }
  }
  getTrack = async (driverDetails, ui, map)=>{
    let trackingArr = [];
    driverDetails.map((e, i)=>{
      trackingArr.push(e.driver_id);
      return('')
    })
    if (trackingArr && trackingArr.length>0){
      let json = await PostFetch2('driv/get_driver_cor/', {driver_ids: trackingArr});
      if(json && json.data && json.data.length>0){
        this.setOnMap(json.data, ui, map);
      }
      setInterval( async() => {
        let json = await PostFetch2('driv/get_driver_cor/', {driver_ids: trackingArr});
        if(json && json.data && json.data.length>0){
          this.setOnMap(json.data, ui, map);
        }
      }, 60000);
    }
  }

  setOnMap = (data, ui, map)=>{
  //  const state = store.getState();
    if(data && data.length>0){
      let markers = [];
      let driverIcon = new window.H.map.Icon('https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/assets/img/map-truck.png');
      data.map((e, i)=>{

        let marker = new window.H.map.Marker({
          lat:e.lat,
          lng:e.lng
        }, {icon: driverIcon});
        marker.setData('<div>'+e.name+'</div>');
        marker.addEventListener('tap', function (evt) {
          var bubble = new window.H.ui.InfoBubble(evt.target.getPosition(), {
            content: evt.target.getData()
          });
          ui.addBubble(bubble);
        }, false);
        markers.push(marker)
      return('')
      })
      map.addObjects(markers)
      setInterval(() => {
        try{
          map.removeObjects(markers)
        }catch(e){
          
        }
      }, 60000);
    }
  }
  componentDidMount() {
      this.platform = this.getPlatform();

      let layers = this.platform.createDefaultLayers();
      let element = document.getElementById('here-map');
      let map = this.getMap(element, layers.normal.map, {
          center: this.state.center,
          zoom: this.state.zoom,
      });

      let events = this.getEvents(map);
      // eslint-disable-next-line
      let behavior = this.getBehavior(events);
      // eslint-disable-next-line
      let ui = this.getUI(map, layers);

      if(this.props.driverDetails && this.props.driverDetails.length>0){
        this.getTrack(this.props.driverDetails, ui, map)
      }

      this.marker = new window.H.map.Marker({
        lat:this.state.center.lat,
        lng:this.state.center.lng
      });
      let onResult = function(result) {
      let route,
        routeShape,
        linestring;
      if(result && result.response && result.response.route) {
        route = result.response.route[0];
        routeShape = route.shape;
        linestring = new window.H.geo.LineString();
        routeShape.forEach(function(point) {
          let parts = point.split(',');
          linestring.pushLatLngAlt(parts[0], parts[1]);
        });
        let routeLine = new window.H.map.Polyline(linestring, {
          style: { strokeColor: 'blue', lineWidth: 5 }
        });
        map.addObjects([routeLine]);
        map.setViewBounds(routeLine.getBounds());
      }else {
        try{
          let x = document.getElementById('here-map')
          x.style.display = 'none';
        }catch(e){

        }
        errorAlert('No route found!')
      }
    };
  //  const state = store.getState();
    if(this.state.points && this.state.points.length>0){
      let obj = [];
      let driverIcon = new window.H.map.Icon('https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/assets/img/map-truck.png');
      this.state.points.map((e, i)=>{
        let marker = new window.H.map.Marker({
          lat:e.lat,
          lng:e.lng
        }, {icon: driverIcon});
        marker.setData('<div>'+e.name+'</div>');
        marker.addEventListener('tap', function (evt) {
          var bubble =  new window.H.ui.InfoBubble(evt.target.getPosition(), {
            content: evt.target.getData()
          });
          ui.addBubble(bubble);
        }, false);
        obj.push(marker)
        return('')
      })
      map.addObjects(obj);
    }

    if (this.state.driverRoutes.length>0) {
      let obj = [];
      this.state.driverRoutes.map((e, i)=>{
        for(let i = 0; i<e.length-1; i++){
          let temp = {
            mode: 'balanced;truck',
            waypoint0: 'geo!'+e[i].latitude+','+e[i].longitude,
            waypoint1: 'geo!'+e[i+1].latitude+','+e[i+1].longitude,
            representation: 'display',
          };
          let router = this.platform.getRoutingService();
          router.calculateRoute(temp, onResult, function(error) {
              errorAlert(error.message);
          });
    //      if(i>0 && i<e.length){
            let middleIcon = null;
            if(e[i].is_pick){
              middleIcon = new window.H.map.Icon('https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/assets/img/map-marker-pickup.png');
            }else {
              middleIcon = new window.H.map.Icon('https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/assets/img/map-marker-drop.png');
            }
            let middleMarker = new window.H.map.Marker({
              lat:e[i].latitude,
              lng:e[i].longitude
            }, {icon: middleIcon});
            middleMarker.setData('<address>'+e[i].address+'</address>');
            middleMarker.addEventListener('tap', function (evt) {
              var bubble =  new window.H.ui.InfoBubble(evt.target.getPosition(), {
                content: evt.target.getData()
              });
              ui.addBubble(bubble);
            }, false);
            obj.push(middleMarker)
        //  }

        }
        // let startIcon = new window.H.map.Icon('https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/assets/img/map-marker-pickup.png');
        // let startMarker = new window.H.map.Marker({
        //   lat:e[0].longitude,
        //   lng:e[0].latitude
        // }, {icon: startIcon});
        // startMarker.setData('<address>'+e[0].address+'</address>');
        // startMarker.addEventListener('tap', function (evt) {
        //   var bubble =  new window.H.ui.InfoBubble(evt.target.getPosition(), {
        //     content: evt.target.getData()
        //   });
        //   ui.addBubble(bubble);
        // }, false);
        let endIcon = new window.H.map.Icon('https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/assets/img/map-marker-pickup.png');
        if(e[e.length-1].is_pick){
          endIcon = new window.H.map.Icon('https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/assets/img/map-marker-pickup.png');
        }else {
          endIcon = new window.H.map.Icon('https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/assets/img/map-marker-drop.png');
        }

        try{
          let endMarker = new window.H.map.Marker({
            lat:e[e.length-1].latitude,
            lng:e[e.length-1].longitude
          }, {icon: endIcon});
          endMarker.setData('<address>'+e[e.length-1].address+'</address>');
          endMarker.addEventListener('tap', function (evt) {
            var bubble =  new window.H.ui.InfoBubble(evt.target.getPosition(), {
              content: evt.target.getData()
            });
            ui.addBubble(bubble);
          }, false);
  //        obj.push(startMarker);
          obj.push(endMarker);
        }catch(e){

        }
        return('')
      })
      map.addObjects(obj);
    }
  }
  render() {
      return (
        <div id="here-map" style={{width: '100%', height: '500px', background: 'grey' }} />
      );
    }
}

export default OrderMap;
