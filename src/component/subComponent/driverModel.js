import React, {Component} from 'react';
import CSVReader from 'react-csv-reader';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {createNewOrderCsv} from '../../actions/carrierActions';

class DriverModel extends Component{
  handleForce = (rows) => {
    let data = this.getJson(rows);
    this.props.closeModel();
  }

  getJson = (rows) =>{
    let arr = [];
    rows.map((d,i)=>{
      if(i>0){
        if(d[0]){
          arr.push({
            driver_name: d[0],
            vehicle_type: d[1],
            vehicle_rego: d[2],
            driver_license_no: d[3],
            phone_number: d[4],
            status: 'driver pending',
          });
        }
      }
      return (
        ''
      )
    });
    return arr;
  }

  render(){
    return(
      <div className={this.props.model_class} style={{'display':this.props.model_style}} id="myModal">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title">select drivers list file</h4>
              <button type="button" className="close" onClick={this.props.closeModel}>&times;</button>
            </div>
            <div className="modal-body">
              <CSVReader
                cssClass="csv-reader-input"
                label="Select CSV with secret Death Star statistics"
                onFileLoaded={this.handleForce}
                inputId="ObiWan"
                inputStyle={{color: 'red'}}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default DriverModel;
/*
DriverModel.propTypes = {
  createNewOrderCsv: PropTypes.func.isRequired,
  //newOrder: PropTypes.array.isRequired,
}

const mapStateToProps = state =>({
  newOrder: state.carrier.newOrder,
});

export default connect(mapStateToProps, {createNewOrderCsv})(DriverModel);
*/
