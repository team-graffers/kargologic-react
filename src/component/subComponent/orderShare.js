import React, {Component} from 'react';
import PostFetch from '../ajax/postFetch';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import {getEncrypt} from '../include/encryptDecrypt';
import store from '../../store';
import {successAlert} from '../include/alert';
class OrderShare extends Component{
  constructor(props) {
    super(props);
    this.state = {
      order_id: '',
      tracking_id: 'ABC123',
      link: '',
      email: '',
      copied1: false,
      copied2: false,
    }
  }
  componentWillMount(){
    if(this.props.order_id){
      const state = store.getState();
      const id = getEncrypt(this.props.order_id+"");
      this.setState({
        order_id: this.props.order_id,
        tracking_id: id,
        link: `${state.carrier.localUrl}trackorder/${id}`
      })
    }
  }
  shareId = async (e)=>{
    e.preventDefault();
    const payload = {
      order_tracking_key: this.state.link,
      send_email_id: this.state.email,
    }
    let json = await PostFetch('orders/order_track_sendemail/?dashboard='+this.props.type, payload);
    if(json!==null && json.message){
      this.setState({
        email: ''
      })
      successAlert('Link Sent')
    }
  }
  render(){
    return(
      <div className="modal fade modelSecondary" id="modelShare" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel"
      aria-hidden="true">
          <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                  <div className="modal-header">
                      <h4 className="modal-title" id="exampleModalCenterTitle">SHARE WITH OTHERS</h4>
                      <a href="#a" className="" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true" style={{fontSize: '14px'}}>&times;</span>
                      </a>
                  </div>
                  <div className="modal-body">
                     <p className="text-center mb-5">TRACKING ID: <strong>{this.state.tracking_id}</strong>
                     <CopyToClipboard text={this.state.link}
                       onCopy={() => this.setState({copied1: true})}>
                       <a href="#a" title="Copy" onClick={(e)=>{
                         e.preventDefault();
                       }} className="f12"> <i className="far fa-clone"></i> Copy</a>
                     </CopyToClipboard>
                     </p>
                     <form method="post" onSubmit={this.shareId}>
                          <div className="form-group mb-0">
                              <label>Share this order via public share link <a href="#a" className="textDark"><i className="far fa-question-circle"></i></a> </label>
                          </div>
                          <div className="form-row">
                              <div className="form-group col-md-9">
                                  <input required type="text" value={this.state.link} onChange={(e)=>{
                                    e.preventDefault();
                                    this.setState({
                                      link: e.target.value,
                                      copied2: false,
                                    })
                                  }} className="form-control"/>
                              </div>
                              <div className="form-group col-md-3">
                              <CopyToClipboard text={this.state.link}
                                onCopy={() => this.setState({copied2: true})}>
                                <a href="#a" title="Copy" onClick={(e)=>{
                                  e.preventDefault();
                                }} className="btn">Copy</a>
                              </CopyToClipboard>

                              </div>
                          </div>
                          <div className="textOr"><span>Or</span></div>
                          <div className="form-group mb-0">
                              <label>Share via Email Address</label>
                          </div>
                          <div className="form-row">
                              <div className="form-group col-md-9">
                                  <input required type="email" value={this.state.email} onChange={(e)=>{
                                    this.setState({
                                      email: e.target.value
                                    })
                                  }} className="form-control"/>
                              </div>
                              <div className="form-group col-md-3">
                                  <input type="submit" value="Send" className="btn"/>
                              </div>
                          </div>
                     </form>
                  </div>
              </div>
          </div>
      </div>
    )
  }
}

export default OrderShare;
