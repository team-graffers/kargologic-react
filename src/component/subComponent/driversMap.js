import React, {Component} from 'react';
import store from '../../store';
//import {HereMapLogin, BatchShadows} from '../include/hereMapApi';
import {infoAlert} from '../include/alert';
import PostFetch from '../ajax/postFetch';

class DriversMap extends Component {
  constructor(props) {
      super(props);

      this.platform = null;
      this.map = null;
      this.marker= null;
      this.layers = null;
      this.state = {
        useCIT: true,
        app_id: '',
        app_code: '',
        useHTTPS: true,
          center: {
            lat: -20,
            lng: 130
          },
          zoom: 4,
          map: null,
          marker: null,
          theme: 'normal.day',
          drivers: [],
      }
  }
  componentWillMount(){
    const state = store.getState();
    this.setState({
      app_id: state.carrier.app_id,
      app_code: state.carrier.app_code
    })
  }
  getPlatform() {
      return new window.H.service.Platform(this.state);
  }

  getMap(container, layers, settings) {
      return new window.H.Map(container, layers, settings);
  }

  getEvents(map) {
      return new window.H.mapevents.MapEvents(map);
  }

  getBehavior(events) {
      return new window.H.mapevents.Behavior(events);
  }

  getUI(map, layers) {
      return new window.H.ui.UI.createDefault(map, layers);
  }
  getTrack = async (trackingArr, ui)=>{
    if (trackingArr && trackingArr.length>0){
      let json = await PostFetch('driv/get_driver_cor/', {driver_ids: trackingArr});
      if(json && json.data && json.data.length>0){
        this.setOnMap(json.data, ui);
      }
      setInterval( async () => {
        let json = await PostFetch('driv/get_driver_cor/', {driver_ids: trackingArr});
        if(json && json.data && json.data.length>0){
          this.setOnMap(json.data, ui);
        }
      }, 60000);
    }
  }
  setOnMap = (data, ui)=>{
//    const state = store.getState();
    if(data){
      let markers = [];
      let driverIcon = new window.H.map.Icon('https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/assets/img/map-truck.png');
      data.map((e, i)=>{
        let marker = new window.H.map.Marker({
          lat:e.lat,
          lng:e.lng
        }, {icon: driverIcon});
        marker.setData('<div>'+e.name+'</div>');
        marker.addEventListener('tap', function (evt) {
          var bubble = new window.H.ui.InfoBubble(evt.target.getPosition(), {
            content: evt.target.getData()
          });
          ui.addBubble(bubble);
        }, false);
        markers.push(marker)
        return('')
      })
      this.map.addObjects(markers)
    }
  }
  componentDidMount() {
    infoAlert('Locating Drivers Please wait!');
      this.platform = this.getPlatform();
      this.layers = this.platform.createDefaultLayers();
      var element = document.getElementById('here-map');
      this.map = this.getMap(element, this.layers.normal.map, {
          center: {
            lat: this.state.center.lat,
            lng: this.state.center.lng
          },
          zoom: this.state.zoom,
      });
      var events = this.getEvents(this.map);
      // eslint-disable-next-line
      var behavior = this.getBehavior(events);
      // eslint-disable-next-line
      let ui = this.getUI(this.map, this.layers);
      if(this.props.driver_id){
        this.getTrack(this.props.driver_id, ui)
      }
  }
  render() {
      return (
        <div className="fixedAssignDriver">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-body">
                      <div className="row rowAssign align-items-center">
                        <div className="col-md-6">
                            <h4 className="pb-0 mb-0">Drivers Location on Map</h4>
                        </div>
                        <div className="col-md-6" style={{"textAlign":"right"}}>
                          <a href="#closemap" onClick={(e)=>{
                            e.preventDefault();
                            this.props.close();
                          }} className="btnClose">
                            <span style={{"fontSize":"30px", 'color':"gray"}}>&times;</span>
                          </a>
                        </div>
                      </div>
                      <div id="here-map" style={{width: '800px', height: '650px', background: 'grey' }} />
                    </div>
                </div>
            </div>
        </div>
      );
    }
}

export default DriversMap;
