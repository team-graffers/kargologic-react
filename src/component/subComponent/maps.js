import React, {Component} from 'react';
import store from '../../store';

class Map extends Component {
  constructor(props) {
      super(props);

      this.platform = null;
      this.map = null;
      this.marker= null;
      this.state = {
        address: this.props.address,
          useCIT: true,
          app_id: '',
          app_code: '',
          useHTTPS: true,
          center: {
            lat: parseFloat(this.props.lng),
            lng: parseFloat(this.props.lat),
          },
          zoom: 14,
          map: null,
          marker: null,
          theme: 'normal.day',
      }
  }
  componentWillMount(){
    const state = store.getState();
    this.setState({
      app_id: state.carrier.app_id,
      app_code: state.carrier.app_code
    })
  }
  getPlatform() {
      return new window.H.service.Platform(this.state);
  }

  getMap(container, layers, settings) {
      return new window.H.Map(container, layers, settings);
  }

  getEvents(map) {
      return new window.H.mapevents.MapEvents(map);
  }

  getBehavior(events) {
      return new window.H.mapevents.Behavior(events);
  }

  getUI(map, layers) {
      return new window.H.ui.UI.createDefault(map, layers);
  }

  componentDidMount() {
      this.platform = this.getPlatform();

      var layers = this.platform.createDefaultLayers();
      var element = document.getElementById('here-map');
      this.map = this.getMap(element, layers.normal.map, {
          center: this.state.center,
          zoom: this.state.zoom,
      });

      var events = this.getEvents(this.map);

      // eslint-disable-next-line
      var behavior = this.getBehavior(events);
      // eslint-disable-next-line
      var ui = this.getUI(this.map, layers);
//      const state = store.getState();
      let icon = new window.H.map.Icon('https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/assets/img/map-marker-drop.png');
      this.marker = new window.H.map.Marker({
        lat:this.state.center.lat,
        lng:this.state.center.lng
      }, {icon: icon});
      this.marker.draggable = true;
      this.map.addObject(this.marker);

      this.map.addEventListener('dragstart', function(ev) {
        var target = ev.target;
        if (target instanceof window.H.map.Marker) {
          behavior.disable();
        }
      }, false);


      // re-enable the default draggability of the underlying map
      // when dragging has completed
      this.map.addEventListener('dragend', function(ev) {
        var target = ev.target;
        if (target instanceof window.H.map.Marker) {
          behavior.enable();
        }
      }, false);


  }
  render() {
      return (
        <div className="fixedAssignDriver">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-body">
                      <div className="row rowAssign align-items-center">
                        <div className="col-md-6">
                            <h4 className="pb-0 mb-0">Show Location on Map</h4>
                        </div>
                        <div className="col-md-6" style={{"textAlign":"right"}}>
                          <a href="#closemap" onClick={(e)=>{
                            e.preventDefault();
                            this.props.close();
                          }} className="btnClose">
                            <span style={{"fontSize":"30px", 'color':"gray"}}>&times;</span>
                          </a>
                        </div>
                      </div>
                      <div id="here-map" style={{width: '600px', height: '500px', background: 'grey' }} />
                      <br/>
                      <div className="table">
                        <table>
                          <thead>

                          </thead>
                          <tbody>
                          <tr>
                            <td><strong>Address</strong></td>
                            <td>{this.state.address}</td>
                          </tr>
                            <tr>
                              <td><strong>Latitude</strong></td>
                              <td>{this.state.center.lat}</td>
                            </tr>
                            <tr>
                              <td><strong>Longitude</strong></td>
                              <td>{this.state.center.lng}</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                </div>
            </div>
        </div>
      );
    }
}

export default Map;
