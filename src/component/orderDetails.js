import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import SubSidebar from './include/orderDetailssubSidebar';
import Path from './include/path';
import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getOrderDetails, getAllJobs, getOrderComments, getJobsCount} from '../actions/carrierActions';
import AssignDriver from './subComponent/assignDriver';
import AdditionalDetails from './subComponent/additionalDetails';
import PostFetch from './ajax/postFetch';
//import DownloadCSV from './ajax/downloadCSV';
import {formateDate, formateDate2, formateDate3, dateDiff, generateETA} from './include/date';
import {Redirect } from 'react-router';
import OrderShare from './subComponent/orderShare';
import UploadAttachments from './subComponent/uploadAttachments';
import makeid from './subComponent/makeid';
import OrderComment from './subComponent/orderComment';
import OrderMap from './subComponent/orderMap';
import {successAlert, errorAlert} from './include/alert';
import moment from 'moment';
import {GetLocation, GetRoute} from './include/hereMapApi';
import {DownloadExcel} from './ajax/downloadExcel';

class OrderDetails extends Component{
 constructor(props){
    super(props);

    let url = window.location.href;
    url = url.split('orderdetails/');
    let order_id = url[1].replace('#a', '');
    order_id = url[1].replace('#attachments', '');
    this.state = {
      order_id: order_id,
      assignDriver: false,
      order_history_class: 'fas fa-chevron-up',
      path: [
              {
                path: "Orders",
                url: "/dashboard/all"
              },
              {
                path: "Order Details",
                url: "/orderDetails/"+order_id
              }
            ],
      assign_pick_driver: [],
      assign_drop_driver: [],
      attachments: [],
      redirect: false,
      changes: false,
      showMap: true,

      orderComments: [],
      driverDetails: [],
      order_history: '',
      heading_id: [],
      coll_id: [],
      orders: '',
      enRoute: true,
      sortArr: [],
      pod: []
    }
    this.changePath = this.changePath.bind(this);
    this.getAttachments = this.getAttachments.bind(this);
    this.assignDriverToOrder = this.assignDriverToOrder.bind(this);
  }
  changeHistoryClass = ()=>{
    if(this.state.order_history_className==="fas fa-chevron-up"){
      this.setState({
        order_history_class: 'fas fa-chevron-down'
      })
    }else{
      this.setState({
        order_history_class: 'fas fa-chevron-up'
      })
    }
  }
 componentWillMount(){
    this.props.getAllJobs('carr/status/?status=all');
    this.props.getJobsCount('carr/order_count/');
    this.props.getOrderDetails(this.state.order_id);
    const payload = {
      order_id: this.state.order_id
    }
    this.props.getOrderComments(payload)
  }
  getComments = ()=>{
    this.props.getOrderDetails(this.state.order_id);
  }
  changeOrder =(id)=>{
    let path = this.state.path;
    path[1].url = '/orderDetails/'+id
    this.setState({
      order_id:id,
      driverDetails: [],
      order_history: '',
      heading_id: [],
      coll_id: [],
      orders: '',
      enRoute: true,
      sortArr: [],
      pod: [],
      showMap: false
    });
    this.props.getOrderDetails(id);
    const payload = {
      order_id: id
    }
    this.props.getOrderComments(payload)
  }
  generateTimeline = async (orders)=>{
    let enRoute = true;
    let sortArr = [];
    let pod = [];
    if(orders && orders.progressbar.length>0){
      let enRouteArr = [];
      orders.progressbar.map((e, i)=>{
        if(e.order_enroute){
          enRouteArr.push(e)
        }
        return('')
      })
      if(enRouteArr && enRouteArr.length>0 && enRouteArr[0].order_enroute){
        enRoute = enRouteArr[enRouteArr.length-1].order_enroute;
      }
    }

    let margeArr = [];
    let trackingIdArr = [];
    if(orders && orders.assign_pick.length>0){
      orders.assign_pick.map((e, i)=>{
        trackingIdArr.push(e.OrderDriverID.DriverID.id);
        margeArr.push({
          id: e.id,
          driver_id: e.OrderDriverID.DriverID.id,
          driver: e.OrderDriverID.DriverID.name,
          is_pick: true,
          date_time: e.pickup_date_time,
          date_time_to: e.pickup_date_time_to,
          actual_date_time: e.actual_pickup_date_time,
          address: e.address,
          assigned_quantity: e.assigned_quantity,
          addressPos: {},
          driverPos: {},
          eta: '',
        })
        return('')
      })
    }

    if(orders && orders.assign_drop.length>0){
      orders.assign_drop.map((e, i)=>{
        trackingIdArr.push(e.OrderDriverID.DriverID.id);
        margeArr.push({
          id: e.id,
          driver_id: e.OrderDriverID.DriverID.id,
          driver: e.OrderDriverID.DriverID.name,
          is_pick: false,
          date_time: e.delivery_date_time,
          date_time_to: e.delivery_date_time_to,
          actual_date_time: e.actual_delivery_date_time,
          address: e.address,
          assigned_quantity: e.assigned_quantity,
          addressPos: {},
          driverPos: {},
          eta: '',
        });
        if(e.pod_image){
          pod.push({
            driver_id: e.OrderDriverID.DriverID.id,
            driver: e.OrderDriverID.DriverID.name,
            address: e.address,
            pod_image: e.pod_image
          })
        }
        return('')
      })
    }
      trackingIdArr = [...new Set(trackingIdArr)];
      if (trackingIdArr && trackingIdArr.length>0){
        let json = await PostFetch('driv/get_driver_cor/', {driver_ids: trackingIdArr});
        if(json && json.data && json.data.length>0){
          margeArr = margeArr.sort(function(a,b){
            return moment(a.date_time_to, 'YYYY-MM-DDTHH:mm:ssZ').utc() - moment(b.date_time_to, 'YYYY-MM-DDTHH:mm:ssZ').utc();
          });
          for(let i=0;i<margeArr.length; i++){
            let pos = await GetLocation(margeArr[i].address);
            if(pos && pos.lat && pos.lng){
              margeArr[i].addressPos.lat = pos.lat;
              margeArr[i].addressPos.lng = pos.lng;
            }
            for(let j=0;j<json.data.length; j++){
              if(margeArr[i].driver_id === json.data[j].id){
                margeArr[i].driverPos.lat = json.data[j].lat;
                margeArr[i].driverPos.lng = json.data[j].lng;
              }
            }
            if(margeArr[i].addressPos && margeArr[i].addressPos.lat && margeArr[i].addressPos.lng && margeArr[i].driverPos && margeArr[i].driverPos.lat && margeArr[i].driverPos.lng){
              if(margeArr[i].actual_date_time){
                continue;
              }else if(i>0 && margeArr[i-1].actual_date_time && !margeArr[i].actual_date_time) {
                let json = await GetRoute('waypoint0=geo!'+margeArr[i].driverPos.lat+','+margeArr[i].driverPos.lng+'&waypoint1=geo!'+margeArr[i].addressPos.lat+','+margeArr[i].addressPos.lng+'');

                if(json && json.trafficTime){
                  margeArr[i].eta = json.trafficTime;
                }else if(json && json.trafficTime === 0){
                  margeArr[i].eta = 0;
                }else{
                  margeArr[i].eta = 'No Route Found 1';
                }

              }else if(i>0 && !margeArr[i-1].actual_date_time && !margeArr[i].actual_date_time) {
                let json = await GetRoute('waypoint0=geo!'+margeArr[i-1].addressPos.lat+','+margeArr[i-1].addressPos.lng+'&waypoint1=geo!'+margeArr[i].addressPos.lat+','+margeArr[i].addressPos.lng+'');

                if(json && json.trafficTime){
                  let p = json.trafficTime;
                  if(parseInt(margeArr[i-1].eta)){
                      margeArr[i].eta = p + margeArr[i-1].eta
                  }else{
                    margeArr[i].eta = p;
                  }
                }else{
                  margeArr[i].eta = 'No Route Found 3';
                }
                //  margeArr[i].eta = await GetRoute('waypoint0=geo!'+margeArr[i].driverPos.lat+','+margeArr[i].driverPos.lng+'&waypoint1=geo!'+margeArr[i].addressPos.lat+','+margeArr[i].addressPos.lng+'');
              }else if(i === 0){
                let json = await GetRoute('waypoint0=geo!'+margeArr[i].driverPos.lat+','+margeArr[i].driverPos.lng+'&waypoint1=geo!'+margeArr[i].addressPos.lat+','+margeArr[i].addressPos.lng+'');
                if(json && json.trafficTime){
                  margeArr[i].eta = json.trafficTime;
                }else{
                  margeArr[i].eta = 'No Route Found';
                }
              }
            }
          }

          this.setState({
            enRoute: enRoute,
            sortArr: margeArr,
            pod: pod,
            showMap: true
          })
        }
      }else {
        sortArr = margeArr.sort(function(a,b){
          return moment(a.date_time_to, 'YYYY-MM-DDTHH:mm:ssZ').utc() - moment(b.date_time_to, 'YYYY-MM-DDTHH:mm:ssZ').utc();
        });
        this.setState({
          enRoute: enRoute,
          sortArr: sortArr,
          pod: pod,
          showMap: true
        })
      }
  }
  componentWillReceiveProps(nextProps){
    let driverDetails = [];
    let order_history = '';
    let heading_id = [];
    let coll_id = [];
    let orders = '';

    if(nextProps.orderDetails[0] && nextProps.orderDetails[0].order_details.length>0){
      orders = nextProps.orderDetails[0];
      this.generateTimeline(orders);
      if(orders && orders.order_history.length>0){
        order_history = orders.order_history.map((e, i)=>{
          if(e.Created_by_CustomerID!==null){
            return(
              <p key={i} style={{"margin":"0", "marginTop":"3px"}}>{e.comments}</p>
            )
          }else if(e.Created_by_CarrierID!==null){
            return(
              <p key={i} style={{"margin":"0", "marginTop":"3px"}}>{e.comments}</p>
            )
          }else {
            return(
              ''
            )
          }
        })
      }

      if(orders && orders.assign_pick.length>0 && orders.assign_drop.length>0){
        orders.assign_pick.map((f, i)=>{
          let flg=1;
          for(let j=0;j<driverDetails.length;j++){
            if(driverDetails[j].name===f.OrderDriverID.DriverID.name){
              flg=0;
              break;
            }
          }
          if(flg===1){
            driverDetails.push({
              driver_id: f.OrderDriverID.DriverID.id,
              name: f.OrderDriverID.DriverID.name,
              truck_type: f.OrderDriverID.DriverID.TruckType.truck_category.categ_name,
              ass_truck_name: f.OrderDriverID.AssetID && f.OrderDriverID.AssetID.registeration_no? f.OrderDriverID.AssetID.asset_id+"-"+f.OrderDriverID.AssetID.registeration_no: '',
              ass_truck_id: f.OrderDriverID.AssetID && f.OrderDriverID.AssetID.id? f.OrderDriverID.AssetID.id: '',
              pickup: [],
              delivery: []
            });
          }
          return(
            ''
          )
        });
        orders.assign_pick.map((f, i)=>{
          driverDetails.map((e, i)=>{
            if(e.driver_id===f.OrderDriverID.DriverID.id && i<driverDetails.length){
              let sender = '';
              if(f.sender && f.sender.id){
                sender = {
                  id: f.sender.id,
                  name: f.sender.name,
                  Customer_code: f.sender.Customer_code,
                  is_sender: f.sender.is_sender,
                  CompanyID: f.sender.CompanyID
                }
              }
              driverDetails[i].pickup.push({
                sender: sender,
                pick_id: f.id,
                DriverID: f.OrderDriverID.DriverID.id,
                actual_pickup_date_time: f.actual_pickup_date_time,
                address: f.address,
                assigned_quantity: f.assigned_quantity,
                created_DT: f.created_DT,
                latitude: f.latitude,
                longitude: f.longitude,
                pickup_date_time: f.pickup_date_time,
                pickup_date_time_to: f.pickup_date_time_to,
                updated_DT: f.updated_DT,
                asset_end: f.asset_end,
                asset: []
              });
            }
            return(
              ''
            )
          });
          return(
            ''
          )
        });
        orders.assign_drop.map((f, i)=>{
          driverDetails.map((e, i)=>{
            if(e.driver_id===f.OrderDriverID.DriverID.id  && i<driverDetails.length){
              let receiver = '';
              if(f.receiver && f.receiver.id){
                receiver = {
                  id: f.receiver.id,
                  name: f.receiver.name,
                  Customer_code: f.receiver.Customer_code,
                  is_sender: f.receiver.is_sender,
                  CompanyID: f.receiver.CompanyID
                }
              }
              driverDetails[i].delivery.push({
                receiver: receiver,
                drop_id: f.id,
                DriverID: f.OrderDriverID.DriverID.id,
                actual_delivery_date_time: f.actual_delivery_date_time,
                address: f.address,
                assigned_quantity: f.assigned_quantity,
                created_DT: f.created_DT,
                latitude: f.latitude,
                longitude: f.longitude,
                delivery_date_time: f.delivery_date_time,
                delivery_date_time_to: f.delivery_date_time_to,
                updated_DT: f.updated_DT,
                asset_start: f.asset_start,
                asset: []
              });
            }
            return(
              ''
            )
          });
          return(
            ''
          )
        });
      }

      for(let i = 0;i<driverDetails.length;i++){
        coll_id.push(makeid());
        heading_id.push(makeid());
        if(nextProps.orderDetails[0].pick_asset && nextProps.orderDetails[0].pick_asset.length>0){
          driverDetails[i].pickup.map((f, j)=>{
            nextProps.orderDetails[0].pick_asset.map((x, y)=>{
              if(f.pick_id === x.PickID){
                driverDetails[i].pickup[j].asset.push(x.AssetID)
              }
              return('')
            })
            return('')
          })
        }

        if(nextProps.orderDetails[0].drop_asset && nextProps.orderDetails[0].drop_asset.length>0){
          driverDetails[i].delivery.map((f, j)=>{
            nextProps.orderDetails[0].drop_asset.map((x, y)=>{
              if(f.drop_id === x.DropID){
                driverDetails[i].delivery[j].asset.push(x.AssetID)
              }
              return('')
            })
            return('')
          })
        }
      }
    this.setState({
      driverDetails: driverDetails,
      order_history: order_history,
      heading_id: heading_id,
      coll_id: coll_id,
      orders: orders,
    })
  }
}

  changePath = (e, fetchUrl) =>{
    let path = this.state.path;
    path[1].path = e;
    this.setState({path:path});
  }
  handleDownload = ()=>{
    if(this.state.order_id){
      let carrier = JSON.parse(localStorage.getItem("userDetails"));
      if(carrier && carrier.carr_com_id){
        let payload = 'orders/carr_order_csv_download/?orderlist='+[parseInt(this.state.order_id)]+'&carr_com_id='+carrier.carr_com_id;
        DownloadExcel(payload)
      }
    }
  }
  modifyOrder =(obj1, obj2)=>{
    if(this.props.orderDetails[0]){
      this.setState({
        [obj1]: obj2
      });
    }
  }
  getAttachments = (files)=>{
    this.getComments();
  }
  deleteAttachment = async (id) =>{
    const payload = {
      att_id_list: [id]
    }
    let json = await PostFetch('order_attach/delete_attach/', payload);
    if(json && json.message==="deleted"){
      successAlert('Attachment Deleted')
      this.getComments();
    }else {
      errorAlert('try again')
    }
  }
  assignDriverToOrder = async ()=>{
    if(this.state.orders && this.state.orders.order_details.length>0){
      let assign_pick = [];
      let assign_drop = [];
      const carrier = JSON.parse(localStorage.getItem("userDetails"));
      const carrierID = {
        id: parseInt(carrier.carr_id)
      }
      if(this.state.assign_pick_driver && this.state.assign_pick_driver[0] && this.state.assign_pick_driver[0].length>0){
        this.state.assign_pick_driver.map((f, j)=>{
          f.map((e, i)=>{
            let id = 0;
            if(!isNaN(parseInt(e.pick_id))){
              id = e.pick_id;
            }
          //  console.log(e);//carrier.carr_id
            assign_pick.push({
              pick_id: id,
              Created_by_CarrierID: this.state.orders.order_details[0].Created_by_CarrierID? this.state.orders.order_details[0].Created_by_CarrierID: carrierID,
              DriverID:parseInt(e.DriverID),
              latitude: e.latitude,
              longitude: e.longitude,
              address: e.address,
              assigned_quantity: e.assigned_quantity,
              pickup_date_time: e.pickup_date_time,
              pickup_date_time_to: e.pickup_date_time_to,
              asset_end: e.asset_end,
              ass_truck_id: e.ass_truck_id,
              ass_truck_name: e.ass_truck_name,
              sender: e.sender,
              asset: e.asset
            })
            return(
              ''
            )
          });
          return(
            ''
          )
        });
      }
      if(this.state.assign_drop_driver && this.state.assign_drop_driver[0] && this.state.assign_drop_driver[0].length>0){
        this.state.assign_drop_driver.map((f, j)=>{
          f.map((e, i)=>{
          let id = 0;
          if(!isNaN(parseInt(e.drop_id))){
            id = e.drop_id;
          }
          assign_drop.push({
            drop_id: id,
            Created_by_CarrierID: this.state.orders.order_details[0].Created_by_CarrierID? this.state.orders.order_details[0].Created_by_CarrierID: carrierID,
            DriverID:parseInt(e.DriverID),
            latitude: e.latitude,
            longitude: e.longitude,
            address: e.address,
            assigned_quantity: e.assigned_quantity,
            delivery_date_time: e.delivery_date_time,
            delivery_date_time_to: e.delivery_date_time_to,
            asset_start: e.asset_start,
            ass_truck_id: e.ass_truck_id,
            ass_truck_name: e.ass_truck_name,
            receiver: e.receiver,
            asset: e.asset
          })
          return(
            ''
          )
        });
          return(
            ''
          )
        });
      }
    //  console.log(this.state.orders);
      if(assign_pick.length>0 || assign_drop.length>0){
        let payload = {
            arr_order:[
            {
              order_detail:{
                order_id: this.state.orders.order_details[0].id,
                CarrierCompanyID: this.state.orders.order_details[0].CarrierCompanyID.id,
                Created_by_CarrierID: this.state.orders.order_details[0].Created_by_CarrierID? this.state.orders.order_details[0].Created_by_CarrierID: carrierID,
                is_active: this.state.orders.order_details[0].is_active
              },
              date_time_of_create: moment(),
              assign_pick_driver: assign_pick,
              assign_drop_driver: assign_drop,
            //  pick_asset: order.pick_asset,
              //drop_asset: order.drop_asset
            }
          ]
        }
        console.log(payload);
         let data = await PostFetch('carr/addmorepickdrop/', payload);
         console.log(data);
        if(data!==null && data.message==="order updated"){
          successAlert(data.message);
          this.changeOrder(this.state.order_id);
          this.setState({
            changes: false
          })
        }else{
          errorAlert('try again!');
        }
      }
    }
  }
  deleteDriver = async(order_id, driver_id)=>{
    let payload = {
      "driver_id":driver_id,
      "order_id":order_id
    }
    console.log(payload);
    let data = await PostFetch('carr/order_detail_indiv_rem_driv/', payload, null, 'DELETE');
    if(data && data.message==="success"){
      successAlert('driver removed');
      this.setState({
        assign_pick_driver: [],
        assign_drop_driver: [],
      })
      this.props.getOrderDetails(this.state.order_id);
    }else{
      errorAlert('try again!');
    }
  }
  changeBool=(val)=>{
    if(val){
      return(
        'Yes'
      )
    }else{
      return(
        'No'
      )
    }
  }
  changeColor = (actual_date, to_date) =>{
    if(actual_date){
      actual_date = moment(actual_date, 'YYYY-MM-DDTHH:mmZ').utc();
      to_date = moment(to_date, 'YYYY-MM-DDTHH:mmZ').utc();
      if(actual_date.diff(to_date, 'minutes')<=0){
        return 'active';
      }else {
        return 'pending';
      }
    }
  }
  render(){
    if(this.state.redirect){
      return(
        <Redirect to="/createorder" />
      )
    }
    if(this.state.orders && this.state.orders.order_details.length>0){
      let driverDetails = this.state.driverDetails;
      let order_history = this.state.order_history;
      let heading_id = this.state.heading_id;
      let coll_id = this.state.coll_id;
      let orders = this.state.orders;
      let enRoute = this.state.enRoute;
      let sortArr = this.state.sortArr;
      let pod = this.state.pod;
      return(
        <React.Fragment>
          <OrderComment getComments={this.getComments} order_id={this.state.order_id} userType="carrier"/>
          <article>
              <div className="dashboardMain">
                  <Header/>
                  <div className="dashboardContent">
                    <MainSidebar/>
                      <div className="rightColumn">
                          <SubSidebar status={orders.order_details[0].StatusID.status_name} jobsCount={this.props.jobsCount}/>
                          <div className="rightColumnContent">
                              <Path path={this.state.path} />
                              <div className="rowOrderDetail">
                                  <div className="columnJobno">
                                      <h6>Job No.</h6>
                                      <ul className="listJobNo">
                                      {this.props.allJobs && this.props.allJobs.order_list && this.props.allJobs.order_list.length? this.props.allJobs.order_list.map(d=>{
                                        if(d.is_active){
                                        return(
                                        <li key={d.id} className=""><a className={parseInt(d.id)===parseInt(this.state.order_id)? 'active': ''} href="#a" onClick={(e)=>{
                                          e.preventDefault();
                                          window.history.pushState('', 'id', `/orderdetails/${d.id}`);
                                          this.changeOrder(d.id);
                                        }}>{d.job_id}</a></li>
                                      )}else{
                                        return(
                                          <li key={d.id} className="">
                                            <NavLink to={`/createorder/?id=${d.id}`}>{d.job_id}</NavLink>
                                          </li>
                                        )
                                      }}): null}
                                      </ul>
                                  </div>
                                  <div className="contentOrderDetail">
                                      <div className="boxOrderdetail">
                                          <div className="row align-items-center">
                                              <div className="col-12 col-sm-5 col-md-5">
                                                  <h3>ORDER Details</h3>
                                                  <ul className="listorderDetail">
                                                      <li>Job Number: <span>{orders.order_details[0].job_id}</span></li>
                                                      <li>Order Placed: <span>{(orders.order_details[0].order_create_time)}</span></li>
                                                      <li>Create By: <span>{orders.order_details[0].Created_by_CustomerID? orders.order_details[0].Created_by_CustomerID.name: orders.order_details[0].Created_by_CarrierID.name}</span></li>
                                                  </ul>
                                              </div>
                                              <div className="col-12 col-sm-7 col-md-7">
                                                  <ul className="listCustomerDetail">
                                                      <li>
                                                          <h6>Customer Name</h6>
                                                          <h5>{orders.order_details[0].CustomerCompanyID.name}</h5>
                                                      </li>
                                                      <li>{!this.state.changes && this.props.orderDetails && this.props.orderDetails[0].assign_pick.length===0?
                                                        <a href="#a" title="Allocate Driver" onClick={(e)=>{
                                                          e.preventDefault();
                                                          this.setState({
                                                            assignDriver: true
                                                          })
                                                        }}
                                                        className="btn btn-sm">Allocate Driver</a>
                                                      :null}
                                                      </li>
                                                      {enRoute===true && orders.order_details && orders.order_details[0] && orders.order_details[0].Created_by_CarrierID && orders.order_details[0].Created_by_CarrierID.id?
                                                        <li><NavLink to={`/createorder/?id=${this.state.order_id}`} className="btnIcon"> <i className="fas fa-pencil-alt"></i></NavLink></li>
                                                      :null}
                                                      <li><a href="#a" data-toggle="modal" data-target="#uploadattachments" className="btnIcon"><i className="fas fa-paperclip"></i></a></li>
                                                      <li><a href="#a" data-toggle="modal" data-target="#modelOrderComment" className="btnIcon"><i className="far fa-comment"></i></a></li>
                                                      <li><a href="#a" className="btnIcon" data-toggle="modal" data-target="#modelShare"><i className="fas fa-share"></i></a></li>
                                                      <li><a href="#a" onClick={(e)=>{
                                                        e.preventDefault();
                                                        this.handleDownload()
                                                      }} className="btnIcon"><i className="fas fa-download"></i></a></li>
                                                  </ul>
                                                  <div className="textPrice">
                                                      Price: <span>${orders.order_details[0].price}</span>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <hr/>
                                      <div className="boxOrderstatus">
                                          <h4>ORDER STATUS</h4>
                                          {!this.state.changes && orders.assign_pick.length===0?
                                          <p className="font-medium">No Driver Assigned. <a href="#a" onClick={(e)=>{
                                            e.preventDefault();
                                            this.setState({
                                              assignDriver: true
                                            })
                                          }}>Allocate Driver</a> now.</p>:
                                          <React.Fragment>{!this.state.changes?
                                            <p className="font-medium">Driver Assigned.</p>:null}
                                          </React.Fragment>}
                                          <div style={{"width":"1024px", "overflow": "auto"}}>
                                          <ul className="listOrderstatus">
                                            {sortArr.length>0 && driverDetails.length>0 && enRoute!==true?
                                              <li className="active">
                                                  <i className="fas fa-check-circle"></i> En-route to Pickup
                                                  <br/>
                                                  <span style={{"color":"black"}}>{formateDate3(enRoute)}</span>
                                              </li>:
                                              <li>
                                                  <i className="fas fa-check-circle"></i> En-route to Pickup
                                              </li>}
                                              {sortArr.length>0 && driverDetails.length>0?
                                                sortArr.map((e, i)=>{
                                                  let name = null;
                                                  if(e.is_pick){
                                                    name = "Pick Up "
                                                  }else{
                                                    name = "Delivery "
                                                  }
                                                  return(
                                                    <li key={i} className={this.changeColor(e.actual_date_time, e.date_time_to)}>
                                                        <i className="fas fa-check-circle"></i> {name}
                                                        <br/>
                                                        <span style={{"color":"black"}}>
                                                          Driver:
                                                          <span style={{"color": "#7a7c75"}}> {e.driver}</span>
                                                        </span>
                                                        <br/>
                                                        <address style={{"color":"black", "width": "170px", "overflow": "auto"}}>
                                                          Address:
                                                          <span style={{"color": "#7a7c75"}}> {e.address}</span>
                                                        </address>
                                                        <span style={{"color":"black"}}>
                                                          <span style={{"color": "black"}}>Quantity: </span>
                                                          <span style={{"color": "#7a7c75"}}>{e.assigned_quantity}</span>
                                                        </span><br/>
                                                        <span style={{"color":"black"}}>
                                                          <span>Schedule Time: </span>
                                                          <span style={{"color": "#7a7c75"}}> {formateDate2(e.date_time_to)}</span>
                                                        </span><br/>
                                                        {!e.actual_date_time && e.eta!==null?
                                                          <span style={{"color":"black"}}>
                                                            <span>ETA: </span>
                                                            <span style={{"color": "#7a7c75"}}> {generateETA(e.eta)}</span>
                                                          </span>:
                                                          <span style={{"color":"black"}}>
                                                            <span>ON: </span>
                                                            <span style={{"color": "#7a7c75"}}> {formateDate3(e.actual_date_time)}</span>
                                                          </span>}<br/>
                                                        <span style={{"color":"black"}}>
                                                          {dateDiff(e.date_time_to, e.actual_date_time, e.date_time)}
                                                        </span>
                                                    </li>
                                                  )
                                                }):
                                                <React.Fragment>
                                                <li>
                                                    <i className="fas fa-check-circle"></i> Pick Up
                                                    <br/>
                                                    <span style={{"color":"black"}}></span>
                                                </li>
                                                <li>
                                                    <i className="fas fa-check-circle"></i> Delivery
                                                    <br/>
                                                    <span style={{"color":"black"}}></span>
                                                </li>
                                                </React.Fragment>
                                              }
                                          </ul>
                                          </div>
                                          <div className="font-medium text-uppercase dropdown mb-2">
                                              <a href="#a" className="text-dark" onClick={(e)=>{
                                                e.preventDefault();
                                                this.changeHistoryClass()
                                              }} data-toggle="collapse" data-target="#orderhistory"> Order History <i className={`fas ${this.state.order_history_class}`}></i></a>
                                          </div>
                                          <div id="orderhistory" className="collapse ">
                                          {order_history?
                                            order_history
                                          :<p className="textGray mb-0">No Order History available.</p>}
                                          </div>
                                      </div>
                                      <hr/>
                                      <div className="boxAdditionaldetails">
                                          <h4 className="pt-3">Additional details</h4>
                                          <div className="tabsAdditionalDetails">
                                              <ul className="nav nav-tabs" role="tablist">
                                                  <li className="nav-item">
                                                      <a className="nav-link" data-toggle="tab" href="#orderDetails">Order Details</a>
                                                  </li>
                                                  <li className="nav-item">
                                                      <a className="nav-link" data-toggle="tab" href="#loadDetails" role="tab">Load Details</a>
                                                  </li>
                                                  <li className="nav-item">
                                                      <a className="nav-link active" data-toggle="tab" href="#driverDetails" role="tab">Driver Details
                                                        {driverDetails.length>1?
                                                          <span className="textSplit"><img src="assets/img/icon-split.svg" alt="" title=""/> Split</span>
                                                        :null}
                                                      </a>
                                                  </li>
                                                  <li className="nav-item">
                                                      <a className="nav-link" data-toggle="tab" href="#pod" role="tab">Delivery POD</a>
                                                  </li>
                                                  <li className="nav-item">
                                                      <a className="nav-link" data-toggle="tab" href="#attachment" role="tab">Attachments</a>
                                                  </li>
                                                  <li className="nav-item">
                                                      <a className="nav-link" data-toggle="tab" href="#additionalDetail" role="tab">Additional Details</a>
                                                  </li>
                                                  <li className="nav-item">
                                                      <a className="nav-link" data-toggle="tab" href="#extraFields" role="tab">Extra Fields</a>
                                                  </li>
                                                  <li className="nav-item">
                                                      <a className="nav-link" data-toggle="tab" href="#orderComments" role="tab">Comments</a>
                                                  </li>
                                              </ul>
                                              <div className="tab-content">
                                                  <div className="tab-pane fade" id="orderDetails">
                                                  <AdditionalDetails
                                                    thead={['Status',
                                                      'Customer Code',
                                                      'Contain Hazardous Goods',
                                                      'Quantity',
                                                      'Pick Up Time From',
                                                      'Pick Up Time To',
                                                      'Pick Up Address',
                                                      'Sender',
                                                      'Delivery Time From',
                                                      'Delivery Time To',
                                                      'Delivery Address',
                                                      'Receiver'
                                                    ]}

                                                    tbody={[
                                                      orders.order_details[0] && orders.order_details[0].StatusID.status_name? orders.order_details[0].StatusID.status_name: '',
                                                      orders.order_details[0] && orders.order_details[0].customer_code? orders.order_details[0].customer_code: '',
                                                      orders.order_details[0] && orders.order_details[0].hazardous? this.changeBool(orders.order_details[0].hazardous): 'No',
                                                      orders.order_details[0] && orders.order_details[0].units? orders.order_details[0].units: '',
                                                      orders.order_details[0] && orders.order_pick && orders.order_pick[0] && orders.order_pick[0].pickup_date_time? formateDate(orders.order_pick[0].pickup_date_time): '',
                                                      orders.order_details[0] && orders.order_pick && orders.order_pick[0] && orders.order_pick[0].pickup_date_time_to? formateDate(orders.order_pick[0].pickup_date_time_to): '',
                                                      orders.order_details[0] && orders.order_pick && orders.order_pick[0] && orders.order_pick[0].address? orders.order_pick[0].address: '',
                                                      orders.order_details[0] && orders.order_pick && orders.order_pick[0] && orders.order_pick[0].sender && orders.order_pick[0].sender.name? orders.order_pick[0].sender.name: '',
                                                      orders.order_details[0] && orders.order_drop && orders.order_drop[0] && orders.order_drop[0].delivery_date_time? formateDate(orders.order_drop[0].delivery_date_time): '',
                                                      orders.order_details[0] && orders.order_drop && orders.order_drop[0] && orders.order_drop[0].delivery_date_time_to? formateDate(orders.order_drop[0].delivery_date_time_to): '',
                                                      orders.order_details[0] && orders.order_drop && orders.order_drop[0] && orders.order_drop[0].address? orders.order_drop[0].address: '',
                                                      orders.order_details[0] && orders.order_drop && orders.order_drop[0] && orders.order_drop[0].receiver && orders.order_drop[0].receiver.name? orders.order_drop[0].receiver.name: ''
                                                    ]}
                                                  />

                                                  </div>
                                                  <div className="tab-pane fade" id="loadDetails">
                                                    <AdditionalDetails
                                                      thead={["Load carried",
                                                        "Load type",
                                                        "Load weight",
                                                        "Dimension (L*W*H)",
                                                        "Quantity"
                                                      ]}
                                                      tbody={[orders.order_details[0].load_carried,
                                                        orders.order_details[0].load_type,
                                                        orders.order_details[0].load_weight,
                                                        orders.order_details[0].length_load_dimen+"*"+orders.order_details[0].width_load_dimen+"*"+orders.order_details[0].height_load_dimen+" "+orders.order_details[0].measure_unit,
                                                        orders.order_details[0].units
                                                      ]}
                                                    />
                                                  </div>
                                                  <div className="tab-pane fade  show active" id="driverDetails">
                                                  <div className="accordion accordionAssignDriver" id="accordionExample">
                                                    {!this.state.changes && !this.state.assignDriver && driverDetails.length>0?
                                                      driverDetails.map((e, i)=>{
                                                        return(
                                                      <div key={i} className="card">
                                                        <div className="card-header" id={heading_id[i]}>
                                                          <div data-toggle="collapse" className=" tablecollapsedHeader" data-target={`#${coll_id[i]}`} aria-expanded="true" aria-controls={coll_id[i]}>
                                                              <table className="table tableDriver">
                                                                  <thead>
                                                                      <tr>
                                                                          <th>#</th>
                                                                          <th>Driver Name</th>
                                                                          <th>Vehicle TYPE</th>
                                                                          <th>Actions</th>
                                                                      </tr>
                                                                  </thead>
                                                              </table>
                                                          </div>
                                                        </div>
                                                        <div id={coll_id[i]} className="collapse show" aria-labelledby={heading_id[i]} data-parent="#accordionExample">
                                                          <div className="card-body">
                                                              <div className="maintableAssignDriver">
                                                                <table className="table tableDriver">
                                                                    <tbody>
                                                                        <tr>
                                                                          <td>{orders.order_details[0].job_id}-{i+1}.</td>
                                                                          <td>{e.name}</td>
                                                                          <td className="font-medium">{e.ass_truck_name}</td>
                                                                          <td>
                                                                          {enRoute===true?
                                                                          <ul className="listAction">
                                                                              <li><a href="#a" onClick={(e)=>{
                                                                                e.preventDefault();
                                                                                this.setState({
                                                                                  assignDriver: true
                                                                                })
                                                                              }}><i className="fas fa-pencil-alt"></i></a></li>
                                                                              <li><a href="#a" onClick={(el)=>{
                                                                                el.preventDefault();
                                                                                this.deleteDriver(this.state.order_id, e.driver_id);
                                                                              }} className="textRed"><i className="far fa-trash-alt"></i></a></li>
                                                                          </ul>
                                                                          :null}
                                                                          </td>
                                                                          </tr>
                                                                    </tbody>
                                                                </table>
                                                                <div className="tableAssignDriver">
                                                                    <div className="table-responsive">
                                                                        <table className="table sub-table">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Order QTY.</th>
                                                                                    <th>Pick Up LOCATION</th>
                                                                                    <th>Pick Up TIME FROM</th>
                                                                                    <th>Pick Up TIME TO</th>
                                                                                    <th>SENDER</th>
                                                                                    <th>ASSETS</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            {driverDetails[i].pickup.map((x, i)=>{
                                                                            return(
                                                                              <tr key={i}>
                                                                                <td>{orders.order_details[0].job_id}-{i+1}.</td>
                                                                                <td>{x.assigned_quantity}</td>
                                                                                <td style={{maxWidth: '150px'}}>{x.address}</td>
                                                                                <td>{formateDate(x.pickup_date_time)}</td>
                                                                                <td>{formateDate(x.pickup_date_time_to)}</td>
                                                                                <td>{x.sender && x.sender.name ? x.sender.name: null}</td>
                                                                                <td>
                                                                                  <ul style={{listStyle: 'none'}}>
                                                                                    {x.asset && x.asset.length>0?
                                                                                      x.asset.map((x, i)=>(
                                                                                        <li key={i}>{i+1}. {x.asset_id} - {x.registeration_no}</li>
                                                                                      ))
                                                                                    :null}
                                                                                  </ul>
                                                                                </td>
                                                                              </tr>
                                                                            )
                                                                          })}
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                <div className="tableAssignDriver">
                                                                        <div className="table-responsive">
                                                                            <table className="table sub-table">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th>#</th>
                                                                                        <th>Order QTY.</th>
                                                                                        <th>DELIVERY LOCATION</th>
                                                                                        <th>DELIVERY TIME FROM</th>
                                                                                        <th>DELIVERY TIME TO</th>
                                                                                        <th>RECEIVER</th>
                                                                                        <th>ASSETS</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                {driverDetails[i].delivery.map((x, i)=>{
                                                                                return(
                                                                                  <tr key={i}>
                                                                                    <td>{orders.order_details[0].job_id}-{i+1}.</td>
                                                                                    <td>{x.assigned_quantity}</td>
                                                                                    <td style={{maxWidth: '150px'}}>{x.address}</td>
                                                                                    <td>{formateDate(x.delivery_date_time)}</td>
                                                                                    <td>{formateDate(x.delivery_date_time_to)}</td>
                                                                                    <td>{x.receiver && x.receiver.name ? x.receiver.name: null}</td>
                                                                                    <td>
                                                                                      <ul style={{listStyle: 'none'}}>
                                                                                        {x.asset && x.asset.length>0?
                                                                                          x.asset.map((x, i)=>(
                                                                                            <li key={i}>{i+1}. {x.asset_id} - {x.registeration_no}</li>
                                                                                          ))
                                                                                        :null}
                                                                                      </ul>
                                                                                    </td>
                                                                                  </tr>
                                                                                )
                                                                              })}
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                </div>
                                                          </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    )})
                                                    : null}
                                                    </div>
                                                      <div className="rowAddMore">
                                                          <div className="row align-items-center">
                                                              <div className="col-sm-12 col-md-6">
                                                                {driverDetails.length>0?
                                                                  <React.Fragment>
                                                                  {enRoute===true?
                                                                  <a href="#a" onClick={(e)=>{
                                                                    e.preventDefault();
                                                                    this.setState({
                                                                      assignDriver: true
                                                                    })
                                                                  }}
                                                                     className="font-medium">+ Add another driver to this order</a>
                                                                     :null}
                                                                    </React.Fragment>
                                                                : <React.Fragment>
                                                                {!this.state.changes?
                                                                  <a href="#a" onClick={(e)=>{
                                                                    e.preventDefault();
                                                                  this.setState({
                                                                    assignDriver: true
                                                                  })
                                                                }} className="font-medium">Allocate Driver
                                                                </a>:null}
                                                              </React.Fragment>}
                                                              </div>

                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div className="tab-pane fade" id="pod">
                                                    <div className="table-responsive">
                                                    {pod && pod.length>0?
                                                      <table className="table">
                                                        <thead>
                                                          <tr>
                                                            <th>S No.</th>
                                                            <th>Driver Name</th>
                                                            <th>Delivery Location</th>
                                                            <th>POD</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                          {pod.map((e, i)=>(
                                                            <tr key={i}>
                                                              <td>{i+1}.</td>
                                                              <td>{e.driver}</td>
                                                              <td>{e.address}</td>
                                                              <td>
                                                                <a href={e.pod_image} target="_blank" download rel="noopener noreferrer">
                                                                  <span style={{"padding":"7px"}} className="badge badge-primary">
                                                                    Download
                                                                  </span>
                                                                </a>
                                                              </td>
                                                            </tr>
                                                          ))}
                                                        </tbody>
                                                      </table>
                                                      :<p style={{"margin":"5px", 'color': '#4f5051'}}>No POD to display.</p>}
                                                    </div>
                                                  </div>
                                                  <div className="tab-pane fade" id="attachment">
                                                    <div className="table-responsive">
                                                      {orders.order_attachment.length>0?
                                                        <table className="table">
                                                          <thead>
                                                            <tr>
                                                              <th>S No.</th>
                                                              <th>Attachment Name</th>
                                                              <th>Created By</th>
                                                              <th>Download</th>
                                                              <th>Action</th>
                                                            </tr>
                                                          </thead>
                                                          <tbody>

                                                            {orders.order_attachment.map((e, i)=>(
                                                              <tr key={i}>
                                                                <td>{i+1}.</td>
                                                                <td>{e.attachment_name}</td>
                                                                <td>
                                                                  {e.Created_by_CarrierID && e.Created_by_CarrierID.name? e.Created_by_CarrierID.name: null}
                                                                  {e.Created_by_CustomerID && e.Created_by_CustomerID.name? e.Created_by_CustomerID.name: null}
                                                                  {e.Created_by_DriverID && e.Created_by_DriverID.name? e.Created_by_DriverID.name: null}
                                                                </td>
                                                                <td>
                                                                  <a href={e.physical_path} className="btn btn-primary btn-sm" target="_blank" download rel="noopener noreferrer">Download</a>
                                                                </td>
                                                                <td>
                                                                  <a href="#a" style={{'fontSize':"16px"}} onClick={(el)=>{
                                                                    el.preventDefault();
                                                                    this.deleteAttachment(e.id)
                                                                  }}><i className="far fa-trash-alt text-danger"></i></a>
                                                                </td>
                                                              </tr>
                                                            ))}

                                                      </tbody>
                                                        </table>
                                                        :<span style={{"margin":"5px", 'padding': '10px'}}>No Attachments</span>
                                                      }
                                                    </div>
                                                  </div>
                                                  <div className="tab-pane fade" id="additionalDetail" style={{overflow: 'auto'}}>
                                                    <p style={{"margin":"5px", 'color': '#4f5051'}}>{orders.order_details[0].additional_note}</p>
                                                  </div>
                                                  <div className="tab-pane fade" id="orderComments">
                                                    {orders.order_comment && orders.order_comment.length>0?
                                                      orders.order_comment.map((e, i)=>{
                                                        if(e.Created_by_CustomerID!==null){
                                                          return(
                                                            <p key={i} style={{"margin":"5px", 'color': '#4f5051'}}><strong>[{formateDate2(e.date_time_of_create)}] {e.Created_by_CustomerID.name.toUpperCase()} : </strong> {e.comment}</p>
                                                          )
                                                        }else if(e.Created_by_CarrierID!==null){
                                                          return(
                                                            <p key={i} style={{"margin":"5px", 'color': '#4f5051'}}><strong>[{formateDate2(e.date_time_of_create)}] {e.Created_by_CarrierID.name.toUpperCase()} : </strong> {e.comment}</p>
                                                          )
                                                        }else if(e.Created_by_DriverID!==null){
                                                          return(
                                                            <p key={i} style={{"margin":"5px", 'color': '#4f5051'}}><strong>[{formateDate2(e.date_time_of_create)}] {e.Created_by_DriverID.name.toUpperCase()} : </strong> {e.comment}</p>
                                                          )
                                                        }else {
                                                          return('')
                                                        }
                                                      }):
                                                      <p style={{"margin":"5px", 'color': '#4f5051'}}>No comments</p>
                                                    }
                                                  </div>
                                                  <div className="tab-pane fade" id="extraFields">
                                                    <div className="table-responsive">
                                                      {orders.extra_fields.length>0?
                                                      <table className="table table-bordered">
                                                        <thead>
                                                          <tr>
                                                            <th>Field Name</th>
                                                            <th>Field Value</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                        {orders.extra_fields.map((e, i)=>(
                                                          <tr key={i}>
                                                            <td>{e.field_name}</td>
                                                            <td>{e.field_value}</td>
                                                          </tr>
                                                        ))}
                                                        </tbody>
                                                      </table>:
                                                      <p style={{"margin":"5px"}}>Not Applicable</p>}
                                                    </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <hr/>
                                      <div className="boxMap">
                                          <h4 className="pt-3">Map</h4>
                                          {driverDetails.length>0 && this.state.showMap?
                                            <OrderMap drivers={orders.tracking_driv} driverDetails={driverDetails}/>
                                          :null}
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              {this.state.assignDriver?
              <AssignDriver
                order={this.props.orderDetails[0]}
                drivers={driverDetails}
                job_id={this.props.orderDetails[0].order_details[0].job_id}
                modifyOrder={this.modifyOrder}
                close={(e)=>{
                  this.setState({
                    assignDriver:false,
                    changes: true,
                  });
                  setTimeout(
                      function() {
                          this.assignDriverToOrder();
                      }
                      .bind(this),
                      1000
                  );
                }}
                cancel={(e)=>{
                  this.setState({
                    assignDriver:false
                  });
                }}
                address ={{
                  units: orders.order_details[0].units? orders.order_details[0].units: '',
                  pickup_date_time: orders.order_pick[0] && orders.order_pick[0].pickup_date_time? orders.order_pick[0].pickup_date_time: '',
                  pickup_date_time_to: orders.order_pick[0] && orders.order_pick[0].pickup_date_time_to? orders.order_pick[0].pickup_date_time_to: '',
                  pickup_location: orders.order_pick[0] && orders.order_pick[0].address? orders.order_pick[0].address: '',
                  delivery_date_time: orders.order_drop[0] && orders.order_drop[0].delivery_date_time? orders.order_drop[0].delivery_date_time: '',
                  delivery_date_time_to: orders.order_drop[0] && orders.order_drop[0].delivery_date_time_to? orders.order_drop[0].delivery_date_time_to: '',
                  delivery_location: orders.order_drop[0] && orders.order_drop[0].address? orders.order_drop[0].address: ''
                }}
              />
              :null}
          </article>
          <UploadAttachments order_id={this.state.order_id} getAttachments={this.getAttachments} userType="carrier" />
          {this.state.showMap?
          <OrderShare order_id={this.state.order_id} type="carrier" />
          :null}
        </React.Fragment>
      )
    }else{
      return(
        <React.Fragment>
          <article>
              <div className="dashboardMain">
                  <Header dummy={true}/>
                  <div className="dashboardContent">
                    <MainSidebar/>
                      <div className="rightColumn">
                          <SubSidebar jobsCount={this.props.jobsCount}/>
                          <div className="rightColumnContent">
                              <Path path={this.state.path} />
                              <div className="rowOrderDetail">
                                  <div className="columnJobno">
                                      <h6>Job No.</h6>
                                      <ul className="listJobNo">
                                      {this.props.allJobs && this.props.allJobs.order_list && this.props.allJobs.order_list.length? this.props.allJobs.order_list.map(d=>(
                                        <li key={d.id} className=""><a className={parseInt(d.id)===parseInt(this.state.order_id)? 'active': ''} href="#a" onClick={(e)=>{
                                          e.preventDefault();
                                          window.history.pushState('', 'id', `/orderdetails/${d.id}`);
                                          this.changeOrder(d.id);
                                        }}>{d.job_id}</a></li>
                                      )): null}
                                      </ul>
                                  </div>
                                  <div className="contentOrderDetail">
                                      <div className="boxOrderdetail">
                                          <div className="row align-items-center">
                                              <div className="col-12 col-sm-5 col-md-5">
                                                  <h3>ORDER Details</h3>
                                                  <p>Please wait...</p>
                                              </div>

                                          </div>
                                      </div>

                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

          </article>
        </React.Fragment>
      )
    }
  }
}

OrderDetails.propTypes = {
  getOrderDetails: PropTypes.func.isRequired,
  getAllJobs: PropTypes.func.isRequired,
  getJobsCount: PropTypes.func.isRequired,
  getOrderComments: PropTypes.func.isRequired,
  //orderDetails: PropTypes.array.isRequired,
}

const mapStateToProps = state =>({
  orderDetails: state.carrier.orderDetails,
  orderComments: state.carrier.orderComments,
  allJobs: state.carrier.allJobs,
  jobsCount: state.carrier.jobsCount
});

export default connect(mapStateToProps, {getJobsCount, getOrderDetails, getOrderComments, getAllJobs})(OrderDetails);
