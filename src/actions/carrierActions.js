import * as Types from './types';
import PostFetch from '../component/ajax/postFetch';
import GetFetch from '../component/ajax/getFetch';
import {errorAlert} from '../component/include/alert';

export const doLogin = (payload, headers) => async dispatch =>{
  let json = await PostFetch('carr/login', payload, headers);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    localStorage.setItem("token", json.token);
    localStorage.setItem("userDetails", JSON.stringify(json));
    localStorage.setItem("userType", "carrier");
    dispatch({
      type: Types.DO_LOGIN,
      payload: json
    });
    const mainSidebar = {
      iconOrder: 'active',
      iconDrivers: '',
      iconSettings: ''
    };
    dispatch({
      type: Types.MAIN_SIDEBAR,
      payload: mainSidebar
    });
  }
};

export const getAllJobs = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.FETCH_ALL_JOBS,
      payload: json
    });
  }
};

export const getJobsCount = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.JOBS_COUNT,
      payload: json
    });
  }
};

export const searchJobs = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.SEARCH_JOBS,
      payload: json
    });
  }
};

export const getAllDrivers = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.FETCH_ALL_DRIVERS,
      payload: json
    });
  }
};

export const getDriversCount = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.DRIVERS_COUNT,
      payload: json
    });
  }
};

export const searchDrivers = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.SEARCH_DRIVERS,
      payload: json
    });
  }
};

export const getDriverDetails = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.DRIVERS_DETAILS,
      payload: json
    });
  }
};

export const updatePassword = (payload) => async dispatch =>{
  let json = await PostFetch('carr/password-reset/', payload, null, 'PUT');
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.UPDATE_PASSWORD,
      payload: json
    });
  }
};

export const createNewOrder = (data) => dispatch =>{
  dispatch({
    type: Types.CREATE_NEW_ORDER,
    payload: data
  });
};

export const createNewOrderCsv = (data) => dispatch =>{
  dispatch({
    type: Types.CREATE_NEW_ORDER_CSV,
    payload: data
  });
};

export const modifyNewOrderCsv = (data) => dispatch =>{
  dispatch({
    type: Types.MODIFY_NEW_ORDER_CSV,
    payload: data
  });
};

export const deleteOrder = (data) => dispatch =>{
  dispatch({
    type: Types.DELETE_ORDER,
    payload: data
  });
};

export const clearOrder = () => dispatch =>{
  dispatch({
    type: Types.CLEAR_ORDER,
    payload: []
  });
};

export const clearDraft = () => dispatch =>{
  dispatch({
    type: Types.CLEAR_DRAFT,
    payload: []
  });
};

export const clearConfig = () => dispatch =>{
  dispatch({
    type: Types.CLEAR_CONFIG_MSG,
    payload: []
  });
};

export const submitNewOrder = (payload) => async dispatch =>{
  let json = await PostFetch('orders/arr/', payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.SUBMIT_NEW_ORDER,
      payload: json
    });
  }
};

export const submitNewOrderAll = (payload) => async dispatch =>{
  let json = await PostFetch('orders/arr/', payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.SUBMIT_NEW_ORDER_ALL,
      payload: json
    });
  }
};

export const draftNewOrder = (payload) => async dispatch =>{
  let json = await PostFetch('orders/arr/', payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.DRAFT_ORDER,
      payload: json
    });
  }
};

export const getOrderDetails = (id) => async dispatch =>{
  let json = await GetFetch('orders/ord_details_carrier/?order_id='+id);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.FETCH_JOB_DETAILS,
      payload: json
    });
  }
};

export const getOrderComments = (payload) => async dispatch =>{
  let json = await PostFetch('comments/ordermsgcomment/', payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.FETCH_JOB_COMMENTS,
      payload: json
    });
  }
};

export const getCustomerList = () => async dispatch =>{
  let json = await GetFetch('carr/customer_com_list/');
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.CUSTOMERS_LIST,
      payload: json
    });
  }
};

export const setSidebar = (id, id1, id2) => dispatch =>{
  var element = document.getElementById(id);
  var element1 = document.getElementById(id1);
  var element2 = document.getElementById(id2);
  if(element!==null && element1!==null && element2!==null){
    element.classList.add('active');
    element1.classList.remove('active');
    element2.classList.remove('active');
  }
  let payload = {
    [id]: 'active',
    [id1]: '',
    [id2]: ''
  }
  localStorage.setItem('sidebar', JSON.stringify(payload))
  dispatch({
    type: Types.MAIN_SIDEBAR,
    payload: payload
  });
};

export const updateEditOrder = (payload) => dispatch =>{
  dispatch({
    type: Types.EDIT_ORDER,
    payload: payload
  });
};

export const submitEditOrder = (payload) => async dispatch =>{
  let json = await PostFetch('orders/carr_edit_order/', payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.SUBMIT_EDIT_ORDER,
      payload: json
    });
  }
};

export const setConfigure = (fetchUrl, payload) => async dispatch =>{
  let json = await PostFetch(fetchUrl, payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.SET_USER_CONFIGURATION,
      payload: json
    });
  }
};

export const getConfigure = (fetchUrl) => async dispatch =>{
  let json = await PostFetch(fetchUrl, '');
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.GET_USER_CONFIGURATION,
      payload: json
    });
  }
};

export const getSenderReceiver = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.SENDER_RECEIVER,
      payload: json
    });
  }
};

export const pushSenderReceiver = (data) => dispatch =>{
  dispatch({
    type: Types.PUSH_SENDER_RECEIVER,
    payload: data
  });
};

export const getAssets = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.GET_ASSETS,
      payload: json
    });
  }
};

export const getAssetDriver = (fetchUrl, payload) => async dispatch =>{
  let json = await PostFetch(fetchUrl, payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.GET_ASSETS_DRIVER,
      payload: json
    });
  }
};

export const setAssetNo = (data) => dispatch =>{
  dispatch({
    type: Types.SET_ASSET_NO,
    payload: data
  });
};

export const editAssetSet = (data) => dispatch =>{
  dispatch({
    type: Types.EDIT_ASSET,
    payload: data
  });
};

export const editAssetGet = (data) => dispatch =>{
  dispatch({
    type: Types.EDIT_ASSET_GET,
    payload: data
  });
};

export const getSenderList = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.GET_SENDER,
      payload: json
    });
  }
};

export const editSenderSet = (data) => dispatch =>{
  dispatch({
    type: Types.EDIT_SENDER,
    payload: data
  });
};

export const getReceiverList = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.GET_RECEIVER,
      payload: json
    });
  }
};

export const editReceiverSet = (data) => dispatch =>{
  dispatch({
    type: Types.EDIT_RECEIVER,
    payload: data
  });
};

export const clearSubmitMsg = () => dispatch =>{
  dispatch({
    type: Types.CLEAR_SUBMIT_MSG,
    payload: []
  });
};

export const getCustList = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: Types.GET_CUSTOMER,
      payload: json
    });
  }
};

export const editCustomerSet = (data) => dispatch =>{
  dispatch({
    type: Types.EDIT_CUSTOMER,
    payload: data
  });
};

export const clearEdit = (data) => dispatch =>{
  dispatch({
    type: Types.CLEAR_EDIT,
    payload: data
  });
};
