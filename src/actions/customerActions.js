import * as CustomerTypes from './customerTypes';
import PostFetch from '../component/ajax/postFetch';
import GetFetch from '../component/ajax/getFetch';
import {errorAlert} from '../component/include/alert';

export const setSidebar = (id, id1, id2) => dispatch =>{
  var element = document.getElementById(id);
  var element1 = document.getElementById(id1);
  var element2 = document.getElementById(id2);
  if(element!==null && element1!==null && element2!==null){
    element.classList.add('active');
    element1.classList.remove('active');
    element2.classList.remove('active');
  }
  let payload = {
    [id]: 'active',
    [id1]: '',
    [id2]: ''
  }
  localStorage.setItem('sidebar', JSON.stringify(payload))
  dispatch({
    type: CustomerTypes.CUSTOMER_MAIN_SIDEBAR,
    payload: payload
  });
};

export const getAllJobs = (fetchUrl, payload) => async dispatch =>{
  let json = await PostFetch(fetchUrl, payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json) {
    dispatch({
      type: CustomerTypes.CUSTOMER_JOBS,
      payload: json
    });
  }
};

export const getJobsCount = (fetchUrl) => async dispatch =>{
  let json = await PostFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_JOBS_COUNT,
      payload: json
    });
  }
};

export const searchJobs = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_SEARCH_JOBS,
      payload: json
    });
  }
};

export const getAllCarriers = (fetchUrl, payload) => async dispatch =>{
  let json = await PostFetch(fetchUrl, payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json) {
    dispatch({
      type: CustomerTypes.CUSTOMER_CARRIER,
      payload: json
    });
  }
};

export const getCarriersCount = (fetchUrl) => async dispatch =>{
  let json = await PostFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_CARRIER_COUNT,
      payload: json
    });
  }
};

export const searchCarriers = (fetchUrl) => async dispatch =>{
  let json = await PostFetch(fetchUrl, {});
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_CARRIER_SEARCH,
      payload: json
    });
  }
};

export const createNewOrder = (data) => dispatch =>{
  dispatch({
    type: CustomerTypes.CUSTOMER_NEW_ORDER,
    payload: data
  });
};

export const createNewOrderCsv = (data) => dispatch =>{
  dispatch({
    type: CustomerTypes.CUSTOMER_NEW_ORDER_CSV,
    payload: data
  });
};

export const modifyNewOrderCsv = (data) => dispatch =>{
  dispatch({
    type: CustomerTypes.CUSTOMER_MODIFY_NEW_ORDER_CSV,
    payload: data
  });
};

export const clearOrder = () => dispatch =>{
  dispatch({
    type: CustomerTypes.CUSTOMER_CLEAR_ORDER,
    payload: []
  });
};

export const submitNewOrder = (payload) => async dispatch =>{
  let json = await PostFetch('orders/orcustcreate/', payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_SUBMIT_NEW_ORDER,
      payload: json
    });
  }
};

export const submitNewOrderAll = (payload) => async dispatch =>{
  let json = await PostFetch('orders/orcustcreate/', payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_SUBMIT_NEW_ORDER_ALL,
      payload: json
    });
  }
};

export const getCarrierList = () => async dispatch =>{
  let json = await GetFetch('cust/carr_list/');
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CARRIERS_LIST,
      payload: json
    });
  }
};

export const getOrderDetails = (id) => async dispatch =>{
  let json = await GetFetch('orders/ord_details_carrier/?order_id='+id);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_JOB_DETAILS,
      payload: json
    });
  }
};

export const getCarrierDetails = (fetchUrl, payload) => async dispatch =>{
  let json = await PostFetch(fetchUrl, payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_CARRIER_DETAILS,
      payload: json
    });
  }
};

export const deleteOrder = (data) => dispatch =>{
  dispatch({
    type: CustomerTypes.CUSTOMER_DELETE_ORDER,
    payload: data
  });
};

export const draftNewOrder = (payload) => async dispatch =>{
  let json = await PostFetch('orders/orcustcreate/', payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_DRAFT_ORDER,
      payload: json
    });
  }
};

export const clearDraft = () => dispatch =>{
  dispatch({
    type: CustomerTypes.CUSTOMER_CLEAR_DRAFT,
    payload: []
  });
};

export const updateEditOrder = (payload) => dispatch =>{
  dispatch({
    type: CustomerTypes.CUSTOMER_EDIT_ORDER,
    payload: payload
  });
};

export const submitEditOrder = (payload) => async dispatch =>{
  let json = await PostFetch('cust/edit_cust_order/', payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_SUBMIT_EDIT_ORDER,
      payload: json
    });
  }
};

export const getSenderReceiver = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_SENDER_RECEIVER,
      payload: json
    });
  }
};

export const pushSenderReceiver = (data) => dispatch =>{
  dispatch({
    type: CustomerTypes.CUSTOMER_PUSH_SENDER_RECEIVER,
    payload: data
  });
};


export const setConfigure = (fetchUrl, payload) => async dispatch =>{
  let json = await PostFetch(fetchUrl, payload);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_SET_USER_CONFIGURATION,
      payload: json
    });
  }
};

export const getConfigure = (fetchUrl) => async dispatch =>{
  let json = await PostFetch(fetchUrl, '');
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_GET_USER_CONFIGURATION,
      payload: json
    });
  }
};

export const clearConfig = () => dispatch =>{
  dispatch({
    type: CustomerTypes.CUSTOMER_CLEAR_CONFIG_MSG,
    payload: []
  });
};

export const getSenderList = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_GET_SENDER,
      payload: json
    });
  }
};

export const editSenderSet = (data) => dispatch =>{
  dispatch({
    type: CustomerTypes.CUSTOMER_EDIT_SENDER,
    payload: data
  });
};

export const getReceiverList = (fetchUrl) => async dispatch =>{
  let json = await GetFetch(fetchUrl);
  if(json===null){
    errorAlert("Can't connect to server");
  }else if (json!==null) {
    dispatch({
      type: CustomerTypes.CUSTOMER_GET_RECEIVER,
      payload: json
    });
  }
};

export const editReceiverSet = (data) => dispatch =>{
  dispatch({
    type: CustomerTypes.CUSTOMER_EDIT_RECEIVER,
    payload: data
  });
};

export const clearEdit = (data) => dispatch =>{
  dispatch({
    type: CustomerTypes.CLEAR_EDIT,
    payload: data
  });
};

export const clearSubmitMsg = () => dispatch =>{
  dispatch({
    type: CustomerTypes.CLEAR_SUBMIT_MSG,
    payload: []
  });
};
