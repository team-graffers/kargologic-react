import React, {Component} from 'react';
//import ReactDom from 'react-dom';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import SubSidebar from './include/orderDetailssubSidebar';
import Path from './include/path';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {clearSubmitMsg, getSenderReceiver, getCarrierList, updateEditOrder, submitEditOrder, getOrderDetails, createNewOrderCsv, clearOrder, createNewOrder, submitNewOrder, submitNewOrderAll,deleteOrder, getJobsCount, draftNewOrder, clearDraft} from '../actions/customerActions';
import CreateOrd from './subComponent/createOrd';
import Model from './subComponent/model';
import {Redirect } from 'react-router';
import {successAlert, errorAlert, infoAlert} from '../component/include/alert';
import {getCurrentTS} from '../component/include/date';

class CustomerCreateOrder extends Component{
  constructor(props){
    super(props);
    let order_create_time = getCurrentTS();
    this.state = {
      orderData:[],
      order_id: null,
      container:[],
      key:'',
      edit: false,
      redirect: false,
      excelModel: false,
      path: [
              {
                path: "Orders",
                url: "/customerdashboard/all"
              },
              {
                path: "Create New Order",
                url: "/customercreateorder"
              }
            ],
    order:{order_detail:{order_id: '', job_id:'',customer_code:'',CarrierCompanyID:'',Carrier_Name:'',price:'',load_carried:'',load_type:'',load_weight:'',length_load_dimen:'',width_load_dimen:'',height_load_dimen:'',measure_unit:'',units:'',hazardous:false,additional_note:'',is_active:"True", order_create_time: order_create_time, create: true},template:{template_name:''},extra_detail:[],pickup:[{sender:{},address: "",longitude: "",latitude: "",pickup_date_time: "",pickup_date_time_to: ""}],delivery:[{receiver:{},address: "",longitude: "",latitude: "",delivery_date_time: "",delivery_date_time_to: ""}], attachment: []}
  }
    this.closeModel = this.closeModel.bind(this);
    this.deleteForm = this.deleteForm.bind(this);
  }
  componentWillUnmount(){
    this.props.clearOrder();
  }
  editOrder = (nextProps)=>{
    if(nextProps.orderDetails[0] && nextProps.orderDetails[0].order_details.length>0){
      let order = nextProps.orderDetails[0];
      let data = [
        {
          order_detail:{
            order_id: this.state.order_id,
            job_id:order.order_details[0].job_id,
            customer_code: order.order_details[0].customer_code,
            CustomerCompanyID: order.order_details[0].CustomerCompanyID.id,
            Customer_Name: order.order_details[0].CustomerCompanyID.name,
            CarrierCompanyID: order.order_details[0].CarrierCompanyID.id,
            Carrier_Name: order.order_details[0].CarrierCompanyID.name,
            Created_by_CustomerID: order.order_details[0].Created_by_CustomerID,
            price: order.order_details[0].price,
            load_carried: order.order_details[0].load_carried,
            load_type: order.order_details[0].load_type,
            load_weight: order.order_details[0].load_weight,
            length_load_dimen: order.order_details[0].length_load_dimen,
            width_load_dimen: order.order_details[0].width_load_dimen,
            height_load_dimen: order.order_details[0].height_load_dimen,
            measure_unit: order.order_details[0].measure_unit,
            units: order.order_details[0].units,
            hazardous: order.order_details[0].hazardous,
            additional_note: order.order_details[0].additional_note,
            is_active: order.order_details[0].is_active
          },
          template:null,
          extra_detail:order.extra_fields,
          pickup:[
            {
              sender: order.order_pick[0] && order.order_pick[0].sender,
              pickup_id: order.order_pick[0] && order.order_pick[0].id,
              address: order.order_pick[0] && order.order_pick[0].address,
              longitude: order.order_pick[0] && order.order_pick[0].longitude,
              latitude: order.order_pick[0] && order.order_pick[0].latitude,
              pickup_date_time: order.order_pick[0] && order.order_pick[0].pickup_date_time? new Date(order.order_pick[0].pickup_date_time): '',
              pickup_date_time_to: order.order_pick[0] && order.order_pick[0].pickup_date_time_to? new Date(order.order_pick[0].pickup_date_time_to): ''
            }
          ],
          delivery:[
            {
              receiver: order.order_drop[0] && order.order_drop[0].receiver,
              delivery_id: order.order_drop[0] && order.order_drop[0].id,
              address: order.order_drop[0] && order.order_drop[0].address,
              longitude: order.order_drop[0] && order.order_drop[0].longitude,
              latitude: order.order_drop[0] && order.order_drop[0].latitude,
              delivery_date_time: order.order_drop[0] && order.order_drop[0].delivery_date_time? new Date(order.order_drop[0].delivery_date_time): '',
              delivery_date_time_to: order.order_drop[0] && order.order_drop[0].delivery_date_time_to? new Date(order.order_drop[0].delivery_date_time_to): ''
            }
          ],
          attachment: order.order_attachment,
        }
      ];
      this.props.createNewOrderCsv(data);
      this.props.updateEditOrder(true);
      setTimeout(
        function() {
          if(this.props.newOrder.length>0){
            this.putForms();
          }
        }
        .bind(this),
        500
      );
      this.setState({
        edit: true
      })
    }
  }
  closeModel = (e) =>{
    this.setState({
      excelModel: false
    })
    this.putForms();
  }
  closeModelX = (e) =>{
    this.setState({
      excelModel: false
    })
  }
  putForms = () =>{
    this.setState({
        container: []
    });
    if(this.props.newOrder){
      this.props.newOrder.map((data,i)=>{
        let con = this.state.container;
        con.push({
          id:i,
          data:<CreateOrd key={i} submitInd={this.submitInd} saveAsDraft={this.saveAsDraft} deleteForm={this.deleteForm} order_key={i} order={data}/>
        });
        this.setState({
            container: con
        });
        return(
          ''
        )
      });
    }
  }
  addNewForm = ()=>{
    let con = [...this.state.container];
    const order_create_time = getCurrentTS();
    const ord = {order_detail:{order_id: '', job_id:'',customer_code:'',CarrierCompanyID:'',Carrier_Name:'',price:'',load_carried:'',load_type:'',load_weight:'',length_load_dimen:'',width_load_dimen:'',height_load_dimen:'',measure_unit:'',units:'',hazardous:false,additional_note:'',is_active:"True", order_create_time: order_create_time, create: true},template:{template_name:''},extra_detail:[],pickup:[{sender:{},address: "",longitude: "",latitude: "",pickup_date_time: "",pickup_date_time_to: ""}],delivery:[{receiver:{},address: "",longitude: "",latitude: "",delivery_date_time: "",delivery_date_time_to: ""}], attachment: []};
    this.props.createNewOrder(ord);
    if(con){
      con.push({
        id:con.length,
        order: this.props.newOrder[con.length],
        data:<CreateOrd key={con.length} submitInd={this.submitInd} saveAsDraft={this.saveAsDraft} deleteForm={this.deleteForm} order_key={con.length}  />
      });
      this.setState({
          container: con
      });
    }
  }
  deleteForm2 =(i)=>{
    let array = this.state.container;
    let newOrder = this.props.newOrder;
    delete array[i];
    array[i] = {};
    delete newOrder[i];
    newOrder[i] = {};
      this.props.deleteOrder(newOrder);
      this.setState({
          container: array
      });
  }
  saveAsDraft = (i)=>{
    //check fields cant be blank
    let data = this.props.newOrder[i];
    if(data && data.order_detail && data.order_detail.job_id && data.order_detail.CarrierCompanyID && data.order_detail.Carrier_Name){
        const payload = {
          arr_order: [
            this.props.newOrder[i]
          ]
        }
        this.setState({
          key: i,
        })
        if(this.props.editOrder && !data.order_detail.is_active){
          if(payload.arr_order[i].attachment && payload.arr_order[i].attachment[0] && !payload.arr_order[i].attachment[0].image_read){
            payload.arr_order[i].attachment = [];
          }
          this.props.submitEditOrder(payload);
        }else if(!this.props.editOrder){
          payload.arr_order[0].order_detail.is_active = false;
          this.props.draftNewOrder(payload);
        }else{
          infoAlert("this order can't be save as draft.")
        }
    }else {
      errorAlert('fields are blank!')
    }
  }
  submitInd = (i)=>{
    const payload = {
      arr_order: [
        this.props.newOrder[i]
      ]
    }
    this.setState({
      key: i,
    })
    if(this.props.editOrder){
      if(payload.arr_order[i].attachment && payload.arr_order[i].attachment[0] && !payload.arr_order[i].attachment[0].image_read){
        payload.arr_order[i].attachment = [];
      }
      this.props.submitEditOrder(payload);
    }else{
      this.props.submitNewOrder(payload);
      this.deleteForm2(i);
    }

  }
  deleteForm =(i, msg=null)=>{
    let array = this.state.container;
    let newOrder = this.props.newOrder;
  //  let flg = 1;

    delete array[i];
    array[i] = {};
    delete newOrder[i];
    newOrder[i] = {};

    // for(let x = 0; x < array.length; x++){
    //   if(newOrder[x] && newOrder[x].order_detail){
    //     flg = 0;
    //     break;
    //   }
    // }
    // if(newOrder.length === 1 && newOrder[0] === null){
    //   flg = 1;
    // }
    // if(flg){
    //   this.props.deleteOrder([]);
    //   this.setState({
    //     container: [],
    //     redirect: true,
    //   });
  //  }else{
      this.props.deleteOrder(newOrder);
      this.setState({
          container: array
      });
  //  }
    if(msg){
      successAlert(msg);
    }else{
      successAlert('form deleted');
    }
  }
  componentWillMount(){
    this.props.clearOrder();
    this.props.getCarrierList();
    this.props.getSenderReceiver('orders/comsendreceive/');
    let search = window.location.search.substring(1);
    try{
      search = JSON.parse('{"' + search.replace(/&/g, '","').replace(/=/g,'":"') + '"}', function(key, value) { return key===""?value:decodeURIComponent(value) })
    }catch(e){
    }
    if(search.id){
      let path = this.state.path;
      path[1].path = 'Edit Order';
      path[1].url = path[1].url+'/?id='+search.id;
      this.setState({
        order_id: parseInt(search.id)
      })
      this.props.getOrderDetails(search.id);
    }else{
      if(this.props.newOrder.length>0){
        window.location.reload();
      }else{
        this.addNewForm();
      }
    }
    this.props.getJobsCount('cust/order_count/', {});
  }

  formSubmit = (e) =>{
    e.preventDefault();
    let orderArr = this.props.newOrder;
    let newOrderArr = [];
    orderArr.map((e, i)=>{
      if(Object.entries(e).length === 0 && e.constructor === Object){

      }else{
        orderArr[i].order_detail.order_create_time = getCurrentTS();
        newOrderArr.push(orderArr[i])
      }
      return(
        ''
      )
    });
    const payload = {
      arr_order: newOrderArr
    }
    if(this.props.editOrder){
      payload.arr_order[0].order_detail.is_active = true;
      if(payload.arr_order[0].attachment && payload.arr_order[0].attachment[0] && !payload.arr_order[0].attachment[0].image_read){
        payload.arr_order[0].attachment = [];
      }
      this.props.submitEditOrder(payload);
    }else{
      this.props.submitNewOrderAll(payload);
    }
  }
  componentWillReceiveProps(nextProps){
    if(this.state.edit===false){
      this.editOrder(nextProps)
    }

    if(nextProps.submitOrderAll && nextProps.submitOrderAll.message === "order success"){
      successAlert('Order submitted');
      this.props.clearOrder();
      this.setState({
        redirect: true,
      });
    }
     let data = nextProps.submitOrder;
    if((data.message && data.message==="order success")){
      successAlert('Order submitted');
      this.props.clearSubmitMsg();
      if(this.props.newOrder && this.props.newOrder.length === 0){
        this.props.clearOrder();
        this.setState({
          redirect: true,
        });
      }else if (this.props.newOrder && this.props.newOrder.length>0) {
        const data = this.props.newOrder.every((e)=>{
          return (Object.entries(e).length === 0 && e.constructor === Object)
        })
        if(data){
          this.setState({
            redirect: true,
          });
        }
      }
    }if(data.message==="order updated"){
      this.props.clearOrder();
      this.props.updateEditOrder(false);
      successAlert('Order successfully updated');
      this.setState({
        redirect: true,
      });
    }else if(data.message && data.exception!==""){
      this.props.clearOrder();
      successAlert(data.message);
      //somethings wents wrong! please try again later.
    }

    let data2 = nextProps.draftOrder;
    if(data2.message && data2.message==="order success"){
      this.deleteForm(this.state.key, "Order saved as draft");
      this.props.clearDraft();
      this.props.updateEditOrder(false);
      this.setState({
        key: '',
      })
    }else if(data2.message && data2.exception!==""){
      successAlert(data2.message);
    }
  }
  render(){
    if(this.state.redirect){
      return(
        <Redirect to="/customerdashboard/all" />
      )
    }
    return(
      <article>
        <div className="dashboardMain">
        <Header/>
        {this.state.excelModel?
          <Model
          closeModel={this.closeModel}
          closeModelX={this.closeModelX}
        />:
        null}
            <div className="dashboardContent">
                <MainSidebar/>
                <div className="rightColumn">
                    <SubSidebar jobsCount={this.props.jobsCount} changePath={this.changePath} />
                    <div className="rightColumnContent">
                        <Path path={this.state.path} />
                        <div style={{width: 'auto', height: '80vh', overflow: 'auto'}}>
                          <form onSubmit={this.formSubmit} method="post">
                            <div className="boxContentInner">
                              <div className="row">
                                  <div className="col-sm-12 col-md-12">
                                      <ul className="listbtns text-md-right">
                                        {this.state.order_id===null?
                                        <li><a onClick={(e)=>{
                                          e.preventDefault();
                                          this.setState({
                                            excelModel: true
                                          })
                                        }} href="#modelexcel" className="textlink" title="Upload Excel">Upload Excel</a></li>
                                        :null}
                                        <li><button type="submit" className="btn btn-sm">{this.props.editOrder? 'Update Order': 'Submit Order'}</button></li>
                                      </ul>
                                  </div>
                              </div>
                              {this.state.container.map((d, i)=>(
                                <div key={i}>
                                  {d.data}
                                </div>
                              ))}
                            </div>
                          </form>
                          {this.state.order_id===null?
                          <p><a href="#a" onClick={(e)=>{
                            e.preventDefault();
                            this.addNewForm();
                          }} className="linkaddMore" title="+ Add another order"><span>+ Add another order</span></a> </p>
                          :null}
                        </div>
                      </div>
                    </div>
                </div>
        </div>
      </article>
    )
  }
}

CustomerCreateOrder.propTypes = {
  createNewOrder: PropTypes.func.isRequired,
  submitNewOrder: PropTypes.func.isRequired,
  deleteOrder: PropTypes.func.isRequired,
  getJobsCount: PropTypes.func.isRequired,
  draftNewOrder: PropTypes.func.isRequired,
  clearDraft: PropTypes.func.isRequired,
  updateEditOrder: PropTypes.func.isRequired,
  submitNewOrderAll: PropTypes.func.isRequired,
  submitEditOrder: PropTypes.func.isRequired,
  clearOrder: PropTypes.func.isRequired,
  getOrderDetails: PropTypes.func.isRequired,
  createNewOrderCsv: PropTypes.func.isRequired,
  getCarrierList: PropTypes.func.isRequired,
  getSenderReceiver: PropTypes.func.isRequired,
  clearSubmitMsg: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  newOrder: state.customer.newOrder,
  jobsCount: state.customer.jobsCount,
  submitOrderAll: state.customer.submitOrderAll,
  submitOrder: state.customer.submitOrder,
  draftOrder: state.customer.draftOrder,
  editOrder: state.customer.editOrder,
  orderDetails: state.customer.orderDetails,
  carrierList: state.customer.carrierList,
  senderReceiver: state.customer.senderReceiver
});

export default connect(mapStateToProps, {clearSubmitMsg, getSenderReceiver, getCarrierList, updateEditOrder, submitEditOrder, getOrderDetails, draftNewOrder, createNewOrderCsv,clearOrder, createNewOrder, submitNewOrder, submitNewOrderAll, getJobsCount, deleteOrder, clearDraft})(CustomerCreateOrder);
