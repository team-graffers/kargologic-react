import React, {Component} from 'react';
import SideImg from './include/side-img';
import {NavLink} from 'react-router-dom';
import PostFetch from '../component/ajax/postFetch';
import {Redirect } from 'react-router';
import PasswordMask from 'react-password-mask';
import store from '../store';
import {successAlert} from '../component/include/alert';
import {validatePassword, verifyEmail, verifyNumber} from '../component/include/validation';
import APIFetch from '../component/ajax/apiFetch';
class CustomerCarrierSignup extends Component{

  constructor(props){
    super(props);
    this.state = {
      email:"",
      user_name:"",
      phone:"",
      phoneError: "",
      password:"",
      domainID: 2,
      roleID:4,
      address1: '',
      address2: '',
    	pin: '',
    	country: '',
      carr_com_name:"",
      redirect: false,
      error_email: '',
      cust_com_id: '',
      cust_com_name: '',
      appdata: '',
      data: [],
      strength: '',
      color: ''
    }
    this.formSubmit = this.formSubmit.bind(this);
  }
  parseQueryString = function() {
    let str = window.location.search;
    let objURL = {};
    str.replace(
        new RegExp( "([^?=&]+)(=([^&]*))?", "g" ),
        function( $0, $1, $2, $3 ){
            objURL[ $1 ] = $3;
        }
    );
    return objURL;
  }
  getSuggestions = async (text)=>{
    const data = await APIFetch('https://autocomplete.geocoder.api.here.com/6.2/suggest.json?'+this.state.appdata+'&query='+text+'&country=aus');
    let options = [];
    if(data.suggestions){
       data.suggestions.map((s,i)=>(
        options.push({
          value:s.label,
          label:s.label,
        })
      ));
    }
    this.setState({
      data:options
    });
  }

  changeCountry = async (pin) =>{
      if(this.state.address1){
        let url = 'https://geocoder.api.here.com/6.2/geocode.json?searchtext='+this.state.address1+'&'+this.state.appdata;
        const data = await APIFetch(url);
        if(data && data.Response && data.Response.View[0] && data.Response.View[0].Result[0] && data.Response.View[0].Result[0].Location){
          let pos = data.Response.View[0].Result[0].Location;
          this.setState({
            pin:pos.Address.PostalCode,
            country:pos.Address.Country,
          });
        }
      }
  }
  componentWillMount(){
    const state = store.getState();
    this.setState({
      appdata: state.carrier.appdata
    })
    let params = this.parseQueryString();
    if(params && params.email && params.cust_com_id && params.cust_com_name){
      let name = params.cust_com_name;
      if(name === 'None'){
        name = ''
      }else{
        name = name.toUpperCase();
        if(name.search('%20')){
          let nameArr = name.split('%20');
          let nameSt = '';
          if(nameArr && nameArr.length>0){
            nameArr.map((e, i)=>{
              if(i===0){
                  nameSt = e
              }else {
                  nameSt += ' '+e
              }
              return('')
            })
            name = nameSt
          }
        }
      }
      let id = params.cust_com_id;
      if(id==="None"){
        id = '';
      }
      this.setState({
        cust_com_id: id,
        email: params.email,
        cust_com_name: name
      });
    }else{
      // this.setState({
      //   redirect: true
      // });
    }
  }
  formSubmit = async (e) =>{
    e.preventDefault();
    if(this.state.phoneError==='' && this.state.error_email===''){
      const payload = {
        ...this.state,
        ExtraInfo:{
          email:this.state.email,
          user_name:this.state.user_name,
          phone:this.state.phone,
          domainID: 2,
          roleID:4,
          somevalue:"main create"
        }
      }
      const headers = {
        'content-type': 'application/json',
      }
      let json = await PostFetch('carr/inv_carr_signup/', payload, headers);
      if(json!==null){
        successAlert(json.message);
        this.setState({
          email:"",
          user_name:"",
          phone:"",
          password:"",
          carr_com_name:'',
          redirect: true
        });
      }
    }
  }

  render(){
    if(this.state.redirect){
      return(
        <Redirect to="/carrier/login" />
      )
    }
    const state = store.getState();
    return(
      <article>
          <section className="sectionColumns">
              <SideImg msg={this.state.cust_com_name}/>
              <div className="rightColumn">
                  <div className="content-rightColumn">
                      <h5 className="mb-4">Sign up for your account</h5>
                      <div className="boxComman">
                          <form autoComplete="off" action="" onSubmit={this.formSubmit} method="POST">
                              <div className="form-group">
                                  <label>Name <span className="text-danger">*</span></label>
                                  <input type="text" value={this.state.user_name} onChange={(e)=>{
                                    this.setState({user_name:e.target.value})
                                  }} placeholder="enter name" required className="form-control"/>
                              </div>
                              <div className="form-group">
                                  <label>Email address <span className="text-danger">*</span></label>
                                  <input type="email" value={this.state.email} onChange={(e)=>{
                                    this.setState({email:e.target.value})
                                  }} onBlur={ async (e)=>{
                                    let data = await verifyEmail(e.target.value);
                                    if(data){
                                      if(data.error!==''){
                                        this.setState({
                                          error_email: data.error
                                        })
                                      }else if(data.data==='Customer' || data.data==='Driver'){
                                        this.setState({
                                          error_email: 'Already registered with '+data.data
                                        })
                                      }else if(data.data==='' && data.error===''){
                                        this.setState({
                                          error_email: ''
                                        })
                                      }else if(data.data==='Carrier' && data.error===''){
                                        this.setState({
                                          error_email: ''
                                        })
                                      }
                                    }
                                  }} required className="form-control" placeholder="Enter Email"/>
                                  <span className="text-danger">{this.state.error_email}</span>
                              </div>
                              <div className="form-group">
                                  <label>Company Name <span className="text-danger">*</span></label>
                                  <input type="text" value={this.state.carr_com_name} onChange={(e)=>{
                                    this.setState({carr_com_name:e.target.value})
                                  }} required className="form-control" placeholder="Enter Company Name"/>
                              </div>
                              <div className="form-group">
                                  <label>Phone Number <span className="text-danger">*</span></label>
                                  <input type="number" value={this.state.phone} onChange={(e)=>{
                                    this.setState({phone:e.target.value})
                                  }} onBlur={ async (e)=>{
                                    let json = await verifyNumber(e.target.value);
                                    this.setState({
                                      phoneError: json.error,
                                    });
                                  }} required className="form-control" placeholder="Enter Phone Number"/>
                                  <span className="text-danger">{this.state.phoneError}</span>
                              </div>
                              <div className="form-group">
                                  <label>Address 1 <span className="text-danger">*</span></label>
                                  <input type="text" value={this.state.address1} onChange={(e)=>{
                                    this.getSuggestions(e.target.value);
                                    this.setState({address1:e.target.value})
                                  }} required list="datalist" onBlur={this.changeCountry} className="form-control" placeholder="Enter Address 1"/>
                                  <datalist id="datalist">
                                    <option value="">select</option>
                                    {this.state.data.length>0?
                                    this.state.data.map((e, i)=>{
                                      return(
                                        <option key={i} value={e.value}>{e.label}</option>
                                      )
                                    }):null}
                                  </datalist>
                              </div>
                              <div className="form-group">
                                  <label>Postal Code <span className="text-danger">*</span></label>
                                  <input type="text" value={this.state.pin} onChange={(e)=>{
                                    this.setState({pin:e.target.value})
                                  }} required className="form-control" placeholder="Enter Postal Code"/>
                              </div>
                              <div className="form-group">
                                  <label>Country <span className="text-danger">*</span></label>
                                  <input type="text" value={this.state.country} onChange={(e)=>{
                                    this.setState({country:e.target.value})
                                  }} required className="form-control" placeholder="Enter Country Name"/>
                              </div>
                              <div className="form-group">
                                  <label>Password <span className="text-danger">*</span></label>
                                  <PasswordMask
                                    inputClassName={"form-control"}
                                    buttonClassName={"btnEye"}
                                    className={"inputPassword"}
                                    placeholder="Enter password"
                                    value={this.state.password}
                                    onChange={(e)=>{
                                      let data = validatePassword(e.target.value);
                                      this.setState({
                                        password:e.target.value,
                                        strength: data.strength,
                                        color: data.color
                                      });
                                    }}
                                    required
                                    showButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                    hideButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                     useVendorStyles={false}
                                  />
                                  <p style={{marginTop: '5px'}}><strong className={this.state.color}>{this.state.strength}</strong></p>
                              </div>
                              <div className="row align-items-center">
                                  <div className="col-md-5">
                                      <input type="submit" title="Sign Up" value="Sign Up" />
                                  </div>
                                  <div className="col-md-7 text-right">
                                      <p className="mb-0 font-weight-medium f12">By signing up, you agree to our <a href="https://kargologic.com/terms-of-use" target="_blank" rel="noopener noreferrer">T&C</a> and <a href="https://kargologic.com/privacy-policy" target="_blank" rel="noopener noreferrer" title="Privacy Policy">Privacy Policy.</a></p>
                                  </div>
                              </div>
                          </form>
                      </div>
                      <div className="boxComman p-4 text-center font-weight-medium">
                          <p className="mb-0">Already have an Kargologic account? <NavLink to="/carrier/login">Login</NavLink></p>
                      </div>
                  </div>
              </div>
          </section>
      </article>
    )
  }
}

export default  CustomerCarrierSignup;
