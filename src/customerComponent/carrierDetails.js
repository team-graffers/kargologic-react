import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import CarrierSidebar from './include/carrierDetailsSidebar';
import Path from './include/path';
import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getCarrierDetails, getCarriersCount, getCarrierList} from '../actions/customerActions';
import Datatable from '../component/subComponent/datatable'
import PostFetch from '../component/ajax/postFetch';
import GetFetch from '../component/ajax/getFetch';
import {formateDate} from '../component/include/date';
import {successAlert, errorAlert, infoAlert} from '../component/include/alert';
import {DownloadExcel} from '../component/ajax/downloadExcel';

class CarrierDetails extends Component{
  constructor(props){
    super(props);
    let url = window.location.href;
    url = url.split('carrierdetails/');
    this.state = {
      carr_com_id: url[1],
      customers:'All Carrier',
      check: false,
      jobs: [],
      allCustomers: [],
      total: 0,
      pending: 0,
      inprocess: 0,
      delivered: 0,
      cancelled: 0,
      job_id: [],
      invite: false,
      carrierList: [],
      search:'',
      path: [
              {
                "path": "All Carrier",
                "url": "/carrier/all"
              },
              {
                "path": "",
                "url": "/carrierdetails/"
              }
            ],
      columns:[
                {
                  name: 'Job Number',
                  selector: 'job_id',
                  sortable: true,
                  width:'150px',
                  cell: row => <NavLink style={{"zIndex":"999", "position":"relative"}} to={row.status==="draft"?`/createorder/?id=${row.id}`:`/customerorderdetail/${row.id}`}>{row.job_id}</NavLink>,
                },
                {
                  name: 'Created Date',
                  selector: 'order_create_time',
                  sortable: true,
                },
                {
                  name: 'Carrier Name',
                  selector: 'name',
                  sortable: true,
                  width:'200px',
                },
                {
                  name: 'Pick Up Time From',
                  selector: 'pickup_date_time',
                  sortable: true,
                },
                {
                  name: 'Pick Up Time To',
                  selector: 'pickup_date_time_to',
                  sortable: true,
                },
                {
                  name: 'Delivery Time From',
                  selector: 'delivery_date_time',
                  sortable: true,
                },
                {
                  name: 'Delivery Time To',
                  selector: 'delivery_date_time_to',
                  sortable: true,
                },
                {
                  name: 'Status',
                  selector: 'status',
                  sortable: true,
                  width:'150px',
                  cell: row => <span className={row.bgColor}>{row.status}</span>,
                }
              ]
    }
    this.changePath = this.changePath.bind(this);
    this.searchJobs = this.searchJobs.bind(this);
  }
  componentWillMount(){
    this.props.getCarrierList();
    this.props.getCarriersCount('cust/cust_dash_carriercount/', {});
    this.changeCarrier();
  }
  handleDelete = async ()=>{
    let payload = this.changeData();
    if(payload.orderlist.length>0){
      this.setState({
        jobs: [],
        check: false,
      });
      let json = await PostFetch('carr/order_delete/', payload, null, 'PUT');
      if(json && json.message){
        successAlert(json.message);
        this.changeCarrier();
      }else{
        errorAlert('try again!')
      }
    }else{
      errorAlert('Please select orders!')
    }
  }
  changeCarrier =(id=null)=>{
    if(id){
      this.setState({
        carr_com_id:id
      });
    }else{
      id = this.state.carr_com_id;
    }
    this.props.getCarrierDetails('cust/ord_carrier_cusdash/', {carr_com_id: id});
  }
  changePath = (e, url, fetchUrl = null) =>{
    //this.props.getAllDrivers(fetchUrl);
    let path = this.state.path;
    path[1].path = e;
    path[1].url = url;
    this.setState({path:path});
  }
  shortName = (name) =>{
  if(name){
    let main = '';
    let first = '';
    //let last = '';
    if(name.indexOf(' ')>0){
      main = name.split(' ');
      first = main[0].split('');
    }else{
      first = name.split('');
    }
    if(main[1]){
    //  last = main[1].split('');
    //  last = last[0].toUpperCase()
    }
    return (
      <span className="bg-info text-center" style={{"margin":"0",
      "borderRadius": "50%",
      "fontWeight": "600",
      "padding":"18px",
      "paddingTop":"10px",
      "paddingBottom":"10px",
        "color":"#fff",
        "fontSize":"22px"}}>{first[0].toUpperCase()}</span>
      )
    }else{
      return(
        ''
      )
    }
  }

  handleCheckbox = (data)=>{
    this.setState({
      job_id:data
    });
  }

  changeData = ()=>{
    if(this.state.job_id.length>0){
      let jid = [];
      this.state.job_id.map((e, i)=>{
        jid.push(e.id);
        return(
          ''
        )
      });
      let obj ={
        orderlist: jid
      };
      return obj;
    }else{
      let obj ={
        orderlist: []
      };
      return obj;
    }
  }
  handleDownload = ()=>{
    let payload = this.changeData();
    if(payload.orderlist && payload.orderlist.length>0){
      let customer = JSON.parse(localStorage.getItem("customerDetails"));
      payload = 'cust/order_download/?orderlist='+JSON.stringify(payload.orderlist)+'&cust_com_id='+customer.cust_com_id;
      DownloadExcel(payload)
    }else{
      errorAlert('something went wrong!')
    }
  }
  componentWillReceiveProps(nextProps) {
    let jobs = [];
    let total = 0;
    let pending = 0;
    let inprocess = 0;
    let delivered = 0;
    let cancelled = 0;
    if(nextProps.carrierList && nextProps.carrierList.length>0){
      this.setState({
        carrierList: nextProps.carrierList
      })
    }
    if(nextProps.carrierDetails && nextProps.carrierDetails.order_data){
      let path = this.state.path;
      path[1].path = nextProps.carrierDetails.company_data.company_name;
      path[1].url = "/carrierdetails/"+this.state.carr_com_id;
      this.setState({
        path: path
      })
/*{
      "id": 55,
      "job_id": "212",
      "CustomerCompanyID": {
        "name": "endrian Co"
      },
      "CarrierCompanyID": {
        "name": "KM CO."
      },
      "pickup_date_time": [
        "2019-07-09T11:00:00Z"
      ],
      "pickup_date_time_to": [
        "2019-07-09T12:00:00Z"
      ],
      "delivery_date_time": [
        "2019-07-09T16:00:00Z"
      ],
      "delivery_date_time_to": [
        "2019-07-09T16:30:00Z"
      ],
      "order_create_time": "08/07/19 05:07 PM",
      "created_DT": "2019-07-08T11:37:26.767264Z",
      "StatusID": {
        "status_name": "unassigned"
      },
      "is_active": true
    },*/
      nextProps.carrierDetails.order_data.map(d =>{
        total++;
        let status = d.StatusID.status_name;
        let bgColor = "";
        if(status==="unassigned"){
          bgColor = "status statusUnassigned";
        }else if(status==="pending"){
          pending++;
          bgColor = "status statusPending";
        }else if(status==="in process"){
          inprocess++;
          status = "in-progress";
          bgColor = "status statusProgress";
        }else if(status==="delivered"){
          delivered++;
          bgColor = "status statusDelivered";
        }else if(status==="cancelled"){
          cancelled++;
          bgColor = "status statusCancelled";
        }
         if(!d.is_active && d.Created_by_CustomerID && parseInt(d.Created_by_CustomerID)){
          status = "draft";
          bgColor = "status statusUnassigned";
        }
        jobs.push({
          id: d.id,
          job_id: d.job_id,
          order_create_time: d.order_create_time,
          name: d.CarrierCompanyID.name,
          pickup_date_time: formateDate(d.pickup_date_time[0]),
          delivery_date_time: formateDate(d.delivery_date_time[0]),
          pickup_date_time_to: formateDate(d.pickup_date_time_to[0]),
          delivery_date_time_to: formateDate(d.delivery_date_time_to[0]),
          dot:'',
          status: status,
          bgColor: bgColor
        })
        return(
          ''
        )
      });
      this.setState({
        check: true,
        jobs: jobs,
        allCustomers: nextProps.customerList,
        total: total,
        pending: pending,
        inprocess: inprocess,
        delivered: delivered,
        cancelled: cancelled,
      });
    }else{
      this.setState({
        check: true,
        jobs: [],
        allCustomers: nextProps.customerList,
        total: total,
        pending: pending,
        inprocess: inprocess,
        delivered: delivered,
        cancelled: cancelled,
      });
    }
  }
  searchJobs= async (text)=>{
    this.setState({
      check: false,
      jobs: [],
    })
    let json = await GetFetch('carr/search_in_driver/?search='+text+'&driver_id='+this.state.driver_id);
    if(json && json.length>0){
      let jobs = [];
      json.map(d=>{
        let status = d.StatusID.status_name;
        let bgColor = "";
        if(status==="unassigned"){
          bgColor = "status progressUnassigned";
        }else if(status==="pending"){
          bgColor = "status statusPending";
        }else if(status==="in process"){
          bgColor = "status progressInProgress";
        }else if(status==="delivered"){
          bgColor = "status statusDelivered";
        }else if(status==="cancelled"){
          bgColor = "status statusCancelled";
        }else if(status==="draft"){
          bgColor = "status progressUnassigned";
        }
        jobs.push({
          id: d.id,
          job_id: d.job_id,
          order_create_time: formateDate(d.order_create_time),
          name: d.CustomerCompanyID.name,
          pickup_date_time: formateDate(d.pickup_date_time[0]),
          delivery_date_time: formateDate(d.delivery_date_time[0]),
          dot:'',
          status: d.StatusID.status_name,
          bgColor: bgColor
        })
        return(
          ''
        )
      });
      this.setState({
        check: true,
        jobs: json,
      });
    }
  }
  render(){

  return(
    <article>
        <div className="dashboardMain">
            <Header/>
            <div className="dashboardContent">
                <MainSidebar/>
                <div className="rightColumn">
                  <CarrierSidebar driversCount={this.props.carriersCount}/>
                    <div className="rightColumnContent">
                        <Path path={this.state.path} />
                        <div className="rowOrderDetail">
                            <div className="columnJobno">
                                <h6 className="nameDriver">Carrier Name <i className="fas fa-sort-down"></i></h6>
                                <ul className="listJobNo listDrivername">
                                {this.props.carrierList && this.props.carrierList.length>0?
                                   this.props.carrierList.map(d=>(
                                  <li key={d.id} className="id-selector marquee">
                                    <a href="#carrier" className={parseInt(d.id)===parseInt(this.state.carr_com_id)? 'active': ''} onClick={(e)=>{
                                      e.preventDefault();
                                      window.history.pushState('', 'id', `/carrierdetails/${d.id}`);
                                      this.setState({
                                        check: false
                                      });
                                      this.changeCarrier(d.id);
                                    }}>
                                    <div className="text-center">
                                    {(d.name)}
                                    </div>
                                  </a></li>
                                )):null}
                                </ul>
                            </div>
                            <div className="contentOrderDetail p-0 boxDriverDetail">
                                    <div className="contentdriverDetail">
                                        <h3>Carrier Details</h3>
                                        {this.props.carrierDetails && this.props.carrierDetails.company_data?
                                          <DispDetails
                                            id={this.state.carr_com_id}
                                            carrierDetails={this.props.carrierDetails.company_data}
                                          />
                                          :null}
                                    </div>
                                    <div className="contentOrderDetails">
                                        <div className="row align-items-center">
                                            <div className="col-md-6">
                                                <h3 className="mb-0">Order Details</h3>
                                            </div>
                                            <div className="col-md-6 ">
                                                <div className="rowTotalOrder">
                                                    <div className="boxtotalOrders">
                                                                 {this.state.total}
                                                        <span>Total Orders</span>
                                                    </div>
                                                    <div>
                                                        <ul className="listOrderStatus">
                                                            <li>Pending <span>{this.state.pending}</span></li>
                                                            <li>In-Progress <span>{this.state.inprocess}</span></li>
                                                            <li>Delivered <span>{this.state.delivered}</span></li>
                                                            <li>Cancelled <span>{this.state.cancelled}</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div className="tableHeader">
                                            <div className="row align-items-center">
                                                <div className="col-12 col-sm-8 col-md-8">
                                                    <ul className="listFilter">


                                                    <li><a href="#download" onClick={(e)=>{
                                                      e.preventDefault();
                                                      if(this.state.job_id.length>0){
                                                        this.handleDownload()
                                                      }else{
                                                        infoAlert('Please select order')
                                                      }
                                                    }} className="iconDownload">Download</a></li>
                                                      <li>
                                                      <select onChange={(e)=>{
                                                        this.setState({customers:e.target.value});
                                                        if(e.target.value!=="All Carrier"){
                                                          window.history.pushState('', 'id', `/carrierdetails/${e.target.value}`);
                                                          this.setState({
                                                            check: false
                                                          });
                                                          this.changeCarrier(e.target.value);
                                                        }else{
                                                          this.changeCarrier();
                                                        }
                                                      }} value={this.state.customers} className="form-control selectCustomers">
                                                          <option value="All Carrier">All Carrier</option>
                                                          {this.state.carrierList && this.state.carrierList.length>0?
                                                            this.state.carrierList.map((e, i)=>(
                                                            <option key={i} value={e.id}>{e.name}</option>
                                                          )):null}
                                                          </select>
                                                      </li>
                                                    </ul>
                                                </div>
                                                <div className="col-12 col-sm-4 col-md-4">
                                                    <div className="boxSearch">
                                                        <input type="text" className="form-control" onChange={(e)=>{
                                                          this.setState({search:e.target.value});
                                                          if(e.target.value){
                                                            this.searchJobs(e.target.value);
                                                          }else{
                                                            this.changeCarrier();
                                                          }
                                                        }} placeholder="Search" />
                                                        <button className="btnSearch"><i className="fas fa-search"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {this.state.jobs.length>0 && this.state.check===true?
                                          <Datatable returnFunc={this.handleCheckbox} data={this.state.jobs} columns={this.state.columns}  />:
                                          <p>no records to display</p>}
                                          </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
    </article>
  )
}
}

const DispDetails = (props)=>{
  if(props.carrierDetails && props.id){
    let carrier = props.carrierDetails;
      return(
        <div className="row align-items-center">
            <div className="col-12 col-sm-5 col-md-5">
                <div className="rowUser">
                    <div>
                      <img className="imgUser" src={carrier.carrier_img? carrier.carrier_img : '/assets/img/usergray.jpg'} alt="" title=""/>
                    </div>
                    <div>
                        <h3 className="mb-0">
                        {carrier.company_name}
                        </h3>
                        <p className="font-medium mb-0">Location <span className="textGray">{carrier.com_address}</span></p>
                    </div>
                </div>
            </div>
            <div className="col-12 col-sm-7 col-md-7 boxInfo">
                <h5 className="headingBorderSmall">Info</h5>
                <div className="row">
                    <div className="col-md-6">
                        <div className="row pb-1">
                            <div className="col-md-6">Email</div>
                            <div className="col-md-6 textGray">{carrier.com_email}</div>
                        </div>
                        <div className="row">
                            <div className="col-md-6">Phone No.</div>
                            <div className="col-md-6 textGray">{carrier.com_mob_no}</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
      )
  }else{
    return (
      'not loaded'
    )
  }
}

CarrierDetails.propTypes = {
  getCarrierDetails: PropTypes.func.isRequired,
  getCarriersCount: PropTypes.func.isRequired,
  getCarrierList: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  carriersCount: state.customer.carriersCount,
  carrierDetails: state.customer.carrierDetails,
  carrierList: state.customer.carrierList,
});

export default connect(mapStateToProps, {getCarrierDetails, getCarriersCount, getCarrierList})(CarrierDetails);


/*<li><a href="#delete" onClick={(e)=>{
  e.preventDefault();
  this.handleDelete()
}} className="iconDelete"><i className="far fa-trash-alt"></i> Delete</a></li>*/
