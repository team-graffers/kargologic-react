import React  from 'react';

let all = 0;
let added = 0;
let pending = 0;

let allClass = "active";
let addedClass = "";
let pendingClass = "";

const allNull=()=>{
  allClass = "";
  addedClass = "";
  pendingClass = "";
}

const CarrierSidebar = (props) =>{
  if(props.driversCount){
    all = props.driversCount.all_carr_com_count;
    added = props.driversCount.added_carr_com_count;
    pending = props.driversCount.pending_carr_com_count;
  }
  return(
    <div className="rightColumnMenu">
        <ul className="menuRightColumn">
          <li><a href="#a" className={allClass} onClick={(e)=>{
            e.preventDefault();
            allNull();
            allClass = "active";
            window.history.pushState('', 'customercarrier', '/customercarrier/all');
            props.changePath('All Carriers', '/customercarrier/all', 'all');
          }}>
            All Carriers <span>{all}</span>
          </a></li>
          <li><a href="#a" className={addedClass} onClick={(e)=>{
            e.preventDefault();
            allNull();
            addedClass = "active";
            window.history.pushState('', 'customercarrier', '/customercarrier/added');
            props.changePath('Added Carriers', '/customercarrier/added', 'added');
          }}>
            Added Carriers <span>{added}</span>
          </a></li>
          <li><a href="#a" className={pendingClass} onClick={(e)=>{
            e.preventDefault();
            allNull();
            pendingClass = "active";
            window.history.pushState('', 'customercarrier', '/customercarrier/pending');
            props.changePath('Pending Carriers', '/customercarrier/pending', 'pending');
          }}>
            Pending Carriers <span>{pending}</span>
          </a></li>
        </ul>
    </div>
  );
}

export default CarrierSidebar;
