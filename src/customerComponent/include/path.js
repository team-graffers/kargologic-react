import React from 'react';
import {NavLink} from 'react-router-dom';

const Path = (props) =>{
  return(
    <nav aria-label="breadcrumb">
        <ol className="breadcrumb mb-0">
        {props.path.map((e,i)=>{
          return (
            <li key={i} className="breadcrumb-item"><NavLink to={e.url}>{e.path}</NavLink></li>
          )
        })}
        </ol>
    </nav>
  );
}

export default Path;
