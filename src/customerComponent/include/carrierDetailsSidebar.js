import React  from 'react';
import {NavLink} from 'react-router-dom';

let all = 0;
let added = 0;
let pending = 0;

const CarrierSidebar = (props) =>{
  if(props.driversCount){
    all = props.driversCount.all_carr_com_count;
    added = props.driversCount.added_carr_com_count;
    pending = props.driversCount.pending_carr_com_count;
  }
  return(
    <div className="rightColumnMenu">
        <ul className="menuRightColumn">
        <li>
          <NavLink className="active" to="/customercarrier/all">
            All Carriers <span>{all}</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/customercarrier/added">
            Added Carriers <span>{added}</span>
          </NavLink>
        </li>
        <li>
          <NavLink to="/customercarrier/pending">
            Pending Carriers <span>{pending}</span>
          </NavLink>
        </li>
        </ul>
    </div>
  );
}

export default CarrierSidebar;
