import React  from 'react';
import {NavLink} from 'react-router-dom';

const SettingSidebar = () =>{
  return(
    <div className="rightColumnMenu">
        <ul className="menuRightColumn">
        <li><NavLink to="/customersettings">Account Settings</NavLink></li>
        <li><NavLink to="/customerpassword">Password</NavLink></li>
        <li><NavLink to="/customerconfigure">Configure</NavLink></li>
        <li><NavLink to="/customersender">Senders</NavLink></li>
        <li><NavLink to="/customerreceiver">Receivers</NavLink></li>
        </ul>
    </div>

  );
}

export default SettingSidebar;
