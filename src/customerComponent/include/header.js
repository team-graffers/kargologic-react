import React, {Component} from 'react';
import createOrder from '../../assets/img/icon-createOrder.png';
import inviteCustomer from "../../assets/img/icon-InviteCustomer.png";
import {NavLink} from 'react-router-dom';
import {Redirect } from 'react-router';
import InviteCarrier from '../subComponent/inviteCarrier';
import GetFetch from '../../component/ajax/getFetch';
import {formateDate2} from '../../component/include/date';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {setSidebar} from '../../actions/carrierActions';

class CustomerHeader extends Component{
  constructor(props){
    super(props);

    this.state = {
      name: '',
      pic: '',
      email: '',
      status: false,
      inviteCarrierModel: false
    }
  }

  componentWillMount(){
    let customer = JSON.parse(localStorage.getItem("customerDetails"));
    let userType = localStorage.getItem("userType");
    if(customer && userType && userType === "customer"){
      this.setState({
        name: customer.cust_com_name,
        email: customer.email,
        pic: customer.cust_image,
      })
    }else {
      this.setState({
        status: true
      })
    }
    this.getNotifications();
  }
  componentDidMount(){
    setInterval(function() {
      this.getNotifications();
    }.bind(this),5000);
  }
  openOneModel=(modalId)=>{
    this.setState({
      inviteCarrierModel: true
    })
  }
  closeOneModel=()=>{
    this.setState({
      inviteCarrierModel: false
    })
  }
  getNotifications = async ()=>{
    let noti = await GetFetch('cust/notify_cust/');
    try{
      if(noti){
        if(noti.data.length>0){
          let count = localStorage.getItem('notiCount');
          if(!count){
            count = 0;
          }
          if(count<noti.data.length){
            this.setState({
              notiAlert: true,
              notification: noti.data
            });
          }else{
            this.setState({
              notification: noti.data
            });
          }

        }
      }
    }catch(e){

    }
  }
  render(){
    if(this.state.status){
      return(
        <Redirect to="/customer/login" />
      )
    }
    return(
      <React.Fragment>
        <div className="dasboardHeader">
            <div className="topbar">
                <div className="container-fluid">
                    <div className="row align-items-center">
                        <div className="col-12 col-sm-3 col-md-6">
                            <h1 id="logo">
                              <NavLink className="navbar-brand" to="/dashboard/all">
                                <img src="https://kargologic.s3-ap-southeast-2.amazonaws.com/front_img/logo.svg" alt="Kargologic" title="Kargologic"/>
                              </NavLink>
                            </h1>
                        </div>
                        <div className="col-12 col-sm-9 col-md-6 text-right">
                            <ul className="listTopMenu">
                            <li className="dropdown dropdownAdd">
                                <a href="#a" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" className="btnAdd">.</a>
                                <div className="dropdown-menu dropdown-menu-right" style={{transform: "translate3d(-214px, 32px, 0px) !important", width: '250px'}}>
                                    <ul className="listAddOptions">
                                        <li style={{width: "50%"}}>
                                            <NavLink to="/customercreateorder">
                                                <img src={createOrder} alt="" title="" />
                                                <h6>Create Order</h6>
                                            </NavLink>
                                        </li>
                                        <li style={{width: "50%"}}>
                                            <a href="#InviteCarrier" onClick={(e)=>{
                                              e.preventDefault();
                                              this.openOneModel();
                                            }}>
                                                <img src={inviteCustomer} alt="" title="" />
                                                <h6>Invite Carrier</h6>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li className="dropdown dropdownNotification">
                                <a href="#a" onClick={(e)=>{
                                  if(this.state.notification && this.state.notification.length>0){
                                    localStorage.setItem('notiCount', this.state.notification.length);
                                    this.setState({
                                      notiAlert: false
                                    })
                                  }
                                }} className="btnNotification" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {this.state.notiAlert?
                                <span className="signNotification"></span>
                                :null}
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <h4>Notification</h4>
                                    <ul className="listNotification">
                                      {this.state.notification && this.state.notification.length>0?
                                        this.state.notification.map((e, i)=>(
                                          <li key={i}>
                                          {e.OrderID?
                                            <NavLink to={`/customerorderdetail/${e.OrderID}`}>
                                              <div className="contentNotification">
                                                <div className="boxUserimg"><img src="assets/img/img-user.png" alt="" title="" /></div>
                                                <div>
                                                  <p>{e.comments}</p>
                                                  <span className="textTime"></span>
                                                </div>
                                              </div>
                                            </NavLink>
                                          :
                                            <a href="#a">
                                              <div className="contentNotification">
                                                <div className="boxUserimg"><img src="assets/img/img-user.png" alt="" title="" /></div>
                                                <div>
                                                  <p>{e.comments}</p>
                                                  <p className="textTime" align="right">{formateDate2(e.updated_DT)}</p>
                                                </div>
                                              </div>
                                            </a>}
                                          </li>
                                        )):<li>
                                          <a href="#a">
                                            <div className="contentNotification">
                                              <div>
                                                <p>No Notification</p>
                                              </div>
                                            </div>
                                          </a>
                                        </li>}

                                    </ul>
                                </div>
                            </li>
                                <li>
                                    <div className="dropdown dropdownSetting">
                                        <a className="btn_icon dropdown-toggle" href="#a" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img className="imgUser" src={this.state.pic} alt="" title="" />   Hi, {this.state.name}  </a>
                                        <div className="dropdown-menu dropdown-menu-right">
                                            <div className="userDetail">
                                                <img className="imgUser" src={this.state.pic} alt="" title="" />
                                                <h5>{this.state.name}</h5>
                                                <h6>{this.state.email}</h6>
                                            </div>
                                            <NavLink className="dropdown-item" onClick={(e)=>{
                                              this.props.setSidebar('iconSettings', 'iconDrivers', 'iconOrder');
                                            }} to="/customersettings">Account</NavLink>
                                            <NavLink className="dropdown-item" onClick={(e)=>{
                                              this.props.setSidebar('iconSettings', 'iconDrivers', 'iconOrder');
                                            }} to="/customerpassword">Update Password</NavLink>
                                            <NavLink className="dropdown-item" to="/customerlogout">Logout</NavLink>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {this.state.inviteCarrierModel?
          <InviteCarrier closeOneModel={this.closeOneModel} />
        :null}
    </React.Fragment>
  );
  }
}

CustomerHeader.propTypes = {
  setSidebar: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  mainSidebar: state.carrier.mainSidebar,
});

export default connect(mapStateToProps, {setSidebar})(CustomerHeader);
