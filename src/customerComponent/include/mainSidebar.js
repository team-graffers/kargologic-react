import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {setSidebar} from '../../actions/carrierActions';

const MainSidebar =(props)=>{
  if(props.mainSidebar){
    return(
      <div className="leftColumn">
        <ul className="menuMainCustomer">
          <li><Link to="/customerdashboard/all" onClick={(e)=>{
            props.setSidebar('iconOrder', 'iconDrivers', 'iconSettings');
          }} className={`iconOrder ${props.mainSidebar.iconOrder}`} id="iconOrder"></Link></li>
          <li><Link to="/customercarrier/all" onClick={(e)=>{
            props.setSidebar('iconDrivers', 'iconOrder', 'iconSettings');
          }} className={`iconDrivers ${props.mainSidebar.iconDrivers}`} id="iconDrivers"></Link></li>
          <li><Link to="/customersettings" onClick={(e)=>{
            props.setSidebar('iconSettings', 'iconDrivers', 'iconOrder');
          }} className={`iconSettings ${props.mainSidebar.iconSettings}`} id="iconSettings"></Link></li>
        </ul>
      </div>
    );
  }else{
    return(
      ''
    )
  }
}

MainSidebar.propTypes = {
  setSidebar: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  mainSidebar: state.carrier.mainSidebar,
});

export default connect(mapStateToProps, {setSidebar})(MainSidebar);
