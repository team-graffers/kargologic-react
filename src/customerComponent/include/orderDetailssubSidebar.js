import React  from 'react';
import {NavLink} from 'react-router-dom';

let all = 0;
let unassigned = 0;
let pending = 0;
let inprocess = 0;
let delivered = 0;
let draft = 0;

let allClass = "active";
let unassignedClass = "";
let pendingClass = "";
let inprocessClass = "";
let deliveredClass = "";
let draftClass = "";

const allNull=()=>{
  allClass = "";
  unassignedClass = "";
  pendingClass = "";
  inprocessClass = "";
  deliveredClass = "";
  draftClass = "";
}
allNull();
const SubSidebar = (props) =>{
  if(props.jobsCount){
    all = props.jobsCount.all;
    unassigned = props.jobsCount.unassigned;
    pending = props.jobsCount.pending;
    inprocess = props.jobsCount.process;
    delivered = props.jobsCount.delivered;
    draft = props.jobsCount.draft;
  }
  if(props.status){
    if(props.status==="unassigned"){
      allNull();
      unassignedClass = 'active';
    }else if(props.status==="pending"){
      allNull();
      pendingClass = 'active';
    }else if(props.status==="in process"){
      allNull();
      inprocessClass = 'active';
    }else if(props.status==="delivered"){
      allNull();
      deliveredClass = 'active';
    }
  }
  return(
    <div className="rightColumnMenu">
      <div className="boxBtnOrders">
        <a href="/customercreateorder" className="btn btn-md">Create Orders</a>
      </div>
      <ul className="menuRightColumn">
        <li>
          <NavLink className={allClass} to="/customerdashboard/all">
            All Jobs <span>{all}</span>
          </NavLink>
        </li>
        <li>
          <NavLink className={unassignedClass} to="/customerdashboard/unassigned">
            Unassigned <span>{unassigned}</span>
          </NavLink>
        </li>
        <li>
          <NavLink className={pendingClass} to="/customerdashboard/pending">
            Pending <span>{pending}</span>
          </NavLink>
        </li>
        <li>
          <NavLink className={inprocessClass} to="/customerdashboard/in-process">
            In-progress <span>{inprocess}</span>
          </NavLink>
        </li>
        <li>
          <NavLink className={deliveredClass} to="/customerdashboard/delivered">
            Delivered <span>{delivered}</span>
          </NavLink>
        </li>
        <li>
          <NavLink className={draftClass} to="/customerdashboard/draft">
            Draft <span>{draft}</span>
          </NavLink>
        </li>
      </ul>
    </div>
  );
}

export default SubSidebar;
/**/
