import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import CarrierSidebar from './include/carrierSidebar';
import Path from './include/path';
import addUser from '../assets/img/iconaddUser.svg';
 import InviteCarrier from './subComponent/inviteCarrier';
 import {NavLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {updateEditOrder, clearOrder, getAllCarriers, searchCarriers, getCarriersCount} from '../actions/customerActions';
import Datatable from '../component/subComponent/datatable'
import {DownloadExcel} from '../component/ajax/downloadExcel';
import {errorAlert, infoAlert} from '../component/include/alert';
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';

class CustomerCarriers extends Component{
  constructor(props){
    super(props);
    let url = window.location.href;
    url = url.split('/customercarrier/');
    this.state = {
      carrier_id:[],
      search:'',
      status: url[1],
      check:false,
      carriers:[],
      tracking: [],
      driversList: [],
      currentStatus: '',
      showMap: false,
      inviteCarrierModel: false,
      path: [
              {
                "path": "Carriers",
                "url": "/customercarrier/all"
              },
              {
                "path": "All",
                "url": "/customercarrier/all"
              }
            ],
      columns:[
        {
          name: 'Carrier Name',
          selector: 'carrier_name',
          sortable: true,
          cell: row => <span><NavLink style={{"zIndex":"999", "position":"relative"}} to={`/carrierdetails/${row.carrier_id}`}>{row.carrier_name}</NavLink><br/>{row.carrier_address}</span>,
        },
        {
          name: 'POC',
          selector: 'poc',
          sortable: true,
        },
        {
          name: 'Phone Number',
          selector: 'phone',
          sortable: true,
        },
        {
          name: 'Email Address',
          selector: 'email',
          sortable: true,
        },
        {
          name: 'Delivery Rate',
          selector: 'dot',
          cell: row => <div ><CircularProgressbar value={parseFloat(row.dot).toFixed(1)} text={`${parseFloat(row.dot).toFixed(1)}%`} /> </div>,
        },
        {
          name: 'Status',
          selector: 'status',
          sortable: true,
          cell: row => <span className={row.bgColor}>{row.status}</span>,
        }
      ]
    }
    this.handleCheckbox = this.handleCheckbox.bind(this);
  }

  componentWillMount(){
     this.props.updateEditOrder(false);
    // this.props.clearOrder();
    if(this.state.status==="all"){
      this.changePath('All Carriers', '/customercarrier/all', 'all');
    }else if(this.state.status==="added"){
      this.changePath('Added Carriers', '/customercarrier/added', 'added');
    }else if(this.state.status==="pending"){
      this.changePath('Pending Carriers', '/customercarrier/pending', 'pending');
    }
    this.props.getCarriersCount('cust/cust_dash_carriercount/', {});

  //  this.trackDrivers([]);
  }
  changePath = (e, url, fetchUrl = null) =>{
    this.props.getAllCarriers('cust/listcarrieroncust/?status='+fetchUrl+' carr_cust&search=', {});
    let path = this.state.path;
    path[1].path = e;
    path[1].url = url;
    this.setState({
      path: path,
      check: false,
      currentStatus: fetchUrl
    });
  }

  handleCheckbox = (data) =>{
    this.setState({
      carrier_id:data
    });
  }
  changeData = ()=>{
    if(this.state.carrier_id.length>0){
      let jid = [];
      this.state.carrier_id.map((e, i)=>{
        jid.push(e.carrier_id);
        return(
          ''
        )
      });
      let obj ={
        orderlist: jid
      };
      return obj;
    }else{
      let obj ={
        orderlist: []
      };
      return obj;
    }
  }
  openOneModel=(modalId)=>{
    this.setState({
      inviteCarrierModel: true
    })
  }
  closeOneModel=()=>{
    this.setState({
      inviteCarrierModel: false
    })
  }
  handleDownload = async ()=>{
    let payload = this.changeData();
    if(payload.orderlist && payload.orderlist.length>0){
      let customer = JSON.parse(localStorage.getItem("customerDetails"));
      if(customer && customer.cust_com_id){
        DownloadExcel('cust/carrier_list_download/?cust_com_id='+customer.cust_com_id+'&carrier_com_id_list='+JSON.stringify(payload.orderlist)+'', 'carrier list.xlsx');
      }
    }else{
      errorAlert('Please select orders!')
    }

  }
  componentWillReceiveProps(nextProps){
    let carriers = [];
    if(nextProps.allCarriers && nextProps.allCarriers.data && nextProps.allCarriers.data.length>0){
      nextProps.allCarriers.data.map(d =>{
        let dot = 0;
        /*carr_dot: [
      {
        carr_id: 2,
        dot: 87.5
      },*/
        if(nextProps.allCarriers.carr_dot && nextProps.allCarriers.carr_dot.length>0){
          nextProps.allCarriers.carr_dot.map(v =>{
            if(parseInt(v.carr_id) === parseInt(d.CarriercompanyID.id)){
              dot = v.dot
            }
            return('')
          });
        }
        let status = d.r_status.status_name;
        if(status){
          status = status.split(' carr_cust')
          status = status[0]
        }
        let bgColor = "";
        if(status==="added"){
          bgColor = "status statusDelivered";
        }else if(status==="pending"){
          bgColor = "status statusPending";
        }else if(status==="invited"){
          bgColor = "status progressUnassigned";
        }
        // let rating = d.CarriercompanyID.rating;
        // if(!rating){
        //   rating = 0;
        // }
        carriers.push({
          carrier_id: d.CarriercompanyID.id,
          carrier_name: d.CarriercompanyID.name,
          carrier_address: d.CarriercompanyID.location,
          phone: d.CarriercompanyID.mobile_num,
          email: d.CarriercompanyID.email,
          bgColor: bgColor,
          poc: '',
          dot: dot,
          status: status,
        })
        return(
          ''
        )
      });
      this.setState({
        carriers: carriers,
    //    carriersList: nextProps.allCarriers.data,
        check: true,
      })
    }
  }
  render(){
    return(
      <article>
        <div className="dashboardMain">
          <Header/>
          <div className="dashboardContent">
            <MainSidebar/>
              <div className="rightColumn">
                <CarrierSidebar driversCount={this.props.carriersCount} changePath={this.changePath}/>
                <div className="rightColumnContent">

                    <Path path={this.state.path} />
                    <div className="tableHeader">
                        <div className="row align-items-center">
                            <div className="col-12 col-sm-8 col-md-8">
                                <ul className="listFilter">
                                <li><a href="#download" className="iconDownload" onClick={(e)=>{
                                  e.preventDefault();
                                  if(this.state.carrier_id.length>0){
                                    this.handleDownload()
                                  }else{
                                    infoAlert('Please select order')
                                  }
                                }}><i className="fas fa-download"></i> Download</a></li>
                                <li>
                                  <a onClick={(e)=>{
                                    e.preventDefault();
                                    this.openOneModel();
                                  }} href="#InviteCarrier" className="btn btn-md">Invite Carrier
                                   <img src={addUser} alt="" title=""/>
                                  </a>
                                </li>
                                </ul>
                            </div>
                            <div className="col-12 col-sm-4 col-md-4">
                                <div className="boxSearch">
                                <input type="search" onChange={(e)=>{
                                  this.setState({
                                    search: e.target.value,
                                    carriers: [],
                                    check: false
                                  });
                                  if(e.target.value.length>1){
                                    this.props.searchCarriers('cust/listcarrieroncust/?status='+this.state.currentStatus+' carr_cust&search='+e.target.value);
                                  }else {
                                    this.props.getAllCarriers('cust/listcarrieroncust/?status=all carr_cust&search=');
                                  }
                                }} value={this.state.search} className="form-control" placeholder="search"/>
                                    <button className="btnSearch"><i className="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {this.state.carriers.length>0 && this.state.check===true?
                      <Datatable returnFunc={this.handleCheckbox} data={this.state.carriers} columns={this.state.columns}  />:
                      <p>no records to display</p>}
              </div>
            </div>
          </div>
        </div>
        {this.state.inviteCarrierModel?
          <InviteCarrier closeOneModel={this.closeOneModel} />
        :null}
      </article>
    )
  }
}

CustomerCarriers.propTypes = {
  getAllCarriers: PropTypes.func.isRequired,
  searchCarriers: PropTypes.func.isRequired,
  getCarriersCount: PropTypes.func.isRequired,
  updateEditOrder: PropTypes.func.isRequired,
  clearOrder: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  allCarriers: state.customer.allCarriers,
  carriersCount: state.customer.carriersCount
});

export default connect(mapStateToProps, {updateEditOrder, clearOrder,getAllCarriers, searchCarriers, getCarriersCount})(CustomerCarriers);
