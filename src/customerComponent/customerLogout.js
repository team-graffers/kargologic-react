// eslint-disable-next-line
import React from 'react';

const CustomerLogout = () =>{
  localStorage.removeItem("token");
  localStorage.removeItem("sidebar");
  localStorage.removeItem("customerDetails");
  window.location.href= "/customer/login";
  return(
    ''
  )
}

export default CustomerLogout;
