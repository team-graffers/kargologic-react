import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import SettingSidebar from './include/settingSidebar';
import Path from './include/path';
//import {successAlert, errorAlert} from './include/alert';
// import axios from 'axios';
// import store from '../store';
import Datatable from '../component/subComponent/datatable';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getSenderList, editSenderSet, clearEdit} from '../actions/customerActions';
import {formateDate} from '../component/include/date';

import UploadExcelASR from '../component/subComponent/uploadExcelASR';
import {DownloadExcel} from '../component/ajax/downloadExcel';

import AddSender from '../component/subComponent/addSender';

class CustomerSenders extends Component{
  constructor(props){
    super(props);
    this.state = {
      file: '',
      sender: [],
      temp: [],
      check: false,
      addSenderModel: false,
      excelModel: false,
      search: '',
      columns: [
        {
          name: 'Name',
          selector: 'name',
          sortable: true,
        },
        {
          name: 'Primary Contact',
          selector: 'primary_contact',
          sortable: true,
        },
        {
          name: 'Primary Email Id',
          selector: 'pri_email_id',
          sortable: true,
        },
        {
          name: 'Primary Phone',
          selector: 'pri_phone',
          sortable: true,
        },
        {
          name: 'Secondary Contact',
          selector: 'secondary_contact',
          sortable: true,
        },
        {
          name: 'Secondary Email Id',
          selector: 'sec_email_id',
          sortable: true,
        },
        {
          name: 'Secondary Phone',
          selector: 'sec_phone',
          sortable: true,
        },
        {
          name: 'Business Address',
          selector: 'business_address',
          sortable: true,
        },
        {
          name: 'Contract Start Date',
          selector: 'contract_start_date',
          sortable: true,
          cell: row=> <span>{formateDate(row.contract_start_date)}</span>
        },
        {
          name: 'Contract End Date',
          selector: 'contract_end_date',
          sortable: true,
          cell: row=> <span>{formateDate(row.contract_end_date)}</span>
        },
        {
          name: 'Contract Documents',
          selector: 'contract_documents',
          cell: row => <div style={{"zIndex":"999", "position":"relative", "width": "120px"}}>
            <ul style={{margin: 0, padding: 0, overflow: 'hidden'}}>
            {row.com_send_rec && row.com_send_rec.length>0?
              row.com_send_rec.map((e, i)=>(
                <li key={i} className="id-selector marquee"><a href={e.doc_upload} style={{'position': 'absolute'}} download>{i+1}. {e.attachment_name? e.attachment_name: 'Document'}</a></li>
              ))
            :null}
            </ul>
          </div>
        },
        {
          name: 'Edit',
          selector: 'id',
          cell: row => <div style={{"zIndex":"999", "position":"absolute"}}><a href="#a" onClick={(e)=>{
            e.preventDefault();
            this.props.editSenderSet(row);
            this.setState({
              check: false,
              addSenderModel: true
            })
          }}>Edit</a></div>,
        }
      ]
    }

  }
  closeOneModel=()=>{
    this.props.clearEdit();
    this.setState({
      addSenderModel: false,
      check: true,
    })
  }
  openExcelModel = ()=>{
    this.setState({
      excelModel: true
    })
  }
  closeExcelModel = ()=>{
    this.refreshData();
    this.setState({
      excelModel: false
    })
  }
  componentWillMount(){
    this.props.getSenderList('companysendrecei/managersenderrecei/');
  }
  componentWillReceiveProps(nextProps){
    console.log(nextProps);
    if(nextProps.senderList && nextProps.senderList.data && nextProps.senderList.data.length>0){
      let senderArr = [];
      nextProps.senderList.data.map((e, i)=>{
        if(e.is_sender){
          senderArr.push(e)
        }
        return('')
      })
      this.setState({
        sender: senderArr,
        check: true
      })
    }
  }
  handleCheckbox = (data)=>{
    this.setState({
      temp:data
    });
  }
  changeData = ()=>{
    if(this.state.temp && this.state.temp.length>0){
      let jid = [];
      this.state.temp.map((e, i)=>{
        jid.push(e.id);
        return('')
      });
      return jid;
    }else{
      return [];
    }
  }
  handleDownload = ()=>{
    let payload = this.changeData();
    let customer = JSON.parse(localStorage.getItem("customerDetails"));
    payload = 'companysendrecei/sr_download/?sr_list='+JSON.stringify(payload)+'&carr_com_id='+customer.cust_com_id+'&s_or_r=True';
    DownloadExcel(payload)
  //  http://13.54.44.169:8000/carr_com_id=37&sr_list=[]
  }


  refreshData = ()=>{
    this.setState({
      sender: [],
      check: false
    })
    this.props.getSenderList('companysendrecei/managersenderrecei/');
  }
  render(){
    return(
      <article>
        {this.state.excelModel?
          <UploadExcelASR
            name= "Upload Sender"
            fetchUrl= "companysendrecei/upload_sender_receiver/?type=sender"
            sampleUrl='companysendrecei/sample_sender_reciever/'
            refreshData={this.refreshData}
            closeModel={this.closeExcelModel}
          />
        :null}
          <div className="dashboardMain">
            <Header/>
              <div className="dashboardContent">
                <MainSidebar/>
                  <div className="rightColumn">
                    <SettingSidebar/>
                    <div className="rightColumnContent">
                    <Path path={[
                                  {
                                    "path": "Manage your account",
                                    "url": "/settings"
                                  },
                                  {
                                    "path": "Senders",
                                    "url": "/sender"
                                  }
                                ]} />
                          <div className="tableHeader">
                              <div className="row align-items-center">
                                  <div className="col-12 col-sm-8 col-md-8">
                                    <ul className="listFilter">
                                    <li><a href="#a" onClick={(e)=>{
                                      e.preventDefault();
                                      this.handleDownload();
                                    }}>Download</a></li>
                                    <li><a href="#uploadexcelasr" onClick={(e)=>{
                                      e.preventDefault();
                                      this.openExcelModel();
                                    }}>Upload</a></li>
                                    <li><a href="#a" onClick={(e)=>{
                                      e.preventDefault();
                                      this.setState({
                                        addSenderModel: true
                                      })
                                }}>Add New Sender</a></li>
                                    </ul>
                                  </div>
                                  <div className="col-12 col-sm-4 col-md-4">
                                      <div className="boxSearch">

                                      </div>
                                  </div>
                              </div>
                          </div>
                          {this.state.sender.length>0 && this.state.check===true?
                            <Datatable returnFunc={this.handleCheckbox} data={this.state.sender} columns={this.state.columns}  />:
                            <p>no records to display</p>}
                  </div>
              </div>
          </div>
        </div>
        {this.state.addSenderModel?
          <AddSender
           closeOneModel={this.closeOneModel}
           addMore={this.refreshData}
           />
         :null}
      </article>
    )
  }
}

CustomerSenders.propTypes = {
  getSenderList: PropTypes.func.isRequired,
  editSenderSet: PropTypes.func.isRequired,
  clearEdit: PropTypes.func.isRequired,
}

const mapStateToProps = state =>({
  editSender: state.customer.editSender,
  senderList: state.customer.senderList
});

export default connect(mapStateToProps, {getSenderList, editSenderSet, clearEdit})(CustomerSenders);
