import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import SettingSidebar from './include/settingSidebar';
import Path from './include/path';
import {successAlert, errorAlert, infoAlert} from '../component/include/alert';
import PostFetch from '../component/ajax/postFetch';
import PasswordMask from 'react-password-mask';
import store from '../store';
import {validatePassword, verfyPassword} from '../component/include/validation';

class CustomerPassword extends Component{
  constructor(props){
    super(props);
    let customer = JSON.parse(localStorage.getItem("customerDetails"));
    this.state = {
      email:customer.email,
      current_password:'',
      new_password:'',
      conStrength: '',
      conColor: '',
    }
    this.submitForm = this.submitForm.bind(this);
  }

  submitForm = async (e) =>{
    e.preventDefault();
    const payload = {
      email: this.state.email,
      current_password: this.state.current_password,
      new_password: this.state.new_password,
    }

    let json = await PostFetch('cust/reset_pass_cus/', payload, null, 'PUT');
    if(json && json.message==="password update success"){
      successAlert(json.message)
      this.setState({
        current_password:'',
        new_password:'',
      })
    }else {
      errorAlert(json.message)
    }

  }
  render(){
    const state = store.getState();
    return(
      <article>
        <div className="dashboardMain">
        <Header/>

            <div className="dashboardContent">
                <MainSidebar/>
                <div className="rightColumn">
                    <SettingSidebar/>
                    <div className="rightColumnContent">
                      <Path path={[
                                    {
                                      "path": "Manage your account",
                                      "url": "/settings"
                                    },
                                    {
                                      "path": "Password",
                                      "url": "/password"
                                    }
                                  ]} />
                      <div className="boxContentInner">
                          <div className="contentAccountSetting">
                              <form method="post" onSubmit={this.submitForm}>
                                  <div className="row">
                                      <div className="col-md-5">
                                          <div className="form-group">
                                              <label>Current Password</label>
                                              <PasswordMask
                                                inputClassName={"form-control"}
                                                buttonClassName={"btnEye"}
                                                className={"inputPassword"}
                                                placeholder="Enter current password"
                                                value={this.state.current_password}
                                                onChange={(e)=>{
                                                  this.setState({current_password:e.target.value})
                                                }}
                                                onBlur={(e)=>{
                                                  if(!verfyPassword(e.target.value)){
                                                    this.setState({current_password:''})
                                                    infoAlert('Password must contain:- minimum 8 character, at least one capital letter, at least one small letter, at least one number and one special symbol. ')
                                                  }
                                                }}
                                                name='old_password'
                                                required
                                                showButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                                hideButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                                 useVendorStyles={false}
                                              />
                                          </div>
                                      </div>
                                      <div className="col-md-5">
                                          <div className="form-group">
                                              <label>New Password</label>
                                              <PasswordMask
                                                inputClassName={"form-control"}
                                                buttonClassName={"btnEye"}
                                                className={"inputPassword"}
                                                placeholder="Enter new password"
                                                value={this.state.new_password}
                                                onChange={(e)=>{
                                                  let data = validatePassword(e.target.value);
                                                  this.setState({
                                                    new_password:e.target.value,
                                                    conStrength: data.strength,
                                                    conColor: data.color
                                                  });
                                                }}
                                                onBlur={(e)=>{
                                                  if(!verfyPassword(e.target.value)){
                                                    this.setState({new_password:''})
                                                    infoAlert('Password must contain:- minimum 8 character, at least one capital letter, at least one small letter, at least one number and one special symbol. ')
                                                  }
                                                }}
                                                name='new_password'
                                                required
                                                showButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                                hideButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                                 useVendorStyles={false}
                                              />
                                              <p style={{marginTop: '5px'}}><strong className={this.state.conColor}>{this.state.conStrength}</strong></p>
                                          </div>
                                      </div>
                                  </div>
                                  <div className="row">
                                      <div className="col-md-6">
                                          <input type="submit" className="btn" value="Save Changes"/>
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>
                    </div>
              </div>
            </div>
        </div>
      </article>

    )
  }
}

export default CustomerPassword;
