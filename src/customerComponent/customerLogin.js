import React, {Component} from 'react';
import SideImg from './include/side-img';
import {NavLink} from 'react-router-dom';
import {Redirect } from 'react-router';
import PasswordMask from 'react-password-mask';
import store from '../store';
import {errorAlert, infoAlert} from '../component/include/alert';
import PostFetch from '../component/ajax/postFetch';
import {verfyPassword} from '../component/include/validation';

class CustomerLogin extends Component{
  constructor(props){
    super(props);
    this.state = {
      email:'',
      password:'',
      redirect: false
    }
    this.formSubmit = this.formSubmit.bind(this);
  }

  formSubmit = async (e) =>{
    e.preventDefault();
    const payload = {
      email:this.state.email,
      password:this.state.password,
    }
    const headers = {
      'content-type': 'application/json',
    }
    let json = await PostFetch('cust/login/', payload, headers);
    if(json && json.token){
      localStorage.setItem("customerDetails", JSON.stringify(json));
      localStorage.setItem("token", json.token);
      localStorage.setItem("userType", "customer");
      this.setState({
        email:'',
        password:'',
        redirect: true,
      });

    }else{
      errorAlert(json && json.message? json.message: 'try again')
    }
  }

  componentWillMount(){
    let x = localStorage.getItem("token");
    let y = localStorage.getItem("customerDetails");
    let z = localStorage.getItem("userType");
    if(x && y && z && z === "customer"){
      localStorage.removeItem("userDetails");
      this.setState({
        redirect: true,
      })
    }else if (x && y && z && z !== "customer") {
      localStorage.removeItem("customerDetails");
    }
    window.history.pushState('', 'login', `/customer/login`);
  }

  render(){
    if(this.state.redirect){
      return(
        <Redirect to="/customerdashboard/all" />
      )
    }
    const state = store.getState();
    return(
      <article>
        <section className="sectionColumns">
          <SideImg msg={''}/>
        <div className="rightColumn">
                  <div className="content-rightColumn">
                      <h5 className="mb-4">Login to your account</h5>
                      <div className="boxComman">
                          <form action="" onSubmit={this.formSubmit} method="POST">
                              <div className="form-group">
                                  <label>Email address</label>
                                  <input type="email" value={this.state.email} onChange={(e)=>{
                                    this.setState({email:e.target.value})
                                  }} placeholder="Enter email address" required className="form-control"/>
                              </div>
                              <div className="form-group">
                                  <label>Password</label>
                                  <PasswordMask
                                    inputClassName={"form-control"}
                                    buttonClassName={"btnEye"}
                                    className={"inputPassword"}
                                    placeholder="Enter password"
                                    value={this.state.password}
                                    onChange={(e)=>{
                                      this.setState({password:e.target.value})
                                    }}
                                    onBlur={(e)=>{
                                      if(!verfyPassword(e.target.value)){
                                        this.setState({password:''})
                                        infoAlert('Password must contain:- minimum 8 character, at least one capital letter, at least one small letter, at least one number and one special symbol. ')
                                      }
                                    }}
                                    required
                                    showButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                    hideButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                     useVendorStyles={false}
                                  />
                              </div>
                              <div className="row align-items-center">
                                  <div className="col-md-6">
                                      <input type="submit" title="Login" value="Login"/>
                                  </div>
                                  <div className="col-md-6 text-right">
                                      <p className="mb-0"><NavLink to="/customer/forgot-password" title="Forgot Password?" className="font-weight-medium"> Forgot Password?</NavLink> </p>
                                  </div>
                              </div>
                          </form>
                      </div>
                      <div className="boxComman p-4 text-center font-weight-medium">
                          <p className="mb-0">Don't have an account? <NavLink to="/customer/signup2">Sign up</NavLink></p>
                      </div>
                  </div>
              </div>
          </section>
      </article>
    )
  }
}
export default CustomerLogin;
