import React, {Component} from 'react';
import SideImg from './include/side-img';
import PostFetch from '../component/ajax/postFetch';
import {Redirect } from 'react-router';
import PasswordMask from 'react-password-mask';
import store from '../store';
import {successAlert, errorAlert} from '../component/include/alert';
import APIFetch from '../component/ajax/apiFetch';
import {validatePassword, verifyEmail, verifyNumber} from '../component/include/validation';
import {NavLink} from 'react-router-dom';

class CustomerSignupNew extends Component{
  constructor(props){
    super(props);
    this.state = {
    	email:'',
    	user_name: '',
    	phone: '',
      phoneError: "",
    	password: '',
    	cust_com_name: '',
    	address1: '',
    	pin: '',
    	country: '',
    	carr_com_id: '',
      redirect: false,
      carr_com_name: '',
      appdata: '',
      data: [],
      secondary_contact: '',
  		sec_email_id: '',
  		sec_phone: '',
  		business_address: '',
  		contract_start_date: '',
  		contract_end_date: '',
  		Customer_code: '',

      error_email: '',
      error_name: '',
      error_mobile_num: '',
      error_business_address: '',
      strength: '',
      color: ''
    }
    this.formSubmit = this.formSubmit.bind(this);
  }
  componentWillMount(){
    const state = store.getState();
    this.setState({
      appdata: state.carrier.appdata
    })
  }
  getSuggestions = async (text)=>{
    const data = await APIFetch('https://autocomplete.geocoder.api.here.com/6.2/suggest.json?'+this.state.appdata+'&query='+text+'&country=aus');
    let options = [];
    if(data.suggestions){
       data.suggestions.map((s,i)=>{
        options.push({
          value:s.label,
          label:s.label,
        })
        return('')
      });
    }
    this.setState({
      data:options
    });
  }

  changeCountry = async (pin) =>{
      if(this.state.address1){
        let url = 'https://geocoder.api.here.com/6.2/geocode.json?searchtext='+this.state.address1+'&'+this.state.appdata;
        const data = await APIFetch(url);
        if(data && data.Response && data.Response.View[0] && data.Response.View[0].Result[0] && data.Response.View[0].Result[0].Location){
          let pos = data.Response.View[0].Result[0].Location;
          this.setState({
            pin:pos.Address.PostalCode,
            country:pos.Address.Country,
          });
        }
      }
  }

  formSubmit = async (e) =>{
    e.preventDefault();
    if(this.state.phoneError==='' && this.state.error_email===''){
      const payload = {
      	cust_email: this.state.email,
    		password: this.state.password,
      	user_name: this.state.user_name,
      	cust_mobile: this.state.phone,
      	company_name: this.state.cust_com_name,
        address1: this.state.address1,
      	address2: this.state.address1,
      	pin: this.state.pin,
      	country: this.state.country,
        extra_details:{
          name: this.state.name,
      		mobile_num: this.state.mobile_num,
      		secondary_contact: this.state.secondary_contact,
      		sec_email_id: this.state.sec_email_id,
      		sec_phone: this.state.sec_phone,
      		business_address: this.state.business_address,
      		contract_start_date: this.state.contract_start_date,
      		contract_end_date: this.state.contract_end_date,
      		Customer_code: this.state.Customer_code,
        }
      }
      const headers = {
        'content-type': 'application/json',
      }
      let json = await PostFetch('cust/signup/', payload, headers);
    if(json!==null && json.message){
      successAlert(json.message);
      this.setState({
        email:'',
      	user_name: '',
      	phone: '',
        phoneError: "",
      	password: '',
      	cust_com_name: '',
      	address1: '',
      	pin: '',
      	country: '',
      	carr_com_id: '',
        redirect: true,
        carr_com_name: '',
        secondary_contact: '',
    		sec_email_id: '',
    		sec_phone: '',
    		business_address: '',
    		contract_start_date: '',
    		contract_end_date: '',
    		Customer_code: '',
        error_email: '',
        error_name: '',
        error_mobile_num: '',
        error_business_address: '',
      });
    }else{
      errorAlert('try again!')
    }
  }
}

  render(){
    if(this.state.redirect){
      return(
        <Redirect to="/customer/login"/>
      )
    }
    const state = store.getState();
    return(
      <article>
          <section className="sectionColumns">
              <SideImg  msg={this.state.carr_com_name}/>
              <div className="rightColumn">
                  <div className="content-rightColumn">
                      <h5 className="mb-4">Sign up for your account</h5>
                      <div className="boxComman">
                          <form action="" onSubmit={this.formSubmit} method="POST">
                              <div className="form-group">
                                  <label>Name <span className="text-danger">*</span></label>
                                  <input type="text" value={this.state.user_name} onChange={(e)=>{
                                    this.setState({user_name:e.target.value})
                                  }} placeholder="enter name" required className="form-control"/>
                              </div>
                              <div className="form-group">
                                  <label>Email address <span className="text-danger">*</span></label>
                                  <input type="email" value={this.state.email} onChange={(e)=>{
                                    this.setState({email:e.target.value})
                                  }} onBlur={ async (e)=>{
                                    let data = await verifyEmail(e.target.value);
                                    if(data){
                                      if(data.error!==''){
                                        this.setState({
                                          error_email: data.error
                                        })
                                      }else if(data.data){
                                        this.setState({
                                          error_email: 'Already registered with '+data.data
                                        })
                                      }else if(data.data==='' && data.error===''){
                                        this.setState({
                                          error_email: ''
                                        })
                                      }
                                    }
                                  }} required className="form-control" placeholder="Enter Email"/>
                                  <span className="text-danger">{this.state.error_email}</span>
                              </div>
                              <div className="form-group">
                                  <label>Company Name <span className="text-danger">*</span></label>
                                  <input type="text" value={this.state.cust_com_name} onChange={(e)=>{
                                    this.setState({cust_com_name:e.target.value})
                                  }} required className="form-control" placeholder="Enter Company Name"/>
                              </div>
                              <div className="form-group">
                                  <label>Phone Number <span className="text-danger">*</span></label>
                                  <input type="number" value={this.state.phone} onChange={(e)=>{
                                    this.setState({phone:e.target.value})
                                  }} onBlur={ async (e)=>{
                                    let json = await verifyNumber(e.target.value);
                                    console.log(json);
                                    this.setState({
                                      phoneError: json && json.error? json.error: '',
                                    });
                                  }} required className="form-control" placeholder="Enter Phone Number"/>
                                  <span className="text-danger">{this.state.phoneError}</span>
                              </div>

                              <div className="form-group">
                                  <label>Password <span className="text-danger">*</span></label>
                                  <PasswordMask
                                    inputClassName={"form-control"}
                                    buttonClassName={"btnEye"}
                                    className={"inputPassword"}
                                    placeholder="Enter password"
                                    value={this.state.password}
                                    onChange={(e)=>{
                                      let data = validatePassword(e.target.value);
                                      this.setState({
                                        password:e.target.value,
                                        strength: data.strength,
                                        color: data.color
                                      });
                                    }}
                                    required
                                    showButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                    hideButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                     useVendorStyles={false}
                                  />
                                  <p style={{marginTop: '5px'}}><strong className={this.state.color}>{this.state.strength}</strong></p>
                              </div>
                              <div className="row align-items-center">
                                  <div className="col-md-5">
                                      <input type="submit" title="Sign Up" value="Sign Up" />
                                  </div>
                                  <div className="col-md-7 text-right">
                                      <p className="mb-0 font-weight-medium f12">By signing up, you agree to our <a href="https://kargologic.com/terms-of-use" target="_blank" rel="noopener noreferrer">T&C</a> and <a href="https://kargologic.com/privacy-policy" target="_blank" rel="noopener noreferrer" title="Privacy Policy">Privacy Policy.</a></p>
                                  </div>
                              </div>
                          </form>
                      </div>
                      <div className="boxComman p-4 text-center font-weight-medium">
                          <p className="mb-0">Already have an account? <NavLink to="/customer/login">Login</NavLink></p>
                      </div>
                  </div>
              </div>
          </section>
      </article>
    )
  }
}
export default CustomerSignupNew;

/*
<div className="form-group">
    <label>Address 1</label>
    <input type="text" value={this.state.address1} onChange={(e)=>{
      this.getSuggestions(e.target.value);
      this.setState({address1:e.target.value})
    }} required list="datalist" onBlur={this.changeCountry} className="form-control" placeholder="Enter Address 1"/>
    <datalist id="datalist">
      <option value="">select</option>
      {this.state.data.length>0?
      this.state.data.map((e, i)=>{
        return(
          <option key={i} value={e.value}>{e.label}</option>
        )
      }):null}
    </datalist>
</div>
<div className="form-group">
    <label>Postal Code</label>
    <input type="text" value={this.state.pin} onChange={(e)=>{
      this.setState({pin:e.target.value})
    }} required className="form-control" placeholder="Enter Postal Code"/>
</div>
<div className="form-group">
    <label>Country</label>
    <input type="text" value={this.state.country} onChange={(e)=>{
      this.setState({country:e.target.value})
    }} required className="form-control" placeholder="Enter Country Name"/>
</div>
*/
