import React, {Component} from 'react';
import Header from './include/header';
import MainSidebar from './include/mainSidebar';
import SettingSidebar from './include/settingSidebar';
import Path from './include/path';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {setConfigure, getConfigure, clearConfig} from '../actions/customerActions';
import {successAlert, infoAlert} from '../component/include/alert';

const columns = ["Job Number", "Created Date", "Carrier Name", "Pick Up Time From", "Pick Up Time To", "Delivery Time From", "Delivery Time To", "Status", "Created By", "Customer Code", "Price ($)", "Load Carried", "Load Type", "Load Weight", "Measure Unit", "Units"];

class CustomerConfigure extends Component{
  constructor(props){
    super(props);
  //  let carrier = JSON.parse(localStorage.getItem("userDetails"));
    this.state = {
      timeslot: '',
      orders_table: [],
      driver: [],
      timeline: [],
      userConfiguration: {},
      deliveryVerify: [],

      timeslot_ch: false,
      orders_table_ch: false,
    }
    this.submitForm = this.submitForm.bind(this);
  }
  componentWillMount(){
    this.props.getConfigure('companyconfig/getconfig/');
  }
  submitForm = (e) =>{
    e.preventDefault();
    const payload = [
      {
      	label_name: "time_slot",
      	label_val: this.state.timeslot
      },
      {
        label_name: "orders_table",
        label_val: this.state.orders_table.join(', ')
      }
      // timeslot: this.state.timeslot,
      // orders_table: this.state.orders_table,
      // driver: this.state.driver,
      // timeline: this.state.timeline,
    ];
    console.log(payload);
    this.props.setConfigure('companyconfig/createconfig/', payload);
    this.props.getConfigure('companyconfig/getconfig/');
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.userConfiguration){
      this.setState({
        userConfiguration: nextProps.userConfiguration
      })
        try{
          let time_slot = 0;
          let orders_table = '';
          let final_col = [];
          nextProps.userConfiguration.data.map((e, i)=>{
            if(e.ConfigureID.label === 'time_slot'){
              time_slot = parseInt(e.configure_val)
            }
            if(e.ConfigureID.label === 'orders_table'){
              orders_table = e.configure_val.split(', ')
              orders_table = orders_table.filter((e, i)=>{
                return e !== "Customer Name";
              })
              final_col = orders_table.filter((e, i)=>{
                return columns.includes(e)
              })
            }
            return('')
          })
          this.setState({
            timeslot: time_slot,
            orders_table: final_col
          })
        }catch(e){
        }
      localStorage.setItem("userConfiguration", JSON.stringify(nextProps.userConfiguration));
    }
    if(nextProps.configMsg && nextProps.configMsg.message && nextProps.configMsg.message === "created"){
      successAlert('Success')
      this.props.clearConfig();
      this.props.getConfigure('companyconfig/getconfig/');
    }
  }
  putOrderList =(e)=>{
    let orders_table = this.state.orders_table;
    if(!orders_table){
      orders_table = [];
    }
    if((orders_table && orders_table.length>0) || orders_table.length === 0){
      if(e.target.checked){
        if(!orders_table.includes(e.target.value)){
          if(orders_table.length<8){
            orders_table.push(e.target.value)
          }else {
            infoAlert('maximum 8 columns can be selected!')
          }
        }
      }else{
        if(orders_table.indexOf(e.target.value)>-1){
          orders_table.splice(orders_table.indexOf(e.target.value), 1)
        }
      }
      this.setState({
        orders_table: orders_table,
        orders_table_ch: true
      })
    }
  }
  render(){
    return(
      <article>
        <div className="dashboardMain">
        <Header/>
            <div className="dashboardContent">
                <MainSidebar/>
                <div className="rightColumn">
                    <SettingSidebar/>
                    <div className="rightColumnContent">
                      <Path path={[
                                    {
                                      "path": "Manage your account",
                                      "url": "/settings"
                                    },
                                    {
                                      "path": "Configure",
                                      "url": "/configure"
                                    }
                                  ]} />
                      <div className="boxContentInner">
                          <div className="contentAccountSetting">
                            <form method="post" onSubmit={this.submitForm}>
                                <div className="row">
                                <div className="col-md-5">
                                  <div className="form-group">
                                    <label>Dashboard Order List</label>
                                    <br/>
                                    <div>
                                    {columns.map((e, i)=>(
                                      <div key={i}>
                                      <input
                                        type="checkbox"
                                        onChange={this.putOrderList}
                                        checked={this.state.orders_table.includes(e)? 'checked': ''}
                                        value={e}
                                      /> {e}
                                      </div>
                                    ))}
                                    </div>
                                    <span className="text-danger">{this.state.orders_table_ch? 'Modified': ''}</span>
                                  </div>
                                </div>
                                <div className="col-md-5">
                                  <div className="form-group">
                                      <label>Time Slot (in minutes)</label>
                                      <input type="number" onChange={(e)=>{
                                        this.setState({
                                          timeslot: e.target.value,
                                          timeslot_ch: true
                                        })
                                      }} value={this.state.timeslot} placeholder="Enter time slot in minutes" className="form-control" />
                                      <span className="text-danger">{this.state.timeslot_ch? 'Modified': ''}</span>
                                  </div>
                                </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <input type="submit" className="btn" value="Save Changes"/>
                                    </div>
                                </div>
                            </form>
                          </div>
                      </div>
                    </div>
              </div>
            </div>
        </div>
      </article>

    )
  }
}

CustomerConfigure.propTypes = {
  setConfigure: PropTypes.func.isRequired,
  getConfigure: PropTypes.func.isRequired,
  clearConfig: PropTypes.func.isRequired,
//  userpassword: PropTypes.array.isRequired,
}

const mapStateToProps = state =>({
  userConfiguration: state.customer.userConfiguration,
  configMsg: state.customer.configMsg
});

export default connect(mapStateToProps, {setConfigure, getConfigure, clearConfig})(CustomerConfigure);


/*  <form method="post" onSubmit={this.submitForm}>
      <div className="row">

          <div className="col-md-5">
            <div className="form-group">
              <label>Driver Order List</label>
              <select multiple onChange={(e)=>{
                let options = e.target.options;
                let value = [];
                for (let i = 0, l = options.length; i < l; i++) {
                  if (options[i].selected) {
                    value.push(options[i].value);
                  }
                }
                this.setState({
                  driver: value
                })
              }} className="form-control">
                <option value="">Select</option>
                <option value="Driver Name">Driver Name</option>
                <option value="Vehicle Type">Vehicle Type</option>
                <option value="Vehicle Rego">Vehicle Rego</option>
                <option value="Driver License No.">Driver License No.</option>
                <option value="Phone Number">Phone Number</option>
                <option value="Delivery On Time">Delivery On Time</option>
                <option value="Status">Status</option>
              </select>
            </div>
          </div>
          <div className="col-md-5">
            <div className="form-group">
              <label>Order Status</label>
              <select multiple onChange={(e)=>{
                let options = e.target.options;
                let value = [];
                for (let i = 0, l = options.length; i < l; i++) {
                  if (options[i].selected) {
                    value.push(options[i].value);
                  }
                }
                this.setState({
                  timeline: value
                })
              }} className="form-control">
                <option value="">Select</option>
                <option value="Driver Name">Driver Name</option>
                <option value="Address">Address</option>
                <option value="Quantity">Quantity</option>
                <option value="Schedule Time">Schedule Time</option>
                <option value="ETA/ ON">ETA/ ON</option>
              </select>
            </div>
          </div>
          <div className="col-md-5">
              <div className="form-group">
                  <label>Time Slice</label>
                  <input type="number" onChange={(e)=>{
                    this.setState({
                      timeslot: parseInt(e.targer.value)
                    })
                  }} value={this.state.timeslot} required placeholder="Enter time slice in minutes" className="form-control" />
              </div>
          </div>
      </div>

  </form>*/
