import React, {Component} from 'react';
import SideImg from './include/side-img';
import PostFetch from '../component/ajax/postFetch';
import PasswordMask from 'react-password-mask';
import store from '../store';
import {successAlert, errorAlert, infoAlert} from '../component/include/alert';
import {validatePassword, verfyPassword} from '../component/include/validation';

class CustomerRecoverPassword extends Component{
  constructor(props){
    super(props);
    this.state = {
      email: '',
      token: '',
      newpassword: '',
      confirmpassword: '',
      newStrength: '',
      newColor: '',
      conStrength: '',
      conColor: '',
    }
  }
  componentWillMount(){
    this.setState({
      email: this.getQueryVariable('email'),
      token: this.getQueryVariable('token'),
    })
  }
  getQueryVariable = (variable)=>{
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) === variable) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query variable %s not found', variable);
}
  formSubmit = async (e)=>{
    e.preventDefault();
    if(this.state.newpassword === this.state.confirmpassword){
      let payload = {
        email: this.state.email,
        token: this.state.token,
        password: this.state.newpassword
      }
      const headers = {
        'content-type': 'application/json',
      }
      let json = await PostFetch('cust/cust_reset_pass/', payload, headers);
      if(json && json.message==="success"){
        this.setState({
          email: '',
          token: '',
          newpassword: '',
          confirmpassword: '',
        })
        successAlert('your password has been changed')
        setTimeout(function() {
          window.location.href = "/customer/login"
        },2000);
      }else {
        errorAlert('try again!')
      }
    }else{
      errorAlert('password not matched!')
    }
  }
  render(){
    const state = store.getState();
    return(
      <article>
        <section className="sectionColumns">
          <SideImg msg={''}/>
        <div className="rightColumn">
                  <div className="content-rightColumn">
                      <h5 className="mb-4">Password Recovery</h5>
                      <div className="boxComman">
                          <form action="" onSubmit={this.formSubmit} method="POST">
                              <div className="form-group">
                                  <label>Email Address</label>
                                  <input type="email" readOnly value={this.state.email} onChange={(e)=>{
                                    this.setState({email:e.target.value})
                                  }} placeholder="Enter New Password" required className="form-control"/>
                              </div>
                              <div className="form-group">
                                  <label>New Password</label>
                                  <PasswordMask
                                    inputClassName={"form-control"}
                                    buttonClassName={"btnEye"}
                                    className={"inputPassword"}
                                    placeholder="Enter New Password"
                                    value={this.state.newpassword}
                                    onChange={(e)=>{
                                      let data = validatePassword(e.target.value);
                                      this.setState({
                                        newpassword:e.target.value,
                                        newStrength: data.strength,
                                        newColor: data.color
                                      });
                                    }}
                                    onBlur={(e)=>{
                                      if(!verfyPassword(e.target.value)){
                                        this.setState({newpassword:''})
                                        infoAlert('Password must contain:- minimum 8 character, at least one capital letter, at least one small letter, at least one number and one special symbol. ')
                                      }
                                    }}
                                    required
                                    showButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                    hideButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                     useVendorStyles={false}
                                  />

                              </div>
                              <div className="form-group">
                                  <label>Comfirm Password</label>
                                  <PasswordMask
                                    inputClassName={"form-control"}
                                    buttonClassName={"btnEye"}
                                    className={"inputPassword"}
                                    placeholder="Enter Comfirm Password"
                                    value={this.state.confirmpassword}
                                    onChange={(e)=>{
                                      let data = validatePassword(e.target.value);
                                      this.setState({
                                        confirmpassword:e.target.value,
                                        conStrength: data.strength,
                                        conColor: data.color
                                      });
                                    }}
                                    onBlur={(e)=>{
                                      if(!verfyPassword(e.target.value)){
                                        this.setState({confirmpassword:''})
                                        infoAlert('Password must contain:- minimum 8 character, at least one capital letter, at least one small letter, at least one number and one special symbol. ')
                                      }
                                    }}
                                    required
                                    showButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                    hideButtonContent={<img src={`${state.carrier.localUrl}assets/img/iconEye.png`} alt="" title=""/>}
                                     useVendorStyles={false}
                                  />
                                  <p style={{marginTop: '5px'}}><strong className={this.state.conColor}>{this.state.conStrength}</strong></p>
                              </div>
                              <div className="row align-items-center">
                                  <div className="col-md-6">
                                      <input type="submit" title="Login" value="Reset Password"/>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </section>
      </article>
    )
  }
}
export default CustomerRecoverPassword;
