import React, {Component} from 'react';
import store from '../../store';
import {ExcelRenderer} from 'react-excel-renderer';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {createNewOrderCsv} from '../../actions/customerActions';
//import readXlsxFile from 'read-excel-file';
import FileDrop from 'react-file-drop';
import upload from '../../assets/img/iconUpload.png';
import moment from 'moment';
import {getCurrentTS} from '../../component/include/date';

class Model extends Component{
  constructor(props){
    super(props);
    this.state = {
      file: '',
      error: '',
      errorClass: 'text-danger',
      flg: 0,
      carr: JSON.parse(localStorage.getItem("customerDetails")),
    }
  }

  handleDrop = (files, event) => {
    if(files.length>0){
      this.setState({
        file: files[0],
      })
    }
  }

  uploadExcel = ()=>{
    if(this.state.file){
      this.getRows(this.state.file);
    }
  }

  getRows = (file)=>{
    this.setState({
      error: 'File is reading please wait...',
      errorClass: 'text-warning',
    })
    let ext = file.name.split('.');
    if(ext[ext.length-1]==="xlsx" || ext[ext.length-1]==="xls"){
      ExcelRenderer(file, (err, resp) => {
        let data = this.getJson(resp.rows);
        if(this.state.flg===0 && data.length>0){
          this.setState({
            file: '',
            error: '',
            flg: 0,
          });
          this.props.createNewOrderCsv(data);
          this.props.closeModel();
        }else{
          this.setState({
            file: '',
            error: 'invalid file or file has no data',
            errorClass: 'text-danger',
          })
        }
      })
    }else{
      this.setState({
        file: '',
        error: 'invalid file, only excel file is accepted',
        errorClass: 'text-danger',
      })
    }
  }

  getJson = (rows) =>{
    this.setState({
      flg: 0
    })
    let data = [];
    let arr = {};
    let extra = [];
    let x = 0;
    let fields = [];
    let template = [];
    let pickup = [];
    let delivery = [];
    let count = 1;
    let totalFields = 28;

    if(totalFields!==rows[0].length){
      this.setState({
        flg: 1
      })
      return 0;
    }
    rows.map((d,i)=>{

      if(d[1]!==null || i===rows.length-1){
        if(i>=1){
          fields.push(count);
          count=1;
        }
      }else{count++;}
      return(
        ''
      )
    });
    rows.map((d,i)=>{
      if(i>0){
        x++;
        if(d[1] || d[2] || d[3] || d[4] || d[5] || d[6] || d[7] || d[8]){
          template = {
            template_name:""
          };
          pickup = [];
          delivery = [];
          extra = [];
          let customer = '';
          try{
            customer = d[2].split(":");
          }catch(e){
            this.setState({
              flg: 1
            })
          }
          let pickup_date_time = this.newDate(d[5], d[6]);
          let pickup_date_time_to = this.newDate(d[7], d[8]);
          let delivery_date_time = this.newDate(d[10], d[11]);
          let delivery_date_time_to = this.newDate(d[12], d[13]);
          let order_create_time = getCurrentTS();
          pickup.push({
            sender: {},
            address: d[4],
            longitude: "",
            latitude: "",
            pickup_date_time: pickup_date_time,
            pickup_date_time_to: pickup_date_time_to,
          });

          delivery.push({
            receiver: {},
            address: d[9],
            longitude: "",
            latitude: "",
            delivery_date_time: delivery_date_time,
            delivery_date_time_to: delivery_date_time_to,
          });

          arr = {
            job_id: '',
            customer_code: d[1],
            CarrierCompanyID:customer[0],
            Carrier_Name: customer[1],
            price: d[3],
            pickup_date_time: pickup_date_time,
            pickup_date_time_to: pickup_date_time_to,
            delivery_date_time: delivery_date_time,
            delivery_date_time_to: delivery_date_time_to,
            load_carried: d[14],
            load_type: d[15],
            load_weight: d[16],
            length_load_dimen: d[17]? d[17]: 0,
            width_load_dimen: d[18]? d[18]: 0,
            height_load_dimen: d[19]? d[19]: 0,
            measure_unit: d[20],
            units: d[21],
            hazardous: d[22],
            additional_note: d[23],
            is_active: "True",
            order_create_time: order_create_time,
            sender_name: d[24],
            receiver_name: d[25],
            create: true
          };
        }
        if(d[26]){
          extra.push({
            field_name: d[26],
            field_value: d[27],
          })
        }
        if(x===fields[data.length] || i===rows.length-1){
          data.push({
            order_detail:arr,
            template:template,
            pickup:pickup,
            delivery:delivery,
            extra_detail:extra,
            attachment: []
          });
          x=0;
        }
      }
      return (
        ''
      )
    });
    return data;
  }

  newDate = (dateOld, serial)=>{
    try{
     //let utc_days  = Math.floor(serial - 25569);
     //let utc_value = utc_days * 86400;
     //let date_info = moment(utc_value * 1000);
     let fractional_day = serial - Math.floor(serial) + 0.0000001;
     let total_seconds = Math.floor(86400 * fractional_day);
     let seconds = total_seconds % 60;
     total_seconds -= seconds;
     let hours = Math.floor(total_seconds / (60 * 60));
     let minutes = Math.floor(total_seconds / 60) % 60;
//     let final = dateOld.getFullYear()+"-"+(dateOld.getMonth()+1)+"-"+dateOld.getDate()+" "+hours+":"+minutes+":"+seconds;
    let date =  (dateOld- (25567 + 2))*86400*1000;
    date= moment(date).utc();
    date.add(hours,'hours').add(minutes,'minutes')
    return date
   }catch(e){
     return null
   }
}

  render(){
    const state = store.getState();
    return(
    <div className="fixedAssignDriver">
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-body">
            <p style={{"margin":"0"}} className="text-right">
              <a href="#a" onClick={(e)=>{
                e.preventDefault();
                this.props.closeModelX()
              }}>
                <span aria-hidden="true">&times;</span>
              </a>
            </p>
            <div className="row rowAssign align-items-center">
              <div className="col-md-12">
                <h4 className="pb-0 mb-0">Upload Excel</h4>
              </div>
            </div>
            <div className="mainDragDrop">
              <FileDrop className="boxDragDrop" onDrop={this.handleDrop}>
                    <div className="boxInput">
                        <div className="boxIcon"><img src={upload} alt="" title="" /></div>
                        <input type="file" onChange={(e)=>{
                          if(e.target.files.length>0){
                            this.setState({
                              file: e.target.files[0],
                              error: '',
                            })
                          }
                        }} id="file" className="boxFile" />
                        <label htmlFor="file">
                          {this.state.file?
                            <React.Fragment>{this.state.file.name}</React.Fragment>:
                            <React.Fragment>
                              Drag and drop to upload
                              <span>or
                                <strong> browse </strong>
                                to choose a file.
                              </span>
                            </React.Fragment>}
                        </label>
                        {this.state.file?
                        <a href="#upload" onClick={(e)=>{
                          e.preventDefault();
                          this.uploadExcel()
                        }} className="boxButton btn btn-sm">Upload Files</a>:
                        <a href="#upload" className="boxButton btn btn-sm disabled">Upload Files</a>}
                        {!this.state.file?
                          <p className="mb-0"><a href={`${state.carrier.mainUrl}cust/sample_download/?cust_com_id=${this.state.carr.cust_com_id}`} download>Click here</a> to download sample Excel Format</p>
                        :null}
                    </div>
                    <div className={this.state.errorClass}><span></span> {this.state.error}</div>
                </FileDrop>
            </div>
          </div>
        </div>
      </div>
    </div>
    )
  }
}
Model.propTypes = {
  createNewOrderCsv: PropTypes.func.isRequired,
  //newOrder: PropTypes.array.isRequired,
}

const mapStateToProps = state =>({
  newOrder: state.customer.newOrder,
});

export default connect(mapStateToProps, {createNewOrderCsv})(Model);
