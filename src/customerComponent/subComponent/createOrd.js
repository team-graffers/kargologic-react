import React, {Component} from 'react';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getSenderReceiver, pushSenderReceiver, modifyNewOrderCsv, getCarrierList} from '../../actions/customerActions';
import APIFetch from '../../component/ajax/apiFetch';
import AddExtraField from '../../component/subComponent/addExtraField';
import Datetime from 'react-datetime';
import {checkDate} from '../../component/include/date';
import UploadAttachments from '../../component/subComponent/uploadAttachmentsCreate';
import makeid from '../../component/subComponent/makeid';
import Map from '../../component/subComponent/maps';
import moment from 'moment';
import store from '../../store';
import AddSender from '../../component/subComponent/addSender';
import AddReceiver from '../../component/subComponent/addReceiver';
import {GetJobId} from '../../component/include/jobId';
import {infoAlert} from '../../component/include/alert';
import {charValidation} from '../../component/include/validation';
import { Prompt } from 'react-router'

class CreateOrd extends Component{
  constructor(props){
    super(props);
    let customer = JSON.parse(localStorage.getItem("customerDetails"));
    this.state = {
      Customer:customer.cust_com_name,
      job_id:'',
      order_id: '',
      customer_code:'',
      Carrier_Name:'',
      CarrierCompanyID:'',
      price:'',
      pickup_date_time:'',
      pickup_date_time_to:'',
      pickup_location:'',
      delivery_date_time:'',
      delivery_date_time_to:'',
      delivery_location:'',
      load_carried:'',
      load_type:'',
      load_weight:'',
      length_load_dimen:'',
      width_load_dimen: '',
      height_load_dimen: '',
      measure_unit: '',
      units:'',
      additional_note:'',
      additional_note_count: 500,
      is_active: false,
      hazardous: false,
      extra_detail:[],
      model_class:'modal fade',
      model_style:'none',
      appdata: '',
      data: '',
      checkPickAddress:'',
      checkDropAddress:'',
      attachments: [],
      showMap: false,
      map_lat: '',
      map_lng: '',
      map_address: '',
      Created_by_CarrierID: '',
      datalist: makeid(),
      datalist1: makeid(),
      sender: '',
      receiver: '',
      senderReceiver: [],
      promptStatus: false,
      job_id_req: false,
      sender_error: '',
      receiver_error: '',
      job_id_error:'',
      customer_code_error:'',
      Carrier_Name_error:'',
      price_error:'',
      pickup_date_time_error:'',
      pickup_date_time_to_error:'',
      pickup_location_error:'',
      delivery_date_time_error:'',
      delivery_date_time_to_error:'',
      delivery_location_error:'',
      load_carried_error:'',
      load_type_error:'',
      load_weight_error:'',
      length_load_dimen_error:'',
      width_load_dimen_error: '',
      height_load_dimen_error: '',
      measure_unit_error: '',
      units_error:'',
      addSenderModel: false,
      addReceiverModel: false,
      timeslot: 30,
    }
    this.getLocation = this.getLocation.bind(this);
    this.getAttachments = this.getAttachments.bind(this);
  }

  closeModel = (e) =>{
    this.setState({
      model_class: 'modal fade',
      model_style: 'none'
    });
  }
  openSenderModel=()=>{
    this.setState({
      addSenderModel: true,
    })
  }
  closeSenderModel=()=>{
    this.setState({
      addSenderModel: false,
    })
  }
  openReceiverModel=()=>{
    this.setState({
      addReceiverModel: true,
    })
  }
  closeReceiverModel=()=>{
    this.setState({
      addReceiverModel: false,
    })
  }
  getJob = async ()=>{
    const val = await GetJobId('customer');
    if(val){
      let new_id = val+this.props.order_key;
      this.setState({
        job_id: new_id,
        job_id_req: true
      })
      this.modifyOrder("order_detail", "job_id", new_id);
    }else{
      this.setState({
        job_id: 1+this.props.order_key,
        job_id_req: true
      })
      this.modifyOrder("order_detail", "job_id", 1+this.props.order_key);
    }
  }
  submitIndData = ()=>{
    if (this.state.job_id && this.state.Carrier_Name && this.state.CarrierCompanyID && this.state.pickup_date_time && this.state.pickup_date_time_to && this.state.pickup_location && this.state.delivery_date_time && this.state.delivery_date_time_to && this.state.delivery_location) {
      this.props.submitInd(this.props.order_key);
    }else {
      infoAlert('fields are blank!')
    }
  }
  componentWillReceiveProps(nextProps){
    if(nextProps.senderReceiver && nextProps.senderReceiver.data && nextProps.senderReceiver.data.length>0){
      this.setState({
        senderReceiver: nextProps.senderReceiver.data
      })
      if(this.props.order && this.props.order.pickup && this.props.order.pickup[0] && this.props.order.pickup[0].sender && this.props.order.pickup[0].sender.name){
        for(let i = 0;i<nextProps.senderReceiver.data.length; i++){
          if(nextProps.senderReceiver.data[i].id === this.props.order.pickup[0].sender.id){
            this.setState({
              sender: i
            })
            break;
          }
        }
      }
      if(this.props.order && this.props.order.delivery && this.props.order.delivery[0] && this.props.order.delivery[0].receiver && this.props.order.delivery[0].receiver.name){
        for(let i = 0;i<nextProps.senderReceiver.data.length; i++){
          if(nextProps.senderReceiver.data[i].id === this.props.order.delivery[0].receiver.id){
            this.setState({
              receiver: i
            })
            break;
          }
        }
      }
    }
  }
  closeOneModel=(modalId)=>{
    const modal = document.getElementById(modalId);
    modal.classList.remove('show');
    modal.setAttribute('aria-hidden', 'true');
    modal.setAttribute('style', 'display: none');
    try{
      const modalBackdrops = document.getElementsByClassName('modal-backdrop');
      document.body.removeChild(modalBackdrops[0]);
    }catch(e){

    }
  }
  componentWillMount(){
    try{
      let x = localStorage.getItem("userConfiguration");
      x = JSON.parse(x);
      let time_slot = 0;
      x.data.map((e, i)=>{
        if(e.ConfigureID.label === 'time_slot'){
          time_slot = parseInt(e.configure_val)
        }
        return('')
      })
      this.setState({
        timeslot: time_slot
      })
    }catch(e){
    }
    if(!this.props.editOrder){
      this.getJob();
    }
    const state = store.getState();
    this.setState({
      appdata: state.carrier.appdata
    })
  //  this.props.getCarrierList();
    if(this.props.order){
      let order_detail = this.props.order.order_detail;
      let pickup = this.props.order.pickup && this.props.order.pickup[0]? this.props.order.pickup[0]: '';
      let delivery = this.props.order.delivery && this.props.order.delivery[0]? this.props.order.delivery[0]: '';
      let extra_detail = this.props.order.extra_detail;
      if(order_detail && order_detail.hazardous && order_detail.hazardous){
        order_detail.hazardous = true;
      }else if(order_detail && order_detail.hazardous){
        order_detail.hazardous = false;
      }
      if(order_detail){
        this.props.getSenderReceiver('orders/comsendreceive/');
        this.setState({
          promptStatus: true,
          Created_by_CarrierID: order_detail && order_detail.Created_by_CarrierID? order_detail.Created_by_CarrierID : null,
          order_id:order_detail.order_id,
          job_id:order_detail.job_id,
          customer_code:order_detail.customer_code,
          Carrier_Name:order_detail.Carrier_Name,
          CarrierCompanyID:order_detail.CarrierCompanyID,
          price:order_detail.price,
          pickup_date_time:pickup.pickup_date_time,
          pickup_date_time_to:pickup.pickup_date_time_to,
          pickup_location:pickup.address,
          delivery_date_time:delivery.delivery_date_time,
          delivery_date_time_to:delivery.delivery_date_time_to,
          delivery_location:delivery.address,
          load_carried:order_detail.load_carried,
          load_type:order_detail.load_type,
          load_weight:order_detail.load_weight,
          length_load_dimen: parseInt(order_detail.length_load_dimen) || 0,
          width_load_dimen: parseInt(order_detail.width_load_dimen) || 0,
          height_load_dimen: parseInt(order_detail.height_load_dimen) || 0,
          measure_unit: order_detail.measure_unit,
          units:order_detail.units,
          additional_note:order_detail.additional_note,
          hazardous:order_detail.hazardous,
          extra_detail: extra_detail,
        });
        if(order_detail && pickup && pickup.address){
          setTimeout(function() {
            this.getLocation("pickup", "longitude", "latitude", pickup.address.toLowerCase(), "pickup_location" ,'checkPickAddress');
          }.bind(this),1000);
        }
        if(order_detail && delivery && delivery.address){
          setTimeout(function() {
            this.getLocation("delivery", "longitude", "latitude", delivery.address.toLowerCase(), "delivery_location", 'checkDropAddress');
          }.bind(this),1000);
        }
      }
    }
  }
  modifyOrder =(obj1, obj2, obj3=null, value=null)=>{
    try{
      if(obj2 !== 'job_id'){
        this.setState({
          promptStatus: true
        })
      }
      if(this.props.newOrder[0]){
      let orders = this.props.newOrder;
      if(obj1==="order_detail"){
        orders[this.props.order_key][obj1][obj2] = obj3;
      }else if(obj1==="extra_detail"){
        orders[this.props.order_key][obj1][parseInt(obj2)][obj3] = value;
      }else if(obj1==="assign_pick_driver" || obj1==="assign_drop_driver"){
        let arr = [];
        obj2.map((e, i)=>{
          e.map((f, j)=>{
            arr.push(f)
            return(
              ''
            )
          })
          return(
            ''
          )
        })
        orders[this.props.order_key][obj1] = arr;
      }else if(obj1==="assign_pick_driver_local" || obj1==="assign_drop_driver_local"){
        orders[this.props.order_key][obj2] = obj3;
      }else if (obj1==="attachment") {
        orders[this.props.order_key][obj1] = obj2;
      }else if(obj1 && obj2 && obj3){
        orders[this.props.order_key][obj1][0][obj2] = obj3;
      }
      this.props.modifyNewOrderCsv(orders);
    }
    }catch(e){
    }
  }
  setSenderReceiver = (type1, type2, data, value)=>{
    let payload = {};
    if(data && this.state.senderReceiver && this.state.senderReceiver.length>0){
      let senderReceiver = this.state.senderReceiver[data];
      if(senderReceiver && senderReceiver.id === 0){
        payload = {
          kid: 0,
          name: senderReceiver.name,
          primary_contact: senderReceiver.primary_contact,
          pri_email_id: senderReceiver.pri_email_id,
          pri_phone: senderReceiver.pri_phone,
          secondary_contact: senderReceiver.secondary_contact,
          sec_email_id: senderReceiver.sec_email_id,
          sec_phone: senderReceiver.sec_phone,
          business_address: senderReceiver.business_address,
          contract_start_date: senderReceiver.contract_start_date,
          contract_end_date: senderReceiver.contract_end_date,
          contract_documents: senderReceiver.contract_documents,
          Customer_code: senderReceiver.Customer_code,
          is_sender : value
        }
      }else if(senderReceiver && senderReceiver.id !== 0){
        payload = {
          kid: senderReceiver.id,
          name: senderReceiver.name,
          is_sender : value
        }

      }
      this.modifyOrder(type1, type2, payload);
    }
  }
  addMore = ()=>{
    this.props.getSenderReceiver('orders/comsendreceive/');
  }
  getAttachments = (files) =>{
    this.setState({
      attachments: files
    });
    this.modifyOrder('attachment', files)
  }
  deleteAttachments = (name)=>{
    let attachments  = this.state.attachments;
    let arr = [];
    attachments.map((e, i)=>{
      if(e.attachment_name!==name){
        arr.push(e)
      }
      return(
        ''
      )
    })
    this.setState({
      attachments: arr
    })
  }
  getSuggestions = async (text)=>{
    const data = await APIFetch('https://autocomplete.geocoder.api.here.com/6.2/suggest.json?'+this.state.appdata+'&query='+text+'&country=aus');
    let options = [];
    if(data.suggestions){
       data.suggestions.map((s,i)=>(
        options.push({
          value:s.label,
          label:s.label,
        })
      ));
    }
    this.setState({
      data:options
    });
  }
  getLocation = async (obj1, lat, lang, address, location=null, check=null)=>{
    let flg = 0;
    if(address){
    const data = await APIFetch('https://geocoder.api.here.com/6.2/geocode.json?searchtext='+address+'&'+this.state.appdata);
     if(data && data.Response.View.length>0){
       if(data.Response.View[0].Result.length>0){
         if(data.Response.View[0].Result[0].Location){
           flg = 0;
           let pos = data.Response.View[0].Result[0].Location.DisplayPosition;
           this.modifyOrder(obj1, lat, pos.Latitude);
           this.modifyOrder(obj1, lang, pos.Longitude);
         }else {
           flg = 1;
         }
       }else {
         flg = 1;
       }
      }else {
        flg = 1;
      }
    }else {
      flg = 1;
    }
    if(flg && location && check){
      this.setState({
        [location]: '',
        [check]: 'invalid address'
      })
    }else{
      this.setState({
        [check]: ''
      })
    }
    return null;
  }
  addNewField = (field) =>{
    if(field.field_name && field.field_value){
      let fields = this.state.extra_detail;
      fields.push({
        id: null,
        field_name:field.field_name,
        field_value:field.field_value
      });
      this.setState({
        extra_detail: fields
      });
      let orders = this.props.newOrder;
      orders[this.props.order_key].extra_detail = fields;
      this.props.modifyNewOrderCsv(orders);
    }
  }
  openOneModel=(modalId)=>{
    const modal = document.getElementById(modalId);
    modal.classList.add('show');
    //modal.setAttribute('aria-hidden', 'true');
    modal.setAttribute('style', 'display: block');
    try{
      const modalBackdrops = document.getElementsByClassName('modal-backdrop');
      document.body.appendChild(modalBackdrops[0]);
    }catch(e){

    }
  }
  makeid =()=>{
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (let i = 0; i < 6; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }
  render(){
    return(
      <React.Fragment>
        <Prompt
          when={this.state.promptStatus}
          message='You have unsaved changes, are you sure you want to leave?'
        />
        <div className="boxCommanBorder">
            <div className="row mb-3">
                <div className="col-md-4">
                    <ul className="listCustomerDetail text-left">
                        <li>
                          <div className="form-group">
                            <label>Job Number</label>
                            <input type="number" required readOnly={this.state.job_id_req} onChange={(e)=>{
                                this.setState({job_id:e.target.value});
                            }} onBlur={(e)=>{
                              if(e.target.value){
                                this.setState({
                                  job_id_error: false
                                })
                                this.modifyOrder("order_detail", "job_id", e.target.value);
                              }else{
                                this.setState({
                                  job_id_error: true
                                })
                              }
                            }} placeholder="" className="form-control" value={this.state.job_id} />
                            {this.state.job_id_error?
                              <span className="text-danger">required</span>
                            :null}
                          </div>
                        </li>
                    </ul>
                </div>
                <div className="col-md-4">
                    <ul className="listAttachment">
                      {this.state.attachments.length>0?
                        Array.from(this.state.attachments).map((e, i)=>(
                          <li key={i}>
                              <div style={{"marginBottom":"5px"}} className="alert alertAttachment alert-dismissible fade show" role="alert">
                                  <button type="button" data-toggle="modal" data-target="#uploadattachments" className="close" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                  </button>
                                  <i className="fas fa-paperclip"></i>
                                  <h5>{e.attachment_name}</h5>
                                  <h6>{e.size/1000}KB</h6>
                              </div>
                          </li>
                        )):null}
                    </ul>
                </div>
                <div className="col-md-4">
                    <ul className="listCustomerDetail">
                        <li><a href="#a" data-toggle="modal" data-target="#uploadattachments" className="btnIcon"><i className="fas fa-paperclip"></i></a></li>
                        {this.props.newOrder && this.props.newOrder[0] && this.props.newOrder[0].order_detail && this.props.newOrder[0].order_detail.create?
                          <li><a href="#a" onClick={(e)=>{
                            e.preventDefault();
                            this.props.saveAsDraft(this.props.order_key);
                          }} className="btnIcon"><i className="far fa-save"></i></a></li>
                        :null}
                        {this.props.editOrder? null:
                          <React.Fragment>
                          <li><a href="#a" onClick={(e)=>{
                            e.preventDefault();
                            this.props.deleteForm(this.props.order_key);
                          }} className="btnIcon textRed"><i className="far fa-trash-alt"></i></a></li>
                          <li>
                            <button type="button" onClick={(e)=>{
                              this.submitIndData();
                            }}>Submit</button>
                          </li>
                          </React.Fragment>
                        }
                    </ul>
                </div>
            </div>

                <h4 className="headingBorder"><span>Job Details</span></h4>
                <div className="row mb-2">
                  <div className="col-md-4">
                      <div className="form-group">
                          <label>Carrier Name <span className="text-danger">*</span></label>
                          <select required onFocus={(e)=>{
                            this.props.getCarrierList();
                          }} onChange={(e)=>{
                            let x = e.target.value;
                            if(x==="new$Carrier"){
                                this.openOneModel('modelInviteCarrier');
                            }else{
                              x = x.split('$');
                              this.setState({
                                Carrier_Name:x[0],
                                CarrierCompanyID: x[1],
                              });
                            }
                          }} onBlur={(e)=>{
                            if(e.target.value){
                              this.setState({
                                Carrier_Name_error: false
                              })
                              let x = e.target.value;
                              if(x==="new$Carrier"){
                                //  this.openOneModel('modelInviteCustomer');
                              }else{
                                x = x.split('$');
                                this.modifyOrder("order_detail", "Carrier_Name", x[0]);
                                this.modifyOrder("order_detail", "CarrierCompanyID", parseInt(x[1]));
                              }
                            }else{
                              this.setState({
                                Carrier_Name_error: true
                              })
                            }
                          }} value={`${this.state.Carrier_Name}$${this.state.CarrierCompanyID}`} className="form-control">
                            <option value="">select</option>
                            {this.props.carrierList && this.props.carrierList.length>0 ?
                              this.props.carrierList.map((e, i)=>(
                                <option key={i} value={`${e.name}$${e.id}`}>{e.name}</option>
                              ))
                            :null}
                            <option value="new$Carrier">Add New Carrier</option>
                          </select>
                          {this.state.Carrier_Name_error?
                            <span className="text-danger">required</span>
                          :null}
                      </div>
                  </div>
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Customer Code</label>
                            <input type="text" onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.setState({customer_code:e.target.value});
                              }else {
                                this.setState({
                                  customer_code: ''
                                })
                              }
                            }} onBlur={(e)=>{
                                this.modifyOrder("order_detail", "customer_code", e.target.value);
                            }} value={this.state.customer_code} className="form-control"/>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Customer Name</label>
                            <input type="text" required onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.setState({Carrier:e.target.value});
                              }else {
                                this.setState({
                                  Carrier: ''
                                })
                              }
                            }} onBlur={(e)=>{
                              this.modifyOrder("order_detail", "Customer", e.target.value);
                            }} value={this.state.Customer} placeholder="" readOnly className="form-control" />
                        </div>
                    </div>
                </div>
                <h4 className="headingBorder"><span>ORDER Details</span></h4>
                <div className="row">
                <div className="col-md-4">
                    <div className="form-group">
                        <label>Pick Up Time Slot <span className="text-danger">*</span></label>
                        <div className="row no-gutters rowTimeSlot">
                            <div className="col-md-6">
                            <Datetime
                              isValidDate={checkDate}
                              dateFormat="DD/MM/YY"
                              utc={true}
                              onChange={(e)=>{
                                this.setState({pickup_date_time:e});
                            //    this.modifyOrder("order_detail", "pickup_date_time", e);
                                this.modifyOrder("pickup", "pickup_date_time", e);
                              }} value={this.state.pickup_date_time}
                              required
                              onBlur={(e)=>{
                                if(e){
                                  let d = moment(this.state.pickup_date_time).add(this.state.timeslot, 'minutes');
                                  this.setState({
                                    pickup_date_time_to:d,
                                    pickup_date_time_error: false,
                                    pickup_date_time_to_error: false
                                  })
                                  this.modifyOrder("pickup", "pickup_date_time_to", d);
                                }else{
                                  this.setState({
                                    pickup_date_time_error: true
                                  })
                                }
                              }}
                            />
                            {this.state.pickup_date_time_error?
                              <span className="text-danger">required</span>
                            :null}
                            </div>
                            <div className="col-md-6">
                            <Datetime
                              isValidDate={checkDate}
                              dateFormat="DD/MM/YY"
                              utc={true}
                              onChange={(e)=>{
                                this.setState({pickup_date_time_to:e});
                            //    this.modifyOrder("order_detail", "pickup_date_time", e);
                                this.modifyOrder("pickup", "pickup_date_time_to", e);
                              }} value={this.state.pickup_date_time_to}
                              required
                              onBlur={(e)=>{
                                if(e){
                                  this.setState({
                                    pickup_date_time_to_error: false
                                  })
                                }else{
                                  this.setState({
                                    pickup_date_time_to_error : true
                                  })
                                }
                              }}
                            />
                            {this.state.pickup_date_time_to_error?
                              <span className="text-danger">required</span>
                            :null}
                            </div>
                        </div>
                    </div>
                </div>
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Pick Up Address <span className="text-danger">*</span></label>
                            <input list={this.state.datalist1} type="text" required onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.getSuggestions(e.target.value);
                                this.setState({pickup_location:e.target.value});
                              }else {
                                this.setState({
                                  pickup_location: ''
                                })
                              }
                            }} onBlur={(e)=>{
                              if(e.target.value){

                              }else{
                                this.setState({

                                })
                              }
                              this.setState({data:[]});
                              this.getLocation("pickup", "longitude", "latitude", e.target.value, 'pickup_location', 'checkPickAddress');
                            //  this.modifyOrder("order_detail", "address", e.target.value);
                              this.modifyOrder("pickup", "address", e.target.value);
                            }} value={this.state.pickup_location} className="form-control" />
                            <datalist id={this.state.datalist1}>
                              <option value="">select</option>
                              {this.state.data.length>0?
                              this.state.data.map((e, i)=>{
                                return(
                                  <option key={i} value={e.value}>{e.label}</option>
                                )
                              }):null}
                            </datalist>
                            {this.state.pickup_location?
                              <a href="#map" onClick={(e)=>{
                                e.preventDefault();
                                let order = this.props.newOrder[this.props.order_key].pickup[0];
                                if(order.latitude && order.longitude){
                                  this.setState({
                                    map_address: this.state.pickup_location,
                                    map_lat: order.latitude,
                                    map_lng: order.longitude,
                                    showMap: true
                                  })
                                }
                              }}>view on map</a>:
                            null}
                            <span className="text-danger"> {this.state.checkPickAddress}</span>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Sender</label>
                            <select onChange={(e)=>{
                              if(e.target.value==="addMore"){
                                this.openSenderModel();
                              }else{
                                this.setState({sender:e.target.value});
                              }
                            }} onBlur={(e)=>{
                              if(e.target.value){
                                this.setState({
                                  sender_error: ''
                                })
                                this.setSenderReceiver('pickup', 'sender', e.target.value, true)
                              }else{
                                this.setState({
                                  sender_error: 'required'
                                })
                              }
                            }} value={this.state.sender} className="form-control">
                              <option value="">select</option>
                              {this.state.senderReceiver && this.state.senderReceiver.length>0?
                                this.state.senderReceiver.map((e, i)=>{
                                  if(e.is_sender){
                                    return(
                                      <option key={i} value={i}>{e.name}</option>
                                    )
                                  }else {
                                    return(
                                      ''
                                    )
                                  }
                                })
                              :null}
                              <option value="addMore">Add Sender</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div className="row mb-2">
                <div className="col-md-4">
                    <div className="form-group">
                        <label>Delivery Time Slot <span className="text-danger">*</span></label>
                        <div className="row no-gutters rowTimeSlot">
                            <div className="col-md-6">
                            <Datetime
                              isValidDate={checkDate}
                              dateFormat="DD/MM/YY"
                              utc={true}
                              onChange={(e)=>{
                                this.setState({delivery_date_time:e});
                              //  this.modifyOrder("order_detail", "delivery_date_time", e);
                                this.modifyOrder("delivery", "delivery_date_time", e);
                              }} value={this.state.delivery_date_time}
                              required
                              onBlur={(e)=>{
                                if(e){
                                  let d = moment(this.state.delivery_date_time).add(this.state.timeslot, 'minutes');
                                  this.setState({
                                    delivery_date_time_to:d,
                                    delivery_date_time_error: false,
                                    delivery_date_time_to_error: false
                                  })
                                  this.modifyOrder("delivery", "delivery_date_time_to", d);
                                }else{
                                  this.setState({
                                    delivery_date_time_error: true
                                  })
                                }
                              }}
                            />
                            {this.state.delivery_date_time_error?
                              <span className="text-danger">required</span>
                            :null}
                            </div>
                            <div className="col-md-6">
                            <Datetime
                              isValidDate={checkDate}
                              dateFormat="DD/MM/YY"
                              utc={true}
                              onChange={(e)=>{
                                this.setState({delivery_date_time_to:e});
                              //  this.modifyOrder("order_detail", "delivery_date_time", e);
                                this.modifyOrder("delivery", "delivery_date_time_to", e);
                              }} value={this.state.delivery_date_time_to}
                              required
                              onBlur={(e)=>{
                                if(e){
                                  this.setState({
                                    delivery_date_time_to_error: false
                                  })
                                }else{
                                  this.setState({
                                    delivery_date_time_to_error: true
                                  })
                                }
                              }}
                            />
                            {this.state.delivery_date_time_to_error?
                              <span className="text-danger">required</span>
                            :null}
                            </div>
                        </div>

                    </div>
                </div>
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Delivery Address <span className="text-danger">*</span></label>
                            <input list={this.state.datalist} type="text" className="form-control" required onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.getSuggestions(e.target.value);
                                this.setState({delivery_location:e.target.value});
                              }else {
                                this.setState({
                                  delivery_location: ''
                                })
                              }
                            }} onBlur={(e)=>{
                              if(e.target.value){

                              }else{
                                this.setState({

                                })
                              }
                              this.setState({data:[]});
                              this.getLocation("delivery", "longitude", "latitude", e.target.value, 'delivery_location', 'checkDropAddress');
                            //  this.modifyOrder("order_detail", "address", e.target.value);
                              this.modifyOrder("delivery", "address", e.target.value);
                            }} value={this.state.delivery_location}/>
                            <datalist id={this.state.datalist}>
                              <option value="">select</option>
                              {this.state.data.length>0?
                              this.state.data.map((e, i)=>{
                                return(
                                  <option key={i} value={e.value}>{e.label}</option>
                                )
                              }):null}
                            </datalist>
                            {this.state.delivery_location?
                              <a href="#map" onClick={(e)=>{
                                e.preventDefault();
                                let order = this.props.newOrder[this.props.order_key].delivery[0];
                                if(order.latitude && order.longitude){
                                  this.setState({
                                    map_address: this.state.delivery_location,
                                    map_lat: order.latitude,
                                    map_lng: order.longitude,
                                    showMap: true
                                  })
                                }
                              }}>view on map</a>:
                            null}
                            <span className="text-danger"> {this.state.checkDropAddress}</span>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Receiver</label>
                            <select onChange={(e)=>{
                              if(e.target.value==="addMore"){
                                this.openReceiverModel();
                              }else{
                                this.setState({receiver:e.target.value});
                              }
                            }} onBlur={(e)=>{
                              if(e.target.value){
                                this.setState({
                                  receiver_error: ''
                                })
                                this.setSenderReceiver('delivery', 'receiver', e.target.value, true)
                              }else{
                                this.setState({
                                  receiver_error: 'required'
                                })
                              }
                            }} value={this.state.receiver} className="form-control">
                              <option value="">select</option>
                              {this.state.senderReceiver && this.state.senderReceiver.length>0?
                                this.state.senderReceiver.map((e, i)=>{
                                  if(!e.is_sender){
                                    return(
                                      <option key={i} value={i}>{e.name}</option>
                                    )
                                  }else {
                                    return(
                                      ''
                                    )
                                  }
                                })
                              :null}
                              <option value="addMore">Add Receiver</option>
                            </select>
                        </div>
                    </div>
                </div>
                <h4 className="headingBorder"><span>Load Details</span></h4>
                <div className="row">
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Load Carried</label>
                            <input type="text" className="form-control" onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.setState({load_carried:e.target.value});
                              }else {
                                this.setState({
                                  load_carried: ''
                                })
                              }
                            }} onBlur={(e)=>{
                              if(e.target.value){
                                this.setState({
                                  load_carried_error: false
                                })
                                this.modifyOrder("order_detail", "load_carried", e.target.value);
                              }else{
                                this.setState({
                                  load_carried_error: true
                                })
                              }
                            }} value={this.state.load_carried}/>

                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Load Type</label>
                            <input type="text" className="form-control" onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.setState({load_type:e.target.value});
                              }else {
                                this.setState({
                                  load_type: ''
                                })
                              }
                            }} onBlur={(e)=>{
                              if(e.target.value){
                                this.modifyOrder("order_detail", "load_type", e.target.value);
                                this.setState({
                                  load_type_error:false
                                })
                              }else{
                                this.setState({
                                  load_type_error:true
                                })
                              }

                            }} value={this.state.load_type}/>

                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Load Weight</label>
                            <input type="text" className="form-control" onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.setState({load_weight:e.target.value});
                              }else {
                                this.setState({
                                  load_weight: ''
                                })
                              }
                            }} onBlur={(e)=>{
                              if(e.target.value){
                                this.setState({
                                  load_weight_error:false
                                })
                                this.modifyOrder("order_detail", "load_weight", e.target.value);
                              }else{
                                this.setState({
                                  load_weight_error:true
                                })
                              }

                            }} value={this.state.load_weight}/>

                        </div>
                    </div>
                </div>
                <div className="row mb-2">
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Load Dimension</label>
                            <div className="row rowLoadDimension">
                                <div className="col-md-3">
                                    <input type="text"className="form-control" onChange={(e)=>{
                                      if(Number(e.target.value)){
                                        this.setState({
                                          length_load_dimen:e.target.value
                                        });
                                      }else{
                                        this.setState({
                                          length_load_dimen:''
                                        });
                                      }
                                    }} onBlur={(e)=>{
                                      if(e.target.value){
                                        this.setState({
                                          length_load_dimen_error: false
                                        })
                                        if(Number(e.target.value)){
                                          this.modifyOrder("order_detail", "length_load_dimen", e.target.value);
                                        }
                                      }else{
                                        this.setState({
                                          length_load_dimen_error: true
                                        })
                                      }
                                    }} value={this.state.length_load_dimen}/>

                                </div>
                                <div className="col-md-3">
                                    <input type="text" className="form-control" onChange={(e)=>{
                                      if(Number(e.target.value)){
                                        this.setState({
                                          height_load_dimen:e.target.value
                                        });
                                      }else{
                                        this.setState({
                                          height_load_dimen:''
                                        });
                                      }
                                    }} onBlur={(e)=>{
                                      if(e.target.value){
                                        this.setState({
                                          height_load_dimen_error: false
                                        })
                                        if(Number(e.target.value)){
                                          this.modifyOrder("order_detail", "height_load_dimen", e.target.value);
                                        }
                                      }else{
                                        this.setState({
                                          height_load_dimen_error: true
                                        })
                                      }
                                    }} value={this.state.height_load_dimen}/>

                                </div>
                                <div className="col-md-3">
                                    <input type="text" className="form-control" onChange={(e)=>{
                                      if(Number(e.target.value)){
                                        this.setState({
                                          width_load_dimen:e.target.value
                                        });
                                      }else{
                                        this.setState({
                                          width_load_dimen:''
                                        });
                                      }
                                    }} onBlur={(e)=>{
                                      if(e.target.value){
                                        this.setState({
                                          width_load_dimen_error: false
                                        })
                                        if(Number(e.target.value)){
                                          this.modifyOrder("order_detail", "width_load_dimen", e.target.value);
                                        }
                                      }else{
                                        this.setState({
                                          width_load_dimen_error: true
                                        })
                                      }

                                    }} value={this.state.width_load_dimen}/>

                                </div>
                                <div className="col-md-3">
                                <select value={this.state.measure_unit} onChange={(e)=>{
                                  this.setState({
                                    measure_unit:e.target.value
                                  });
                                }} onBlur={(e)=>{
                                  if(e.target.value){
                                    this.setState({
                                      measure_unit_error: false
                                    })
                                    this.modifyOrder("order_detail", "measure_unit", e.target.value);
                                  }else{
                                    this.setState({
                                      measure_unit_error: true
                                    })
                                  }
                                }} className="form-control" style={{"width":"80px"}} id="">
                                    <option value="">select</option>
                                    <option>cm</option>
                                    <option>inch</option>
                                    <option>feet</option>
                                    <option>meter</option>
                                </select>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>No. Of Units</label>
                            <input type="text" className="form-control" onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.setState({units:e.target.value});
                              }else {
                                this.setState({
                                  units: ''
                                })
                              }
                            }} onBlur={(e)=>{
                              if(e.target.value){
                                this.setState({
                                  units_error: false
                                })
                                this.modifyOrder("order_detail", "units", e.target.value);
                              }else{
                                this.setState({
                                  units_error: true
                                })
                              }
                            }} value={this.state.units}/>

                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="form-group">
                            <label>Price</label>
                            <input type="text" className="form-control" onChange={(e)=>{
                              if(e.target.value && charValidation(e.target.value)){
                                this.setState({price:e.target.value});
                              }else {
                                this.setState({
                                  price: ''
                                })
                              }
                            }} onBlur={(e)=>{
                              if(e.target.value){
                                this.setState({
                                  price_error: false
                                })
                                this.modifyOrder("order_detail", "price", e.target.value);
                              }else{
                                this.setState({
                                  price_error: true
                                })
                              }
                            }} value={this.state.price}/>

                        </div>
                    </div>
                </div>
                <h4 className="headingBorder"><span>Additional Information</span></h4>
                <div className="row">
                    <div className="col-md-12">
                    <div className="form-group pb-3">
                        <textarea className="form-control" onChange={(e)=>{
                          if(e.target.value && e.target.value.length<501){
                            this.setState({
                              additional_note:e.target.value,
                              additional_note_count: Number(500 - parseInt(e.target.value.length))
                            });
                          }else if(!e.target.value){
                            this.setState({
                              additional_note: '',
                              additional_note_count: 500
                            })
                          }
                        }} onBlur={(e)=>{
                          this.modifyOrder("order_detail", "additional_note", e.target.value);
                        }} value={this.state.additional_note} placeholder="Add Additional Notes"></textarea>
                        <span className="text-success">{this.state.additional_note_count} characters is remaining.</span>
                    </div>
                    </div>
                    <div className="col-md-12">

                            <input type="checkbox" checked={this.state.hazardous} onChange={(e)=>{
                              if(e.target.checked){
                                this.setState({hazardous: true});
                              }else{
                                this.setState({hazardous: false});
                              }
                            }} onBlur={(e)=>{
                              this.modifyOrder("order_detail", "hazardous", e.target.value);
                            }} value={this.state.hazardous} />
                            <label >&nbsp;Contains Hazardous & Dangerous Goods</label>
                    </div>
                </div>
                <br/>
                <h4 className="headingBorder"><span>Extra Fields</span></h4>

                <div className="row">
                    {this.state.extra_detail[0]? this.state.extra_detail.map((f,i)=>(
                      <div key={i} className="col-lg-4">
                        <div className="form-group">
                          <label className="col-form-label">{f.field_name}</label>
                          <input type="text" onChange={(e)=>{
                            if(e.target.value && charValidation(e.target.value)){
                              let fields = this.state.extra_detail;
                              fields[i].field_value = e.target.value;
                              this.setState({
                                extra_detail: fields
                              });
                              this.modifyOrder("extra_detail", i, f.field_name, e.target.value);
                            }else {
                              this.setState({
                                extra_detail: ''
                              })
                              this.modifyOrder("extra_detail", i, f.field_name, e.target.value);
                            }
                          }} className="form-control" value={f.field_value} />
                        </div>
                      </div>
                    )):
                    <div className="col-lg-4">
                      <p>No extra fields
                        <a data-toggle="modal" data-target="#myModal"
                        href="#myModal"> Add New Field</a>
                       </p>
                    </div>}
                </div>
                {this.state.extra_detail[0]?<a data-toggle="modal" data-target="#myModal"
                 href="#myModal"> Add New Field</a>:null}
                <AddExtraField
                  close={(e)=>{
                    this.setState({
                      assignDriverModel:false
                    });
                  }}
                  addNewField = {this.addNewField}
                />
                <UploadAttachments
                  getAttachments={this.getAttachments}
                  carrier = {this.props.order && this.props.order.order_detail?
                    this.props.order.order_detail.Created_by_CustomerID
                  :null}
                  valueType = "Created_by_CustomerID"
                 />
                {this.state.showMap?
                <Map
                  address={this.state.map_address}
                  lat={this.state.map_lat}
                  lng={this.state.map_lng}
                  close={()=>{
                    this.setState({
                      showMap: false
                    })
                  }}
                />: null}
        </div>

        {this.state.addSenderModel?
          <AddSender
             closeOneModel={this.closeSenderModel}
             openOneModel={this.openSenderModel}
             addMore={this.addMore}
           />
         :null}

         {this.state.addReceiverModel?
          <AddReceiver
             closeOneModel={this.closeReceiverModel}
             openOneModel={this.openReceiverModel}
             addMore={this.addMore}
           />
         :null}

      </React.Fragment>
    )
  }
}

CreateOrd.propTypes = {
  modifyNewOrderCsv: PropTypes.func.isRequired,
  getCarrierList: PropTypes.func.isRequired,
  getSenderReceiver: PropTypes.func.isRequired,
  pushSenderReceiver: PropTypes.func.isRequired,
  //newOrder: PropTypes.array.isRequired,
}

const mapStateToProps = state =>({
  newOrder: state.customer.newOrder,
  carrierList: state.customer.carrierList,
  editOrder: state.customer.editOrder,
  senderReceiver: state.customer.senderReceiver
});

export default connect(mapStateToProps, {getSenderReceiver, pushSenderReceiver, modifyNewOrderCsv, getCarrierList})(CreateOrd);
